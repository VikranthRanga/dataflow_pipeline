package com.allegis.search.enums;

/**
 * Represents the various Candidate statuses
 * 
 * @author jalpino
 *
 */
public enum CandidateStatus {
    DEFAULT("Candidate"),
    CANDIDATE("Candidate"),
    FORMER("Former"),
    CURRENT("Current"),
    APPLICANT("Applicant");
    
    private String status;
    
    /**
     * Constructor
     * @param status    string representation of the status
     */
    private CandidateStatus( String status ) {
        this.status = status;
    }
    
    /**
     * @return the string representation of the candidate status
     */
    public String status() {
        return this.status;
    }
    
    /**
     * @param stat the string representation of the status
     * @return  true if equal, false otherwise
     */
    public boolean equals( String stat ) {
        return this.status.equalsIgnoreCase( stat );
    }
}
