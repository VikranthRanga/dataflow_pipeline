package com.allegis.search.enums;

/**
 * Represents Event/Task Types
 * 
 * @author jalpino
 *
 */
public enum EventType {
    G2("G2"),
    REF_CHECK("Reference Check"),
	SERV_TOUCHPOINT("Service Touchpoint"),
	PERF_FEEDBACK("Performance Feedback"),
	CALL("Call"),
	MEETING("Meeting"),
	BD_CALL("BD Call"),
	FINISH("Finish"),
	CORRESPONDENCE("Correspondence"),
	OFFER_ACCEPTED("Offer Accepted"),
	MEAL("Meal"),
	INTERVIEWING("Interviewing"),
	INTERNAL_INTERVIEW("Internal Interview");
	
    
    private String type;
    
    private EventType( String type ) {
        this.type = type;
    }
    
    /**
     * @return the string representation of the TaskType
     */
    public String type() {
        return this.type;
    }
    
    /**
     * @param type the TaskType to cehck for equality
     * @return  true if they are equal, false otherwise;
     */
    public boolean equals( String type  ) {
        return this.type.equalsIgnoreCase(type);
    }
   
}
