package com.allegis.search.enums;

/**
 * Defines all of the signals used for LTR (hijacked from the LTR training project)
 * 
 */
public enum OrderStatusRelevance {
	
	Started(5, 12), 
	Offer_Accepted(4, 11), 	
	Interviewing(4, 9),
	Submitted(4, 8),
	Linked(3, 6), 	
	Screening(3, 6),
	Pursuing(3, 6),
	Not_Proceeding(3, 6),
	Network_Referral(2, 4),
	Applicant(1, 3),
	NP_Not_Qualified;


	private final int experienceFactor;
	private final int relevancy;
	

	private OrderStatusRelevance(int relevancy, int experienceFactor) {
		this.relevancy = relevancy;
		this.experienceFactor = experienceFactor;
	}
	
	private OrderStatusRelevance() {
		this.relevancy = 0;
		this.experienceFactor = 0;
	}

	/**
	 * LTR training relevance
	 * @return
	 */
	public int getRelevancy() {
		return relevancy;
	}

	/**
	 * Experience in months
	 * @return
	 */
	public int getExperienceFactor() {
		return experienceFactor;
	}
	

	public boolean isGreatHit() {
		return relevancy > Network_Referral.relevancy;
	}
	
	public static OrderStatusRelevance find(String label, String notProceedingReason) {
		String l = label.replaceAll(" |-", "_");
		for (OrderStatusRelevance s : OrderStatusRelevance.values()) {
			if (s.name().equals(l)) {
				if (null==notProceedingReason) {
					return s;
				} else if (s == Not_Proceeding) {
					return (notProceedingReason.equals("Candidate Not Qualified")) ? NP_Not_Qualified : Not_Proceeding;
				} else
					return s; // bad salesforce data: these should not have a reason!
			}
		}
		if (label.startsWith("Not Proceeding")) {
			if ("Candidate Not Qualified".equals(notProceedingReason))
				return NP_Not_Qualified;
			return Not_Proceeding;
		}
		return null;
	}
}
