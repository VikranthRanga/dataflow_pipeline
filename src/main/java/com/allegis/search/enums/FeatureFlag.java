package com.allegis.search.enums;

/**
 * Represents the set of feature flags that control or influence various behavior within
 * the pipelines associated with this project
 * 
 * @author jalpino
 *
 */
public enum FeatureFlag {
    
    PSN_ALERTS("psn_alert_release_num"),
    JOB_TEST_RIG("job_test_rig_release_num"),
    JOB_ALERTS("job_alert_release_num"),
    EST_PAYRATES("est_payrates_release_num"),
	JOB_FUNCTIONS("job_function_release_num"),
	INDUSTRY_CODE("industry_code_release_num"),
	PROVEN_TRACK_RECORD("is_proven_track_record"),
	OPP_JOB_FUNCTION("is_opp_job_function");
    
    private String fieldName;
    
    private FeatureFlag( String fieldName ) {
        this.fieldName = fieldName;
    }
    
    /**
     * @return  the field name associated with this feature
     */
    public String fieldName() {
        return this.fieldName;
    }
    
}
