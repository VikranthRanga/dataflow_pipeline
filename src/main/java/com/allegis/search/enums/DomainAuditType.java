package com.allegis.search.enums;

	
public enum DomainAuditType {
	////////////////////////////////////////////////
	// This group provides SF Domain Audit Types
	////////////////////////////////////////////////
	/** Initial during First Pass - Lightweight classification */
	RT("Related Titles"),
	/** Second Pass before Confidence Check */
	BCC("Before Confidence Check"),
	/** Final Set After all Checks */
	FIN("Final");

	private String type;

	private DomainAuditType(String type) {
		this.type = type;
	}

	private DomainAuditType() {
	}
	
	/**
     * @return  the type associated with audit
    */
	public String type() {
		return this.type;
	}

}
