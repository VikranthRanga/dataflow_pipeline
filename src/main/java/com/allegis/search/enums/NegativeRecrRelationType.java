package com.allegis.search.enums;

/**
 * Represent Negative Recruiter Relationship type associated to datapoint for Proven Track Record 
 *
 */
public enum NegativeRecrRelationType {
    
    ATTEMPTED_CONTACT("Attempted Contact");
    
    private String type;
    
    /**
     * Constructor
     * @param type the string representation of this type
     */
    private NegativeRecrRelationType( String type ) {
        this.type = type;
    }
    
    /**
     * @return the string representation of this type
     */
    public String type() {
        return this.type;
    }
    
    /**
     * @param type the string representation of the type to compare
     * @return  true if equal, false otherwise
     */
    public boolean equals( String type ) {
        return this.type.equalsIgnoreCase( type );
    }
    
    /**
     * Check the given string is negative recruiter relationship type or not
     * @param relType the string representation of relation type
     * @return true if found in the list of negative recruiter relation type
     */
    public static boolean isNegativeType( String relType ) {
    	for(NegativeRecrRelationType relationType : NegativeRecrRelationType.values()) {
            if( relationType.type.equalsIgnoreCase(relType) ) {
                return true;
            }
        }
        return false;
    }
}
