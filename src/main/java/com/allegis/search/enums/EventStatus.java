package com.allegis.search.enums;

/**
 * Represent Event status
 *
 */
public enum EventStatus {
    
    COMPLETED("Completed");
    
    private String status;
    
    /**
     * Constructor
     * @param status the string representation of this status
     */
    private EventStatus( String status ) {
        this.status = status;
    }
    
    /**
     * @return the string representation of this status
     */
    public String status() {
        return this.status;
    }
    
    /**
     * @param stat the string representation of the status to compare
     * @return  true if equal, false otherwise
     */
    public boolean equals( String stat ) {
        return this.status.equalsIgnoreCase( stat );
    }
}
