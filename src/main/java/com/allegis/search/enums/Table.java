package com.allegis.search.enums;

/** 
 * Used to maintain spanner table properties.
 * 
 * @author dhartwig
 */
public class Table {


	public static class Account {

		public static final String RECORD_TYPE_ID_FOR_TALENT = "01224000000FFqQAAW";

		public static final String TABLE  = "account";		
		
		public static enum Column {
			Id,
	        Talent_Id__c,
			Peoplesoft_ID__c,
			Source_System_Id__c,
			Talent_Ownership__c,	
			Do_Not_Contact__c,
			Do_Not_Recruit_Talent__c,
			Skill_Comments__c,	        
			Shift__c,	
			Date_Available__c,
			LastActivityDate,
			Talent_Profile_Last_Modified_Date__c,
			CreatedDate,	
			Candidate_Status__c,
			Talent_Current_Employer_Formula__c,
			Desired_Placement_type__c,
			Desired_Total_Compensation__c,
			Skills__c,
			Desired_Rate_Frequency__c,
			Desired_Rate__c,
			Desired_Rate_Max__c,
			Talent_Preference_Internal__c,	        
			Talent_End_Date__c,
			RecordTypeId,
			Talent_Committed_Flag__c,
            Name,
            Siebel_ID__c,
            Talent_Security_Clearance_Type__c,
            Talent_Preference__c,
            TC_Talent_Profile_Last_Modified_Date__c,
            LastModifiedDate,
            IsDeleted,
            orgId,
            Sector_Code__c,
            Sector_Description__c; // Found in spanner and BQ only
		}
				 
	}

	
	public static class Contact {
		public static final String TABLE = "contact";
		public static final String INDEX = "contact_ByAccountId_ES_v3";
			
		public static enum Column {
			Id,
			FirstName,
			LastName,			
			MailingStreet,
			MailingCity,
			MailingState,
			MailingPostalCode,
			Talent_Country_Text__c,			
			MailingLatitude,
			MailingLongitude,			
			Preferred_Phone_Value__c,
			Preferred_Email_Value__c,			
			Work_Email__c,
			Other_Email__c,
			Email,			
			MobilePhone,
			Phone,
			OtherPhone,
			HomePhone,				            
			Title,			
			Data_Quality_Record_Flag__c,
			Data_Quality_Resume_Flag__c, 
			TC_Title__c,
			AccountId,
			LastModifiedDate,
			Search_Ingestion_Trigger_Timestamp__c;
		}
	}
	
	
	public static class Order {
		public static final String TABLE = "order";				
        public static final String INDEX = "order_ByContactId_ES_v4";
        public static final String INDEX_BY_OPP = "order_ByOpportunityId_ES_v1";
		public static enum Column {
			Id, 
			Status, 
			OrderNumber, 
			OpportunityId,
            EffectiveDate,
            ATS_Job__c,
            ShipToContactId,
            Submittal_Not_Proceeding_Reason__c,
            Has_Application__c;
		}
				 
	}

    
    public static class Event {
        public static final String TABLE = "event";
        public static final String INDEX = "event_ByAccountId_ES_v6";

        public static enum Column {
        	Id,
        	Type,
        	LastModifiedById,
            WhatId,
            LastModifiedDate,
        	Not_Proceeding_Reason__c,
        	ActivityDate,
        	ActivityDateTime,
        	Activity_Type__c,
        	Category__c;
        }

    }
 
    
    public static class Experience {
        public static final String TABLE = "talent_experience__c";
        public static final String INDEX = "talent_experience_ByAccountId_ES";

        public static enum Column {
        	Id,
			Type__c,
			Organization_Name__c,
			Title__c,
			Start_Date__c,
			End_Date__c,
			Certification__c,
			Graduation_Year__c,
			School_Name__c,
			Major__c,
			Degree_Level__c,
			Notes__c,
			Talent__c;
        }

    }

	
    public static class Pipeline {
    	public static final String TABLE = "pipeline__c";
        public static final String INDEX = "pipeline_ByContactId_ES";        

        public static enum Column {
        	Id, 
        	User__c,
        	Contact__c;
        }

    }
    
    
 	public static class TalentDocument {
 	    public static final String TABLE = "talent_document__c";
 	    public static final String INDEX = "talent_document_ByAccountId_ES_v3";

 	    public static enum Column {
 	        Id,
 	        CreatedDate,
 	        Document_Type__c,
 	        HTML_Version_Available__c,
 	        Talent__c,
 	        IsDeleted;
 	    }
 	}
    
    
 	public static class Task {
 		public static final String TABLE = "task";
 	    public static final String INDEX = "task_ByAccountId_ES_v4";
 	    
 	    public static enum Column {
 	    	Id,
 	    	Type,
 	    	Status,
 	    	Subject,
 	    	Activity_Type__c,
 	    	ActivityDate,
 	    	Description;
 	    }

 	}
 	
    
 	public static class WorkHistory {
 		public static final String TABLE = "talent_work_history__c";
 		public static final String INDEX = "talent_work_history_ByAccountId_ES_v4"; 	 	
 	    
 	    public static enum Column {
 	    	Id,
 	    	OrderID__c,
    		End_Date__c, 
    		Start_Date__c, 
    		SourceCompany__c,
            Job_Title__c, 
            Payment_Frequency_Type__c, 
            Pay_Rate__c, 
            Finish_Reason__c,
            DivisionName__c,
            Client_Account_ID__c,
            RegionCode__c,
            Finish_Code__c,
            SourceCompanyId__c;
 	    }
 	}

	
    public static class Contact_tag {
        public final static String TABLE = "contact_tag__c";
        public final static String INDEX = "contact_tag_ByContactId_ES";

        public static enum Column {
        	Id, 
        	Tag_Definition__c;
        }

    }
 
    public static class Tag_definition {
        public final static String TABLE = "tag_definition__c";

        public static enum Column {
            Id, 
            CreatedById, 
            Tag_Name__c;
        }
    }
    
    
	public static class User {
		public final static String TABLE = "user";

		public static enum Column {
			Id, 
			Name, 
			Peoplesoft_Id__c;
		}
	}
    
	public static class Opportunity {
		public static final String RECORD_TYPE_ID_REQ = "01224000000kMQ4AAM";
		public static final String STAGING_STAGENAME = "Staging";
		public static final String QUALIFIED_STAGENAME = "Qualified";
		public static final String TABLE = "opportunity";
				
        public static enum Column {
        	Id,
        	
            Opportunity_Num__c,
            Req_Job_Title__c,
            Global_Account_Name__c,
            Req_Pay_Rate_Max__c,
        	
			RecordTypeName__c,            
			Req_Practice_Area__c,
			Req_Qualification__c,
			Req_Payment_terms__c,
			Req_Job_Description__c,
			Req_External_Job_Description__c,
			Req_Product__c,
			Req_Open_Positions__c,
			Start_Date__c,
			Req_Skill_Details__c,
			Req_Skill_Specialty__c,
			EnterpriseReqSkills__c,
			Req_Work_Remote__c,
			Name,
			OwnerId,
			Req_Division__c,
			OpCo__c,
			Req_Duration__c,
			Req_Duration_Unit__c,
			Req_OFCCP_Required__c,
			CreatedDate,
			Req_OpCo_Code__c,
			Req_Worksite_Postal_Code__c,
			Req_Worksite_Country__c,
			Req_Worksite_State__c,
			Req_Worksite_City__c,
			Req_Worksite_Street__c,
			Req_GeoLocation__Longitude__s,
			Req_GeoLocation__Latitude__s,
			Req_Minimum_Education_Required__c,
			Req_Can_Use_Approved_Sub_Vendor__c,
			StageName,
			Req_Qualifying_Stage__c,
			Bill_Rate_Max_Multi_Currency__c,
			Bill_Rate_Min_Multi_Currency__c,
			Bill_Rate_max_Normalized__c,
			Bill_Rate_min_Normalized__c,
			Req_Rate_Frequency__c,
			Pay_Rate_Max_Multi_Currency__c,
			Pay_Rate_Min_Multi_Currency__c,
			Pay_Rate_Max_Normalized__c,
			Pay_Rate_Min_Normalized__c,
			Salary_Max_Multi_Currency__c,
			Salary_Min_Multi_Currency__c,           
			AccountId,
			Organization_Office__c,
			Req_VMS__c, 
			RecordTypeId,
			IsDeleted,
			Req_RVT_Occupation_Code__c, // HCS code
			Req_Job_Level__c, // Experience Level
			Req_Category__c, // Product Hierarchy: Category
			Suggested_Job_Titles__c, 
            LastModifiedDate,
            Legacy_Product__c, // ph id
            orgId, // Exists in spanner and BQ only
            
            ED_Outcome__c, // req win probability score
            
            Req_Exclusive__c, //is opp exclusive to Allegis
            
            CloseDate,
            Req_Qualified_Date__c,
            IsClosed;
        }		 
	}
	
	
    public static class UserOrganization {
        public static final String TABLE = "user_organization__c";
    
        public static enum Column {
            Id,
            Name;
        }
  
    }
    
    
    public static class OpportunityContactRole {
        public static final String TABLE = "opportunitycontactrole";
        public static final String INDEX = "opportunitycontactrole_byOpportunityId_ES_v1";
        
        public static enum Column {
            Id,
            ContactId,
            IsPrimary,
            OpportunityId,
            Role;
        }        
    }
    

    public static class IngestDeadletters {        
        public static final String TABLE = "es_ingest_deadletters_v2";
        
        public static enum Column {
            uuid,
            timestamp,
            feeder_id,
            index,
            entity_id,
            stage,
            type,
            doctype,
            jobname,
            general_msg,
            detail_msg,
            stacktrace;
        }
    }

    public static class Alert {
        public static final String TABLE = "search_alert_criteria__c";
    	
        public static enum Columns {
        	Id,
            Alert_Criteria__c,
            OwnerId,
            Alert_Trigger__c,
            Query_String__c,
            Request_Body__c, 
            IsDeleted;
        }
    }
    
    public static class Payrate {
        public static final String TABLE = "payrate_estimate";
        
        public static enum Column {
            candidateId,
            estimated_payrate,
            actual_payrate;
        }
    }
    
    public static class LTRTestRig {
        public static final String TABLE = "ltr_test_rig";
        
        public static enum Column {
        	model,
        	opportunityId,
        	rank,
        	accountId,
        	timestamp,
        	features, 
        	opco, 
        	score
        }

		public static final String SUMMATION_MODEL = "summation";
    }
    
    public static class UnifiedMerge {
        public static final String TABLE = "unified_merge__c";
        
        public static enum Column {
        	Process_State__c,
        	Survivor_Account_ID__c;
        }
    }
    
    public static class TalentPredictions {
        public static final String TABLE = "talent_predictions";
        
        public static enum Column {
            AccountId,
            LastModifiedDate,
            JobCode,
            JobCodeHash,
            JobCodeLastModifiedDate,
            IndustryCode,
            IndustryCodeHash,
            IndustryCodeLastModifiedDate,
            ProvenTrackRecord,
            ProvenTrackRecordHash,
            ProvenTrackRecordLastModifiedDate,
            recruiterRelationships;
        }
    }
    
    public static class TalentRecommendation {
        public static final String TABLE = "talent_recommendation__c";
        public static final String INDEX = "talent_recommendation_ByContactId_ES";
        
        public static enum Column {
        	Talent_Contact__c,
            Reference_Check_Completed__c;
        }
    }
    
    public static class OpportunityTeamMember {
        public static final String TABLE = "opportunityteammember";
        public static final String INDEX = "oppTeamMembers_byOppId_ES_v1"; 
        
        public static enum Column {
            OpportunityId,
            TeamMemberRole,
            CreatedById;
        }
    }
    
    public static class OpportunitySupportRequest {
        public static final String TABLE = "support_request__c";
        public static final String INDEX = "supportRequest_byOppId_ES_v1"; 
        
        public static enum Column {
        	Id,
        	Opportunity__c,
        	CreatedById,
        	CreatedDate
        }
    }
    
    public static class OpportunityReqRouterAction {
        public static final String TABLE = "req_router_action__c";
        public static final String INDEX = "reqRouterAction_byOppId_ES_v1"; 
        
        public static enum Column {
        	Id,
        	Action_Type__c,
        	IsDeleted,
        	Opportunity__c,
        	OwnerId
        }
    }
    
    public static class Case {
        public static final String TABLE = "case";
        public static final String INDEX = "case_byAccountId_ES_v2"; 
        
        public static enum Column {
            Id,
            AccountId,
            Type,
            CreatedDate,
            Account_Ownership__c
        }
    }
    
    /**
     * Represents a set of generic table properties, namely columns
     * to be used when the underlying table is not known
     */
    public static class CommonTable {
        public static enum Column {
            Id,
            LastModifiedDate
        }
    }
}
