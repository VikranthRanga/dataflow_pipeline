package com.allegis.search.enums;

/**
 * Metric Namespaces represents labels for categorizing metrics during our pipeline.
 */
public enum MetricsNamespace {
    STAGE,
    CONNECTIONS
}
