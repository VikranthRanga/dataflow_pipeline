package com.allegis.search.enums;

/**
 * Represent Positive Recruiter Relationship type associated to datapoint for Proven Track Record 
 *
 */
public enum PositiveRecrRelationType {
    
    MEAL("Meal"),
    MEETING("Meeting"),
    INTERVIEWING("Interviewing");
    
    private String type;
    
    /**
     * Constructor
     * @param type the string representation of this type
     */
    private PositiveRecrRelationType( String type ) {
        this.type = type;
    }
    
    /**
     * Check the given string is positive recruiter relationship type or not
     * @param relType the string representation of relation type
     * @return true if found in the list of positive recruiter relation type
     */
    public static boolean isPostiveType( String relType ) {
    	for(PositiveRecrRelationType relationType : PositiveRecrRelationType.values()) {
            if( relationType.type.equalsIgnoreCase(relType) ) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @return the string representation of this type
     */
    public String type() {
        return this.type;
    }
    
    /**
     * @param type the string representation of the type to compare
     * @return  true if equal, false otherwise
     */
    public boolean equals( String type ) {
        return this.type.equalsIgnoreCase( type );
    }
}
