package com.allegis.search.enums;

/**
 * Represents the different types of pipeline feeders
 */
public enum FeederType {
	// keep to 4 chars for readability
    BQAd,	// BigQuery ad hoc
    _ES_,	// Elastic query
    File,	// File 
    Bulk,	// Bulk load
    Real,	// Realtime
    Span,	// Spanner Query
    Dead,	// Deadletter query
    True,   // True-up
}
