package com.allegis.search.enums;

import com.allegis.search.model.MatchDoc;

/**
 * Represents various match document types used within our pipelines
 */
public enum DocType {
    PERSON("candidate_id"),
	_DOC("candidate_id");
    
    private String idField;
    
    /**
     * Constructor
     * @param idField   the name of the field that represents this doc type's id
     */
    private DocType( String idField ) {
        this.idField = idField;
    }
    
    /**
     * @param doc  a reference to a {@link MatchDoc} to derive the doctype from
     * @return     the enum that matches the supplied doc, or UNKNOWN if it doesn't match
     */
    public static DocType getByClass( MatchDoc doc ) {
        return PERSON;
    }
    
    /**
     * @return the name of the id field associated with this Doc Type
     */
    public String getIdField() {
        return this.idField;
    }
}
