package com.allegis.search.enums;

/**
 * Represents the various rate frequencies, generally associated with Pay Rates
 * 
 * @author jalpino
 *
 */
public enum RateFrequency {
    HOURLY,
    DAILY,
    WEEKLY,
    MONTHLY;
    
    /**
     * Get a RateFrequency using a string that rpresents the frequency
     * @param freq  the string representation of a frequency
     * @return      A RateFrequency if found, null otherwise 
     */
    public static RateFrequency getFrequency( String freq ) {
        for(RateFrequency f : RateFrequency.values() ) {
            if( f.name().equalsIgnoreCase(freq) ) {
                return f;
            }
        }
        return null;
    }
}
