package com.allegis.search.model;

import java.io.Serializable;
import java.util.Objects;


/**
 * Use to store GeoLocation Data 
 * 
 * @author mannrivera
 *
 */
public class GeoLocation implements Serializable {

	private static final long serialVersionUID = 8606121073095014381L;
	public Double lat;
    public Double lon;
    
    /**
     * Constructor required by {@link PercolationAppPipeline}
     */
    public GeoLocation(){ }
    
	public GeoLocation(Double lat, Double lon) {
		this.lat = lat;
		this.lon = lon;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(lat, lon);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeoLocation other = (GeoLocation) obj;
		return Objects.equals(lat, other.lat) && Objects.equals(lon, other.lon);
	}
	@Override
	public String toString() {
		return "GeoLocation [" + (lat != null ? "lat=" + lat + ", " : "") + (lon != null ? "lon=" + lon : "") + "]";
	}
}