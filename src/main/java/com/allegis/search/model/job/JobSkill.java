package com.allegis.search.model.job;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.allegis.search.model.CommonDoc;
import com.allegis.search.stage.DeadLetterService;

/**
 * JobSkills 
 * @author mannrivera
 *
 */
public class JobSkill implements Serializable, Cloneable{
	
	private static final long serialVersionUID = -9091417205416755619L;
	
	public String skill_name;
	public boolean desired = false; 
	
    /**
     * Parse String to JSONObject.   Logs errors to deadletter service on failure.
     * 
     * @param json JSON String
     * @param doc person or job doc (for logging)
     * @param fieldName name of field being parsed (for logging)
     * @param deadLetterService
     * @return List<JobSkill> 
     */
    public static List<JobSkill> extractJobSkills(String json, CommonDoc doc, String fieldName, DeadLetterService deadLetterService) {
    	
        if(json == null || json.trim().isEmpty()) 
            return null;
        
        if (json.startsWith("[{\"\""))  {
        	//  scrub pairs of double quotes into ones'ies  
        	json = json.replaceAll("\"\"", "\""); 
        } 
        
        try {
        	return extractJobSkills(new JSONArray(json));
        } catch (JSONException e) {
            String generalMessage = String.format("JSON %s parse exception", fieldName);
            String errorMessage = String.format("errorMessage:[%s]; JSON: %s", e.getMessage(), json);
            deadLetterService.writeDataQualityWarning(generalMessage, errorMessage, doc);
        }
        
        return null;
    }
    /**
     * Method Used to parse EnterpriseReqSkills__c json
     * 
     * spanner ->  EnterpriseReqSkills__c - [{"name":"Mechanical Skills","favorite":true},
     *          {"name":" visual inspection","favorite":true},
     *          {"name":" manufacturing","favorite":true},
     *          {"name":" machine operator","favorite":true},
     *          {"name":" Small Engine Repair","favorite":true}]
     *          
     * es -> "skills": [
     *      {
     *          "name": "Mechanical Skills",
     *          "essential": true,
     *      },
     * 
     * @param json JSON string of skills
     * @return List<String> a list of skills in lower case
     */
    public static List<JobSkill> extractJobSkills(JSONArray jsonArray) {

        List<JobSkill> skills = new ArrayList<JobSkill>();

        if(jsonArray != null) {

            for(Object currentObject : jsonArray) {
                if(!(currentObject instanceof JSONObject)) 
                    continue;
                JSONObject jsonObject = (JSONObject) currentObject;

                Object nameObj = jsonObject.has("name") ? jsonObject.get("name") : null;
                if (nameObj == null)
                	continue;
                
                String name = String.valueOf(nameObj).trim();
                if (name.isEmpty())
                	continue;
                
                Object favoriteObj = jsonObject.has("favorite") ? jsonObject.get("favorite") : null;
                boolean favorite = false;
                if (favoriteObj != null && favoriteObj instanceof  Boolean)
                	favorite = (Boolean)favoriteObj;
                
                JobSkill jobSkill =  new JobSkill(name, favorite);

                skills.add(jobSkill);
            }
        }

        if (skills.isEmpty())
            return null;
        else
            return skills;
    }


    public JobSkill() {
	}
    

	
    public JobSkill(String name, boolean essential) {
		super();
		this.skill_name = name;
		this.desired = essential;
	}
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (desired ? 1231 : 1237);
        result = prime * result + ((skill_name == null) ? 0 : skill_name.hashCode());
        return result;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JobSkill other = (JobSkill) obj;
        if (desired != other.desired)
            return false;
        if (skill_name == null) {
            if (other.skill_name != null)
                return false;
        } else if (!skill_name.equals(other.skill_name))
            return false;
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("JobSkills [desired=").append(desired).append(", skill_name=").append(skill_name).append("]");
        return builder.toString();
    }
    
}