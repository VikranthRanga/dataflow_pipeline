package com.allegis.search.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.MetricsNamespace;
import com.allegis.search.io.ElasticsearchIO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Represents common properties and functionality of documents processed without our pipeline
 * {@see MatchDoc} and other objects such as {@see EstimatedPayrateDoc}
 */
public abstract class CommonDoc implements Serializable, Cloneable {

	private static final long serialVersionUID = -8878678362504254554L;
	
	// Common Properties to both Person and Jobs
    // ---------------------------------------------------------
	@JsonIgnore
	public String id; 

	@JsonIgnore
	public Boolean isRealTime = false;
	
	@JsonIgnore 
	public Boolean isQaData = false;
	
	@JsonIgnore
	public Boolean isDelete = false;  

    public Date submitTime = new Date(); // Ingestion processing start time
    public Long pipelineDuration;        // Ingestion pipeline processing time (in ms) from submitTime to JSONization
    public String feederId;
    
    /**
     * Constructor
     * @param id    the id of the common doc
     */
    public CommonDoc(String id) {
        super();
        this.id = id;
    }
    
    /**
     * Method used by ElasticsearctIO.Write to extract the feeder ID from the ES document.
     */
    public static class FeederIdExtractFn implements ElasticsearchIO.Write.FieldValueExtractFn {
        private static final long serialVersionUID = -1;
        private static Counter missing = Metrics.counter(MetricsNamespace.STAGE.name(), "Missing feederId");

        @Override
        public String apply(JsonNode personJson) {
            JsonNode personIdNode = personJson.findValue("feederId");
            if (personIdNode == null || personIdNode.isMissingNode() || personIdNode.isNull()) {
                missing.inc();
                return null;
            }
            return personIdNode.asText();
        }
    }
    
	// read https://www.artima.com/lejava/articles/equality.html
	public boolean canEqual(Object other) {
        return (other instanceof CommonDoc);
    }
	
    @Override
    public String toString() {
        return "CommonDoc [id=" + id + ", isRealTime=" + isRealTime + ", feederId=" + feederId + ", submitTime=" + submitTime + ", isQaData=" + isQaData + ", forceDelete=" + isDelete +"]";
    }

    // -----------------------------------------------------------
    // EXCLUDE submitTime from hashcode() and equals()
    // -----------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(id, isRealTime, isQaData, isDelete);
    }

    @Override
    public boolean equals( Object obj ) {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        CommonDoc other = (CommonDoc) obj;
        return Objects.equals(id, other.id) && Objects.equals(isRealTime, other.isRealTime) && Objects.equals(isQaData, other.isQaData) && Objects.equals(isDelete, other.isDelete);
    }
	
	
}
