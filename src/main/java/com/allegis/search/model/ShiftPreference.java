package com.allegis.search.model;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used to encapsulate shift preference extraction and ingestion 
 * @author david
 *
 */
public abstract class ShiftPreference {
	
    private static final Logger logger = LoggerFactory.getLogger( ShiftPreference.class );

	protected enum Name {
		First,		
		Second,
		Third,
		Weekend
	};

	
	
	/**
	 * Returns a match pattern, else null
	 * @param pattern
	 * @param text
	 * @return
	 */
	private String find(Pattern pattern, String text) {
		 if (text == null || text.isEmpty())
			 return null;
		 
		 text = text.replaceAll("\\s+", " ").toLowerCase();
		 
		 Matcher matcher = pattern.matcher(text);
		 if (matcher.find(0))
			 return matcher.group().trim();
		 return null;
	}


	/**
	 * 	 Extracts pattern and populates the supplied document.  
	 *
	 * @param doc
	 * @param text
	 * @param source
	 * @param type
	 * @param year
	 * @param name
	 */
	public boolean extract(Pattern pattern, MatchDoc doc, String text, String source, String type, Integer year, Name name) {
		String match = find(pattern, text);
		if (match != null) {
			doc.shift_preferences_audit.add(new ShiftPreference.Audit(source, type, year, match, name));
			doc.shift_preferences.add(name);
			return true;
		}
		return false;
	}	
	
	
	

	/**
	 * Extract shift preferences from supplied text.
	 * @param doc
	 * @param text
	 * @param source
	 * @param type
	 * @param year
	 */
	public boolean extract(MatchDoc doc, String text, String source, String type, Integer year) {

		int shifts = 0;
		for (Entry<Name, Pattern> shift : getShiftPatterns().entrySet()) {
			Name shiftName = shift.getKey();
			Pattern pattern = shift.getValue();
						
			if (extract(pattern, doc, text, source, type, year, shiftName))  {
				shifts++;
			}
		}
		return (shifts > 0);			
	}
	
			
	public abstract Map<Name, Pattern> getShiftPatterns();



	/**
	 * Temporary class used to index to gather statistics
	 */
	public static class Audit implements Serializable {
		private static final long serialVersionUID = -9222780547750344632L;
		public String source;
		public String type;
		public Integer year;
		public Name name;
		public String match;
		
		public Audit(String source, String type, Integer year, String match, Name name) {
			super();
			this.source = source;
			this.type = type;
			this.year = year;
			this.match = match;
			this.name = name;
		}
				

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((match == null) ? 0 : match.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((source == null) ? 0 : source.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			result = prime * result + ((year == null) ? 0 : year.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Audit other = (Audit) obj;
			if (match == null) {
				if (other.match != null)
					return false;
			} else if (!match.equals(other.match))
				return false;
			if (name != other.name)
				return false;
			if (source == null) {
				if (other.source != null)
					return false;
			} else if (!source.equals(other.source))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			if (year == null) {
				if (other.year != null)
					return false;
			} else if (!year.equals(other.year))
				return false;
			return true;
		}


		@Override
		public String toString() {
			return "Audit [source=" + source + ", type=" + type + ", year=" + year + ", name=" + name
					+ ", match=" + match + "]";
		}

	}
	

}
