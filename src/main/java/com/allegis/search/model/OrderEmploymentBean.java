package com.allegis.search.model;

import java.io.Serializable;

import com.allegis.candidate.profile.EmploymentBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


/**
 *  Used to carry extra useful order data contained when associated jobs are used to extract
 *  extra vector terms. 
 *    
 * @author david
 *
 */
@JsonInclude(Include.NON_NULL)
public class OrderEmploymentBean extends EmploymentBean implements Serializable  {
	private static final long serialVersionUID = 5295471418957287180L;
	
	public String skills;
	public GeoLocation geoLocation;
	public String status;
	public String opportunityId;
	@JsonIgnore
	public String orderId;
	@JsonIgnore
	// on Allegis work history, this is the name of the customer (account) and is normally much better than
	// the orgName of the Order
	public String accountName=null;
	@JsonIgnore
	public String accountDetails=null;
	
	public OrderEmploymentBean(String name) {
		super(name);
		
		// null out to avoid JSON materializtion
		setCity(null);
		setState(null);
		setCountry(null);
	}
	
}