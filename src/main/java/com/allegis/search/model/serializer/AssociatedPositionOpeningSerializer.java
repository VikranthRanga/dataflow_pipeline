package com.allegis.search.model.serializer;

import java.io.IOException;

import com.allegis.search.model.person.AssociatedPositionOpening;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * Custom json serializer for the Associated position object
 *                "associated_position_openings": {
                  "AssociatedPositionOpening": [
                     {
                        "PositionOpeningID": "18988501",
                        "StaffingOrderID": "O-2247615",
                        "PositionTitle": null,
                        "CandidateAssociationTypeCode": null,
                        "UserArea": {
                           "Status": "Applicant",
                           "ClientName": "Pactiv Llc",
                           "SubmittedById": "0051l000000EjsCAAS",
                           "SubmittedByName": "Careersite Integration",
                           "SubmittedDate": "2019-01-29T00:12:17.000Z",
                           "ActivityDate": "2019-01-29T00:12:17.000Z",
                           "MaxPayRate": "13.00",
                           "NotProceedingReason": null
                        },
                        "URI": null
                     }
                  ]
               },
 
 *
 */
public class AssociatedPositionOpeningSerializer extends JsonSerializer<AssociatedPositionOpening> {
	public AssociatedPositionOpeningSerializer() {
		super();
	}
	@Override
	public void serialize(AssociatedPositionOpening apo, JsonGenerator jgen,
			SerializerProvider provider) throws IOException, JsonProcessingException {

		if (apo == null) {
			return;
		}

		jgen.writeStartObject();    // {
		jgen.writeArrayFieldStart("AssociatedPositionOpening");		// "AssociatedPositionOpening"  [
			jgen.writeStartObject();	 // {
				jgen.writeStringField("PositionOpeningID", apo.PositionOpeningID);
				jgen.writeStringField("StaffingOrderID", apo.StaffingOrderID);
				jgen.writeStringField("PositionTitle", apo.PositionTitle);
				jgen.writeNullField("CandidateAssociationTypeCode");
				jgen.writeFieldName("UserArea");
					jgen.writeStartObject();
						jgen.writeStringField("Status", apo.Status);
						jgen.writeStringField("ClientName", apo.ClientName);
						jgen.writeStringField("SubmittedById", apo.SubmittedById);
						jgen.writeStringField("SubmittedByName", apo.SubmittedByName);
						jgen.writeObjectField("SubmittedDate", apo.SubmittedDate);
						jgen.writeObjectField("ActivityDate", apo.ActivityDate);
						jgen.writeObjectField("MaxPayRate", apo.MaxPayRate);
						jgen.writeStringField("NotProceedingReason", apo.NotProceedingReason);
					jgen.writeEndObject();;
				jgen.writeNullField("URI");
			jgen.writeEndObject();;		//  }
		jgen.writeEndArray();		// ]
		jgen.writeEndObject();	//  }

	}
}