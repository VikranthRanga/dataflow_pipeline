package com.allegis.search.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.candidate.profile.ProfileSource.SourceType;
import com.allegis.search.model.person.PersonDoc;

/**
 * Class used to encapsulate heavy lifting extraction and ingestion 
 * @author david
 *
 */
public class HeavyLifting {

	/**  Regex that tries to match negative sentiment toward heavy lifting */
	private static Pattern noHeavyLifterPattern = Pattern.compile(
			"(nothing|no|without|less|too much|(will|wo|was|is|can?|could|did|does)\\s?(n.?t)"
			+ "|avoid|due to|because|away from|not interested|unable)\\s?(like|prefer|need|require|want|feel|wanna)?.{0,3}\\s?"
			+ "(anything with|comfortable with|to do|any more|doing)?((as)?\\s?much|any)?\\s?(heavy\\s?(things|object)"
			+ "|(heavy)?\\s(lift|move? heavy)(ing)?\\s?(heavy)?)", Pattern.CASE_INSENSITIVE);
	
	/**  Regex that tries to match positive sentiment toward heavy lifting */
	private static Pattern heavyLifterPattern = Pattern.compile(
			"((pull|push|transport|carry|mov|lift|pick)(ing)?.{0,15}"
			+ "(heavy|\\d{2,3}(-\\d{2,3})?)\\s?"
			+ "(component|product|equipment|load|item|object|weight|lbs|pounds)"
			+ "|heavy (lift|mov)(er|ing)?|(mov|lift)(e|ing)? heavy)", Pattern.CASE_INSENSITIVE);

	/**
	 * Returns a match pattern, else null
	 * @param pattern
	 * @param text
	 * @return
	 */
	private static String find(Pattern pattern, String text) {
		 if (text == null || text.isEmpty())
			 return null;
		 
		 Matcher matcher = pattern.matcher(text);
		 if (matcher.find(0))
			 return matcher.group().trim();
		 return null;
	}


	/**
	 * 	 Extracts heavy lifting sentiment and populates the supplied document.  
	 *
	 * @param doc
	 * @param text
	 * @param source
	 * @param type
	 * @param year
	 */
	public static void extract(MatchDoc doc, String text, String source, String type, Integer year) {
		Boolean sentiment = null;  
		// Per DSIA, check for negative first.
		String match = find(noHeavyLifterPattern, text);
		if (match != null) {
			sentiment = false;
		}
		else {
			match = find(heavyLifterPattern, text);
			if (match != null) {
				sentiment = true;				
			}
		}
		 
		if (match != null) {
			// this will not affect the index but we would like to capture these negative sentiments in the audit
//			doc.heavy_lifting_audit.add(new HeavyLifting.Audit(source, type, year, match, sentiment));
			
			// only set if it is empty or true;  once false it cannot be overridden
			if (doc.heavy_lifting == null || doc.heavy_lifting)
				doc.heavy_lifting = sentiment;
		} 
	}	
	

	/**
	 * 	 Extracts heavy lifting sentiment and populates the supplied document.  
	 *
	 * @param doc
	 * @param text
	 * @param source
	 * @param type
	 * @param year
	 */
	public static void extractPositiveSentiment(MatchDoc doc, String text, String source, String type) {
		String match = findPositiveSentiment(text);
		if (match != null)	{		
//			doc.heavy_lifting_audit.add(new HeavyLifting.Audit(source, type, null, match, true));
			doc.heavy_lifting = true;
		}
	}	
	
	
	/**
	 * Extracts heavy lifting sentiment and populates the supplied document.
	 * 
	 * @param doc
	 * @param text
	 * @param source
	 * @param type
	 */
	public static void extract(MatchDoc doc, String text, String source, String type) {
		extract(doc, text, source, type, null); 
	}
	
	/**
	 * Returns the matched pattern if the text indicate positivity toward heavy lifting, else null
	 * @param text
	 * @return
	 */
	public static String findPositiveSentiment(String text) {
		// Per DSIA, check for negative first.
		String match = find(noHeavyLifterPattern, text);
		 if (match != null) 
			 return null;
		return find(heavyLifterPattern, text);
	}

	/**
	 * Returns the matched pattern if the text indicate negativity toward heavy lifting, else null
	 * @param text
	 * @return
	 */
	public static String findNegativeSentiment(String text) {
		return find(noHeavyLifterPattern, text);
	}
	
			
	
	/**
	 * Temporary class used to index heavy lifting to gather statistics
	 */
	public static class Audit implements Serializable {
		private static final long serialVersionUID = -9222780547750344632L;
		public String source;
		public String type;
		public Integer year;
		public boolean sentiment;
		public String match;
		
		public Audit(String source, String type, Integer year, String match, boolean sentiment) {
			super();
			this.source = source;
			this.type = type;
			this.year = year;
			this.match = match;
			this.sentiment = sentiment;
		}
				

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((match == null) ? 0 : match.hashCode());
			result = prime * result + (sentiment ? 1231 : 1237);
			result = prime * result + ((source == null) ? 0 : source.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			result = prime * result + ((year == null) ? 0 : year.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Audit other = (Audit) obj;
			if (match == null) {
				if (other.match != null)
					return false;
			} else if (!match.equals(other.match))
				return false;
			if (sentiment != other.sentiment)
				return false;
			if (source == null) {
				if (other.source != null)
					return false;
			} else if (!source.equals(other.source))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			if (year == null) {
				if (other.year != null)
					return false;
			} else if (!year.equals(other.year))
				return false;
			return true;
		}


		@Override
		public String toString() {
			return "Audit [source=" + source + ", type=" + type + ", year=" + year + ", sentiment=" + sentiment
					+ ", match=" + match + "]";
		}

	}




	/**
	 * Employment history should never contain no heavy lifting.  And even if it exists in the text it 
	 * is not an indication that the person cannot let.  It only indicates that heavy lifting is not required.
	 * 
	 * @param person
	 * @param employmentHistory
	 * @param source
	 */
	public static void extractPositiveSentiment(PersonDoc person, List<EmploymentBean> employmentHistory, SourceType source) {
		if (employmentHistory == null)
			return;
				
		for (Iterator<EmploymentBean> iterator = employmentHistory.iterator(); iterator.hasNext();) {
			EmploymentBean empBean = iterator.next();
			String match = findPositiveSentiment(empBean.getPositionDescription());
			if (match != null) {
								
				if (person.heavy_lifting == null) {
					// we cannot change from false to true.  But we still want to audit.
					person.heavy_lifting = true;
				}
				
				Integer year = new Date().getYear();
				if (empBean.getHistoryRange() != null && empBean.getHistoryRange().getEndDate() != null) {
					year = empBean.getHistoryRange().getEndDate().getYear();
				}
				year += 1900;
				
				String type = source.name();
				if (empBean instanceof OrderEmploymentBean)  {
					type = ((OrderEmploymentBean)empBean).status;
				}
				else {
					type = "employment";
				}
				
				// person.heavy_lifting_audit.add(new Audit(source.name(), type, year, match, true)); 
			}           		
		}	
	}

}
