package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;

/**
 * A POJO to hold job code classifier specific work history data
 *
 */
public class JobCodeWorkHist implements Serializable {

	private static final long serialVersionUID = 4109394859008211078L;

	
	public String positionTitle;	// position title   
	public String positionDescr;	// position description
    public Date startDate;			// employment start date  
    public Date endDate;			// employment end date
    public String workHistId;		// work history block id
    public String phId;				// legacy product if 
    public String hcsCode;			// alternate job code
    public String talentId;			// talent id
    public String orderId;			// order id
    
    public String source;			// source of work history
    
    // Null Safe Getters
    // ----------------------------------------------------
    public String getNullSafeTalentId() {
		if (talentId == null)
            return "";
		return talentId;
	}
    
	public String getNullSafePositionTitle() {
		if (positionTitle == null)
            return "";
		return positionTitle;
	}
	
	public String getNullSafePositionDescr() {
		if (positionDescr == null)
            return "";
		return positionDescr;
	}
	
	public String getNullSafeWorkHistId() {
		if (workHistId == null)
            return "";
		return workHistId;
	}
	
	public String getNullSafePhId() {
		if (phId == null)
            return "";

		return phId;
	}
	
	public String getNullSafeHcsCode() {
		if (hcsCode == null)
            return "";

		return hcsCode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((hcsCode == null) ? 0 : hcsCode.hashCode());
		result = prime * result + ((phId == null) ? 0 : phId.hashCode());
		result = prime * result + ((positionTitle == null) ? 0 : positionTitle.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((talentId == null) ? 0 : talentId.hashCode());
		result = prime * result + ((workHistId == null) ? 0 : workHistId.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((positionDescr == null) ? 0 : positionDescr.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobCodeWorkHist other = (JobCodeWorkHist) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (hcsCode == null) {
			if (other.hcsCode != null)
				return false;
		} else if (!hcsCode.equals(other.hcsCode))
			return false;
		if (phId == null) {
			if (other.phId != null)
				return false;
		} else if (!phId.equals(other.phId))
			return false;
		if (positionTitle == null) {
			if (other.positionTitle != null)
				return false;
		} else if (!positionTitle.equals(other.positionTitle))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (talentId == null) {
			if (other.talentId != null)
				return false;
		} else if (!talentId.equals(other.talentId))
			return false;
		if (workHistId == null) {
			if (other.workHistId != null)
				return false;
		} else if (!workHistId.equals(other.workHistId))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (positionDescr == null) {
			if (other.positionDescr != null)
				return false;
		} else if (!positionDescr.equals(other.positionDescr))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}    
}
