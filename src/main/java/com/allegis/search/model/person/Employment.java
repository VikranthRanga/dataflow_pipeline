package com.allegis.search.model.person;

import java.io.Serializable;

import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Employment implements Serializable {

	private static final long serialVersionUID = 8378815663526152677L;
    @JsonIgnore
	public String Id;
	public String OrganizationName;
    public String PositionTitle;
    public String Description;
    @JsonIgnore
    public String OrderId;
    @JsonIgnore
    public String DivisionName;
    @JsonIgnore
    public String ClientAccountId;
    @JsonIgnore
    public String RegionCode;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    public Date StartDate;	
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    public Date EndDate;

    public String PaymentFrequency;
    public String PayRate;
    public String FinishReason;
    @JsonIgnore
    public String FinishCode;
    @JsonIgnore
    public String SourceCompanyId;
    @JsonIgnore
    public Double IndustryConfidence;
    @JsonIgnore
    public String CompanyName;
    @JsonIgnore
    public String NaicsDescription ;
    @JsonIgnore
    public String SectorDescription ;
    @JsonIgnore
    public String NaicsCode;
    @JsonIgnore
    public String IndustryWorkHistoryId;
    
    /**
     * Default Constructor
     */
    public Employment() {
        
    }
    
    /**
     * Constructor 
     * @param Id String
     * @param organizationName String
     * @param positionTitle String
     * @param description String
     * @param startDate Date
     * @param endDate Date 
     * @param paymentFrequency String
     * @param payRate String
     * @param finishReason String
     */
    public Employment(String id, String organizationName, String positionTitle, String description, Date startDate, Date endDate,
            String paymentFrequency, String payRate, String finishReason) {
        super();
        Id = id;
        OrganizationName = organizationName;
        PositionTitle = positionTitle;
        Description = description;
        StartDate = startDate;
        EndDate = endDate;
        PaymentFrequency = paymentFrequency;
        PayRate = payRate;
        FinishReason = finishReason;
    }
    
    public Employment( Double confidence, String company_name,String naics_description, String naics_code, String sector_description, String work_history_id) {
        this.IndustryConfidence = confidence;
        this.CompanyName = company_name;
        this.NaicsDescription = naics_description;
        this.NaicsCode = naics_code;
        this.SectorDescription = sector_description;
        this.IndustryWorkHistoryId = work_history_id;
    }


    @Override
    public String toString() {
        String description = Description;
        if (Description != null && Description.length() > 15)
            description = Description.substring(0, 15)+"...";
        return "Employment [OrganizationName=" + OrganizationName + ", PositionTitle=" + PositionTitle
                + ", Description=" + description + ", StartDate=" + StartDate + ", EndDate=" + EndDate
                + ", PaymentFrequency=" + PaymentFrequency + ", PayRate=" + PayRate + ", FinishReason=" + FinishReason
                + ", Id=" + Id + ", OrderId=" + OrderId + ", DivisionName=" + DivisionName + ", ClientAccountId=" + ClientAccountId 
                + ", RegionCode=" + RegionCode + ", FinishCode=" + FinishCode + ", SourceCompanyId=" + SourceCompanyId 
                + ", IndustryConfidence=" + IndustryConfidence + ", CompanyName=" + CompanyName + ", NaicsDescription=" + NaicsDescription
                + ", NaicsCode=" + NaicsCode + ", SectorDescription=" + SectorDescription + ", IndustryWorkHistoryId=" + IndustryWorkHistoryId +"]";
    }

	@Override
	public int hashCode() {
		return Objects.hash(Description, EndDate, FinishReason, OrganizationName, PayRate, PaymentFrequency,
				PositionTitle, StartDate, Id, OrderId, DivisionName, ClientAccountId, RegionCode, FinishCode, SourceCompanyId,
				IndustryConfidence, CompanyName, NaicsDescription, NaicsCode, SectorDescription, IndustryWorkHistoryId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employment other = (Employment) obj;
		return Objects.equals(Description, other.Description) && Objects.equals(EndDate, other.EndDate)
				&& Objects.equals(FinishReason, other.FinishReason)
				&& Objects.equals(OrganizationName, other.OrganizationName) && Objects.equals(PayRate, other.PayRate)
				&& Objects.equals(PaymentFrequency, other.PaymentFrequency)
				&& Objects.equals(PositionTitle, other.PositionTitle) && Objects.equals(StartDate, other.StartDate)
				&& Objects.equals(Id, other.Id) && Objects.equals(OrderId, other.OrderId)
				&& Objects.equals(DivisionName, other.DivisionName) && Objects.equals(ClientAccountId, other.ClientAccountId)
				&& Objects.equals(RegionCode, other.RegionCode) && Objects.equals(FinishCode, other.FinishCode)
				&& Objects.equals(SourceCompanyId, other.SourceCompanyId)&& Objects.equals(IndustryConfidence, other.IndustryConfidence)
				&& Objects.equals(CompanyName, other.CompanyName) && Objects.equals(NaicsDescription, other.NaicsDescription)
				&& Objects.equals(NaicsCode, other.NaicsCode) && Objects.equals(SectorDescription, other.SectorDescription) && Objects.equals(IndustryWorkHistoryId, other.IndustryWorkHistoryId);
	}
}
