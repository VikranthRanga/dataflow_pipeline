package com.allegis.search.model.person;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * A POJO to hold Proven Track Record specific request info
 * 
 * @author jperecha
 *
 */
public class ProvenTrackRecordData implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public String id;			// talent id
	public String status;			// talent status
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Integer pf_count;				// performance feedback/service touchpoint count
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public String pf_detail;			// performance feedback/service touchpoint description detail
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Integer ma_count;				// no.of companies where multiple assignments held
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Integer ma_detail;			// no.of positions held at multiple companies
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Integer rc_count;				// no.of Reference checks
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Integer fr_detail;			// finish reasons
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Integer rr_detail;			// recruiter relationships
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Integer recency_month;		// count of the number of months since the last interaction with a recruiter
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fr_detail == null) ? 0 : fr_detail.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ma_count == null) ? 0 : ma_count.hashCode());
		result = prime * result + ((ma_detail == null) ? 0 : ma_detail.hashCode());
		result = prime * result + ((pf_count == null) ? 0 : pf_count.hashCode());
		result = prime * result + ((pf_detail == null) ? 0 : pf_detail.hashCode());
		result = prime * result + ((rc_count == null) ? 0 : rc_count.hashCode());
		result = prime * result + ((recency_month == null) ? 0 : recency_month.hashCode());
		result = prime * result + ((rr_detail == null) ? 0 : rr_detail.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvenTrackRecordData other = (ProvenTrackRecordData) obj;
		if (fr_detail == null) {
			if (other.fr_detail != null)
				return false;
		} else if (!fr_detail.equals(other.fr_detail))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ma_count == null) {
			if (other.ma_count != null)
				return false;
		} else if (!ma_count.equals(other.ma_count))
			return false;
		if (ma_detail == null) {
			if (other.ma_detail != null)
				return false;
		} else if (!ma_detail.equals(other.ma_detail))
			return false;
		if (pf_count == null) {
			if (other.pf_count != null)
				return false;
		} else if (!pf_count.equals(other.pf_count))
			return false;
		if (pf_detail == null) {
			if (other.pf_detail != null)
				return false;
		} else if (!pf_detail.equals(other.pf_detail))
			return false;
		if (rc_count == null) {
			if (other.rc_count != null)
				return false;
		} else if (!rc_count.equals(other.rc_count))
			return false;
		if (recency_month == null) {
			if (other.recency_month != null)
				return false;
		} else if (!recency_month.equals(other.recency_month))
			return false;
		if (rr_detail == null) {
			if (other.rr_detail != null)
				return false;
		} else if (!rr_detail.equals(other.rr_detail))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
    
}
