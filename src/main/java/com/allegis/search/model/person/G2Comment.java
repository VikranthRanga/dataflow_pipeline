package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A POJO to hold G2 Comments for Eagnerness Check
 * 
 */
public class G2Comment implements Serializable {

    private static final long serialVersionUID = -2229038073555522103L;
    
    public String id;
	public Date activityDate;
	public String description;
	
	public G2Comment() {}
	
	public G2Comment(String id, String description, Date activitDate) {
		this.id = id;
		this.description = description;
		this.activityDate = activitDate;
	}

    @Override
    public int hashCode() {
        return Objects.hash(activityDate, description, id);
    }

    @Override
    public boolean equals( Object obj ) {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        G2Comment other = (G2Comment) obj;
        return Objects.equals(activityDate, other.activityDate) && Objects.equals(description, other.description) && Objects.equals(id, other.id);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("EagernessComments [id=").append(id).append(", activityDate=").append(activityDate).append(", description=").append(description).append("]");
        return builder.toString();
    }

	
}
