package com.allegis.search.model.person;

import java.io.Serializable;

/**
 * A POJO to hold work history related data point for Proven Track Record
 * 
 * @author jperecha
 *
 */
public class ProvenTrackRecordWorkInfo implements Serializable {

	private static final long serialVersionUID = -2784934759255997606L;
	
	public String id;
	public String sourceCompanyId;
	
	public ProvenTrackRecordWorkInfo() {
		super();
	}
	
	public ProvenTrackRecordWorkInfo(String id, String sourceCompanyId) {
		super();
		this.id = id;
		this.sourceCompanyId = sourceCompanyId;
	}

	public String getSourceCompanyId() {
		return sourceCompanyId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((sourceCompanyId == null) ? 0 : sourceCompanyId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvenTrackRecordWorkInfo other = (ProvenTrackRecordWorkInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (sourceCompanyId == null) {
			if (other.sourceCompanyId != null)
				return false;
		} else if (!sourceCompanyId.equals(other.sourceCompanyId))
			return false;
		return true;
	}

}
