package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;

/**
 * A POJO to hold Event/Task activity related data point for Proven Track Record
 * 
 * @author jperecha
 *
 */
public class ProvenTrackRecordActivityInfo implements Serializable {

	private static final long serialVersionUID = 8421473842248510851L;
	
	public String id;
	public String description;
	public Date activityDate;

	public ProvenTrackRecordActivityInfo() {
		super();
	}
	
	public ProvenTrackRecordActivityInfo(String id, String description, Date activitDate) {
		super();
		this.id = id;
		this.description = description;
		this.activityDate = activitDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activityDate == null) ? 0 : activityDate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvenTrackRecordActivityInfo other = (ProvenTrackRecordActivityInfo) obj;
		if (activityDate == null) {
			if (other.activityDate != null)
				return false;
		} else if (!activityDate.equals(other.activityDate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProvenTrackRecordActivityInfo [id=" + id + ", description=" + description + ", activityDate="
				+ activityDate + "]";
	}
}
