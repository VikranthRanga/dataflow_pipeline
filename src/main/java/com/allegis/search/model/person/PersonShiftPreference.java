package com.allegis.search.model.person;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.candidate.profile.ProfileSource.SourceType;
import com.allegis.search.model.MatchDoc;
import com.allegis.search.model.OrderEmploymentBean;
import com.allegis.search.model.ShiftPreference;
import com.allegis.search.stage.DeadLetterService;

/**
 * Class used to encapsulate shift preference extraction and ingestion 
 * @author david
 *
 */
public class PersonShiftPreference extends ShiftPreference {
	
    private static final Logger logger = LoggerFactory.getLogger( PersonShiftPreference.class );
    /**
     * 2 years of milliseconds
     */
    private static final long TWO_YEARS_MILS = (24L * 60L * 60L * 1000L) * 730L;

	private static PersonShiftPreference instance;

	/**  Regex that tries to match negative sentiment toward heavy lifting */
	private Pattern talent1stPattern = Pattern.compile(
			"(?:(?:first(?:, second (?:and|or) t| (?:and|or) t)hird|(?:morning (?:and (?:after)?|or (?:after)?)no|open )on|morning (?:and (?:evening|night|day)|or (?:evening|night))|first (?:(?:and|or) )?second|flex(?:ible (?:with|[io]n)| (?:with|[io]n))|(?:good|o(?:pen|k)) with|flexible|o(?:pen (?:for|to)|ne)|morning|first|(?:earl|an)y|a(?:ll|m)) |1(?:st(?:, (?:(?:2nd (?:and|or) |2nd, )?3r|2n)d | (?:(?:2nd (?:and|or) )?3rd |and (?:3r|2n)d |or (?:3r|2n)d |2nd )?)| ))shift"
			);
	
	/**  Regex that tries to match positive sentiment toward heavy lifting */
	private Pattern talent2ndPattern = Pattern.compile(
			"(?:(?:(?:first, second (?:and|or) |second(?: (?:and |or )?|, ))thi|1st, 2nd(?: and|,) 3|1st, 2nd or 3)rd|morning (?:and (?:after)?|or (?:after)?)noon|a(?:fternoon (?:and (?:evening|night)|or (?:evening|night))|ll|ny)|noon (?:and (?:evening|night)|or (?:evening|night))|(?:first (?:and |or )?seco|1st (?:and|or) 2)nd|day (?:and|or) evening|morning (?:and|or) day|1st 2nd (?:and |or )?3rd|flex(?:ible)? with|(?:flex(?:ible [io]| [io])|open o)n|2(?:nd(?: (?:and |or )?|, )3rd|nd)?|afternoon|(?:good|o(?:pen|k)) with|flexible|1st, 2nd|open (?:for|to)|day time|daylight|1st 2nd|second|noon|\\bday\\b) shift"
			);
	
	
	private Pattern talent3rdPattern = Pattern.compile(
			"(?:(?:(?:first(?:, second)? and |first(?:, second)? or |second(?: (?:and |or )?|, ))?thi|(?:1st(?:, (?:2nd and |2nd(?: or|,) )?| (?:2nd (?:and |or )?|and |or )?)|2nd(?: (?:and |or )?|, ))3|grave ya)rd|(?:afternoon (?:and|or) |morning (?:and|or) |noon (?:and|or) |day (?:and|or) )?evening|(?:afternoon (?:and|or) |morning (?:and|or) |over)night|n(?:oon (?:and|or) n)?ight|(?:flex(?:ible)?|open) with|flex(?:ible [io]| [io])n|good with|(?:flexibl|grav)e|open (?:for|on|to)|ok with|swing|late|a(?:ll|ny)|3(?:rd)?) shift"
			);

	private Pattern talentWeekendPattern = Pattern.compile(
			"(?:s(?:aturday(?: (?:and|or)|,) s)?unday|flex(?:ible (?:with|[io]n)| (?:with|[io]n))|(?:good|o(?:pen|k)) with|flexible|saturday|open (?:for|to|on)|weekend|any|all) shift"
			);

	
	// Preserve insertion order
	private LinkedHashMap<Name, Pattern> shiftPatterns = new LinkedHashMap<>();
	
	
    /**
     * Factory method to retrieve an instance
     * @return a singleton instance of this class
     */
    public static PersonShiftPreference getInstance() {

        if (null == PersonShiftPreference.instance) {
            synchronized (PersonShiftPreference.class) {
                if (null == PersonShiftPreference.instance) {
                	PersonShiftPreference.instance = new PersonShiftPreference();
                }
            }
        }
        return PersonShiftPreference.instance;
    }
    
        
	
	private PersonShiftPreference() {
		shiftPatterns.put(Name.First, talent1stPattern);
		shiftPatterns.put(Name.Second, talent2ndPattern);
		shiftPatterns.put(Name.Third, talent3rdPattern);
		shiftPatterns.put(Name.Weekend, talentWeekendPattern);		
	}
	
	
    /**
     * Parse ShiftPreference
     * @param doc
     * @param shiftPreferencesRaw String (First;Weekend)
     * @return List of Strings (["First","Second","Weekend"]
     */
    public boolean extractAccountPreference(MatchDoc doc, String shiftPreferenceRaw, DeadLetterService deadLetterService){
        if(shiftPreferenceRaw == null) 
            return false;
        
        boolean gotShift = false;
        for(String item: shiftPreferenceRaw.split(";")) {
            try {
            	Name name = Name.valueOf(item);
                gotShift = true;
				doc.shift_preferences.add(name);
				doc.shift_preferences_audit.add(new PersonShiftPreference.Audit(shiftPreferenceRaw, "Account", null, null, name));
			} catch (Exception e) {
				deadLetterService.writeDataQualityWarning(e.getMessage(), "item="+item, doc);
			}
        }
        
        return gotShift;
    }
	


	/**
	 * Extract shift preferences from employment history.
	 * 
	 * @param person
	 * @param employmentHistory
	 * @param source
	 */
	@SuppressWarnings("deprecation")
	public boolean extract(PersonDoc person, List<EmploymentBean> employmentHistory, SourceType source) {
		if (employmentHistory == null)
			return false;
		
		Date fourYearsAgo = new Date(System.currentTimeMillis() - TWO_YEARS_MILS*2);
		
		// default ending year
		Integer endingYear = new Date().getYear();
		
		int shifts = 0;
		for (EmploymentBean empBean : employmentHistory) {
			for (Entry<Name, Pattern> shift : shiftPatterns.entrySet()) {
				Name shiftName = shift.getKey();
				Pattern pattern = shift.getValue();

				if (empBean.getHistoryRange() != null && empBean.getHistoryRange().getEndDate() != null) {
					if (empBean.getHistoryRange().getEndDate().before(fourYearsAgo))  {
						continue;  // to old
					}
					endingYear = empBean.getHistoryRange().getEndDate().getYear();
				}
				
				String type = source.name();
				if (empBean instanceof OrderEmploymentBean)  {
					type = ((OrderEmploymentBean)empBean).status;
				}
				else {
					type = "employment";
				}  
				
				if (extract(pattern, person, empBean.getPositionDescription(), source.name(), type, endingYear+1900, shiftName))  {
					shifts++;
				}
			}
		}
		return (shifts > 0);
	}



	@Override
	public Map<Name, Pattern> getShiftPatterns() {
		return shiftPatterns;
	}
		

}
