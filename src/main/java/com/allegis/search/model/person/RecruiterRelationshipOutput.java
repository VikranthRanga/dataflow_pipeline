package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;

import com.allegis.search.utils.DateFormatTypeAdapter;
import com.allegis.search.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.annotations.JsonAdapter;

/**
 * A POJO to hold Recruiter Relationship JSON for forgotten relationships
 *
 */
public class RecruiterRelationshipOutput implements Serializable {

	private static final long serialVersionUID = -8879717856809249653L;
	public String user_id;
    public Float score; 
    @JsonAdapter(DateFormatTypeAdapter.class)
    public Date touch_date;

    public RecruiterRelationshipOutput(String id, Float strength, Date lastContactDate) {
		this.user_id = id;
		this.score = strength;
		this.touch_date = lastContactDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
		result = prime * result + ((score == null) ? 0 : score.hashCode());
		result = prime * result + ((touch_date == null) ? 0 : touch_date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecruiterRelationshipOutput other = (RecruiterRelationshipOutput) obj;
		if (user_id == null) {
			if (other.user_id != null)
				return false;
		} else if (!user_id.equals(other.user_id))
			return false;
		if (score == null) {
			if (other.score != null)
				return false;
		} else if (!score.equals(other.score))
			return false;
		if (touch_date == null) {
			if (other.touch_date != null)
				return false;
		} else if (!touch_date.equals(other.touch_date))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RecruiterRelationshipOutput [user_id=" + user_id + ", score=" + score + ", touch_date="
				+ touch_date + "]";
	}

	
}
