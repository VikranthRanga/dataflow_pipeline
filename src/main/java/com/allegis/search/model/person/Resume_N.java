package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Resume_N represents an attachment, i.e. a Resume
 */
@JsonSerialize
public class Resume_N implements Serializable, Cloneable {
	private static final long serialVersionUID = 7628684096004105850L;
	
	//Keep this field in the index for debugging purposes
	public String id;	
	public int size;
	public String body;
	public String first_name;
	public String last_name;
	public Date modified;
	public String type;
	public boolean is_full_name_blacklisted;    
	
	@JsonIgnore
	public boolean hasHtmlVersion=false;
    	
	/**
	 * Needed for cloning
	 */
	public Resume_N() {
	}
	
	public Resume_N(String id) {
		super();
		this.id = id;
	}
	
    @Override
    public Object clone() throws CloneNotSupportedException {
    	return super.clone();
    }

    @Override
    public int hashCode() {
        return Objects.hash(body, first_name, hasHtmlVersion, id, is_full_name_blacklisted, last_name, modified, size, type);
    }

    @Override
    public boolean equals( Object obj ) {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        Resume_N other = (Resume_N) obj;
        return Objects.equals(body, other.body) && Objects.equals(first_name, other.first_name) && hasHtmlVersion == other.hasHtmlVersion && Objects.equals(id, other.id)
                && is_full_name_blacklisted == other.is_full_name_blacklisted && Objects.equals(last_name, other.last_name) && Objects.equals(modified, other.modified) && size == other.size && Objects.equals(type, other.type);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Resume_N [id=");
        builder.append(id);
        builder.append(", size=");
        builder.append(size);
        builder.append(", body=");
        builder.append(body);
        builder.append(", first_name=");
        builder.append(first_name);
        builder.append(", last_name=");
        builder.append(last_name);
        builder.append(", modified=");
        builder.append(modified);
        builder.append(", type=");
        builder.append(type);
        builder.append(", is_full_name_blacklisted=");
        builder.append(is_full_name_blacklisted);
        builder.append(", hasHtmlVersion=");
        builder.append(hasHtmlVersion);
        builder.append("]");
        return builder.toString();
    }

	
}
