package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * Represents the collection of Certification history for a Talent
 *
 */
public class CertHist implements Serializable {
	
	private static final long serialVersionUID = 2826525951173551015L;

	public String CertName;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy")
	public Date CertYear;
	
    public CertHist(String certName, Date certYear) {
		super();
		CertName = certName;
		CertYear = certYear;
	}

    public CertHist() {
		super();
	}

    
	@Override
	public int hashCode() {
		return Objects.hash(CertName, CertYear);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CertHist other = (CertHist) obj;
		return Objects.equals(CertName, other.CertName) && Objects.equals(CertYear, other.CertYear);
	}
	@Override
	public String toString() {
		return "CertHist [" + (CertName != null ? "CertName=" + CertName + ", " : "")
				+ (CertYear != null ? "CertYear=" + CertYear : "") + "]";
	}

}
