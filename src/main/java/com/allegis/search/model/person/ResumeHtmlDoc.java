package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.DocType;
import com.allegis.search.enums.MetricsNamespace;
import com.allegis.search.io.ElasticsearchIO;
import com.allegis.search.model.CommonDoc;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * An object to represent Resume Html specific properties. 
 * 
 * Place class members that map to the fields within Elasticsearch
 * 
 */
@JsonSerialize
public class ResumeHtmlDoc extends CommonDoc implements Serializable {

	private static final long serialVersionUID = -701671498422399515L;
	
	public ResumeHtmlDoc(String id) {
		super(id);
	}
	
	public String candidate_id;
	public String resume_html;
	public String resume_id;
	public String resume_type;
	public Date resume_last_modified;

	@Override
	public String toString() {
		return "ResumeHtmlDoc [candidate_id=" + candidate_id + ", resume_id="
				+ resume_id + ", resume_type=" + resume_type + ", resume_last_modified=" + resume_last_modified + ", resume_html=" + resume_html + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((candidate_id == null) ? 0 : candidate_id.hashCode());
		result = prime * result + ((resume_html == null) ? 0 : resume_html.hashCode());
		result = prime * result + ((resume_id == null) ? 0 : resume_id.hashCode());
		result = prime * result + ((resume_last_modified == null) ? 0 : resume_last_modified.hashCode());
		result = prime * result + ((resume_type == null) ? 0 : resume_type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResumeHtmlDoc other = (ResumeHtmlDoc) obj;
		if (candidate_id == null) {
			if (other.candidate_id != null)
				return false;
		} else if (!candidate_id.equals(other.candidate_id))
			return false;
		if (resume_html == null) {
			if (other.resume_html != null)
				return false;
		} else if (!resume_html.equals(other.resume_html))
			return false;
		if (resume_id == null) {
			if (other.resume_id != null)
				return false;
		} else if (!resume_id.equals(other.resume_id))
			return false;
		if (resume_last_modified == null) {
			if (other.resume_last_modified != null)
				return false;
		} else if (!resume_last_modified.equals(other.resume_last_modified))
			return false;
		if (resume_type == null) {
			if (other.resume_type != null)
				return false;
		} else if (!resume_type.equals(other.resume_type))
			return false;
		return true;
	}
	
	
	/**
     * Method used by ElasticsearctIO.Write to extract the unique SalesForce ID to the ES document s_id.
     */
    public static class DocIdExtractFn implements ElasticsearchIO.Write.FieldValueExtractFn {
        private static final long serialVersionUID = -1;
        private static Counter missing= Metrics.counter(MetricsNamespace.STAGE.name(), "Missing candidate_id");

        @Override
        public String apply(JsonNode resumeJson) {
            JsonNode resumeIdNode = resumeJson.findValue( DocType._DOC.getIdField() );
            if (resumeIdNode == null || resumeIdNode.isMissingNode() || resumeIdNode.isNull()) {
                missing.inc();
                return null;
            }
            return resumeIdNode.asText();
        }
    }    
	
	
	
	
	
}
