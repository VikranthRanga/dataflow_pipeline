package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.allegis.candidate.profile.ContactInfoBean;
import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.candidate.profile.ProfileBean;
import com.allegis.candidate.profile.ProfileSource;
import com.allegis.candidate.profile.ResumeSource;
import com.allegis.candidate.profile.SkillTaxonomyBean;

/**
 * Lean & mean profile (source) bean for easy serialization.
 * @author dhartwig
 *
 */
public class ResumeProfile implements Serializable, ProfileSource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7600021213845157829L;
	private List<EmploymentBean> employmentHistory;
	private ProfileBean profile;
	private String skills;
	private ContactInfoBean contactInfo;
	private List<SkillTaxonomyBean> skillTaxonomyBeans;

	public ResumeProfile() {
		super();
	}
	
	public ResumeProfile(ResumeSource source) {
		super();
		this.contactInfo = source.getContactInfo();
		this.profile = source.getProfile();
		this.skills = source.getSkills();
		this.employmentHistory = source.getEmploymentHistory();
		this.skillTaxonomyBeans =  source.getSkillTaxonomy();
	}

	@Override
	public List<EmploymentBean> getEmploymentHistory() {
		return employmentHistory;
	}


	@Override
	public SourceType getSourceType() {
		return SourceType.SOV;
	}

	@Override
	public ProfileBean getProfile() {
		return profile;
	}

	@Override
	public String getSkills() {
		return skills;
	}

	/**
	 * This is extra non-profile data used to detect black listed terms in the talent's name
	 */
	public ContactInfoBean getContactInfo() {
		return contactInfo;
	}
	
	/**
	 * This is extra non-profile data used in computing block hash
	 */
	public List<SkillTaxonomyBean> getSkillTaxonomy() {
		return this.skillTaxonomyBeans;
	}
	
	@Override
	public String getCommunitiesJobTitles() {
		return null;
	}
	
	@Override
	public String getCommunitiesSkills() {
		return null;
	}
	
	@Override
	public Date getCommunitiesProfileLastModifiedDate() {
		return null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(contactInfo, employmentHistory, profile, skillTaxonomyBeans, skills);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResumeProfile other = (ResumeProfile) obj;
		return Objects.equals(contactInfo, other.contactInfo)
				&& Objects.equals(employmentHistory, other.employmentHistory) && Objects.equals(profile, other.profile)
				&& Objects.equals(skillTaxonomyBeans, other.skillTaxonomyBeans) && Objects.equals(skills, other.skills);
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "ResumeProfile ["
				+ (employmentHistory != null
						? "employmentHistory="
								+ employmentHistory.subList(0, Math.min(employmentHistory.size(), maxLen)) + ", "
						: "")
				+ (profile != null ? "profile=" + profile + ", " : "")
				+ (skills != null ? "skills=" + skills + ", " : "")
				+ (contactInfo != null ? "contactInfo=" + contactInfo + ", " : "")
				+ (skillTaxonomyBeans != null
						? "skillTaxonomyBeans="
								+ skillTaxonomyBeans.subList(0, Math.min(skillTaxonomyBeans.size(), maxLen))
						: "")
				+ "]";
	}

}
