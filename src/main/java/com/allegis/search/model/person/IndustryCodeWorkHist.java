package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;

/**
 * A POJO to hold industry code classifier specific work history data
 *
 */
public class IndustryCodeWorkHist implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String talentId;			// talent id
	public String workHistId;		// work history block id
	public String companyName;		// company name
	public String division;			// division
    public Date startDate;			// employment start date  
    public Date endDate;			// employment end data
    public String country;			// country
    public String state;			// state
    public String city;				// city
    public String source;			// source of work history
    
    public String accountId;		// account id
      
    
    // Null Safe Getters
    // ----------------------------------------------------
    public String getNullSafeTalentId() {
		if (talentId == null)
            return "";
		return talentId;
	}
    
    public String getNullSafeAccountId() {
		if (accountId == null)
            return "";
		return accountId;
	}
    
	public String getNullSafeWorkHistId() {
		if (workHistId == null)
            return "";
		return workHistId;
	}
	
	public String getNullSafeCompanyName() {
		if (companyName == null)
            return "";

		return companyName;
	}
	
	public String getNullSafeDivision() {
		if (division == null)
            return "";

		return division;
	}
	
	public String getNullSafeCountry() {
		if (country == null)
            return "";

		return country;
	}
	
	public String getNullSafeState() {
		if (state == null)
            return "";

		return state;
	}
	
	public String getNullSafeCity() {
		if (city == null)
            return "";

		return city;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((division == null) ? 0 : division.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((talentId == null) ? 0 : talentId.hashCode());
		result = prime * result + ((workHistId == null) ? 0 : workHistId.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndustryCodeWorkHist other = (IndustryCodeWorkHist) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (division == null) {
			if (other.division != null)
				return false;
		} else if (!division.equals(other.division))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (talentId == null) {
			if (other.talentId != null)
				return false;
		} else if (!talentId.equals(other.talentId))
			return false;
		if (workHistId == null) {
			if (other.workHistId != null)
				return false;
		} else if (!workHistId.equals(other.workHistId))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}	
	
}
