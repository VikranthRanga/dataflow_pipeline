package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Objects;

/**
 * A POJO to hold industry codes from industry code classifier api
 *
 */
public class IndustryCodeOutput implements Serializable {

    private static final long serialVersionUID = -4512949510314096584L;
    public Double confidence;
    public String sector_description;    

    public IndustryCodeOutput( Double confidence, String sectorDescription ) {
        this.confidence = confidence;
        this.sector_description = sectorDescription;
    }

    @Override
    public int hashCode() {
        return Objects.hash(confidence, sector_description);
    }

    @Override
    public boolean equals( Object obj ) {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        IndustryCodeOutput other = (IndustryCodeOutput) obj;
        return Objects.equals(confidence, other.confidence) && Objects.equals(sector_description, other.sector_description);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("IndustryCodeOutput [confidence=").append(confidence).append(", sector_description=").append(sector_description).append("]");
        return builder.toString();
    }

}
