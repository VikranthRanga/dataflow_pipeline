package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.List;

/**
 * Used to capture and persist a alternate vector state prior to a recompute.
 * The first use case is to capture alternative blending strategies so that 
 * can be compared using nDCG in the point-in-time indices.
 */
public class AltVectorState implements Serializable {   
	private static final long serialVersionUID = 5063268935787528469L;
	
	public String docvector;
    public Double docvector_magnitude;

    public String titlevector;
    public Double titlevector_magnitude;
    public List<String> titlevector_audit;

    public String skillsvector;
    public Double skillsvector_magnitude;
    public List<String> skillsvector_audit;

    public String keywordvector;
    public Double keywordvector_magnitude;
    public List<String> keywordvector_audit;

    public String acronymvector;
    public Double acronymvector_magnitude;
    public List<String> acronymvector_audit;

    public String sf_domain = "UNKNOWN";
    public List<String> sf_domain_audit;
    public String sf_vector;
    public Double sf_vector_magnitude;
    public Double sf_domain_confidence = 0.0;
    public String most_relevant_skill_family;
    
    
	public AltVectorState(PersonDoc person) {
		super();
		
		docvector = person.docvector;
	    docvector_magnitude = person.docvector_magnitude;

	    titlevector = person.titlevector;
	    titlevector_magnitude = person.titlevector_magnitude;
	    titlevector_audit = person.titlevector_audit;

	    skillsvector = person.skillsvector;
	    skillsvector_magnitude = person.skillsvector_magnitude;
	    skillsvector_audit = person.skillsvector_audit;

	    keywordvector = person.keywordvector;
	    keywordvector_magnitude = person.keywordvector_magnitude;
	    keywordvector_audit = person.keywordvector_audit;

	    acronymvector = person.acronymvector;
	    acronymvector_magnitude = person.acronymvector_magnitude;
	    acronymvector_audit = person.acronymvector_audit;

	    sf_domain = person.sf_domain;
	    sf_domain_audit = person.sf_domain_audit;
	    sf_vector = person.sf_vector;
	    sf_vector_magnitude = person.sf_vector_magnitude;
	    sf_domain_confidence = person.sf_domain_confidence;
	    most_relevant_skill_family = person.most_relevant_skill_family;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acronymvector == null) ? 0 : acronymvector.hashCode());
		result = prime * result + ((acronymvector_audit == null) ? 0 : acronymvector_audit.hashCode());
		result = prime * result + ((acronymvector_magnitude == null) ? 0 : acronymvector_magnitude.hashCode());
		result = prime * result + ((docvector == null) ? 0 : docvector.hashCode());
		result = prime * result + ((docvector_magnitude == null) ? 0 : docvector_magnitude.hashCode());
		result = prime * result + ((keywordvector == null) ? 0 : keywordvector.hashCode());
		result = prime * result + ((keywordvector_audit == null) ? 0 : keywordvector_audit.hashCode());
		result = prime * result + ((keywordvector_magnitude == null) ? 0 : keywordvector_magnitude.hashCode());
		result = prime * result + ((most_relevant_skill_family == null) ? 0 : most_relevant_skill_family.hashCode());
		result = prime * result + ((sf_domain == null) ? 0 : sf_domain.hashCode());
		result = prime * result + ((sf_domain_audit == null) ? 0 : sf_domain_audit.hashCode());
		result = prime * result + ((sf_domain_confidence == null) ? 0 : sf_domain_confidence.hashCode());
		result = prime * result + ((sf_vector == null) ? 0 : sf_vector.hashCode());
		result = prime * result + ((sf_vector_magnitude == null) ? 0 : sf_vector_magnitude.hashCode());
		result = prime * result + ((skillsvector == null) ? 0 : skillsvector.hashCode());
		result = prime * result + ((skillsvector_audit == null) ? 0 : skillsvector_audit.hashCode());
		result = prime * result + ((skillsvector_magnitude == null) ? 0 : skillsvector_magnitude.hashCode());
		result = prime * result + ((titlevector == null) ? 0 : titlevector.hashCode());
		result = prime * result + ((titlevector_audit == null) ? 0 : titlevector_audit.hashCode());
		result = prime * result + ((titlevector_magnitude == null) ? 0 : titlevector_magnitude.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AltVectorState other = (AltVectorState) obj;
		if (acronymvector == null) {
			if (other.acronymvector != null)
				return false;
		} else if (!acronymvector.equals(other.acronymvector))
			return false;
		if (acronymvector_audit == null) {
			if (other.acronymvector_audit != null)
				return false;
		} else if (!acronymvector_audit.equals(other.acronymvector_audit))
			return false;
		if (acronymvector_magnitude == null) {
			if (other.acronymvector_magnitude != null)
				return false;
		} else if (!acronymvector_magnitude.equals(other.acronymvector_magnitude))
			return false;
		if (docvector == null) {
			if (other.docvector != null)
				return false;
		} else if (!docvector.equals(other.docvector))
			return false;
		if (docvector_magnitude == null) {
			if (other.docvector_magnitude != null)
				return false;
		} else if (!docvector_magnitude.equals(other.docvector_magnitude))
			return false;
		if (keywordvector == null) {
			if (other.keywordvector != null)
				return false;
		} else if (!keywordvector.equals(other.keywordvector))
			return false;
		if (keywordvector_audit == null) {
			if (other.keywordvector_audit != null)
				return false;
		} else if (!keywordvector_audit.equals(other.keywordvector_audit))
			return false;
		if (keywordvector_magnitude == null) {
			if (other.keywordvector_magnitude != null)
				return false;
		} else if (!keywordvector_magnitude.equals(other.keywordvector_magnitude))
			return false;
		if (most_relevant_skill_family == null) {
			if (other.most_relevant_skill_family != null)
				return false;
		} else if (!most_relevant_skill_family.equals(other.most_relevant_skill_family))
			return false;
		if (sf_domain == null) {
			if (other.sf_domain != null)
				return false;
		} else if (!sf_domain.equals(other.sf_domain))
			return false;
		if (sf_domain_audit == null) {
			if (other.sf_domain_audit != null)
				return false;
		} else if (!sf_domain_audit.equals(other.sf_domain_audit))
			return false;
		if (sf_domain_confidence == null) {
			if (other.sf_domain_confidence != null)
				return false;
		} else if (!sf_domain_confidence.equals(other.sf_domain_confidence))
			return false;
		if (sf_vector == null) {
			if (other.sf_vector != null)
				return false;
		} else if (!sf_vector.equals(other.sf_vector))
			return false;
		if (sf_vector_magnitude == null) {
			if (other.sf_vector_magnitude != null)
				return false;
		} else if (!sf_vector_magnitude.equals(other.sf_vector_magnitude))
			return false;
		if (skillsvector == null) {
			if (other.skillsvector != null)
				return false;
		} else if (!skillsvector.equals(other.skillsvector))
			return false;
		if (skillsvector_audit == null) {
			if (other.skillsvector_audit != null)
				return false;
		} else if (!skillsvector_audit.equals(other.skillsvector_audit))
			return false;
		if (skillsvector_magnitude == null) {
			if (other.skillsvector_magnitude != null)
				return false;
		} else if (!skillsvector_magnitude.equals(other.skillsvector_magnitude))
			return false;
		if (titlevector == null) {
			if (other.titlevector != null)
				return false;
		} else if (!titlevector.equals(other.titlevector))
			return false;
		if (titlevector_audit == null) {
			if (other.titlevector_audit != null)
				return false;
		} else if (!titlevector_audit.equals(other.titlevector_audit))
			return false;
		if (titlevector_magnitude == null) {
			if (other.titlevector_magnitude != null)
				return false;
		} else if (!titlevector_magnitude.equals(other.titlevector_magnitude))
			return false;
		return true;
	}
    
    
}
