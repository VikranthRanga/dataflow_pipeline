package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Objects;


/**
 * TagDefinitionRecordRaw
 * Represents data from spanner 
 * 
 * @author mannrivera
 *
 */
public class TagDefinitionRecordRaw implements Serializable{
    private static final long serialVersionUID = 367188535831026127L;
    
    public String tagDefinitionId;
    public String tagName;
    public String createdById;
    public String name;
    public String peopleSoftId;
    
    /**
     * Default Constructor
     */
    public TagDefinitionRecordRaw() {
        
    }
    
    /**
     * Default Constructor with Parameters 
     * 
     * @param tagDefinitionId
     * @param tagName
     * @param createdById
     * @param name
     * @param peopleSoftId
     */
    public TagDefinitionRecordRaw(String tagDefinitionId, String tagName, String createdById, String name,
            String peopleSoftId) {
        super();
        this.tagDefinitionId = tagDefinitionId;
        this.tagName = tagName;
        this.createdById = createdById;
        this.name = name;
        this.peopleSoftId = peopleSoftId;
    }

	@Override
	public int hashCode() {
		return Objects.hash(createdById, name, peopleSoftId, tagDefinitionId, tagName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagDefinitionRecordRaw other = (TagDefinitionRecordRaw) obj;
		return Objects.equals(createdById, other.createdById) && Objects.equals(name, other.name)
				&& Objects.equals(peopleSoftId, other.peopleSoftId)
				&& Objects.equals(tagDefinitionId, other.tagDefinitionId) && Objects.equals(tagName, other.tagName);
	}

	@Override
	public String toString() {
		return "TagDefinitionRecordRaw [" + (tagDefinitionId != null ? "tagDefinitionId=" + tagDefinitionId + ", " : "")
				+ (tagName != null ? "tagName=" + tagName + ", " : "")
				+ (createdById != null ? "createdById=" + createdById + ", " : "")
				+ (name != null ? "name=" + name + ", " : "")
				+ (peopleSoftId != null ? "peopleSoftId=" + peopleSoftId : "") + "]";
	}
    
}