package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.google.gson.JsonArray;


/**
 * Place metadata and other temporary field data here
 */
public class MetaData implements Serializable {
	
	private static final long serialVersionUID = -3921747039094170527L;
	public ResumeProfile resumeProfile;
	
    public String data_quality_record_flag;
    public String data_quality_resume_flag;

    public String jobcode_hash;
    public String jobcode_last_modified_date;
    public Boolean jobcode_needsUpdate = false;
    
    public String industry_code_resp;
    public String industrycode_hash;
    public Map<String,String> normCompanyMap = new HashMap<>();
    public String industrycode_last_modified_date;
    public Boolean industrycode_needsUpdate = false;
    
    public ProvenTrackRecordCounters provenTrackRecordCounters = new ProvenTrackRecordCounters();
    public List<ProvenTrackRecordWorkInfo> provenTrackRecordWorkInfo = new ArrayList<ProvenTrackRecordWorkInfo>();
    public List<ProvenTrackRecordActivityInfo> provenTrackRecordActivityInfo = new ArrayList<ProvenTrackRecordActivityInfo>();
    public String proventrackrecord_output;
    
    public String proventrackrecord_hash;
    public String proventrackrecord_last_modified_date;
    public Boolean proventrackrecord_needsUpdate = false;

    public List<G2Comment> g2Comments = new ArrayList<>();

    public Boolean estimatedPayrate_needsUpdate = false;
    
    @Override
    public int hashCode() {
        return Objects.hash(data_quality_record_flag, data_quality_resume_flag, estimatedPayrate_needsUpdate, industrycode_hash, industrycode_last_modified_date, industrycode_needsUpdate,  
        		jobcode_hash, jobcode_last_modified_date, jobcode_needsUpdate, resumeProfile, provenTrackRecordCounters, provenTrackRecordWorkInfo, provenTrackRecordActivityInfo,
        		proventrackrecord_output, proventrackrecord_hash, proventrackrecord_last_modified_date, proventrackrecord_needsUpdate,
        		g2Comments,industry_code_resp, normCompanyMap);
    }

    @Override
    public boolean equals( Object obj ) {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        MetaData other = (MetaData) obj;
        return Objects.equals(data_quality_record_flag, other.data_quality_record_flag) && Objects.equals(data_quality_resume_flag, other.data_quality_resume_flag)
                && Objects.equals(estimatedPayrate_needsUpdate, other.estimatedPayrate_needsUpdate) && Objects.equals(industrycode_hash, other.industrycode_hash)
                && Objects.equals(industrycode_last_modified_date, other.industrycode_last_modified_date) && Objects.equals(industrycode_needsUpdate, other.industrycode_needsUpdate)  
                && Objects.equals(jobcode_hash, other.jobcode_hash)
                && Objects.equals(jobcode_last_modified_date, other.jobcode_last_modified_date) && Objects.equals(jobcode_needsUpdate, other.jobcode_needsUpdate)
                && Objects.equals(resumeProfile, other.resumeProfile)
                && Objects.equals(provenTrackRecordCounters, other.provenTrackRecordCounters) && Objects.equals(provenTrackRecordWorkInfo, other.provenTrackRecordWorkInfo)
                && Objects.equals(provenTrackRecordActivityInfo, other.provenTrackRecordActivityInfo) && Objects.equals(proventrackrecord_hash, other.proventrackrecord_hash)
                && Objects.equals(proventrackrecord_last_modified_date, other.proventrackrecord_last_modified_date) && Objects.equals(proventrackrecord_needsUpdate, other.proventrackrecord_needsUpdate)
                && Objects.equals(proventrackrecord_output, other.proventrackrecord_output)  && Objects.equals(industry_code_resp, other.industry_code_resp)
        		&& Objects.equals(g2Comments, other.g2Comments) && Objects.equals(normCompanyMap, other.normCompanyMap);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MetaData [resumeProfile=").append(resumeProfile).append(", data_quality_record_flag=").append(data_quality_record_flag).append(", data_quality_resume_flag=").append(data_quality_resume_flag).append(", jobcode_hash=")
                .append(jobcode_hash).append(", jobcode_last_modified_date=").append(jobcode_last_modified_date).append(", jobcode_needsUpdate=").append(jobcode_needsUpdate)
                .append(", industrycode_hash=").append(industrycode_hash)
                .append(", industrycode_last_modified_date=").append(industrycode_last_modified_date).append(", industrycode_needsUpdate=").append(industrycode_needsUpdate).append(", estimatedPayrate_needsUpdate=")
                .append(estimatedPayrate_needsUpdate).append(", provenTrackRecordCounters=").append(provenTrackRecordCounters).append(", provenTrackRecordWorkInfo=").append(provenTrackRecordWorkInfo).append(", provenTrackRecordActivityInfo=")
                .append(provenTrackRecordActivityInfo).append(", proventrackrecord_hash=").append(proventrackrecord_hash).append(", proventrackrecord_last_modified_date=").append(proventrackrecord_last_modified_date)
                .append(", proventrackrecord_needsUpdate=").append(proventrackrecord_needsUpdate).append(", proventrackrecord_output=").append(proventrackrecord_output)
                .append(", eagernessComments=").append(g2Comments).append(", industry_code_resp=").append(industry_code_resp)
        		.append(", normCompanyMap=").append(normCompanyMap);
        return builder.toString();
    }
    
   
    
    
    
    
}