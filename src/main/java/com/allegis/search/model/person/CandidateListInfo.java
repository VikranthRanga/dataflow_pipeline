package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Objects;


/**
 * This Object represents CandidateList
 * 
 * Example Json:
 * "candidate_list": {
        "ListInfo": [
            {
                "UserName": "Devika ",
                "UserId": "05711124",
                "ListName": "Transferred from A's network",
                "SfdcUserId": "abc123def456ghi"
            },
            {
                "UserName": "Devika ",
                "UserId": "05711124",
                "ListName": "Category Manager",
                "SfdcUserId": "abc123def456ghi"
            }
        ]
    },
                    
 * @author mannrivera
 *
 */
public class CandidateListInfo  implements Serializable{
    private static final long serialVersionUID = -3835945489733187433L;
       
    public String UserName;
    public String UserId;
    public String ListName;
    public String SfdcUserId;
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CandidateListInfo [UserName=").append(UserName).append(", UserId=").append(UserId).append(", ListName=").append(ListName).append(", SfdcUserId=").append(SfdcUserId).append("]");
        return builder.toString();
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(ListName, SfdcUserId, UserId, UserName);
    }
    
    @Override
    public boolean equals( Object obj ) {
        if( this == obj )
            return true;
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        CandidateListInfo other = (CandidateListInfo) obj;
        return Objects.equals(ListName, other.ListName) && Objects.equals(SfdcUserId, other.SfdcUserId) && Objects.equals(UserId, other.UserId) && Objects.equals(UserName, other.UserName);
    }
    
    
    
}