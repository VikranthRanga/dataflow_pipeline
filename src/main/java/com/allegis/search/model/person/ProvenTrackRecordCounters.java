package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * A POJO to hold counters of Proven Track Record data points 
 * 
 * @author jperecha
 *
 */
public class ProvenTrackRecordCounters implements Serializable {

	private static final long serialVersionUID = -2423177838021696901L;

	public int posFinishReasonCounter;
	public int negFinishReasonCounter;
	public int pfCounter;
	public int rrPositiveCounter;
	public int rrNegativeCounter;
	public int rcCounter;
	public SortedSet<Date> lastActivityDate = new TreeSet<>(Collections.reverseOrder());
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lastActivityDate == null) ? 0 : lastActivityDate.hashCode());
		result = prime * result + negFinishReasonCounter;
		result = prime * result + pfCounter;
		result = prime * result + posFinishReasonCounter;
		result = prime * result + rcCounter;
		result = prime * result + rrNegativeCounter;
		result = prime * result + rrPositiveCounter;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvenTrackRecordCounters other = (ProvenTrackRecordCounters) obj;
		if (lastActivityDate == null) {
			if (other.lastActivityDate != null)
				return false;
		} else if (!lastActivityDate.equals(other.lastActivityDate))
			return false;
		if (negFinishReasonCounter != other.negFinishReasonCounter)
			return false;
		if (pfCounter != other.pfCounter)
			return false;
		if (posFinishReasonCounter != other.posFinishReasonCounter)
			return false;
		if (rcCounter != other.rcCounter)
			return false;
		if (rrNegativeCounter != other.rrNegativeCounter)
			return false;
		if (rrPositiveCounter != other.rrPositiveCounter)
			return false;
		return true;
	}
	
	
	
}
