package com.allegis.search.model.person;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.candidate.profile.AbstractProfileSource;
import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.candidate.profile.HistoryRange;
import com.allegis.candidate.profile.ProfileBean;
import com.allegis.candidate.profile.ProfileSource;
import com.allegis.search.enums.DocType;
import com.allegis.search.enums.MetricsNamespace;
import com.allegis.search.io.ElasticsearchIO;
import com.allegis.search.model.MatchDoc;
import com.allegis.search.model.serializer.AssociatedPositionOpeningSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * An object to represent Person specific properties. Generally, the class member variables
 * should have a one-to-one alignment with the Person Doc within our Elasticsearch 'person' index.
 * 
 * Place class members that map to the fields within Elasticsearch
 * 
 * NOTE:  intentionally choose to use nulls over empty arrays
 * 
 * If you have meta-data or temporary field data that you need to carry with the element
 * add it to the {@link IngestionDoc}. If you have a property that is used by both the
 * {@see JobDoc} and {@see PersonDoc}, then add it to the {@see CommonDoc}
 */
public class PersonDoc extends MatchDoc implements ProfileSource {
	
	private static final long serialVersionUID = -8232104077092262302L;
	
	
    public String getCandidate_id() { // keyword
    	return super.id;
    }
    public void setCandidate_id( String id ) {
        super.id = id;
    }
    
    public String getTalent_id() {
        return super.visibleNumber;
    }
    public void setTalent_id( String id) {
        super.visibleNumber = id;
    }

    public String peoplesoft_id; // keyword
    public String source_system_id; // text
    public boolean do_not_call = false;
    public boolean do_not_recruit = false;
    public String skills_comments; // keyword
    
    @JsonProperty // force skills to be JSONified since there is an ambiguity with getSkills() 
    public List<String> skills;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    public Date person_availability;

    public Date qualifications_last_activity_date;
    public Date qualifications_last_contact_date; 
    public Date getTalent_created_date() {
    	return super.createdDate;
    }
    public void setTalent_created_date(Date date) {
    	super.createdDate = date;
    }

    public String lang; // keyword
    public List<String> languages_spoken; // text
    

    public List<String> communities_skills; // text(analyzer:keyword)
    public List<String> communities_job_title; // text(analyzer:keyword)
    public Date communities_profile_last_modified_date;
    
    
    public String getCurrent_employer_name() { // keyword
    	return super.companyName;
    }
    public void setCurrent_employer_name(String name) { 
    	super.companyName = name;
    }

    public List<String> placement_type; // keyword
    public Double total_desired_compensation;
    public Double min_desired_payrate;
    public Double max_desired_payrate;
    public String getCandidate_status() {// text Ex) Former, Candidate
    	return super.status;
    }
    public void setCandidate_status(String status) {
    	super.status = status;
    }    
    public Boolean national_op = false; 
    public String security_clearance; // keyword

    /*
     * contact_tag__c table
     */
    public List<CandidateListInfo> candidate_list;

    /*
     * From Contact table (22 fields)
     */
    public String contact_id; // keyword
    public String family_name; // keyword
    public String given_name; // keyword
    public String full_name; // text: generated
    public Boolean is_full_name_modified = false;  
    
    public String getAddress_line() { // text
    	return super.streetAddress;
    }    
    public void setAddress_line(String line) {
    	super.streetAddress = line;
    }
    public String rollup; // keyword
    public List<String> data_quality_flags; // keyword (List)
    public String communication_preferred_email; // keyword
    public String communication_preferred_phone; // keyword
    public String communication_work_email; // keyword
    public String communication_other_email; // keyword
    public String communication_email; // keyword
    public String communication_mobile_phone; // keyword
    public String communication_work_phone; // keyword
    public String communication_other_phone; // keyword
    public String communication_home_phone; // keyword (shows as null in index)
    public List<String> homephoneformats; // keyword: generated derived from `communication_home_phone`
    public List<String> mobilephoneformats; // keyword: generated derived from `communication_mobile_phone`
    public List<String> otherphoneformats; // keyword: generated derived from `communication_other_phone`
    // Should there be workphoneformats? it is not in the index mapping
    // Aspire - if communication_*_phone is null, then *phoneformats was not in the index document

    public Boolean ownership = false;

    /*
     * Resume Generated (8 Fields)
     * Resume sourced  (talent_document__c, HTML & Sovren Resumes)
     */
    // Resume_N = object(body, first_name, last_name, html, link, is_full_name_blacklisted, modified, size, type)
    public Resume_N resume_1;
    public Integer resume_count; 
    
    public Date qualifications_last_resume_modified_date; // date
    public String teaser; // keyword(not indexed)
    public Integer sov_months_experience;
    public String taxonomy; // keyword
    
    @JsonInclude(JsonInclude.Include.NON_NULL)  // The custom plugin parser for this data type will fail if null.
    public List<Long> blockhash_dense_vector; 
    public List<Long> blockhash_lsh;   

    /*
     * Derived from vectors
     */
    public List<String> top_skills; // text(analyzer:keyword)
    public List<String> top_titles; // text(analyzer:keyword)
    public String most_relevant_job_title; // keyword
    public String getEmployment_position_title() {
    	return super.title;
    }
    public void setEmployment_position_title(String title) {
    	super.title = title;
    }

    /*
     * Sourced from Talent_Experience__c (type=Work) & talent_work_history__c
	*
	* employment_history will NOT be emitted by the ingestion-dataflow.  The leaner employ_hist 
	* will replace its functionality - which was to only "copyto" (via mapping) other searchable fields.
	* live along side (and eventually replace the employment_history
	* Aspire real-time will continue to emit employment_history until real-time moves to dataflow.  They can coexists for now.
     */
    public List<Employment> employ_hist;
    public String most_recent_finish_reason = "UNKNOWN"; // keyword
    public String payrate_currency = "USD"; // keyword
    
    public Float actual_payrate; 
    public String getPayrate_interval() {
    	return super.payrateInterval;
    }
    public void setPayrate_interval(String interval) {
    	super.payrateInterval = interval;
    }
    public String payrate_title; // text

    /*
      * Eagerness
      */
    public Long eagerness_availability_days_since_epoch;
	public Long eagerness_placement_end_days_since_epoch;
    public Long eagerness_recent_application_days_since_epoch;
    public Long eagerness_resume_modified_days_since_epoch;
    public Long eagerness_started_days_since_epoch;
    public Long eagerness_submittal_days_since_epoch;
    public Long eagerness_tc_job_inquiry_days_since_epoch = 0L;
    public Long eagerness_tc_end_dt_review_days_since_epoch = 0L;
    public Long eagerness_comment_days_since_epoch = 0L;
    
    public Long deboost_recent_submital_days_since_epoch = 0L;
    
    public Date tc_job_inquiry_date;
    public Date tc_placement_end_review_date;
    public Date g2_comment_eagerness_date;
    
    public Integer number_of_recent_applications;
    public Date recent_application_date;
    public List<String> recent_applications; // keyword (List)

    /*
     * These need to be typed/ordered/group correctly
     * Not a complete list yet
     */
    @JsonIgnore
    public Float estimated_payrate; // TODO: consider for removal
    public Integer rate_category;
    public Date placement_end_date;

    
    // Sourced from Event, Order, Opportunity, job_title__c, and user
    @JsonSerialize(using = AssociatedPositionOpeningSerializer.class)
    public AssociatedPositionOpening associated_position_openings; // nested should be an object
    
    /** historical relevant associations via: account->contact->order-opportunity */
    public List<EmploymentBean> associated_jobs;    

    public Set<String> company_names = new HashSet<>();

    // Sourced from Talent_Experience__c  (type=Training or Education)
    public List<EduHist> edu_hist;
    
    public List<CertHist> cert_hist;
    
    public List<JobCodeOutput> job_functions;
    public List<IndustryCodeOutput> industry_code;
    public Set<String> norm_company_names;

    public List<RecruiterRelationshipOutput> recruiter_relationships;
        
    public Float proven_track_record_score;
    public String proven_track_record_label;
    public String proven_track_record_explain;

    // Sourced from Event 
    public List<SubmitHist> submit_hist; 
    public Date recent_submittal_date;
    public String submittal_status; // keyword

    // Sourced from both Event and Task 
    public Set<String> interactions; // keyword (set)
    public Date recruiter_last_activity_date;  // or task table
    public boolean recruiter_activity = false; 
   
    // Sourced from pipeline__c
    public Set<String> pipelines; // holds the "peopleSoft" ids
    public Set<String> pipelines_sfdcUserIds; // holds the sfdc user ids of the equivelant pipelines 

    public int docvector_length;   
    public Integer pre_associated_jobs_docvector_length;   
	public boolean associated_jobs_geolocation;
    
    /** holds the original vector state prior to */
    public AltVectorState pre_associated_jobs_vector_state;
    
    // Block JSON emission from this object
    @JsonIgnore
    public MetaData meta = new MetaData();
        
    /**
     * Constructor required by {@link PercolationAppPipeline}
     */
    public PersonDoc() {
        super(null);
        this.docType = DocType.PERSON;
    }
    
    /**
     * Constructor
     * @param id            the id of the talent 
     */
    public PersonDoc(String id) {
		super(id);
		this.docType = DocType.PERSON;
	}
        
    /**
     * Constructor
     * @param id        the id of the talent
     * @param feederId  the feeder id
     */
    public PersonDoc(String id, String feederId) {
		super(id, feederId);
		this.docType = DocType.PERSON;
	}
    
    /**
     * Constructor
     * @param id        the id of the talent
     * @param feederId  the feeder id
     * @param isQaData  if true, this doc will be flagged as QA data
     */
    public PersonDoc(String id, String feederId, boolean isQaData ) {
        super(id, feederId, isQaData);
        this.docType = DocType.PERSON;
    }
        
    
    /**
     * Method used by ElasticsearctIO.Write to extract the unique SalesForce ID to the ES document s_id.
     */
    public static class DocIdExtractFn implements ElasticsearchIO.Write.FieldValueExtractFn {
        private static final long serialVersionUID = -1;
        private static Counter missing= Metrics.counter(MetricsNamespace.STAGE.name(), "Missing candidate_id");

        @Override
        public String apply(JsonNode personJson) {
            JsonNode personIdNode = personJson.findValue( DocType.PERSON.getIdField() );
            if (personIdNode == null || personIdNode.isMissingNode() || personIdNode.isNull()) {
                missing.inc();
                return null;
            }
            return personIdNode.asText();
        }
    }    

    
	/**
     * Uses to implement ProfileSource for creating doc vectors and blockhash
     */
    @Override
    @Transient
    @JsonIgnore
    public List<EmploymentBean> getEmploymentHistory() {
        List<EmploymentBean> employmentHistory = new ArrayList<>();
        if (employ_hist == null)
            return employmentHistory;

        for (Employment employment : employ_hist) {
            EmploymentBean employmentBean = new EmploymentBean();
            employmentBean.setEmploymentOrgName(employment.OrganizationName);
            employmentBean.setPositionTitle(employment.PositionTitle);
            employmentBean.setPositionDescription(employment.Description);

            if (employment.StartDate != null)  {
                employmentBean.setHistoryRange(new HistoryRange(employment.StartDate, employment.EndDate));
            }
            employmentHistory.add(employmentBean);
        }
        return employmentHistory;
    }

    
    /**
     * Uses to implement ProfileSource for creating doc vectors and blockhash
     */
    @Transient
    @Override
    @JsonIgnore
    public String getCommunitiesSkills() {
        if (communities_skills == null)
            return "";

        StringBuilder commSkillsBuilder = new StringBuilder();
        for (String competencyName : communities_skills) {
        	commSkillsBuilder.append(competencyName).append(AbstractProfileSource.SKILL_VALUE_DELIMITER);
        }
        if (commSkillsBuilder.length() > 0) {
        	commSkillsBuilder.setLength(commSkillsBuilder.length() - 1);
        }
        return commSkillsBuilder.toString();
    }
    
    /**
     * Uses to implement ProfileSource for creating doc vectors and blockhash
     */
    @Transient
    @Override
    @JsonIgnore
    public String getCommunitiesJobTitles() {
        if (communities_job_title == null)
            return "";

        StringBuilder commJobTitleBuilder = new StringBuilder();
        for (String competencyName : communities_job_title) {
        	commJobTitleBuilder.append(competencyName).append(AbstractProfileSource.SKILL_VALUE_DELIMITER);
        }
        if (commJobTitleBuilder.length() > 0) {
        	commJobTitleBuilder.setLength(commJobTitleBuilder.length() - 1);
        }
        return commJobTitleBuilder.toString();
    }
    
    /**
     * Uses to implement ProfileSource for creating doc vectors
     */
    @Transient
    @Override
    @JsonIgnore
    public Date getCommunitiesProfileLastModifiedDate() {
        if (communities_profile_last_modified_date == null)
            return null;
        else
        	return communities_profile_last_modified_date;
    }



    /**
     * Uses to implement ProfileSource for creating doc vectors and blockhash
     */
    @Override
    @Transient
    @JsonIgnore
    public SourceType getSourceType() {
        return SourceType.G2;
    }

    /**
     * Uses to implement ProfileSource for creating doc vectors and blockhash
     */
    @Override
    @Transient
    @JsonIgnore
    public ProfileBean getProfile() {
        ProfileBean profileBean = new ProfileBean();
        profileBean.setEmploymentPositionTitle(super.title);
        profileBean.setCurrentEmployerName(super.companyName);
        profileBean.setSkillsComments(skills_comments);
        profileBean.setLastUpdate(generatedtimestamp);
        return profileBean;
    }


    /**
     * Uses to implement ProfileSource for creating doc vectors and blockhash
     */
    @Transient
    @Override
    @JsonIgnore
    public String getSkills() {
        if (skills == null)
            return "";

        StringBuilder builder = new StringBuilder();
        for (String competencyName : skills) {
            builder.append(competencyName).append(AbstractProfileSource.SKILL_VALUE_DELIMITER);
        }
        if (builder.length() > 0) {
            builder.setLength(builder.length() - 1);
        }
        return builder.toString();
    }



	// read https://www.artima.com/lejava/articles/equality.html
	@Override
	public boolean canEqual(Object other) {
        return (other instanceof PersonDoc);
    }
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects
                .hash(actual_payrate, associated_jobs, associated_jobs_geolocation, associated_position_openings, blockhash_dense_vector, blockhash_lsh, candidate_list, cert_hist, communication_email, communication_home_phone, communication_mobile_phone, communication_other_email, communication_other_phone, communication_preferred_email, communication_preferred_phone, communication_work_email, communication_work_phone, communities_job_title, communities_profile_last_modified_date, communities_skills, company_names, contact_id, data_quality_flags, deboost_recent_submital_days_since_epoch, do_not_call, do_not_recruit, docvector_length, eagerness_availability_days_since_epoch, eagerness_comment_days_since_epoch, eagerness_placement_end_days_since_epoch, eagerness_recent_application_days_since_epoch, eagerness_resume_modified_days_since_epoch, eagerness_started_days_since_epoch, eagerness_submittal_days_since_epoch, eagerness_tc_end_dt_review_days_since_epoch, eagerness_tc_job_inquiry_days_since_epoch, edu_hist, employ_hist, estimated_payrate, family_name, full_name, g2_comment_eagerness_date, given_name, homephoneformats, industry_code, interactions, is_full_name_modified, job_functions, lang, languages_spoken, max_desired_payrate, meta, min_desired_payrate, mobilephoneformats, most_recent_finish_reason, most_relevant_job_title, national_op, norm_company_names, number_of_recent_applications, otherphoneformats, ownership, payrate_currency, payrate_title, peoplesoft_id, person_availability, pipelines, pipelines_sfdcUserIds, placement_end_date, placement_type, pre_associated_jobs_docvector_length, pre_associated_jobs_vector_state, proven_track_record_explain, proven_track_record_label, proven_track_record_score, qualifications_last_activity_date, qualifications_last_contact_date, qualifications_last_resume_modified_date, rate_category, recent_application_date, recent_applications, recent_submittal_date, recruiter_activity, recruiter_last_activity_date, recruiter_relationships, resume_1, resume_count, rollup, security_clearance, skills, skills_comments, source_system_id, sov_months_experience, submit_hist, submittal_status, taxonomy, tc_job_inquiry_date, tc_placement_end_review_date, teaser, top_skills, top_titles, total_desired_compensation);
        return result;
    }
    @Override
    public boolean equals( Object obj ) {
        if( this == obj )
            return true;
        if( !super.equals(obj) )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        PersonDoc other = (PersonDoc) obj;
        return Objects.equals(actual_payrate, other.actual_payrate) && Objects.equals(associated_jobs, other.associated_jobs) && associated_jobs_geolocation == other.associated_jobs_geolocation
                && Objects.equals(associated_position_openings, other.associated_position_openings) && Objects.equals(blockhash_dense_vector, other.blockhash_dense_vector) && Objects.equals(blockhash_lsh, other.blockhash_lsh)
                && Objects.equals(candidate_list, other.candidate_list) && Objects.equals(cert_hist, other.cert_hist) && Objects.equals(communication_email, other.communication_email)
                && Objects.equals(communication_home_phone, other.communication_home_phone) && Objects.equals(communication_mobile_phone, other.communication_mobile_phone)
                && Objects.equals(communication_other_email, other.communication_other_email) && Objects.equals(communication_other_phone, other.communication_other_phone)
                && Objects.equals(communication_preferred_email, other.communication_preferred_email) && Objects.equals(communication_preferred_phone, other.communication_preferred_phone)
                && Objects.equals(communication_work_email, other.communication_work_email) && Objects.equals(communication_work_phone, other.communication_work_phone) && Objects.equals(communities_job_title, other.communities_job_title)
                && Objects.equals(communities_profile_last_modified_date, other.communities_profile_last_modified_date) && Objects.equals(communities_skills, other.communities_skills) && Objects.equals(company_names, other.company_names)
                && Objects.equals(contact_id, other.contact_id) && Objects.equals(data_quality_flags, other.data_quality_flags) && Objects.equals(deboost_recent_submital_days_since_epoch, other.deboost_recent_submital_days_since_epoch)
                && do_not_call == other.do_not_call && do_not_recruit == other.do_not_recruit && docvector_length == other.docvector_length
                && Objects.equals(eagerness_availability_days_since_epoch, other.eagerness_availability_days_since_epoch) && Objects.equals(eagerness_comment_days_since_epoch, other.eagerness_comment_days_since_epoch)
                && Objects.equals(eagerness_placement_end_days_since_epoch, other.eagerness_placement_end_days_since_epoch)
                && Objects.equals(eagerness_recent_application_days_since_epoch, other.eagerness_recent_application_days_since_epoch)
                && Objects.equals(eagerness_resume_modified_days_since_epoch, other.eagerness_resume_modified_days_since_epoch) && Objects.equals(eagerness_started_days_since_epoch, other.eagerness_started_days_since_epoch)
                && Objects.equals(eagerness_submittal_days_since_epoch, other.eagerness_submittal_days_since_epoch) && Objects.equals(eagerness_tc_end_dt_review_days_since_epoch, other.eagerness_tc_end_dt_review_days_since_epoch)
                && Objects.equals(eagerness_tc_job_inquiry_days_since_epoch, other.eagerness_tc_job_inquiry_days_since_epoch) && Objects.equals(edu_hist, other.edu_hist) && Objects.equals(employ_hist, other.employ_hist)
                && Objects.equals(estimated_payrate, other.estimated_payrate) && Objects.equals(family_name, other.family_name) && Objects.equals(full_name, other.full_name)
                && Objects.equals(g2_comment_eagerness_date, other.g2_comment_eagerness_date) && Objects.equals(given_name, other.given_name) && Objects.equals(homephoneformats, other.homephoneformats)
                && Objects.equals(industry_code, other.industry_code) && Objects.equals(interactions, other.interactions) && Objects.equals(is_full_name_modified, other.is_full_name_modified)
                && Objects.equals(job_functions, other.job_functions) && Objects.equals(lang, other.lang) && Objects.equals(languages_spoken, other.languages_spoken) && Objects.equals(max_desired_payrate, other.max_desired_payrate)
                && Objects.equals(meta, other.meta) && Objects.equals(min_desired_payrate, other.min_desired_payrate) && Objects.equals(mobilephoneformats, other.mobilephoneformats)
                && Objects.equals(most_recent_finish_reason, other.most_recent_finish_reason) && Objects.equals(most_relevant_job_title, other.most_relevant_job_title) && Objects.equals(national_op, other.national_op)
                && Objects.equals(norm_company_names, other.norm_company_names)
                && Objects.equals(number_of_recent_applications, other.number_of_recent_applications) && Objects.equals(otherphoneformats, other.otherphoneformats) && Objects.equals(ownership, other.ownership)
                && Objects.equals(payrate_currency, other.payrate_currency) && Objects.equals(payrate_title, other.payrate_title) && Objects.equals(peoplesoft_id, other.peoplesoft_id)
                && Objects.equals(person_availability, other.person_availability) && Objects.equals(pipelines, other.pipelines) && Objects.equals(pipelines_sfdcUserIds, other.pipelines_sfdcUserIds)
                && Objects.equals(placement_end_date, other.placement_end_date) && Objects.equals(placement_type, other.placement_type) && Objects.equals(pre_associated_jobs_docvector_length, other.pre_associated_jobs_docvector_length)
                && Objects.equals(pre_associated_jobs_vector_state, other.pre_associated_jobs_vector_state) && Objects.equals(proven_track_record_explain, other.proven_track_record_explain)
                && Objects.equals(proven_track_record_label, other.proven_track_record_label) && Objects.equals(proven_track_record_score, other.proven_track_record_score)
                && Objects.equals(qualifications_last_activity_date, other.qualifications_last_activity_date) && Objects.equals(qualifications_last_contact_date, other.qualifications_last_contact_date)
                && Objects.equals(qualifications_last_resume_modified_date, other.qualifications_last_resume_modified_date) && Objects.equals(rate_category, other.rate_category)
                && Objects.equals(recent_application_date, other.recent_application_date) && Objects.equals(recent_applications, other.recent_applications) && Objects.equals(recent_submittal_date, other.recent_submittal_date)
                && recruiter_activity == other.recruiter_activity && Objects.equals(recruiter_last_activity_date, other.recruiter_last_activity_date) && Objects.equals(recruiter_relationships, other.recruiter_relationships)
                && Objects.equals(resume_1, other.resume_1) && Objects.equals(resume_count, other.resume_count) && Objects.equals(rollup, other.rollup) && Objects.equals(security_clearance, other.security_clearance)
                && Objects.equals(skills, other.skills) && Objects.equals(skills_comments, other.skills_comments) && Objects.equals(source_system_id, other.source_system_id)
                && Objects.equals(sov_months_experience, other.sov_months_experience) && Objects.equals(submit_hist, other.submit_hist) && Objects.equals(submittal_status, other.submittal_status) && Objects.equals(taxonomy, other.taxonomy)
                && Objects.equals(tc_job_inquiry_date, other.tc_job_inquiry_date) && Objects.equals(tc_placement_end_review_date, other.tc_placement_end_review_date) && Objects.equals(teaser, other.teaser)
                && Objects.equals(top_skills, other.top_skills) && Objects.equals(top_titles, other.top_titles) && Objects.equals(total_desired_compensation, other.total_desired_compensation);
    }
    
    @Override
    public String toString() {
        final int maxLen = 10;
        StringBuilder builder = new StringBuilder();
        builder.append("PersonDoc [peoplesoft_id=").append(peoplesoft_id).append(", source_system_id=").append(source_system_id).append(", do_not_call=").append(do_not_call).append(", do_not_recruit=").append(do_not_recruit)
                .append(", skills_comments=").append(skills_comments).append(", skills=").append(skills != null ? toString(skills, maxLen) : null).append(", person_availability=").append(person_availability)
                .append(", qualifications_last_activity_date=").append(qualifications_last_activity_date).append(", qualifications_last_contact_date=").append(qualifications_last_contact_date).append(", lang=").append(lang)
                .append(", languages_spoken=").append(languages_spoken != null ? toString(languages_spoken, maxLen) : null).append(", communities_skills=").append(communities_skills != null ? toString(communities_skills, maxLen) : null)
                .append(", communities_job_title=").append(communities_job_title != null ? toString(communities_job_title, maxLen) : null).append(", communities_profile_last_modified_date=").append(communities_profile_last_modified_date)
                .append(", placement_type=").append(placement_type != null ? toString(placement_type, maxLen) : null).append(", total_desired_compensation=").append(total_desired_compensation).append(", min_desired_payrate=")
                .append(min_desired_payrate).append(", max_desired_payrate=").append(max_desired_payrate).append(", national_op=").append(national_op).append(", security_clearance=").append(security_clearance).append(", candidate_list=")
                .append(candidate_list != null ? toString(candidate_list, maxLen) : null).append(", contact_id=").append(contact_id).append(", family_name=").append(family_name).append(", given_name=").append(given_name)
                .append(", full_name=").append(full_name).append(", is_full_name_modified=").append(is_full_name_modified).append(", rollup=").append(rollup).append(", data_quality_flags=")
                .append(data_quality_flags != null ? toString(data_quality_flags, maxLen) : null).append(", communication_preferred_email=").append(communication_preferred_email).append(", communication_preferred_phone=")
                .append(communication_preferred_phone).append(", communication_work_email=").append(communication_work_email).append(", communication_other_email=").append(communication_other_email).append(", communication_email=")
                .append(communication_email).append(", communication_mobile_phone=").append(communication_mobile_phone).append(", communication_work_phone=").append(communication_work_phone).append(", communication_other_phone=")
                .append(communication_other_phone).append(", communication_home_phone=").append(communication_home_phone).append(", homephoneformats=").append(homephoneformats != null ? toString(homephoneformats, maxLen) : null)
                .append(", mobilephoneformats=").append(mobilephoneformats != null ? toString(mobilephoneformats, maxLen) : null).append(", otherphoneformats=").append(otherphoneformats != null ? toString(otherphoneformats, maxLen) : null)
                .append(", ownership=").append(ownership).append(", resume_1=").append(resume_1).append(", resume_count=").append(resume_count).append(", qualifications_last_resume_modified_date=")
                .append(qualifications_last_resume_modified_date).append(", teaser=").append(teaser).append(", sov_months_experience=").append(sov_months_experience).append(", taxonomy=").append(taxonomy).append(", blockhash_dense_vector=")
                .append(blockhash_dense_vector != null ? toString(blockhash_dense_vector, maxLen) : null).append(", blockhash_lsh=").append(blockhash_lsh != null ? toString(blockhash_lsh, maxLen) : null).append(", top_skills=")
                .append(top_skills != null ? toString(top_skills, maxLen) : null).append(", top_titles=").append(top_titles != null ? toString(top_titles, maxLen) : null).append(", most_relevant_job_title=").append(most_relevant_job_title)
                .append(", employ_hist=").append(employ_hist != null ? toString(employ_hist, maxLen) : null).append(", most_recent_finish_reason=").append(most_recent_finish_reason).append(", payrate_currency=").append(payrate_currency)
                .append(", actual_payrate=").append(actual_payrate).append(", payrate_title=").append(payrate_title).append(", eagerness_availability_days_since_epoch=").append(eagerness_availability_days_since_epoch)
                .append(", eagerness_placement_end_days_since_epoch=").append(eagerness_placement_end_days_since_epoch).append(", eagerness_recent_application_days_since_epoch=").append(eagerness_recent_application_days_since_epoch)
                .append(", eagerness_resume_modified_days_since_epoch=").append(eagerness_resume_modified_days_since_epoch).append(", eagerness_started_days_since_epoch=").append(eagerness_started_days_since_epoch)
                .append(", eagerness_submittal_days_since_epoch=").append(eagerness_submittal_days_since_epoch).append(", eagerness_tc_job_inquiry_days_since_epoch=").append(eagerness_tc_job_inquiry_days_since_epoch)
                .append(", eagerness_tc_end_dt_review_days_since_epoch=").append(eagerness_tc_end_dt_review_days_since_epoch).append(", eagerness_comment_days_since_epoch=").append(eagerness_comment_days_since_epoch)
                .append(", deboost_recent_submital_days_since_epoch=").append(deboost_recent_submital_days_since_epoch).append(", tc_job_inquiry_date=").append(tc_job_inquiry_date).append(", tc_placement_end_review_date=")
                .append(tc_placement_end_review_date).append(", g2_comment_eagerness_date=").append(g2_comment_eagerness_date).append(", number_of_recent_applications=").append(number_of_recent_applications)
                .append(", recent_application_date=").append(recent_application_date).append(", recent_applications=").append(recent_applications != null ? toString(recent_applications, maxLen) : null).append(", estimated_payrate=")
                .append(estimated_payrate).append(", rate_category=").append(rate_category).append(", placement_end_date=").append(placement_end_date).append(", associated_position_openings=").append(associated_position_openings)
                .append(", associated_jobs=").append(associated_jobs != null ? toString(associated_jobs, maxLen) : null).append(", company_names=").append(company_names != null ? toString(company_names, maxLen) : null).append(", edu_hist=")
                .append(edu_hist != null ? toString(edu_hist, maxLen) : null).append(", cert_hist=").append(cert_hist != null ? toString(cert_hist, maxLen) : null).append(", job_functions=")
                .append(job_functions != null ? toString(job_functions, maxLen) : null).append(", industry_code=").append(industry_code != null ? toString(industry_code, maxLen) : null).append(", recruiter_relationships=")
                .append(recruiter_relationships != null ? toString(recruiter_relationships, maxLen) : null).append(", proven_track_record_score=").append(proven_track_record_score).append(", proven_track_record_label=")
                .append(proven_track_record_label).append(", proven_track_record_explain=").append(proven_track_record_explain).append(", submit_hist=").append(submit_hist != null ? toString(submit_hist, maxLen) : null)
                .append(", recent_submittal_date=").append(recent_submittal_date).append(", submittal_status=").append(submittal_status).append(", interactions=").append(interactions != null ? toString(interactions, maxLen) : null)
                .append(", recruiter_last_activity_date=").append(recruiter_last_activity_date).append(", recruiter_activity=").append(recruiter_activity).append(", pipelines=").append(pipelines != null ? toString(pipelines, maxLen) : null)
                .append(", pipelines_sfdcUserIds=").append(pipelines_sfdcUserIds != null ? toString(pipelines_sfdcUserIds, maxLen) : null).append(", docvector_length=").append(docvector_length)
                .append(", pre_associated_jobs_docvector_length=").append(pre_associated_jobs_docvector_length).append(", associated_jobs_geolocation=").append(associated_jobs_geolocation).append(", pre_associated_jobs_vector_state=")
                .append(pre_associated_jobs_vector_state).append(", meta=")
				.append(", norm_company_names=").append(norm_company_names != null ? toString(norm_company_names, maxLen) : null).append(meta).append("]");
        return builder.toString();
    }
    private String toString( Collection<?> collection, int maxLen ) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        int i = 0;
        for( Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++ ){
            if( i > 0 )
                builder.append(", ");
            builder.append(iterator.next());
        }
        builder.append("]");
        return builder.toString();
    }
	
    
}
