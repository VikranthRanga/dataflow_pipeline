package com.allegis.search.model.person;

import java.io.Serializable;

/**
 * A POJO to hold job codes from job code classifier api
 *
 */
public class JobCodeOutput implements Serializable {

	private static final long serialVersionUID = 1L;
	public Double confidence;
    public String onet_code;
    public String onet_description;    

    public JobCodeOutput( Double confidence, String onetCode, String onetDescription ) {
        this.confidence = confidence;
        this.onet_code = onetCode;
        this.onet_description = onetDescription;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((confidence == null) ? 0 : confidence.hashCode());
		result = prime * result + ((onet_code == null) ? 0 : onet_code.hashCode());
		result = prime * result + ((onet_description == null) ? 0 : onet_description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobCodeOutput other = (JobCodeOutput) obj;
		if (confidence == null) {
			if (other.confidence != null)
				return false;
		} else if (!confidence.equals(other.confidence))
			return false;
		if (onet_code == null) {
			if (other.onet_code != null)
				return false;
		} else if (!onet_code.equals(other.onet_code))
			return false;
		if (onet_description == null) {
			if (other.onet_description != null)
				return false;
		} else if (!onet_description.equals(other.onet_description))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JobCodeOutput [Confidence=" + confidence + ", OnetCode=" + onet_code + ", OnetDescription="
				+ onet_description + "]";
	}
     
}
