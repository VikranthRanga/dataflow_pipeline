package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 *  Class to hold details of the Company object. 
 *  1. Source Organization name
 *  2. Stemmed Organization name
 *  3. Source type (G2 or Sovren)
 *  4. Split 
 *  5. Start Date
 *  6. End Date
 *  
 * @author cmahajan
 *
 *
 */
public class Company implements Serializable {

	public Company() {}


	public Company(String sourceName, String sourceType, String stemName, boolean split, Date start, Date end) {
		super();
		SourceName = sourceName;
		SourceType = sourceType;
		StemName = stemName;
		Split = split;
		Start = start;
		End = end;
	}

	private static final long serialVersionUID = 8076006748382584543L;
	public String SourceName;
    public String StemName;
    public String SourceType;
    public boolean Split;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    public Date Start;	
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    public Date End;
	@Override
	
	public int hashCode() {
		return Objects.hash(End, SourceName, SourceType, Split, Start, StemName);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		return Objects.equals(End, other.End) && Objects.equals(SourceName, other.SourceName)
				&& Objects.equals(SourceType, other.SourceType) && Split == other.Split
				&& Objects.equals(Start, other.Start) && Objects.equals(StemName, other.StemName);
	}
	
	@Override
	public String toString() {
		return "Company [" + (SourceName != null ? "SourceName=" + SourceName + ", " : "")
				+ (StemName != null ? "StemName=" + StemName + ", " : "")
				+ (SourceType != null ? "SourceType=" + SourceType + ", " : "") + "Split=" + Split + ", "
				+ (Start != null ? "Start=" + Start + ", " : "")
				+ (End != null ? "End=" + End : "") + "]";
	}

}