package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author dhartwig
 */
public class AssociatedPositionOpening implements Serializable {

	private static final long serialVersionUID = -1280375016047466146L;
	/** Order ID is stored in whatId of the events table. */
    @JsonIgnore
    public String whatId;
    @JsonIgnore
    public String opportunityId;
	public String PositionOpeningID;
	public String StaffingOrderID;
	public String PositionTitle;
	public String Status;
	public String ClientName;
	public String SubmittedById;
	public String SubmittedByName;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	public Date ActivityDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	public Date SubmittedDate;

	public Double MaxPayRate;
	public String NotProceedingReason;
	@Override
	public int hashCode() {
		return Objects.hash(ClientName, MaxPayRate, NotProceedingReason, PositionOpeningID, PositionTitle,
				StaffingOrderID, Status, SubmittedById, SubmittedByName, SubmittedDate, opportunityId, ActivityDate,
				whatId);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssociatedPositionOpening other = (AssociatedPositionOpening) obj;
		return Objects.equals(ClientName, other.ClientName)
				&& Objects.equals(MaxPayRate, other.MaxPayRate)
				&& Objects.equals(NotProceedingReason, other.NotProceedingReason)
				&& Objects.equals(PositionOpeningID, other.PositionOpeningID)
				&& Objects.equals(PositionTitle, other.PositionTitle)
				&& Objects.equals(StaffingOrderID, other.StaffingOrderID) 
				&& Objects.equals(Status, other.Status)
				&& Objects.equals(SubmittedById, other.SubmittedById)
				&& Objects.equals(SubmittedByName, other.SubmittedByName)
				&& Objects.equals(SubmittedDate, other.SubmittedDate)
				&& Objects.equals(opportunityId, other.opportunityId) 
				&& Objects.equals(ActivityDate, other.ActivityDate)
				&& Objects.equals(whatId, other.whatId);
	}
	@Override
	public String toString() {
		return "AssociatedPositionOpening [" + (whatId != null ? "whatId=" + whatId + ", " : "")
				+ (opportunityId != null ? "opportunityId=" + opportunityId + ", " : "")
				+ (PositionOpeningID != null ? "PositionOpeningID=" + PositionOpeningID + ", " : "")
				+ (StaffingOrderID != null ? "StaffingOrderID=" + StaffingOrderID + ", " : "")
				+ (PositionTitle != null ? "PositionTitle=" + PositionTitle + ", " : "")
				+ (Status != null ? "Status=" + Status + ", " : "")
				+ (ClientName != null ? "ClientName=" + ClientName + ", " : "")
				+ (SubmittedById != null ? "SubmittedById=" + SubmittedById + ", " : "")
				+ (SubmittedByName != null ? "SubmittedByName=" + SubmittedByName + ", " : "")
				+ (ActivityDate != null ? "ActivityDate=" + ActivityDate + ", " : "")
				+ (SubmittedDate != null ? "SubmittedDate=" + SubmittedDate + ", " : "") + "MaxPayRate=" + MaxPayRate
				+ ", " + (NotProceedingReason != null ? "NotProceedingReason=" + NotProceedingReason : "") + "]";
	}


}
