package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


/**
 * Represents the collection of submit_hist for a Talent
 */
public class SubmitHist implements Serializable {
	
	private static final long serialVersionUID = -3444991258999792314L;
	

	public String SubmittedById;   
    public Date SubmittedDate; // Use the default JSON to ES timestamp
    public String Status;
    public String UserId;

    public SubmitHist() {
        super();
    }


	public SubmitHist(String submittedById, Date submittedDate, String status, String userId) {
		super();
		SubmittedById = submittedById;
		SubmittedDate = submittedDate;
		Status = status;
		UserId = userId;
	}


	@Override
	public int hashCode() {
		return Objects.hash(Status, SubmittedById, SubmittedDate, UserId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubmitHist other = (SubmitHist) obj;
		return Objects.equals(Status, other.Status) && Objects.equals(SubmittedById, other.SubmittedById)
				&& Objects.equals(SubmittedDate, other.SubmittedDate) && Objects.equals(UserId, other.UserId);
	}

	@Override
	public String toString() {
		return "SubmitHist [" + (SubmittedById != null ? "SubmittedById=" + SubmittedById + ", " : "")
				+ (SubmittedDate != null ? "SubmittedDate=" + SubmittedDate + ", " : "")
				+ (Status != null ? "Status=" + Status + ", " : "")  + (UserId != null ? "UserId=" + UserId : "") + "]";
	}
	
}
