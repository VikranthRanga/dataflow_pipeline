package com.allegis.search.model.person;

import java.io.Serializable;
import java.util.Objects;


/**
 * Represents the collection of Education History for a Talent
 */
public class EduHist implements Serializable {

	private static final long serialVersionUID = -3994399120092197734L;

	public String OrgName;
    public String DegreeType;
    public String DegreeYear;
    public String DegreeMajor;

    public EduHist(String orgName, String degreeType, String degreeYear, String degreeMajor) {
        this.OrgName = orgName;
        this.DegreeType = degreeType;
        this.DegreeYear = degreeYear;
        this.DegreeMajor = degreeMajor;
    }

	@Override
	public int hashCode() {
		return Objects.hash(DegreeMajor, DegreeType, DegreeYear, OrgName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EduHist other = (EduHist) obj;
		return Objects.equals(DegreeMajor, other.DegreeMajor) && Objects.equals(DegreeType, other.DegreeType)
				&& Objects.equals(DegreeYear, other.DegreeYear) && Objects.equals(OrgName, other.OrgName);
	}

	@Override
	public String toString() {
		return "EduHist [" + (OrgName != null ? "OrgName=" + OrgName + ", " : "")
				+ (DegreeType != null ? "DegreeType=" + DegreeType + ", " : "")
				+ (DegreeYear != null ? "DegreeYear=" + DegreeYear + ", " : "")
				+ (DegreeMajor != null ? "DegreeMajor=" + DegreeMajor : "") + "]";
	}
}
