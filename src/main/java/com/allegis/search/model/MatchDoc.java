package com.allegis.search.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.allegis.docvector.NonExtractedData;
import com.allegis.search.enums.DocType;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a set of common properties and functionality for documents that are indexed 
 * within ElasticSearch for purposes of Match
 */
public abstract class MatchDoc extends CommonDoc implements Serializable, Cloneable {

    /**
     * Default Constructor
     * @param id    the id of the matchDoc
     */
    public MatchDoc(String id) {
        super(id);
    }
    
    /**
     * Constructor
     * @param id        the id of the matchDoc
     * @param feederId  the feeder id
     */
    public MatchDoc(String id, String feederId) {
		super(id);
		super.feederId = feederId;
	}
    
    /**
     * Constructor
     * @param id        the id of the matchDoc
     * @param feederId  the feeder id
     * @param isQaData  if true, this doc will be flagged as QA data
     */
    public MatchDoc(String id, String feederId, boolean isQaData ) {
        super(id);
        super.feederId = feederId;
        super.isQaData = isQaData;
    }

    

	private static final long serialVersionUID = 2851875017762865931L; 
	
	@JsonIgnore
    public DocType docType = null;
    
    
    public String docvector; // text (list of paired strings (skill:weight) delimited by ;)
    public Double docvector_magnitude; // float

    public String titlevector; // text
    public Double titlevector_magnitude; // float
    public List<String> titlevector_audit; // keyword

    public String skillsvector; // skillsvector
    public Double skillsvector_magnitude; // float
    public List<String> skillsvector_audit; // keyword

    public String keywordvector;
    public Double keywordvector_magnitude; // float
    public List<String> keywordvector_audit; // keyword

    public String acronymvector;
    public Double acronymvector_magnitude; // float
    public List<String> acronymvector_audit; // keyword

    public String sf_domain = "UNKNOWN";
    public List<String> sf_domain_audit;
    public String sf_vector;
    public Double sf_vector_magnitude;
    public Double sf_domain_confidence = 0.0;
    public String most_relevant_skill_family; // keyword
    
    public String skillsetvector; // text (list of paired strings (skill:weight) delimited by ;)
    public Double skillsetvector_magnitude;
    
    public String most_relevant_skillset; // keyword
    
    public Boolean heavy_lifting;
    // public List<HeavyLifting.Audit> heavy_lifting_audit = new ArrayList<>();  //used for traceability during QA;  TODO remove later    
    
    public Set<ShiftPreference.Name> shift_preferences = new HashSet<>();
    public List<ShiftPreference.Audit> shift_preferences_audit = new ArrayList<>();  //used for traceability during QA;  TODO remove later    
    
    public String jobcodevector;
    public Double jobcodevector_magnitude; // float
    
    public String industryvector;
    public Double industryvector_magnitude; // float
    
    public List<NonExtractedData> nonextracted_data;
    
    // Used to be HRXML generation time
    public Date generatedtimestamp;   // Last modified in salesforce
    
    public String group_filter; // text
    public String city_name; // text
    public String country_code; // text
    public String country_sub_division_code; // text
    public String postal_code;  // keyword
    public GeoLocation geolocation; // geo_point
    @JsonIgnore // Job and Person call it differently
    public String streetAddress;
    @JsonIgnore // Job and Person call it differently
    public Date createdDate;
    @JsonIgnore // Job and Person call it differently
    public String title;
    @JsonIgnore // Job and Person call it differently
    public String status;
    @JsonIgnore // Job and Person call it differently    
    public String payrateInterval;
    @JsonIgnore // Job and Person call it differently    
    public String companyName;
    @JsonIgnore // Job and Person call it differently    
    public String visibleNumber;
    @JsonIgnore // TODO: Should really refactor MetaData so that have one instance... 
    public String percolateEventName;
    
    
	@Override
	public String toString() {
		final int maxLen = 10;
		return "MatchDoc [" + (docType != null ? "docType=" + docType + ", " : "")
				+ (docvector != null ? "docvector=" + docvector + ", " : "")
				+ (docvector_magnitude != null ? "docvector_magnitude=" + docvector_magnitude + ", " : "")
				+ (titlevector != null ? "titlevector=" + titlevector + ", " : "")
				+ (titlevector_magnitude != null ? "titlevector_magnitude=" + titlevector_magnitude + ", " : "")
				+ (skillsvector != null ? "skillsvector=" + skillsvector + ", " : "")
				+ (skillsvector_magnitude != null ? "skillsvector_magnitude=" + skillsvector_magnitude + ", " : "")
				+ (keywordvector != null ? "keywordvector=" + keywordvector + ", " : "")
				+ (keywordvector_magnitude != null ? "keywordvector_magnitude=" + keywordvector_magnitude + ", " : "")
				+ (acronymvector != null ? "acronymvector=" + acronymvector + ", " : "")
				+ (acronymvector_magnitude != null ? "acronymvector_magnitude=" + acronymvector_magnitude + ", " : "")
				+ (sf_domain != null ? "sf_domain=" + sf_domain + ", " : "")
				+ (sf_vector != null ? "sf_vector=" + sf_vector + ", " : "")
				+ (sf_vector_magnitude != null ? "sf_vector_magnitude=" + sf_vector_magnitude + ", " : "")
				+ (sf_domain_confidence != null ? "sf_domain_confidence=" + sf_domain_confidence + ", " : "")
				+ (most_relevant_skill_family != null
						? "most_relevant_skill_family=" + most_relevant_skill_family + ", "
						: "")
				+ (skillsetvector != null ? "skillsetvector=" + skillsetvector + ", " : "")
				+ (skillsetvector_magnitude != null ? "skillsetvector_magnitude=" + skillsetvector_magnitude + ", " : "")
				+ (most_relevant_skillset != null ? "most_relevant_skillset=" + most_relevant_skillset + ", " : "")
				+ (heavy_lifting != null ? "heavy_lifting=" + heavy_lifting + ", " : "")
				+ (shift_preferences != null ? "shift_preferences=" + shift_preferences + ", " : "")
				+ (jobcodevector != null ? "jobcodevector=" + jobcodevector + ", " : "")
				+ (jobcodevector_magnitude != null ? "jobcodevector_magnitude=" + jobcodevector_magnitude + ", " : "")
				+ (industryvector != null ? "industryvector=" + industryvector + ", " : "")
				+ (industryvector_magnitude != null ? "industryvector_magnitude=" + industryvector_magnitude + ", "
						: "")
				+ (nonextracted_data != null
						? "nonextracted_data="
								+ nonextracted_data.subList(0, Math.min(nonextracted_data.size(), maxLen)) + ", "
						: "")
				+ (generatedtimestamp != null ? "generatedtimestamp=" + generatedtimestamp + ", " : "")
				+ (group_filter != null ? "group_filter=" + group_filter + ", " : "")
				+ (city_name != null ? "city_name=" + city_name + ", " : "")
				+ (country_code != null ? "country_code=" + country_code + ", " : "")
				+ (country_sub_division_code != null ? "country_sub_division_code=" + country_sub_division_code + ", "
						: "")
				+ (postal_code != null ? "postal_code=" + postal_code + ", " : "")
				+ (geolocation != null ? "geolocation=" + geolocation + ", " : "")
				+ (streetAddress != null ? "streetAddress=" + streetAddress + ", " : "")
				+ (createdDate != null ? "createdDate=" + createdDate + ", " : "")
				+ (title != null ? "title=" + title + ", " : "") + (status != null ? "status=" + status + ", " : "")
				+ (payrateInterval != null ? "payrateInterval=" + payrateInterval + ", " : "")
				+ (companyName != null ? "companyName=" + companyName + ", " : "")
				+ (visibleNumber != null ? "visibleNumber=" + visibleNumber + ", " : "")
				+ (percolateEventName != null ? "percolateEventName=" + percolateEventName : "") + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((acronymvector == null) ? 0 : acronymvector.hashCode());
		result = prime * result + ((acronymvector_magnitude == null) ? 0 : acronymvector_magnitude.hashCode());
		result = prime * result + ((city_name == null) ? 0 : city_name.hashCode());
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((country_code == null) ? 0 : country_code.hashCode());
		result = prime * result + ((country_sub_division_code == null) ? 0 : country_sub_division_code.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((docType == null) ? 0 : docType.hashCode());
		result = prime * result + ((docvector == null) ? 0 : docvector.hashCode());
		result = prime * result + ((docvector_magnitude == null) ? 0 : docvector_magnitude.hashCode());
		result = prime * result + ((generatedtimestamp == null) ? 0 : generatedtimestamp.hashCode());
		result = prime * result + ((geolocation == null) ? 0 : geolocation.hashCode());
		result = prime * result + ((group_filter == null) ? 0 : group_filter.hashCode());
		result = prime * result + ((heavy_lifting == null) ? 0 : heavy_lifting.hashCode());
		result = prime * result + ((shift_preferences == null) ? 0 : shift_preferences.hashCode());
		result = prime * result + ((industryvector == null) ? 0 : industryvector.hashCode());
		result = prime * result + ((industryvector_magnitude == null) ? 0 : industryvector_magnitude.hashCode());
		result = prime * result + ((jobcodevector == null) ? 0 : jobcodevector.hashCode());
		result = prime * result + ((jobcodevector_magnitude == null) ? 0 : jobcodevector_magnitude.hashCode());
		result = prime * result + ((keywordvector == null) ? 0 : keywordvector.hashCode());
		result = prime * result + ((keywordvector_magnitude == null) ? 0 : keywordvector_magnitude.hashCode());
		result = prime * result + ((most_relevant_skill_family == null) ? 0 : most_relevant_skill_family.hashCode());
		result = prime * result + ((most_relevant_skillset == null) ? 0 : most_relevant_skillset.hashCode());
		result = prime * result + ((nonextracted_data == null) ? 0 : nonextracted_data.hashCode());
		result = prime * result + ((payrateInterval == null) ? 0 : payrateInterval.hashCode());
		result = prime * result + ((percolateEventName == null) ? 0 : percolateEventName.hashCode());
		result = prime * result + ((postal_code == null) ? 0 : postal_code.hashCode());
		result = prime * result + ((sf_domain == null) ? 0 : sf_domain.hashCode());
		result = prime * result + ((sf_domain_confidence == null) ? 0 : sf_domain_confidence.hashCode());
		result = prime * result + ((sf_vector == null) ? 0 : sf_vector.hashCode());
		result = prime * result + ((sf_vector_magnitude == null) ? 0 : sf_vector_magnitude.hashCode());
		result = prime * result + ((skillsetvector == null) ? 0 : skillsetvector.hashCode());
		result = prime * result + ((skillsetvector_magnitude == null) ? 0 : skillsetvector_magnitude.hashCode());
		result = prime * result + ((skillsvector == null) ? 0 : skillsvector.hashCode());
		result = prime * result + ((skillsvector_magnitude == null) ? 0 : skillsvector_magnitude.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((streetAddress == null) ? 0 : streetAddress.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((titlevector == null) ? 0 : titlevector.hashCode());
		result = prime * result + ((titlevector_magnitude == null) ? 0 : titlevector_magnitude.hashCode());
		result = prime * result + ((visibleNumber == null) ? 0 : visibleNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchDoc other = (MatchDoc) obj;
		if (acronymvector == null) {
			if (other.acronymvector != null)
				return false;
		} else if (!acronymvector.equals(other.acronymvector))
			return false;
		if (acronymvector_magnitude == null) {
			if (other.acronymvector_magnitude != null)
				return false;
		} else if (!acronymvector_magnitude.equals(other.acronymvector_magnitude))
			return false;
		if (city_name == null) {
			if (other.city_name != null)
				return false;
		} else if (!city_name.equals(other.city_name))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (country_code == null) {
			if (other.country_code != null)
				return false;
		} else if (!country_code.equals(other.country_code))
			return false;
		if (country_sub_division_code == null) {
			if (other.country_sub_division_code != null)
				return false;
		} else if (!country_sub_division_code.equals(other.country_sub_division_code))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (docType != other.docType)
			return false;
		if (docvector == null) {
			if (other.docvector != null)
				return false;
		} else if (!docvector.equals(other.docvector))
			return false;
		if (docvector_magnitude == null) {
			if (other.docvector_magnitude != null)
				return false;
		} else if (!docvector_magnitude.equals(other.docvector_magnitude))
			return false;
		if (generatedtimestamp == null) {
			if (other.generatedtimestamp != null)
				return false;
		} else if (!generatedtimestamp.equals(other.generatedtimestamp))
			return false;
		if (geolocation == null) {
			if (other.geolocation != null)
				return false;
		} else if (!geolocation.equals(other.geolocation))
			return false;
		if (group_filter == null) {
			if (other.group_filter != null)
				return false;
		} else if (!group_filter.equals(other.group_filter))
			return false;
		if (heavy_lifting == null) {
			if (other.heavy_lifting != null)
				return false;
		} else if (!heavy_lifting.equals(other.heavy_lifting))
			return false;
		if (shift_preferences == null) {
			if (other.shift_preferences != null)
				return false;
		} else if (!shift_preferences.equals(other.shift_preferences))
			return false;
		if (industryvector == null) {
			if (other.industryvector != null)
				return false;
		} else if (!industryvector.equals(other.industryvector))
			return false;
		if (industryvector_magnitude == null) {
			if (other.industryvector_magnitude != null)
				return false;
		} else if (!industryvector_magnitude.equals(other.industryvector_magnitude))
			return false;
		if (jobcodevector == null) {
			if (other.jobcodevector != null)
				return false;
		} else if (!jobcodevector.equals(other.jobcodevector))
			return false;
		if (jobcodevector_magnitude == null) {
			if (other.jobcodevector_magnitude != null)
				return false;
		} else if (!jobcodevector_magnitude.equals(other.jobcodevector_magnitude))
			return false;
		if (keywordvector == null) {
			if (other.keywordvector != null)
				return false;
		} else if (!keywordvector.equals(other.keywordvector))
			return false;
		if (keywordvector_magnitude == null) {
			if (other.keywordvector_magnitude != null)
				return false;
		} else if (!keywordvector_magnitude.equals(other.keywordvector_magnitude))
			return false;
		if (most_relevant_skill_family == null) {
			if (other.most_relevant_skill_family != null)
				return false;
		} else if (!most_relevant_skill_family.equals(other.most_relevant_skill_family))
			return false;
		if (most_relevant_skillset == null) {
			if (other.most_relevant_skillset != null)
				return false;
		} else if (!most_relevant_skillset.equals(other.most_relevant_skillset))
			return false;
		if (nonextracted_data == null) {
			if (other.nonextracted_data != null)
				return false;
		} else if (!nonextracted_data.equals(other.nonextracted_data))
			return false;
		if (payrateInterval == null) {
			if (other.payrateInterval != null)
				return false;
		} else if (!payrateInterval.equals(other.payrateInterval))
			return false;
		if (percolateEventName == null) {
			if (other.percolateEventName != null)
				return false;
		} else if (!percolateEventName.equals(other.percolateEventName))
			return false;
		if (postal_code == null) {
			if (other.postal_code != null)
				return false;
		} else if (!postal_code.equals(other.postal_code))
			return false;
		if (sf_domain == null) {
			if (other.sf_domain != null)
				return false;
		} else if (!sf_domain.equals(other.sf_domain))
			return false;
		if (sf_domain_confidence == null) {
			if (other.sf_domain_confidence != null)
				return false;
		} else if (!sf_domain_confidence.equals(other.sf_domain_confidence))
			return false;
		if (sf_vector == null) {
			if (other.sf_vector != null)
				return false;
		} else if (!sf_vector.equals(other.sf_vector))
			return false;
		if (sf_vector_magnitude == null) {
			if (other.sf_vector_magnitude != null)
				return false;
		} else if (!sf_vector_magnitude.equals(other.sf_vector_magnitude))
			return false;
		if (skillsetvector == null) {
			if (other.skillsetvector != null)
				return false;
		} else if (!skillsetvector.equals(other.skillsetvector))
			return false;
		if (skillsetvector_magnitude == null) {
			if (other.skillsetvector_magnitude != null)
				return false;
		} else if (!skillsetvector_magnitude.equals(other.skillsetvector_magnitude))
			return false;		
		if (skillsvector == null) {
			if (other.skillsvector != null)
				return false;
		} else if (!skillsvector.equals(other.skillsvector))
			return false;
		if (skillsvector_magnitude == null) {
			if (other.skillsvector_magnitude != null)
				return false;
		} else if (!skillsvector_magnitude.equals(other.skillsvector_magnitude))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (streetAddress == null) {
			if (other.streetAddress != null)
				return false;
		} else if (!streetAddress.equals(other.streetAddress))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (titlevector == null) {
			if (other.titlevector != null)
				return false;
		} else if (!titlevector.equals(other.titlevector))
			return false;
		if (titlevector_magnitude == null) {
			if (other.titlevector_magnitude != null)
				return false;
		} else if (!titlevector_magnitude.equals(other.titlevector_magnitude))
			return false;
		if (visibleNumber == null) {
			if (other.visibleNumber != null)
				return false;
		} else if (!visibleNumber.equals(other.visibleNumber))
			return false;
		return true;
	}
	
	
	

}
