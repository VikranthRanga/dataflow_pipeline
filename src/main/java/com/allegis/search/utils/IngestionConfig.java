package com.allegis.search.utils;

/**
 * A Utility class to read and hold properties specific to the Ingestion
 * Dataflow project
 */
public class IngestionConfig {
	// totalTalentDocs value will change as # talent docs increase in ATS-->
	public final static long totalTalentDocs = 29000000;

	// totalJobDocs value will change as # job docs increase in ATS
	public final static long totalJobDocs = 600000;

	public final static String payrate_target_docType = "payrate";
	public final static String billrate_target_docType = "billrate";
	public final static String generic_target_docType = "doc";

	// Used to identify QA submitted test messages for ingestion
	public final static String qa_feeder_id = "qa_feeder";
	public final static String qa_id_prefix = "es_";

	public final static String payrate_source_index = "person_payrate_source";
	public final static String payrate_target_index = "payrate";

	public final static String billrate_source_index = "job_billrate_source";
	public final static String billrate_target_index = "billrate";
}
