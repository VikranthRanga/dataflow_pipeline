package com.allegis.search.utils;

import java.io.Serializable;
import java.util.Arrays;

import com.allegis.search.enums.FeatureFlag;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.ReadContext;
import com.google.cloud.spanner.Struct;

/**
 * A utility to aid in working with feature flags.
 * 
 * Feature Flags are managed in the "es_ingest_feature_flags" table within Spanner. As of this current version, there
 * is 1 record per environment, keyed off of the environment identifier, with supporting columns for new features.
 * 
 * @author jalpino
 *
 */
public class FeatureFlagUtil implements Serializable {
    private static final long serialVersionUID = 3073259927735807812L;

    protected static final String TABLE = "es_ingest_feature_flags";
    private DatabaseClient spannerClient;
    private String DB_KEY;
    
    /**
     * Default Constructor
     * @param primaryKey        the primary key associated with the feature flags for this execution
     * @param spannerClient     a reference to a database client
     */
    public FeatureFlagUtil( String primaryKey, DatabaseClient spannerClient ) {
        this.spannerClient = spannerClient;
        this.DB_KEY = primaryKey;
    }
    
    /**
     * Compares the supplied value (nulls are allowed) against the feature flag value stored for the given key and returns true if the match or 
     * false if they don't. The comparison is case-insensitive. If both the supplied value and the feature flag value are 
     * null, then 'true' will be returned.
     * 
     * @param key      the key of the flag to compare
     * @param value    the value of the feature flag to compare with
     * @return         true if the feature flag value matches the supplied value, false otherwise
     */
    public Boolean matches( FeatureFlag key, String value ) {
        boolean matches = false;
        
        // Determine if this running application is allowed to percolate. Check on each bundle
        try (ReadContext readContext = this.spannerClient.singleUse()){
            SpannerKey primaryKey = SpannerKey.of( this.DB_KEY );
            Struct result = readContext.readRow( TABLE,  primaryKey.get(), Arrays.asList( key.fieldName() ) );
            if( null != result ){
                String flagVal = result.isNull(key.fieldName()) ? null : result.getString( key.fieldName() ); 
                if( null == value && null == flagVal ) {
                    matches = true;
                }else {
                    if( null == value ) {
                        matches = false;
                    }else {
                        matches = value.equalsIgnoreCase(flagVal);
                    }
                }
            }
        }
        
        return matches;
    }
    
    /**
     * Returns the value in the feature flags table for the supplied key
     * 
     * @param key      the key of the flag to compare
     * @return         the value of the supplied key from the feature flags table
     */
    public String getValue( FeatureFlag key ) {
        String flagVal = null;
        
        try (ReadContext readContext = this.spannerClient.singleUse()){
            SpannerKey primaryKey = SpannerKey.of( this.DB_KEY );
            Struct result = readContext.readRow( TABLE,  primaryKey.get(), Arrays.asList( key.fieldName() ) );
            if( null != result ){
                flagVal = result.isNull(key.fieldName()) ? null : result.getString( key.fieldName() ); 
            }
        }
        
        return flagVal;
    }
}
