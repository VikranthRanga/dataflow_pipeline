package com.allegis.search.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
/**
 * This Class is to limit vectors decimal values
 */
public class VectorPrecision {

	/**
	 * This method takes incoming String vector and extract numeric value and limit
	 * decimal to 5 places
	 * 
	 * @param vector incoming String vector with alphabet and numeric value.
	 * @return returns the original String  with modified numeric value
	 */
	public static String vectorFormat(String vector) {

		List<String> newVect = new ArrayList<>();
		String[] terms = vector.split(";");
		for (String termweight : terms) {
			String[] pairs = termweight.split(":");
			String term = pairs[0];
			String weight = pairs[1].substring(0, Math.min(7, pairs[1].length()));
			newVect.add(term + ":" + weight);
		}
		return String.join(";", newVect);
	}
	
	/**
	 * This method takes incoming vectormagnitude and limit decimal to 5 places
	 * 
	 * @param magnitude incoming vector magnitude numeric value.
	 * @return returns the magnitude with modified numeric value
	 */
	public static Double magnitudeFormat(Double magnitude) {
		Double result = 0.0;
		if (magnitude > 0.0) {
			String[] str = magnitude.toString().split("\\.");
			if (StringUtils.isNotBlank(str[1]))
				result = str[1].toString().length() > 5? Double.valueOf(str[0] + "." + str[1].substring(0, 5))
						: magnitude;
				return result;
		}
		return magnitude;
	}
}