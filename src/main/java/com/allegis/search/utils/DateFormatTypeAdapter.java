package com.allegis.search.utils;

import java.io.IOException;
import java.util.Date;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * Date format type adapter to customize the conversion of date field to JSON
 * 
 * @author jperecha
 */
public class DateFormatTypeAdapter extends TypeAdapter<Date> {

	@Override
	public void write(JsonWriter out, Date value) throws IOException {
		if (value == null) {
            out.nullValue();
        } else {
            out.value(value.getTime());
        }		
	}

	@Override
	public Date read(JsonReader in) throws IOException {
		if (in.peek() == null) {
            return null;
        }
        String str = in.nextString();
        Date d  = DateUtils.parse(str);
        return d;
	}

}
