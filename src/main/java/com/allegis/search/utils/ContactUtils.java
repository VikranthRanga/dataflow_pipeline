package com.allegis.search.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.allegis.search.model.person.PersonDoc;

/**
 * Utils used for ContactTransform
 * 
 * @author mannrivera
 *
 */
public class ContactUtils {

    /**
     * Generate Phone Number Formats
     * Code reformatted from aspire groovy code
     * 
     * @param phNumber
     * @return
     */
    protected static String phNumberFormats(String phNumber){
        if(phNumber==null) 
            return "";
        
        HashMap<String, ArrayList<ArrayList<Integer>>> phoneFormatsIndices = new HashMap<String,ArrayList<ArrayList<Integer>>>();
    
        StringBuffer phNumberFormatsBuffer = new StringBuffer();
        int phLength = phNumber.length();
    
        ArrayList<ArrayList<Integer>> indicesOfPhoneLength10 = new ArrayList<ArrayList<Integer>>();
        ArrayList<ArrayList<Integer>> indicesOfPhoneLength11 = new ArrayList<ArrayList<Integer>>();
        ArrayList<ArrayList<Integer>> indicesOfPhoneLength12 = new ArrayList<ArrayList<Integer>>();
    
        indicesOfPhoneLength10.add(new ArrayList<Integer>(Arrays.asList(0,3,6,10)));
        indicesOfPhoneLength10.add(new ArrayList<Integer>(Arrays.asList(0,3,10)));
        indicesOfPhoneLength10.add(new ArrayList<Integer>(Arrays.asList(0,6,10)));
    
        indicesOfPhoneLength11.add(new ArrayList<Integer>(Arrays.asList(0,1,4,7,11)));
        indicesOfPhoneLength11.add(new ArrayList<Integer>(Arrays.asList(0,1,4,11)));
        indicesOfPhoneLength11.add(new ArrayList<Integer>(Arrays.asList(0,1,7,11)));
        indicesOfPhoneLength11.add(new ArrayList<Integer>(Arrays.asList(0,1,11)));
        indicesOfPhoneLength11.add(new ArrayList<Integer>(Arrays.asList(0,2,4,7,11)));
        indicesOfPhoneLength11.add(new ArrayList<Integer>(Arrays.asList(0,3,6,10,11)));
    
        indicesOfPhoneLength12.add(new ArrayList<Integer>(Arrays.asList(0,2,4,7,12)));
        indicesOfPhoneLength12.add(new ArrayList<Integer>(Arrays.asList(0,2,5,8,12)));
        indicesOfPhoneLength12.add(new ArrayList<Integer>(Arrays.asList(0,2,7,12)));
        indicesOfPhoneLength12.add(new ArrayList<Integer>(Arrays.asList(0,2,4,8,12)));
        indicesOfPhoneLength12.add(new ArrayList<Integer>(Arrays.asList(0,2,12)));
        indicesOfPhoneLength12.add(new ArrayList<Integer>(Arrays.asList(0,3,12)));
        indicesOfPhoneLength12.add(new ArrayList<Integer>(Arrays.asList(0,4,8,12)));
    
        phoneFormatsIndices.put("PhoneLength10", indicesOfPhoneLength10);
        phoneFormatsIndices.put("PhoneLength11", indicesOfPhoneLength11);
        phoneFormatsIndices.put("PhoneLength12", indicesOfPhoneLength12);
    
        //phone numbers which starts with international dialing prefixes will be stripped off and so on for phone numbers starting with "00" or "0".
        if(phNumber.startsWith("011") || phNumber.startsWith("010")|| phNumber.startsWith("001")|| phNumber.startsWith("002")){
            phNumber = phNumber.substring(3, phLength);}
        else if(phNumber.startsWith("00")){
            phNumber = phNumber.substring(2, phLength);}
        else if(phNumber.startsWith("0")){
            phNumber = phNumber.substring(1, phLength);}
    
        phLength = phNumber.length();
    
        phNumberFormatsBuffer.append(phNumber);
    
        for(ArrayList<Integer> innerArrayList: phoneFormatsIndices.get("PhoneLength" + phLength)){
    
            int len = innerArrayList.size();
            String phoneFormats = "";
            String SEPARATOR = " ";
            for(int i=0; i<len-1;i++){
                phoneFormats += phNumber.substring(innerArrayList.get(i), innerArrayList.get(i+1))+SEPARATOR;
            }
            phNumberFormatsBuffer.append(";").append(phoneFormats.trim());
        }
        if(phNumberFormatsBuffer.length() != 0){
            return phNumberFormatsBuffer.toString();
        }
        return "";
    }

    /**
     * Phone Extractor
     * Code reformatted from aspire groovy code
     * 
     * @param phNumber
     * @return
     */
    public static String phExtractor(String phNumber){
        if(phNumber == null) {
            return "";
        }
        Pattern extensionPatternCompiled = Pattern.compile("[a-zA-Z]+");
        try{
            String extractedPhoneNumbers = "";
            if(phNumber != null){
                String[] multiplePhoneNumbers = phNumber.split("[/:;]");
                for(int i=0; i < multiplePhoneNumbers.length; i++){
                    Matcher extensionMatcher = extensionPatternCompiled.matcher(multiplePhoneNumbers[i]);
                    if(extensionMatcher.find()){
                        multiplePhoneNumbers[i] = multiplePhoneNumbers[i].substring(0,extensionMatcher.start());}
                    multiplePhoneNumbers[i] = multiplePhoneNumbers[i].replaceAll("[^0-9]", "");
                    extractedPhoneNumbers += phNumberFormats(multiplePhoneNumbers[i]);}
            }
            return extractedPhoneNumbers;
        }catch(Exception e){
            // errorLogger.log("Person-app", "FormatPhoneNumbers", doc, "Exception occured while trying to format phoneNumbers", null, e, false, false, "Invalid/Missing data", "Unable to parse the phone number of candidate: "+ doc.id?.getText(), doc.id?.getText());
        }
        return "";
    }

    /**
     * This function specifically created to format phone numbers.
     * There are three different phone number fields such as mobile_phone, home_phone and other_phone.
     * We will create differents formats for each phone type of phone number which is of length 10, 11 or 12
     * 
     * Aspire <component name="FormatPhoneNumbers" subType="default" factoryName="aspire-groovy">
     * 
     * def homePhoneObj = doc.candidate?.communication_home_phone?.getText();
     * if(homePhoneObj != null){
     *   def homePhoneFormats = phExtractor(homePhoneObj);
     *   candidateObj.add("homephoneformats", homePhoneFormats);}
     * 
     * @param resultSet
     * @param person
     */
    public static void formatPhoneNumbers (PersonDoc person ) {
        if(person.communication_home_phone != null) {
             String[] temp = phExtractor(person.communication_home_phone).split(";");
             person.homephoneformats = Arrays.asList(temp);
        }
        
        if(person.communication_mobile_phone != null) {
            String[] temp = phExtractor(person.communication_mobile_phone).split(";");
            person.mobilephoneformats = Arrays.asList(temp);
        }
        
        if(person.communication_other_phone != null) {
            String[] temp = phExtractor(person.communication_other_phone).split(";");
            person.otherphoneformats = Arrays.asList(temp);
        }
        
        // TODO why does the index mappings not have `workphoneformats`
    }

}
