package com.allegis.search.utils;

import com.allegis.search.model.person.PersonDoc;
import com.google.gson.JsonObject;

/**
 * Utils to build proven track record ouput from json
 * 
 * @author jperecha
 *
 */
public class ProvenTrackRecordUtils {
	
	private static String EXPLAIN_NEGATIVE_PARAM = "explain_negative";
	private static String EXPLAIN_POSITIVE_PARAM = "explain_positive";
	// TODO If DSIA passes label in output then remove buildLabels method and just capture label from JsonObject.
	// If not, remove below variable since Oompa Loompas is building the label - This is decided after DSIA analysis
	//private static String LABEL_PARAM = "label";
	private static String SCORE_PARAM = "score";

	/**
	 * Method to build proven track record ouput from json
	 * 
	 * @param person the person object to build on 
	 * @param ptrOutput the output json from proven track record api
	 */
	public static void buildProvenTrackRecordFields( PersonDoc person, JsonObject ptrOutput ) {
		person.proven_track_record_score = ptrOutput.get(SCORE_PARAM).getAsFloat();
    	person.proven_track_record_label = buildLabels(person.proven_track_record_score);
    	person.proven_track_record_explain = ptrOutput.get(EXPLAIN_NEGATIVE_PARAM).getAsString().concat(",").concat(ptrOutput.get(EXPLAIN_POSITIVE_PARAM).getAsString());	    
	}
	
	
	/**
	 * Method to build labels from score
	 * 
	 * @param score the score
	 * @return the label
	 */
	private static String buildLabels(Float score) {
    	String label = "UNKNOWN";
		if (score >= 58) {
			label = "GOOD";
		} else if (score < 58) {
			label = "BAD";
		} 
		return label;
	}
}
