package com.allegis.search.utils;

import java.util.List;

import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.ProvenTrackRecordActivityInfo;

/**
 * Utils to sort work history/activity info blocks
 * 
 * @author jperecha
 *
 */
public class WorkHistUtils {

	/**
	 * Method to sort AG work history based on dates
	 * 
	 * @param history	the work history
	 */
	public static void sortWorkHist(List<Employment> history) {
		 
        // descending (most recent first)
        history.sort((lhs, rhs) -> {
            if (rhs.StartDate == lhs.StartDate)
                return 0;
            if (rhs.StartDate == null)
                return -1;
            if (lhs.StartDate == null)
                return 1;
            return rhs.StartDate.compareTo(lhs.StartDate);
        });		 
	}
	
	/**
	 * Method to sort Resume work history based on dates
	 * 
	 * @param history	the resume work history
	 */
	public static void sortResumeWorkHist(List<EmploymentBean> history) {
		 
        // descending (most recent first)
        history.sort((lhs, rhs) -> {
        	if (null != lhs.getHistoryRange() && null != rhs.getHistoryRange() ) {
        		if (rhs.getHistoryRange().getStartDate() == null)
	                return -1;
	            if (lhs.getHistoryRange().getStartDate() == null)
	                return 1;
        		if (rhs.getHistoryRange().getStartDate() == lhs.getHistoryRange().getStartDate()) 
	                return 0;	            
	            return rhs.getHistoryRange().getStartDate().compareTo(lhs.getHistoryRange().getStartDate());
        	}
			return 0;
        });		 
	}
	
	/**
	 * Method to sort activity info for proven track record based on dates
	 * 
	 * @param info	the activity info
	 * @param size	the size to repackage the list so that we can get top 10 most recent descriptions
	 */
	public static void sortActivityInfo(List<ProvenTrackRecordActivityInfo> info) {
		 
        // descending (most recent first)
        info.sort((lhs, rhs) -> {        	
    		if (rhs.activityDate == null)
                return -1;
            if (lhs.activityDate == null)
                return 1;
    		if (rhs.activityDate == lhs.activityDate) 
                return 0;	            
            return rhs.activityDate.compareTo(lhs.activityDate);        	
        });	       
	}
	
}
