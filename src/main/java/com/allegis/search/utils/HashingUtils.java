package com.allegis.search.utils;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * Simple class to encapsulate hashing functionality
 */
public class HashingUtils {
    
    private static final HashFunction primaryHash = Hashing.murmur3_128();

    /**
     * Given the supplied message, a hash representation is returned.
     * @param message   the message to hash
     * @return          the hash representation of the supplied message. 
     * @throws IllegalArgumentException if the supplied message is null
     */
    public static String getHash( String message ) {
        if( null == message ) {
            throw new IllegalArgumentException("Cannot hash a null value");
        }
        HashCode code = primaryHash.hashBytes( message.getBytes() );
        return code.toString();
    }
    
    
    /**
     * Hashes and compares the supplied message against the given hash
     * @param message       the message to compare
     * @param targetHash    the target hash to compare against
     * @return              true if the hash of the message matches exactly the supplied hash; false if the message is NULL or it does not mat
     */
    public static Boolean matchesHash( String message, String targetHash ) {
    	if(message != null && !message.trim().isEmpty()){
    		String sourceHash = getHash( message );
            return sourceHash.equals(targetHash);
    	}
    	else
    		return false;
        
    }
    
    
}
