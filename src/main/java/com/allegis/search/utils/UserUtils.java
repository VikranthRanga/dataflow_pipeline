package com.allegis.search.utils;

import java.util.Arrays;
import java.util.List;

import com.allegis.search.enums.Table.User;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.ReadContext;
import com.google.cloud.spanner.Struct;

/**
 * Methods for working with the "user" table of the data contained therein.
 * 
 * @author dhartwig
 *
 */
public class UserUtils {
	
	/**
	 * Given a user ID, returns the Peoplesoft_Id__c value.  Return null if not found.
	 *  
	 * @param spannerClient
	 * @param userId
	 * @return
	 */
	public static String  getPeopleSoftId(DatabaseClient spannerClient, String userId) {
		return getColumn(spannerClient, userId, User.Column.Peoplesoft_Id__c.name());       	        	
	}

	/**
	 * Given a user ID, returns the Name value.  Return null if not found.
	 *  
	 * @param spannerClient
	 * @param userId
	 * @return
	 */
	public static String getName(DatabaseClient spannerClient, String userId) {
		return getColumn(spannerClient, userId, User.Column.Name.name());       	        	
	}

	/**
	 * Given a user ID, returns a single column  value.  Return null if not found.
	 *  
	 * @param spannerClient
	 * @param userId
	 * @param columnName
	 * @return
	 */
	private static String getColumn(DatabaseClient spannerClient, String userId, String columnName) {
		List<String> returnColumn = Arrays.asList(columnName);
  		try (ReadContext readContext = spannerClient.singleUse())  // force quick closure
  		{
            SpannerKey primaryKey = SpannerKey.of( userId );
            Struct userRow = readContext.readRow(User.TABLE, primaryKey.get(), returnColumn);
            if (userRow == null) {
            	return null;
            }
            String value = GeneralUtils.getNullSafeString(userRow, columnName);
        	return value;       	        	
        }
	}


}
