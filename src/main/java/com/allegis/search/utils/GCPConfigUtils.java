package com.allegis.search.utils;

import org.apache.beam.sdk.io.gcp.spanner.SpannerConfig;

import com.allegis.search.options.CommonOptions;
import com.allegis.search.util.AllegisSafeConfig;
import com.allegis.search.util.GCPConfig;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.DatabaseId;
import com.google.cloud.spanner.Spanner;
import com.google.cloud.spanner.SpannerOptions;

/**
 * GCP configuration utils
 * 
 * @author dhartwig
 *
 */
public class GCPConfigUtils {

	public final static String GCS_PREFIX = "gs://";

	/**
	 * Builds a Spanner instance based on the environment
	 * @param env
	 * @return
	 */
	public static Spanner buildSpannerInstance(String env) {		
		return SpannerOptions.newBuilder()
	        .setProjectId( GCPConfig.getInstance(env).getGcpProjectId() )
	        .build()
	        .getService();
	}

	/**
	 * Builds a Spanner configuration for the correct env using the predictions database
	 * @param env
	 * @return
	 */
	public static SpannerConfig getPredictionsSpannerConfig(CommonOptions options) {		
		GCPConfig gcpConfig = GCPConfig.getInstance(getGcpEnv(options));
		return SpannerConfig.create()
				.withProjectId(gcpConfig.getGcpProjectId())  
                .withInstanceId(gcpConfig.getSpannerInstanceId())
                .withDatabaseId(getPredictionsSpannerDatabaseId());
	}

	/**
	 * Builds a Spanner configuration for the correct env
	 * @param env
	 * @return
	 */
	public static SpannerConfig getSpannerConfig(CommonOptions options) {		
		GCPConfig gcpConfig = GCPConfig.getInstance(getGcpEnv(options));
		return SpannerConfig.create()
				.withProjectId(gcpConfig.getGcpProjectId())  
                .withInstanceId(gcpConfig.getSpannerInstanceId())
                .withDatabaseId(gcpConfig.getSpannerDbId());
	}

	/**
	 * Returns a spanner client with dirty read connection
	 * 
	 * @param spanner
	 * @param env
	 * @return
	 */
	public static DatabaseClient getSpannerClient(Spanner spanner, String env) {
		GCPConfig gcpConfig = GCPConfig.getInstance(env);
		
		DatabaseId databaseId = DatabaseId.of( gcpConfig.getGcpProjectId(),
												gcpConfig.getSpannerInstanceId(),
												gcpConfig.getSpannerDbId());
		
        return spanner.getDatabaseClient(databaseId);
	}

	
	/**
	 * Returns a spanner client with dirty read connection
	 * 
	 * @param spanner
	 * @param env
	 * @return
	 */
	public static DatabaseClient getPredictionsSpannerClient(Spanner spanner, String env) {
		GCPConfig gcpConfig = GCPConfig.getInstance(env);
		
		DatabaseId databaseId = DatabaseId.of( gcpConfig.getGcpProjectId(),
											gcpConfig.getSpannerInstanceId(),
											getPredictionsSpannerDatabaseId());
		
        return spanner.getDatabaseClient(databaseId);
	}


	public static String getPredictionsSpannerDatabaseId() {
		return "predictions";
	}


	/**
	 * This is where the HTML resumes live
	 * 
	 * @param options
	 * @return
	 */
	public static String getHtmlResumePath(CommonOptions options) {
		return getProjectCloudStorageBase(options)+"/resumes/html/";
	}

	/**
	 * This is where the Sovren parsed resumes live
	 * 
	 * @param options
	 * @return
	 */
	public static String getXmlResumePath(CommonOptions options) {
		return getProjectCloudStorageBase(options)+"/resumes/parsed/";
	}
	
	/**
	 * This is the based directory (bucket) for the projects dataflow jobs
	 * 
	 * @param options
	 * @return
	 */
	public static String getDataflowCloudStoragePath(CommonOptions options) {
		return getProjectCloudStorageBase(options)+"/dataflow/";
	}

	

	/**
	 * This is the cloud storage base path
	 * 
	 * @param options
	 * @return
	 */
	public static String getProjectCloudStorageBase(CommonOptions options) {
		String projectId = getGcpConfig(options).getGcpProjectId();
		return GCS_PREFIX+projectId.replaceAll("-", "_");
	}

	/**
	 * Returns the value of option --allegisEnv, if set, else returns the value of option --env
	 * 
	 * @param options
	 * @return AllegisConfig
	 */
	public static String getAllegisEnv(CommonOptions options) {
		return (options.getAllegisEnv() != null) ? options.getAllegisEnv().toUpperCase() : options.getEnv().toUpperCase();
	}

	
	//  DEV and TEST are the same project in the GCP world
	static final String ALLOWED_GCP_ENVS = "DEV|TEST|LOAD|PROD|UAT";  
	
	/**
	 * Returns the value of option --env
	 * 
	 * @param options
	 * @return GCPConfig
	 */
	public static String getGcpEnv(CommonOptions options) {
		String env = options.getEnv().toUpperCase();
		if ( ! env.matches(ALLOWED_GCP_ENVS))  {
			throw new RuntimeException(env+" is not a valid GCP environement. Use --env="+ALLOWED_GCP_ENVS+".  Plus use --allegisEnv to tie into other Allegis environments");
		}
		return env;
	}


	/**
	 * Returns GCPConfig instance based on the value of option --env
	 * 
	 * @param options
	 * @return GCPConfig
	 */
	public static GCPConfig getGcpConfig(CommonOptions options) {
		return GCPConfig.getInstance(getGcpEnv(options));
	}

	/**
	 * Returns AllegisSafeConfig instance based on the value of option --env and --allegisEnv
	 * 
	 * @param options
	 * @return AllegisSafeConfig
	 */
	public static AllegisSafeConfig getAllegisConfig(CommonOptions options) {
		return AllegisSafeConfig.getInstance(getAllegisEnv(options));
	}


	/**
	 * Use the environment to build the full subscription path.
	 * 
	 * @param name      the name of the subscriber (just the name)
	 * @param options   the project options  
	 * @return          a full path to the supplied subscriber name
	 */
	public static String buildSubscriptionPath(CommonOptions options, String name ) {
	    if (name.startsWith("projects/")) {
	        return name; 
	    }
	    return getGcpConfig(options).getFullSubscription(name); 
	}

	/**
	 * Use the environment to build the full topic path.
	 * 
	 * @param name      the name of the subscriber (just the name)
	 * @param options   the project options  
	 * @return          a full path to the supplied subscriber name
	 */
	public static String buildTopicPath(CommonOptions options, String name ) {
	    if (name.startsWith("projects/")) {
	        return name; 
	    }
	    return getGcpConfig(options).getFullTopic(name); 
	}	
}
