package com.allegis.search.utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.allegis.search.enums.RateFrequency;
import com.allegis.search.model.CommonDoc;
import com.allegis.search.stage.DeadLetterService;

/**
 * Utils for AccountTransform
 * 
 * @author mannrivera
 *
 */
public class AccountUtils implements Serializable{

    private static final long serialVersionUID = -557549980796999466L;
    private static Map<String, String> languageMap = new HashMap<String, String>();

    static {
        // Loading languageMap came from Global_LOV__c table
        // for (Global_LOV__c lov : [SELECT Text_Value__c, Text_Value_2__c, Text_Value_3__c, LOV_Name__c FROM Global_LOV__c WHERE LOV_Name__c IN ('LanguageCode')]){
        // if (lov.LOV_Name__c.equals('LanguageCode')) {
        //    langCodeMap.put(lov.Text_Value__c, lov.Text_Value_2__c);
        // }}
        languageMap.put("gsw", "Swiss German");
        languageMap.put("aa", "Afar");
        languageMap.put("ab", "Abkhazian");
        languageMap.put("ae", "Avestan");
        languageMap.put("af", "Afrikaans");
        languageMap.put("ak", "Akan");
        languageMap.put("am", "Amharic");
        languageMap.put("an", "Aragonese");
        languageMap.put("ar", "Arabic");
        languageMap.put("as", "Assamese");
        languageMap.put("av", "Avaric");
        languageMap.put("ay", "Aymara");
        languageMap.put("az", "Azerbaijani");
        languageMap.put("ba", "Bashkir");
        languageMap.put("be", "Belarusian");
        languageMap.put("bg", "Bulgarian");
        languageMap.put("bh", "Bihari languages");
        languageMap.put("bi", "Bislama");
        languageMap.put("bm", "Bambara");
        languageMap.put("bn", "Bengali");
        languageMap.put("bo", "Tibetan");
        languageMap.put("br", "Breton");
        languageMap.put("bs", "Bosnian");
        languageMap.put("ca", "Catalan");
        languageMap.put("ce", "Chechen");
        languageMap.put("ch", "Chamorro");
        languageMap.put("cv", "Chuvash");
        languageMap.put("co", "Corsican");
        languageMap.put("cr", "Cree");
        languageMap.put("cs", "Czech");
        languageMap.put("cy", "Welsh");
        languageMap.put("dz", "Dzongkha");
        languageMap.put("da", "Danish");
        languageMap.put("de", "German");
        languageMap.put("dv", "Divehi");
        languageMap.put("ee", "Ewe");
        languageMap.put("el", "Greek");
        languageMap.put("en", "English");
        languageMap.put("eu", "Basque");
        languageMap.put("eo", "Esperanto");
        languageMap.put("es", "Spanish");
        languageMap.put("et", "Estonian");
        languageMap.put("fa", "Persian");
        languageMap.put("ff", "Fulah");
        languageMap.put("fi", "Finnish");
        languageMap.put("fj", "Fijian");
        languageMap.put("ga", "Irish");
        languageMap.put("fo", "Faroese");
        languageMap.put("fr", "French");
        languageMap.put("fy", "Western Frisian");
        languageMap.put("gd", "Gaelic");
        languageMap.put("gl", "Galician");
        languageMap.put("gn", "Guarani");
        languageMap.put("gu", "Gujarati");
        languageMap.put("hi", "Hindi");
        languageMap.put("gv", "Manx");
        languageMap.put("ha", "Hausa");
        languageMap.put("he", "Hebrew");
        languageMap.put("ho", "Hiri Motu");
        languageMap.put("hr", "Croatian");
        languageMap.put("hz", "Herero");
        languageMap.put("ht", "Haitian");
        languageMap.put("hu", "Hungarian");
        languageMap.put("hy", "Armenian");
        languageMap.put("ia", "Interlingua");
        languageMap.put("id", "Indonesian");
        languageMap.put("ie", "Interlingue");
        languageMap.put("io", "Ido");
        languageMap.put("ig", "Igbo");
        languageMap.put("ii", "Sichuan Yi");
        languageMap.put("ik", "Inupiaq");
        languageMap.put("is", "Icelandic");
        languageMap.put("it", "Italian");
        languageMap.put("iu", "Inuktitut");
        languageMap.put("ja", "Japanese");
        languageMap.put("jv", "Javanese");
        languageMap.put("ka", "Georgian");
        languageMap.put("kg", "Kongo");
        languageMap.put("ki", "Kikuyu");
        languageMap.put("kj", "Kuanyama");
        languageMap.put("kk", "Kazakh");
        languageMap.put("kl", "Kalaallisut");
        languageMap.put("km", "Central Khmer");
        languageMap.put("ks", "Kashmiri");
        languageMap.put("kn", "Kannada");
        languageMap.put("ko", "Korean");
        languageMap.put("kr", "Kanuri");
        languageMap.put("ku", "Kurdish");
        languageMap.put("lg", "Ganda");
        languageMap.put("kv", "Komi");
        languageMap.put("kw", "Cornish");
        languageMap.put("ky", "Kirghiz");
        languageMap.put("la", "Latin");
        languageMap.put("lb", "Luxembourgish");
        languageMap.put("li", "Limburgan");
        languageMap.put("ln", "Lingala");
        languageMap.put("lo", "Lao");
        languageMap.put("lt", "Lithuanian");
        languageMap.put("mk", "Macedonian");
        languageMap.put("lu", "Luba-Katanga");
        languageMap.put("lv", "Latvian");
        languageMap.put("mg", "Malagasy");
        languageMap.put("mh", "Marshallese");
        languageMap.put("mi", "Maori");
        languageMap.put("ml", "Malayalam");
        languageMap.put("mn", "Mongolian");
        languageMap.put("mr", "Marathi");
        languageMap.put("na", "Nauru");
        languageMap.put("ms", "Malay");
        languageMap.put("mt", "Maltese");
        languageMap.put("my", "Burmese");
        languageMap.put("nd", "Ndebele; North"); // "nd",""Ndebele"," North""
        languageMap.put("no", "Norwegian");
        languageMap.put("ne", "Nepali");
        languageMap.put("ng", "Ndonga");
        languageMap.put("nl", "Dutch; Flemish");
        languageMap.put("nr", "Ndebele; South");
        languageMap.put("nv", "Navajo; Navaho");
        languageMap.put("ny", "Chichewa");
        languageMap.put("oc", "Occitan");
        languageMap.put("oj", "Ojibwa");
        languageMap.put("om", "Oromo");
        languageMap.put("or", "Oriya");
        languageMap.put("os", "Ossetian");
        languageMap.put("pa", "Punjabi");
        languageMap.put("pi", "Pali");
        languageMap.put("pl", "Polish");
        languageMap.put("ps", "Pushto");
        languageMap.put("pt", "Portuguese");
        languageMap.put("ro", "Romanian");
        languageMap.put("qu", "Quechua");
        languageMap.put("rm", "Romansh");
        languageMap.put("rn", "Rundi");
        languageMap.put("ru", "Russian");
        languageMap.put("rw", "Kinyarwanda");
        languageMap.put("sa", "Sanskrit");
        languageMap.put("sc", "Sardinian");
        languageMap.put("sd", "Sindhi");
        languageMap.put("se", "Northern Sami");
        languageMap.put("sg", "Sango");
        languageMap.put("sh", "Serbo-Croatian");
        languageMap.put("si", "Sinhala");
        languageMap.put("sk", "Slovak");
        languageMap.put("so", "Somali");
        languageMap.put("sl", "Slovenian");
        languageMap.put("sm", "Samoan");
        languageMap.put("sn", "Shona");
        languageMap.put("sq", "Albanian");
        languageMap.put("sr", "Serbian");
        languageMap.put("ss", "Swati");
        languageMap.put("st", "Southern Sotho");
        languageMap.put("ta", "Tamil");
        languageMap.put("su", "Sundanese");
        languageMap.put("sv", "Swedish");
        languageMap.put("sw", "Swahili");
        languageMap.put("ti", "Tigrinya");
        languageMap.put("te", "Telugu");
        languageMap.put("tg", "Tajik");
        languageMap.put("th", "Thai");
        languageMap.put("tk", "Turkmen");
        languageMap.put("tr", "Turkish");
        languageMap.put("tl", "Tagalog");
        languageMap.put("tn", "Tswana");
        languageMap.put("to", "Tonga");
        languageMap.put("ts", "Tsonga");
        languageMap.put("tt", "Tatar");
        languageMap.put("tw", "Twi");
        languageMap.put("ty", "Tahitian");
        languageMap.put("ug", "Uighur");
        languageMap.put("vo", "Volapük");
        languageMap.put("uk", "Ukrainian");
        languageMap.put("ur", "Urdu");
        languageMap.put("uz", "Uzbek");
        languageMap.put("ve", "Venda");
        languageMap.put("vi", "Vietnamese");
        languageMap.put("wa", "Walloon");
        languageMap.put("wo", "Wolof");
        languageMap.put("xh", "Xhosa");
        languageMap.put("yi", "Yiddish");
        languageMap.put("zu", "Zulu");
        languageMap.put("yo", "Yoruba");
        languageMap.put("za", "Zhuang");
        languageMap.put("zh", "Chinese");
        languageMap.put("zh-yue", "Cantonese");
        languageMap.put("zh-cmn", "Mandarin");
    }

    /**
     * Convert rateFrequency and rate to Hourly rate
     * 
     * @param frequency [daily, weekly, monthly, hourly]
     * @param rate salary rate
     * @return String Hourly Pay Rate
     */
    public static String convertDesiredRateToHourly( RateFrequency frequency, Double rate) {
        Double hourlyRate = 0.0;

        if (frequency != null) {
            switch ( frequency) {
            case DAILY:
                // assumption 8 hours per day
                hourlyRate = rate / 8;
                break;
            case WEEKLY:
                // assumption 40 hrs per week
                hourlyRate = rate / 40;
                break;
            case MONTHLY:
                // assumption 173 hrs per month
                hourlyRate = rate / 173;
                break;
            case HOURLY:
                hourlyRate = rate;
                break;
            }
        }
        // Always returns 2 digits and a leading leading zero (if there is no whole dollar amount)
        return new DecimalFormat("########0.00").format( hourlyRate );
    }

    /**
     * Method Used to parse skill json
     * 
     * JsonString({"skills":["Forklift","Production Support","Production
     * Supervision","Union Experience"]})
     * 
     * @param json JSON string of skills
     * @return List<String> a list of skills
     */
    public static List<String> parseSkillJson(JSONObject jsonObject) {
        List<String> skills = new ArrayList<String>();
       
        if(jsonObject != null) {
            if (jsonObject.containsKey("skills")) {
                JSONArray skillArray = (JSONArray) jsonObject.get("skills");
                for (Object currentSkill : skillArray) {
                    skills.add((String) currentSkill);
                }
            }
        }

        if (skills.isEmpty())
        	return null;
        else
        	return skills;
    }

    /**
     * Parse String to JSONObject
     * @param json JSON String
     * @return JSONObject JSONObject 
     */
    public static JSONObject parseJsonObject(String json, CommonDoc doc, String fieldName, DeadLetterService deadLetterService) {
        if(json == null) 
            return null;
        else if(json.trim().length() == 0)
            return null;

        
        if (json.startsWith("\"{"))  {
        	//  scrub leading quote  
        	json = json.substring(1, json.length()); 
        }   
        
        if (json.endsWith("}\""))  {
        	//  scrub trailing guote
        	json = json.substring(0, json.length() - 1); 
        } 
        
        if (json.startsWith("{\"\""))  {
        	//  scrub pairs of double quotes into ones'ies  
        	json = json.replaceAll("\"\"", "\""); 
        } 
        
        try {
            JSONParser jsonParser = new JSONParser(); 
            Object parsedObject = jsonParser.parse(json);
            
            if(parsedObject instanceof JSONObject) {
                return (JSONObject) parsedObject;
            }
        } catch (ParseException e) {
            String generalMessage = String.format("JSON %s parse exception", fieldName);
            String errorMessage = String.format("errorMessage:[%s]; JSON: %s", e.getMessage(), json);
            deadLetterService.writeDataQualityWarning(generalMessage, errorMessage, doc);
        }
        return null;
    }
    
    
    /**
     * Method Used to parse talent_preference_c skills json
     * 
     * {"skills":{"data":"apex,sfdc,lightning,visualforce,crm,c#,java,agile,development,api,salesforce.com,javascript"}}
     * 
     * @param json JSON string of skills
     * @return List<String> a list of skills
     */
    public static List<String> parseCommunitiesSkillJSON(JSONObject jsonObject) {
        List<String> skills = new ArrayList<String>();
        
        if(jsonObject != null) {
            if (jsonObject.containsKey("skills")) {
            	
            	Object skillJSON = jsonObject.get("skills");
            	
                if(skillJSON instanceof JSONObject) {
                	
                	JSONObject skillsJSONObject = (JSONObject) skillJSON;
                	
                	if(skillsJSONObject != null) {
                    	if (skillsJSONObject.containsKey("data")) {
                    		
                    		String skillsValue = (String) skillsJSONObject.get("data");
                    		
                    		if(skillsValue != null && !skillsValue.isEmpty()) { 
                    			skills = Arrays.asList(skillsValue.split(","));
                    		}
                    	}
                    }
                }
            }
        }
        /*else {
        	//Test data
        	skills.add("Java");
        	skills.add("C#");
        	skills.add("Spark");
        }*/
       
        if (skills.isEmpty())
        	return null;
        else
        	return skills;
    }
    
    
    

    /**
     * Parses TalentPreference JSON String
     * 
     * Example JSON String
     * {"languages":["en"],
     *  "geographicPrefs":
     *  {"nationalOpportunities":null,
     *  "desiredLocation":[],
     *  "commuteLength":{"unit":null,"distance":null},"geoComments":""},
     *  "employabilityInformation":{
     *  "securityClearance":null,
     *  "reliableTransportation":true,
     *  "eligibleIn":[],
     *  "drugTest":null,
     *  "backgroundCheck":null}}
     * 
     * @param json TalentPreference JSON String
     * @return List<String> a list of 
     */
    public static List<String> parseLanguagePreferenceJson(JSONObject jsonObject) {
        List<String> languageCodes = new ArrayList<String>();
        
        if (jsonObject!=null && jsonObject.containsKey("languages")) {
            Object languagesArray = jsonObject.get("languages");
            if(languagesArray instanceof JSONArray) {
                JSONArray languageArray = (JSONArray) languagesArray;
                for (Object currentLanguage : languageArray) {
                    String currentLangStr = (String) currentLanguage;
                    if (languageMap.containsKey(currentLangStr)) {
                        languageCodes.add(languageMap.get(currentLangStr));
                    }
                }
            }
        }
        if (languageCodes.isEmpty())
        	return null;
        else
        	return languageCodes;
        }
    
    
    /**
     * 
     * "nationalOpportunities":false
     * 
     * @param JSONObject  PreferenceJson object
     * @return Boolean Value
     */
    public static Boolean parseNationalOpportunitiesPreferenceJson(JSONObject jsonObject) {
        JSONObject geographicPrefs = null;
        if(jsonObject!=null && jsonObject.containsKey("geographicPrefs")) {
            Object geographicPrefsObject = jsonObject.get("geographicPrefs");
            if(geographicPrefsObject instanceof JSONObject) {
                geographicPrefs = (JSONObject) geographicPrefsObject;
            }
        }
        
        if (geographicPrefs!=null && geographicPrefs.containsKey("nationalOpportunities")) {
            Object nationalOpportunitiesObject = geographicPrefs.get("nationalOpportunities");
            if(nationalOpportunitiesObject instanceof Boolean) {
                Boolean nationalOpportunities = (Boolean) nationalOpportunitiesObject;
                return nationalOpportunities;
            }
        }
        
        return null;
    }
    
}
