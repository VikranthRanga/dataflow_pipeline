package com.allegis.search.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.MetricsNamespace;

/**
 * Utility to monitor counts over a small set of values.
 * @author dhartwig
 */
public class CounterSet implements Serializable {
	private static final long serialVersionUID = 1L;
	private Map<String, Counter> counters = new HashMap<>();
    private String prefix;
	
    public CounterSet(String prefix) {
    	super();
    	this.prefix = prefix+": ";
    }
    
    /**
     * Increment count by 1
     * 
     * @param key  The value discriminator
     */
	public void inc(String key)
	{
        Counter counter = counters.get( key );
        if( counter == null ){
        	counter = Metrics.counter( MetricsNamespace.STAGE.name() , prefix + key);
        	counters.put(key, counter);
        }
        counter.inc();
	}


}
