package com.allegis.search.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.metrics.Distribution;
import org.apache.beam.sdk.metrics.DistributionResult;
import org.apache.beam.sdk.metrics.MetricResult;
import org.apache.beam.sdk.metrics.MetricsFilter;
import org.apache.beam.sdk.options.PipelineOptions;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.model.CommonDoc;
import com.allegis.search.model.GeoLocation;
import com.allegis.search.model.MatchDoc;
import com.allegis.search.stage.DeadLetterService;
import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.spanner.ResultSet;
import com.google.cloud.spanner.Struct;

/**
 * A general utility class of static methods that can be useful to many classes
 */
public class GeneralUtils {

	public static final Logger logger = LoggerFactory.getLogger( GeneralUtils.class );

    private static final String NULL = "null";
    
    // This character is used to determine if we should accept, or reject
    // records with a given org id, provided than an org id has been provided
    protected static final String ORG_ID_EXCLUSION_CHAR = "!";
    
    
    /**
     * This method is used to determine if an ingestion should be prevented (i.e. dropped for consideration)
     * if the sourceOrgId (the org id on the update) aligns/does not align with the targetOrgId (the org id 
     * passed as a pipeline option) based on the intended behavior of the targetOrgId's value. 
     * See method implementation for details.
     *  
     * @param sourceOrgId   the org id of an incoming update
     * @param targetOrgId   the org id defined as a pipeline options
     * @return              true if the org id alignment criteria is met, false otherwise  
     */
    public static boolean preventIngestByOrgId( String sourceOrgId, String targetOrgId ) {
        
        // If the Target Org Id starts with the exclusion character, it means that
        // we should return FALSE when the supplied orgIds match. If the target org Id
        // does not start with the exclusion character, it means that we should return 
        // TRUE when the supplied orgId matches
        Boolean acceptAllOrgsExcept = targetOrgId.startsWith( ORG_ID_EXCLUSION_CHAR );
        if( acceptAllOrgsExcept ) {
            targetOrgId = targetOrgId.substring(1);
        }
        
        return ! ( acceptAllOrgsExcept ^ targetOrgId.equalsIgnoreCase( sourceOrgId ) );
    }
    
    /**
     * Determines if the supplied feeder id, is one from  QA
     * @param feederId  the name of a feeder id
     * @return          true if this feeder id is from our QA utils
     */
    public static boolean isQAdata( String feederId ) {
        return IngestionConfig.qa_feeder_id.equalsIgnoreCase(feederId);
    }
    
    /**
     * Returns a QA identifiable id from the given id
     * @param id    the id to 'prep'
     * @return      the prepped id
     */
    public static String prepareQAid( String id ) {
        return id.startsWith( IngestionConfig.qa_id_prefix ) ? id : IngestionConfig.qa_id_prefix + id;
    }
    
    /**
     * For the given structure (i.e. database row) return null or the string value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static String getNullSafeString(Struct row, String columnName) {
        if (row.isNull(columnName))
            return null;
        String s = row.getString(columnName);
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ) )
            return null;
        return s;
    }
    
    /**
     * For the given structure (i.e. JSON object) return null or the string value of the supplied columName
     * 
     * @param data          a reference to the json from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static String getNullSafeString(JSONObject data, String columnName) {
        String s = (String) data.get(columnName);
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        return s;
    }
    
    
    /**
     * For the given structure (i.e. database row) return null or the string value of the supplied column
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param column    the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static String getNullSafeString(Struct row, Enum<?> column) {
        return getNullSafeString(row, column.name());
    }
    
    /**
     * For the given structure (i.e. table row) return null or the string value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static String getNullSafeString(TableRow row, String columnName) {
        String s = (String)row.get(columnName);
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ) )
            return null;
        return s;
    }

    /**
     * For the given structure (i.e. database row) return null or the string value of the supplied colum
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param column        the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static String getNullSafeString(ResultSet row, Enum<?> column) {
        return getNullSafeString(row, column.name());
    }

    /**
     * For the given structure (i.e. database row) return null or the string value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static String getNullSafeString(ResultSet row, String columnName) {
        if (row.isNull(columnName))
            return null;
        String s = row.getString(columnName);
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        return s;
    }
    
    /**
     * Get the integer representation of a number stored as a double within spanner
     * @param row               a reference to the row
     * @param columnsMapping    the column mapping of the column to get
     * @param doc               a reference to the assigning object
     * @return                  the integer representation of the data
     */
    public static Integer getNullSafeIntFromDouble(Struct row, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
        Double dbl = GeneralUtils.getNullSafeDouble(row, columnName, doc, deadLetterService);
        return ( null != dbl ) ? dbl.intValue() : null;
    }
    

    /**
     * For the given structure (i.e. database row) return null or the Date value of the supplied columName
     * 
     * @param data          a reference to the json object to extract data from
     * @param columnName    the name of the column to retrieve its value from
     * @return              a date or null if the value cannot be parsed as a date
     */
    public static Date getNullSafeDate(JSONObject data, String columnName) {

        String s = (String) data.get(columnName);
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        try {
            return DateUtils.parse(s);
        } catch (Exception e) {         
            return null;
        }
    }
    
    /**
     * For the given structure (i.e. database row) return null or the Date value of the supplied column
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param column        the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @return              a date
     */
    public static Date getNullSafeDate(ResultSet row, Enum<?> column, CommonDoc doc, DeadLetterService deadLetterService) 
    {
        return getNullSafeDate(row, column.name(), doc, deadLetterService);
    }
    
    /**
     * For the given structure (i.e. database row) return null or the Date value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @return              a date
     */
    public static Date getNullSafeDate(ResultSet row, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
        if (row.isNull(columnName))
            return null;
        String s = row.getString(columnName);
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        return getNullSafeDate(s, columnName, doc, deadLetterService);
    }
    
    
    /**
     * For the given structure (i.e. database row) return null or the Date value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a date
     */
    public static Date getNullSafeDate(Struct row, String columnName) {
        if (row.isNull(columnName))
            return null;
        
        String s = row.getString(columnName);
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        
        try {
            return DateUtils.parse(s);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * For the given structure, return null or the date value of the supplied column name
     * that holds a timestamp 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @return              a date (or null)
     */
    public static Date getNullSafeTimeStamp(Struct row, String columnName) {
        if (row.isNull(columnName))
            return null;
        
        String s = row.getTimestamp(columnName).toString();
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        
        try {
            return DateUtils.parse(s);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * For the given structure (i.e. database row) return null or the Date value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a date
     */
    public static Date getNullSafeDate(Struct row, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
        if (row.isNull(columnName))
            return null;
        String s = row.getString(columnName);
        return getNullSafeDate(s, columnName, doc, deadLetterService);
    }

    /**
     * For the given string return null or the Date value of the supplied columName
     * 
     * @param s             a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a date
     */

	public static Date getNullSafeDate(String s, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
		if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        try {
			return DateUtils.parse(s);
		} catch (Exception e) {			
			deadLetterService.writeDataQualityWarning(e, "columnName="+columnName+"; value="+s, doc);
			return null;
		}
	}


    /**
     * For the given structure (i.e. database row) return null or the Double value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a double
     */
    public static Double getNullSafeDouble(Struct row, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
        if (row.isNull(columnName))
            return null;
        String s = row.getString(columnName);
        return getNullSafeDouble(s, columnName, doc, deadLetterService);
    }


    /**
     * For the given string return null or the Double value of the supplied columName
     * 
     * @param s             a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a double
     */
	public static Double getNullSafeDouble(String s, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
		if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        try {
			return Double.parseDouble(s);
		} catch (NumberFormatException e) {			
			deadLetterService.writeDataQualityWarning(e, "columnName="+columnName+"; value="+s, doc);
			return null;
		}
	}

	
	/**
     * For the given structure (i.e. database row) return null or the Float value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a float
     */
    public static Float getNullSafeFloat(Struct row, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
        if (row.isNull(columnName))
            return null;
        String s = row.getString(columnName);
        return getNullSafeFloat(s, columnName, doc, deadLetterService);
    }


    /**
     * For the given string return null or the Float value of the supplied columName
     * 
     * @param s             a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a float
     */
    public static Float getNullSafeFloat(String s, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
        if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ))
            return null;
        try {
            return Float.parseFloat(s);
        } catch (NumberFormatException e) {         
            deadLetterService.writeDataQualityWarning(e, "columnName="+columnName+"; value="+s, doc);
            return null;
        }
    }
    

    /**
     * For the given structure (i.e. database row) return null or the Integer value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a Integer
     */
    public static Integer getNullSafeInteger(Struct row, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
        if (row.isNull(columnName))
            return null;
        String s = row.getString(columnName);
        return getNullSafeInteger(s, columnName, doc, deadLetterService);
    }


    /**
     * For the given string return null or the Integer value of the supplied columName
     * 
     * @param s             a reference to the table row from which to extract the columnName data from
     * @param columnName    the name of the column to retrieve its value from
     * @param doc           the id of the document this field is being retrieved from (used for debugging purposes)
     * @param deadLetterService
     * @return              a Integer
     */

	public static Integer getNullSafeInteger(String s, String columnName, CommonDoc doc, DeadLetterService deadLetterService) {
		if (s == null || s.trim().isEmpty() || s.trim().equalsIgnoreCase( NULL ) )
            return null;
        try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {			
			deadLetterService.writeDataQualityWarning(e, "columnName="+columnName+"; value="+s, doc);
			return null;
		}
	}


    /**
     * For the given structure (i.e. database row) return null or the boolean value of the supplied columName
     * 
     * @param row           a reference to the table row from which to extract the columnName data from
     * @param s             the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static boolean getNullSafeBoolean(Struct data, String s) {
        String temp = GeneralUtils.getNullSafeString(data, s);

        if (temp != null && (temp.equalsIgnoreCase("true") || temp.equalsIgnoreCase("yes") )) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * For the given result set record (i.e. database row) return null or the boolean value of the supplied columName
     * 
     * @param row           a reference to the result set from which to extract the columnName data from
     * @param s             the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static boolean getNullSafeBoolean(ResultSet data, String s) {
        String temp = GeneralUtils.getNullSafeString(data, s);

        if (temp != null && ( temp.equalsIgnoreCase("true") || temp.equalsIgnoreCase("yes") )) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * For the given result set record (i.e. database row) return null or the boolean value of the supplied colum
     * 
     * @param row           a reference to the result set from which to extract the column data from
     * @param s             the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static boolean getNullSafeBoolean(ResultSet data, Enum<?> e) {
        return getNullSafeBoolean(data, e.name());
    }
    
    /**
     * For the given structure (i.e. json doc) return null or the boolean value of the supplied columName
     * 
     * @param row           a reference to the json from which to extract the columnName data from
     * @param s             the name of the column to retrieve its value from
     * @return              the value of the supplied column or null if it does not exist
     */
    public static Boolean getNullSafeBoolean(JSONObject data, String s) {
        Boolean bool = null;
        Object obj = data.get(s);
        if( null != obj ) {
           try {
               bool = (Boolean) obj;
           }catch(ClassCastException e) { 
               try {
                   String sBool = (String) obj;
                   bool = Boolean.valueOf( sBool );
               }catch(Exception x) {
                   /* do nothing */
               }
           } 
        }
        return bool;
    }
    

    /**
     * Convert enums to there name();
     * 
     * @param enums
     * @return
     */
    public static List<String> toNameList(Enum<?> ...enums) {
    	List<String> columns = new ArrayList<String>();
    	for (Enum<?> e : enums) {
    		columns.add(e.name());
    	}
    	return columns;
    }
    
    /**
     * Replace LF, CR & tabs with space
     * @param s
     * @return
     */
	public static String scrubCRLFTab(String s) {
		if (s == null)
			return null;
        return s.replaceAll("\\r\\n|\\r|\\n|\\t", " ");
	}
	

	/**
	 * Applies a GeoLocation object against the supplied match doc for the given lat /lon coordinates
	 * @param matchDoc             a reference to the match doc
	 * @param latitude             the latitude to apply
	 * @param longitude            the longitude to apply
	 * @param deadLetterService    a refrence to the deadletter service to record failures
	 */
	public static void populateGeoLocation(MatchDoc matchDoc, String latitude, String longitude, DeadLetterService deadLetterService) {
		matchDoc.geolocation = parseGeoLocation(matchDoc, latitude, longitude, deadLetterService);
	}

	
	/**
	 * Returns a GeoLocation object against the supplied match doc for the given lat /lon coordinates
	 * @param matchDoc             a reference to the match doc
	 * @param latitude             the latitude to apply
	 * @param longitude            the longitude to apply
	 * @param deadLetterService    a refrence to the deadletter service to record failures
	 */
	public static GeoLocation parseGeoLocation(MatchDoc matchDoc, String latitude, String longitude, DeadLetterService deadLetterService) {
	    if (latitude != null && latitude != null)  {
	        try {
	            
	            Double lat = Double.valueOf(latitude);
	            Double lon = Double.valueOf(longitude);
	            
	            // latitude and longitude cannot both be zero, unless the talent or the job is in middle of the south atlantic ocean!
	            if( lat != 0 || lon != 0 ) {
	                return new GeoLocation(lat, lon);
	            }
	        } catch (NumberFormatException | NullPointerException e) {
	        	if (deadLetterService != null)	{	        	
		            deadLetterService.writeDataQualityWarning(
		                    "NumberFormatException parsing geolocation", "lat="+latitude+"; lon="+longitude, matchDoc);
	        	}
	        	else {
	        		logger.warn("ID="+matchDoc.id+", NumberFormatException parsing geolocation", "lat="+latitude+"; lon="+longitude);
	        	}
	        }
	    }
	    return null;
	}	
	
	
	/**
	 * Returns a GeoLocation object against the supplied match doc for the given lat /lon coordinates
	 * @param matchDoc             a reference to the match doc
	 * @param latitude             the latitude to apply
	 * @param longitude            the longitude to apply
	 */
	public static GeoLocation parseGeoLocation(MatchDoc matchDoc, String latitude, String longitude) {
	    return parseGeoLocation(matchDoc, latitude, longitude, null);
	}

	/**
	 * Initializations for the environment and pipeline
	 * @param pipeline the custom options for this pipeline
	 */
	static public Pipeline initPipeline(PipelineOptions options) {      	
	
		RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
	    logger.info("------------------------------------------------------------------");
	    logger.info("  JVM ARGUMENTS ");
	    logger.info("------------------------------------------------------------------");
		List<String> jvmArgs = runtimeMxBean.getInputArguments();
		for (String arg : jvmArgs) {
			logger.info(arg);
		}
	
	    // Dump options
	    logger.info("------------------------------------------------------------------");
	    logger.info("  OPTIONS ");
	    logger.info("------------------------------------------------------------------");
	    logger.info( options.toString() );
	
	    logger.info("GOOGLE_APPLICATION_CREDENTIALS: "+  System.getenv().get("GOOGLE_APPLICATION_CREDENTIALS") );
	    
	    Pipeline pipeline = Pipeline.create( options );
	    return pipeline;
	}

	/**
	 * Dumps the set of Metrics, Distributions and Gauges that were set within the pipeline
	 * @param results   a reference the pipeline results
	 */
	public static void dumpMetrics( PipelineResult results ) {
	
	    logger.info("------------------------------------------------------------------");
	    logger.info("  METRICS ");
	    logger.info("------------------------------------------------------------------");
	    for (MetricResult<Long> counter: results.metrics().queryMetrics( MetricsFilter.builder().build() ).getCounters()) {
	        logger.info(counter.getName() + " = " + counter.getAttempted());
	    }
	
	    logger.info("------------------------------------------------------------------");
	    logger.info("  DISTRIBUTIONS ");
	    logger.info("------------------------------------------------------------------");
	    for( MetricResult<DistributionResult> dist : results.metrics().queryMetrics( MetricsFilter.builder().build() ).getDistributions() ) {
	        DistributionResult d = dist.getAttempted();
	        logger.info( dist.getName() +" = count: "+ d.getCount() +", min: "+ d.getMin() +", mean: "+ d.getMean() +", max: "+ d.getMax() );
	    }
	}

	
    /**
     * Reads a resource file as a string.  Removed lines that start with #.
     * @param path 
     */
    public static String readResource( String path) {
              
        // To make life more readable, the aggregation query is stored in /src/main/resources
        StringBuilder sb = new StringBuilder();
        
        InputStream is = GeneralUtils.class.getResourceAsStream(path);
        if (is == null)  {
        	throw new RuntimeException("Resource '"+path+"' not found.");
        }
        
        try (Scanner scan = new Scanner(is))  {
	        while(scan.hasNextLine() ) {
	            String line = scan.nextLine();
	            if( line.startsWith("#") ) {
	                continue;
	            }
	            sb.append( line );
	            sb.append( "\n" );
	        }
        }
        
        return sb.toString();  
    }
    

    /**
     * Compress a string for BigQuery BYTES type column
     * 
     * @param s
     * @return
     */
    public static byte[] compress(String s)
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();    
        try (DeflaterOutputStream dos = new DeflaterOutputStream(os)) {
            dos.write(s.getBytes());    
        } catch (IOException e) {
			logger.error(e.toString(), e);
		}
        return os.toByteArray();
    }
    
    /**
     * Decompresses from BigQuery data returned from BYTES type column
     * @param base64encoded
     * @return
     */
    public static String decompress(String base64encoded)  {
    	byte compressedBytes[] = Base64.getDecoder().decode(base64encoded);
        ByteArrayOutputStream os = new ByteArrayOutputStream();    
        try (OutputStream ios = new InflaterOutputStream(os)) {
            ios.write(compressedBytes); 
        } catch (IOException e) {
			logger.error(e.toString(), e);
		}
        return new String(os.toByteArray());
    }
    
    
    
    /**
     * Method to convert date to number of days since epoch
     * 
     * @param inDate
     * @param parsedDate
     * @return long days since Epoch
     */
    public static  long daysSinceEpoch(Date inDate) {
        long dateInMs = inDate.getTime();
        long totDays = dateInMs/86400000; // 86400000 is no.of milliseconds per day which is (24*60*60*1000)
        return totDays;
    }
    
    /**
     * Adds the duration to a given counterset and distribution
     * @param duration      the duration of time
     * @param counterSet    a reference to a counterset
     * @param distribution  a reference to the distribution which tracks runtime groups in 1 sec incremements
     */
    public static void stratify(long duration, CounterSet counterSet, Distribution distribution) {

        distribution.update(duration);
        
        int max = 6000;
        if( duration >= max ) {
            counterSet.inc( "> 6s");
        }else {
            for( int i=1000; i < 6000; i+=1000 ) {
                if( duration < i ) {
                    counterSet.inc( "< "+ i/1000 +"s");
                    break;
                }
            }
        }
        
    }

}
