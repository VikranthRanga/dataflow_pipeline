package com.allegis.search.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.io.fs.ResourceId;
import org.apache.beam.sdk.util.MimeTypes;
import org.apache.commons.io.IOUtils;

/**
 * 
 * Maintains Google Cloud Storage utilities
 *
 */
public class CloudStorageUtils {

	/**
	 * Reads a text file from Google Storage and returns the contents as a string.
	 *  
	 * @param fullPath      the path to the file 
	 * @return              the contents of the file
	 * @throws IOException  if the file is not found or cannot be read 
	 */
	public static String readGoogleFile(String fullPath) throws IOException {
        ResourceId resourceId = FileSystems.matchNewResource(fullPath, false /* is_directory */);
        try (
        		ReadableByteChannel chan = FileSystems.open(resourceId);
        		InputStream is = Channels.newInputStream(chan);
        		StringWriter writer = new StringWriter();
        	)
        {
            IOUtils.copy(is, writer, "UTF-8");
            String content = writer.toString();
            return content;
        }
	}

	/**
	 * Reads a text file from Google Storage and returns the contents as a string.
	 *  
	 * @param fullPath      the path to the file 
	 * @return              the contents of the file
	 * @throws IOException  if the file is not found or cannot be read 
	 */
	public static void writeGoogleFile(String fullPath, String content) throws IOException {
	    ResourceId resourceId = FileSystems.matchNewResource(fullPath, false /* is_directory */);
	    try (
	    		WritableByteChannel chan = FileSystems.create(resourceId, MimeTypes.TEXT);
	    		OutputStream os = Channels.newOutputStream(chan);
	    		StringReader reader = new StringReader(content);
	    	)
	    {
	    	IOUtils.copy(reader, os, "UTF-8");
	    }
	}
	
	
	

}
