package com.allegis.search.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * A general utility class for handling date parsing and formatting
 * @author jalpino
 *
 */
public class DateUtils {

	public static String endsInMillisPattern = ".*(\\.[0-9]{3,})$";
    private static DateTimeFormatter multiPatternParser = DateTimeFormatter.ofPattern("[yyyy-MM-dd'T'HH:mm:ss[.SSS'Z'][.SSS'+'0000][.SSSSSSSSS'Z']]"+
                                                                                      "[yyyy-MM-dd'T'HH:mm:ssZ]"+
	                                                                                  "[yyyy-MM-dd]"+
	                                                                                  "[MM/dd/yyyy]");
	
	private static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
	/**
	 * Parse any of multiple date formats into a Date.   
	 * Thread safe.
	 * 
	 * @param strDate      the string format of the date to parse
	 * @return             the date representation of the supplied string, or null if the string cannot be parsed
	 */
	public static Date parse(String strDate) {
		if(strDate == null || strDate.isEmpty() ){		
			return null;
		}
	    if( strDate.contains("T") ) {
	    	// Contains time info 
	        if( ! ( strDate.endsWith("+0000") || strDate.endsWith("Z") ) ) {
	            if( ! Pattern.matches( endsInMillisPattern , strDate) ) {
	                strDate += ".000";
	            }
	            strDate += "Z";
	        }
	        LocalDateTime tmp = LocalDateTime.parse(strDate, multiPatternParser );
	        return Date.from( tmp.atZone( ZoneId.systemDefault()).toInstant() );
	    }else {
	        LocalDate tmp = LocalDate.parse(strDate, multiPatternParser );
	        return Date.from( tmp.atStartOfDay().atZone( ZoneId.systemDefault()).toInstant() );
	    }
	}
	

	/**
	 * Parse any of multiple date formats into a Date and reformat as yyyy-MM-dd.
	 * Thread safe.
	 * 
	 * @param strDate      the date to reformat
	 * @return             the reformated date
	 */
	public static String reformatToDate(String strDate) {
		Date date = parse(strDate);
		if (date == null)  {
			return null;
		}
		return dateFormat.format(date.toInstant().atZone(ZoneId.systemDefault()) );
	}
	
	/**
	 * Formats the supplied date into a String
	 * @param date the date format
	 * @return     the string representation of the date
	 */
	public static String reformatToDate(Date date ) {
	    if (date == null)  {
            return null;
        }
	    return dateFormat.format(date.toInstant().atZone(ZoneId.systemDefault()) );
    }

	
	/**
     * Parse any of multiple date formats into a Date and reformat as yyyy-MM-dd'T'HH:mm:ss.SSS'Z'.
     * Thread safe.
     * 
     * @param strDate      the date to reformat
     * @return             the reformated date
     */
    public static String reformatToDateTime(String strDate) {
        Date date = parse(strDate);
        if (date == null)  {
            return null;
        }
        return dateTimeFormat.format(date.toInstant().atZone(ZoneId.systemDefault()));
    }
    
    /**
     * Formats the supplied date into a String
     * @param date the date format
     * @return     the string representation of the date time
     */
    public static String reformatToDateTime(Date date ) {
        if (date == null)  {
            return null;
        }
        return dateTimeFormat.format(date.toInstant().atZone(ZoneId.systemDefault()) );
    }
    
    /**
     * @return a new Date instance
     */
    public static Date now() {
        // You're probably wondering why someone would create such a useless method? Its a very
        // good question. The reason this exists is so that we can control (via mocking) the 
        // value of the current timestamps when applied to objects during the course of our unit 
        // tests. Without something like this, current timestamp field comparisons, and the objects 
        // that hold them are impossible!
        // For example: 
        //    PowerMockito.spy( DateUtils.class );
        //    PowerMockito.doReturn( <whatever_date_you_want> ).when( DateUtils.class, "now");
        //
        return new Date();
    }
    
}
