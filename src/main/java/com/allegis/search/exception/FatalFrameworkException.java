package com.allegis.search.exception;

/**
 * FatalFrameworkException represent an exception that should be bubbled up to the framework. Generally speaking
 * this is a non-recoverable exception that should be delegated to the framework for handling. In terms of the document
 * associated with the process throwing this exception, it will be treated similar to the {@link FatalDataQualityException}
 * in that it will not progress further in the ingestion pipeline.
 * 
 * @author jalpino
 *
 */
public class FatalFrameworkException extends Exception {

    private static final long serialVersionUID = 1L;
    
    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param   message   the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     */
    public FatalFrameworkException(String message){
        super( message);
    }

}
