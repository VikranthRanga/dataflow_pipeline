package com.allegis.search.exception;

/**
 * FatalIngestException represents an exception that is fatal in terms of further processing a document
 * through the ingestion pipeline. Stages that throw this exception during the transformation of a document
 * will prevent the associated document from progressing through the pipeline. It will be removed from the 
 * main PCollection.
 * 
 * @author jalpino
 *
 */
public class FatalDataQualityException extends Exception {

    private static final long serialVersionUID = 1L;
    
    /**
     * Constructs a new exception with the specified cause and a detail
     * message of <tt>(cause==null ? null : cause.toString())</tt> (which
     * typically contains the class and detail message of <tt>cause</tt>).
     * This constructor is useful for exceptions that are little more than
     * wrappers for other throwables (for example, {@link
     * java.security.PrivilegedActionException}).
     *
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
    public FatalDataQualityException( Throwable cause ) {
        super(cause);
    }
    
    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param   message   the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     */
    public FatalDataQualityException(String message){
        super( message);
    }

}
