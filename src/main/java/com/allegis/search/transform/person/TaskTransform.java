package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.EventStatus;
import com.allegis.search.enums.EventType;
import com.allegis.search.enums.NegativeRecrRelationType;
import com.allegis.search.enums.PositiveRecrRelationType;
import com.allegis.search.enums.Table.Task;
import com.allegis.search.model.HeavyLifting;
import com.allegis.search.model.person.G2Comment;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.PersonShiftPreference;
import com.allegis.search.model.person.ProvenTrackRecordActivityInfo;
import com.allegis.search.model.person.ProvenTrackRecordCounters;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.ResultSet;

/**
 * Sourced from task table.
 * Updates PersonDoc fields:
 * 			interactions
 * 			recruiter_last_activity_date
 */
public class TaskTransform implements Serializable {
	private static final long serialVersionUID = 1L;

    public static List<String> COLUMNS = GeneralUtils.toNameList(
    		Task.Column.Id,
        	Task.Column.Type,
        	Task.Column.Status,
        	Task.Column.Subject,
        	Task.Column.ActivityDate,
        	Task.Column.Activity_Type__c,
        	Task.Column.Description
    );
	
	// create some metrics gathering objects
	private static Counter gotInteractionCounter = Metrics.counter(TaskTransform.class, "Got Interaction");
	private static Counter gotShiftCounter = Metrics.counter(TaskTransform.class, "Got Shift");
	   
   
    // TODO This is a copy of a hardcoded Salesforce HRXML creation.  It may get lost here.
	// Valid types that are considered a interactions 
    private static String taskInteractionDomainValues[] = {
    	"G2",
    	"Reference Check",
    	"Attempted Contact",
    	"BD Call", 
    	"Call",
    	"Email",
    	"Performance Feedback",
    	"Service Touchpoint",
    	"Meeting",
    	"Finish",
    	"Correspondence"
    };
    private static Set<String> taskInteractionDomain = new HashSet<>(Arrays.asList(taskInteractionDomainValues));

    /**
     * 2 years of milliseconds
     */
    private static final long TWO_YEARS_MILS = (24L * 60L * 60L * 1000L) * 730L;
    
    private DeadLetterService deadLetterService;
    
    public TaskTransform(DeadLetterService deadLetterService) {
		super();
		this.deadLetterService = deadLetterService;
	}


    /**
     * Builds upon the supplied person with the provided data 
     * @param resultSet     Event details
     * @param person    the person object to build on
     * @return          the modified person object
     */
    public PersonDoc transform( ResultSet resultSet, PersonDoc person ) {
    	
    	Date twoYearsAgo = new Date(System.currentTimeMillis() - TWO_YEARS_MILS);
    	Date fourYearsAgo = new Date(System.currentTimeMillis() - TWO_YEARS_MILS*2);
    	
    	Set<String> interactions = new HashSet<>();  // Set will make values distinct
    	
    	ProvenTrackRecordCounters ptrCounters = person.meta.provenTrackRecordCounters;
		List<ProvenTrackRecordActivityInfo> ptrActivityInfoList = new ArrayList<ProvenTrackRecordActivityInfo>();
		SortedSet<Date> activityDatesSet = new TreeSet<>();			
			
		PersonShiftPreference shiftPreference = PersonShiftPreference.getInstance();
	    while (resultSet.next()) {
	    	
	    	String type = GeneralUtils.getNullSafeString(resultSet, Task.Column.Type);	    
	    	
	    	if (type == null)
	    		continue;  // safety test
	    	
	    	String id = GeneralUtils.getNullSafeString(resultSet, Task.Column.Id.name());
            String activityType = GeneralUtils.getNullSafeString(resultSet, Task.Column.Activity_Type__c.name());           
            String description = GeneralUtils.getNullSafeString(resultSet, Task.Column.Description.name());
	    	Date activityDate = GeneralUtils.getNullSafeDate(resultSet, Task.Column.ActivityDate.name(), person, this.deadLetterService);
	    	
	    	if (taskInteractionDomain.contains(type)) {
	    		
	    		person.recruiter_activity = true;

	    		// need the most recent interaction date for eagerness
	    		if (person.recruiter_last_activity_date == null || (activityDate != null && person.recruiter_last_activity_date.before(activityDate)))  {	    			
	    			person.recruiter_last_activity_date = activityDate;
	    		}

	    		String status = GeneralUtils.getNullSafeString(resultSet, Task.Column.Status);
	    		
	    		// add to interactions for only these specific events
	    		if ( EventStatus.COMPLETED.equals( status ) ) {
			    	String subject = GeneralUtils.getNullSafeString(resultSet, Task.Column.Subject);
		    		if ( EventType.G2.equals( type ) && subject != null && subject.contains("Completed") ) {
		    			interactions.add( EventType.G2.type() );
		    		}
		    		else if( EventType.REF_CHECK.equals( type )) {
		    			interactions.add(  EventType.REF_CHECK.type().replaceAll(" ","") ); // strip whitespace; TODO: why?
		    		}
	    		}
	    		
	    		if(null != activityDate && activityDate.after(fourYearsAgo)) {
	    			// capture heavy lifting mentioned in the last 4 years
	    			HeavyLifting.extract(person, description, "Task", type, activityDate.getYear() + 1900);
	    			boolean gotShift = shiftPreference.extract(person, description, "Task", type, activityDate.getYear() + 1900);
	                if (gotShift)  {
	                	gotShiftCounter.inc();
	                }
	    		}
	    	}
	    	
	    	// Capture G2 Comments to check for Eagerness
	    	if ( EventType.G2.equals( type ) && null != description && description.trim().length() > 0 && null != activityDate) {
	    		person.meta.g2Comments.add( new G2Comment(id, description, activityDate) );
	    	}
	    	
	    	
	    	// Capture PTR Counters - positive recruiter relationship
    		if (PositiveRecrRelationType.isPostiveType(activityType)) {
    			ptrCounters.rrPositiveCounter++;
    		}
    		
    		// Capture PTR Counters - negative recruiter relationship
    		if (NegativeRecrRelationType.isNegativeType(activityType)) {
    			ptrCounters.rrNegativeCounter++;
    		}
    		
    		// Capture PTR Counters - event type is Service Touchpoint/Performance Feedback
    		if (EventType.SERV_TOUCHPOINT.equals(type) || EventType.PERF_FEEDBACK.equals(type)) {
    			ptrCounters.pfCounter++;
    		}
    		
    		// Capture datapoints pf_detail and recency_month for PTR within last two years
	    	if(null != activityDate && activityDate.after(twoYearsAgo)) {

	    		// recency
	    		if(EventType.CALL.equals(type) || EventType.BD_CALL.equals(type) || EventType.G2.equals(type) ||
	    				EventType.MEETING.equals(type) || EventType.FINISH.equals(type) || EventType.CORRESPONDENCE.equals(type)) {
	    			activityDatesSet.add(activityDate);
	    		}
	    			    		
	    		// Store Dates and Descriptions to get top 10 recent descriptions
	    		if (EventType.SERV_TOUCHPOINT.equals(type) || EventType.PERF_FEEDBACK.equals(type)) {
	    			if ( null != description && description.length() > 10 ) {
	    				// replace ; in the description so that it wont create any issue when we do concatenation on all descriptions with ; 
	    				description = description.replaceAll(";", "");
	    				ptrActivityInfoList.add(new ProvenTrackRecordActivityInfo(id, description, activityDate));
	    			}
	    		}	
	    	}
	    }
	    if ( !activityDatesSet.isEmpty() ) {
	    	// Check if another upstream transform such as EventTransform has already added some activity dates	    	
	    	if ( ptrCounters.lastActivityDate == null ) {
	    		ptrCounters.lastActivityDate = activityDatesSet;
	    	} else {
	    		ptrCounters.lastActivityDate.addAll(activityDatesSet);
	    	}
    	}
	    if ( !ptrActivityInfoList.isEmpty() ) {
	    	// Check if another upstream transform such as EventTransform has already added some activity info	    	
	    	if ( person.meta.provenTrackRecordActivityInfo == null ) {
	    		person.meta.provenTrackRecordActivityInfo = ptrActivityInfoList;
	    	} else {
	    		person.meta.provenTrackRecordActivityInfo.addAll(ptrActivityInfoList);
	    	}
	    	
	    }	    
	    
	    person.meta.provenTrackRecordCounters = ptrCounters;
	    	    

	    if ( ! interactions.isEmpty())  {	    	
	    	// Check if another upstream transform such as EventTransform has already added some interactions
	    	if (person.interactions == null)  {
	    		person.interactions = interactions;
	    	}
	    	else  {
	    		person.interactions.addAll(interactions);
	    	}
	    	gotInteractionCounter.inc();
	    }
	    
		return person;
    }

}
