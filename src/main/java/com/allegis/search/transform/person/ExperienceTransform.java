package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.Table.Experience;
import com.allegis.search.model.person.CertHist;
import com.allegis.search.model.person.EduHist;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.ResultSet;


/**
 * ExperienceTransform
 * Look at ColumnsMapping for fields
 *
 * @author dhartwig
 *
 */
public class ExperienceTransform implements Serializable {
    private static final long serialVersionUID = 1L;


    public static List<String> COLUMNS = GeneralUtils.toNameList(
    		Experience.Column.Id,
			Experience.Column.Type__c,
			Experience.Column.Organization_Name__c,
			Experience.Column.Title__c,
			Experience.Column.Start_Date__c,
			Experience.Column.End_Date__c,
			Experience.Column.Certification__c,
			Experience.Column.Graduation_Year__c,
			Experience.Column.School_Name__c,
			Experience.Column.Major__c,
			Experience.Column.Degree_Level__c,
			Experience.Column.Notes__c
    );


    // create some metrics gathering objects
    private static Counter work = Metrics.counter(ExperienceTransform.class, "Got Work");
    private static Counter educate = Metrics.counter(ExperienceTransform.class, "Got Education");
    private static Counter train = Metrics.counter(ExperienceTransform.class, "Got Training");
    private static Counter nullType = Metrics.counter(ExperienceTransform.class, "Null Type");

    
    private DeadLetterService deadLetterService;
    
    public ExperienceTransform(DeadLetterService deadLetterService) {
		super();
		this.deadLetterService = deadLetterService;
	}


    /**
     * Builds upon the supplied person with the provided data
     *
     * @param data
     *            Talent Account and Contact details
     * @param person
     *            the person object to build on
     * @return the modified person object
     */
    public PersonDoc transform(ResultSet resultSet, PersonDoc person) {
        boolean gotEducation = false;
        boolean gotTraining = false;
        boolean gotWork = false;

        while (resultSet.next()) {
            String type = GeneralUtils.getNullSafeString(resultSet, Experience.Column.Type__c.name());
            if (type == null) {
                nullType.inc();
                continue;
            }
            switch (type) {
            case "Education":
                educate(resultSet, person);
                gotEducation = true;
                break;
            case "Training":
                train(resultSet, person);
                gotTraining = true;
                break;
            case "Work":
                work(resultSet, person);
                gotWork = true;
                break;
            }
        }

        if (gotEducation)
            educate.inc();

        if (gotTraining)
            train.inc();

        if (gotWork)
            work.inc();

        return person;
    }

    /**
     * Add to index field employment_hist
     *
     * @param resultSet
     * @param person
     */
    private void work(ResultSet resultSet, PersonDoc person) {
        List<Employment> history = person.employ_hist;
        if (history == null) {
            history = new ArrayList<>();
            person.employ_hist = history;
        }
        Employment employment = new Employment();
        history.add(employment);
        employment.OrganizationName = GeneralUtils.getNullSafeString(resultSet, Experience.Column.Organization_Name__c.name());
        employment.PositionTitle = GeneralUtils.getNullSafeString(resultSet, Experience.Column.Title__c.name());
        employment.Description = GeneralUtils.getNullSafeString(resultSet, Experience.Column.Notes__c.name());
        employment.StartDate = GeneralUtils.getNullSafeDate(resultSet, Experience.Column.Start_Date__c.name(), person, deadLetterService);
        employment.EndDate = GeneralUtils.getNullSafeDate(resultSet, Experience.Column.End_Date__c.name(), person, deadLetterService);
        employment.Id = GeneralUtils.getNullSafeString(resultSet, Experience.Column.Id.name());
        work.inc();
    }

    /**
     * Add to index field cert_hist
     *
     * @param resultSet
     * @param person
     */
    private void train(ResultSet resultSet, PersonDoc person) {
        List<CertHist> certHist = person.cert_hist;
        if (certHist == null) {
            certHist = new ArrayList<CertHist>();
            person.cert_hist = certHist;
        }

		String year = GeneralUtils.getNullSafeString(resultSet, Experience.Column.Graduation_Year__c.name());
		Date gradYear = null;
        if( null != year ) {
            gradYear = GeneralUtils.getNullSafeDate( year + "-01-01", Experience.Column.Graduation_Year__c.name(), person, deadLetterService);
        }

        certHist.add(new CertHist(GeneralUtils.getNullSafeString(resultSet, Experience.Column.Certification__c.name()),
		        gradYear));
        train.inc();
    }

    /**
     * Add to index field edu_hist
     *
     * @param resultSet
     * @param person
     */
    private void educate(ResultSet resultSet, PersonDoc person) {
        List<EduHist> eduHist = person.edu_hist;
        if (eduHist == null) {
            eduHist = new ArrayList<EduHist>();
            person.edu_hist = eduHist;
        }

	    String gradYear = GeneralUtils.getNullSafeString(resultSet, Experience.Column.Graduation_Year__c.name());
	    if (null==gradYear)
	    	gradYear = "0000";
	    
	    String schoolName = GeneralUtils.getNullSafeString(resultSet, Experience.Column.School_Name__c.name());
	    if (null==schoolName)
	    	schoolName="UNKNOWN";
	    
        eduHist.add(new EduHist(schoolName,
                GeneralUtils.getNullSafeString(resultSet, Experience.Column.Degree_Level__c.name()),
				gradYear,
				GeneralUtils.getNullSafeString(resultSet, Experience.Column.Major__c.name())));
        
        educate.inc();
    }

}
