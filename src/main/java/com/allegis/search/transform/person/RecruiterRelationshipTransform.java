package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import com.google.gson.Gson;

import com.allegis.search.enums.Table.TalentPredictions;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.RecruiterRelationshipOutput;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.Struct;
import com.google.common.reflect.TypeToken;

/**
 * The recruiter relationship transform is responsible for applying the transformations
 * {@see PersonDoc} for purposes of the forgotten relationships
 * 
 * @author jperecha
 *
 */
public class RecruiterRelationshipTransform implements Serializable {

	private static final long serialVersionUID = 4947966459388682038L;
	
    private static final String AGG_BUCKET_FOR_ANY = "-ANY-";
    
    /**
     * Builds upon the supplied person with the provided data
     * 
     * @param resultsSet Talent Predictions details
     * @param person the person object to build on
     * @return 		the modified person object
     */
    public PersonDoc transform( Struct resultsSet, PersonDoc person ) {
    	String recruiterRelationshipJSON = GeneralUtils.getNullSafeString( resultsSet, TalentPredictions.Column.recruiterRelationships );
        if( recruiterRelationshipJSON != null ) {
            @SuppressWarnings("serial")
            List<RecruiterRelationshipOutput> recruiterRelationshipOutput = new Gson().fromJson(recruiterRelationshipJSON, new TypeToken<List<RecruiterRelationshipOutput>>() {}.getType());
            
            if (recruiterRelationshipOutput != null && !recruiterRelationshipOutput.isEmpty()) {
            	recruiterRelationshipOutput = addAnyForgottenRelationship(recruiterRelationshipOutput);
            }

            person.recruiter_relationships = recruiterRelationshipOutput;
        }

    	return person;
    }
    
    /**
     * addAnyForgottenRelationship: Checks all relationships for the user and finds the max touch date
     * and max score of all recruiter relationships. It adds the max touch date and the max score with 
     * (which are not required to occur on the same relationship) the -ANY- record.
     * @param relationshipList
     * @return List<RecruiterRelationshipOutput> with added -ANY- user, max score and max date
     */
    private List<RecruiterRelationshipOutput> addAnyForgottenRelationship(List<RecruiterRelationshipOutput> relationshipList){
    	
		RecruiterRelationshipOutput maxScore = relationshipList.parallelStream()
	            .max(Comparator.comparing(r -> ((RecruiterRelationshipOutput) r).score))
	            .get();
		
		RecruiterRelationshipOutput maxDate = relationshipList.parallelStream()
	            .max(Comparator.comparing(r -> ((RecruiterRelationshipOutput) r).touch_date))
	            .get();
	
		relationshipList.add(new RecruiterRelationshipOutput(AGG_BUCKET_FOR_ANY,maxScore.score,maxDate.touch_date));
		
		return relationshipList;
    }

}
