package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.allegis.candidate.profile.AbstractProfileSource;
import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.candidate.profile.HistoryRange;
import com.allegis.candidate.profile.ProfileSource.SourceType;
import com.allegis.search.enums.OrderStatusRelevance;
import com.allegis.search.enums.Table;
import com.allegis.search.enums.Table.Account;
import com.allegis.search.enums.Table.Opportunity;
import com.allegis.search.enums.Table.Order;
import com.allegis.search.model.GeoLocation;
import com.allegis.search.model.HeavyLifting;
import com.allegis.search.model.OrderEmploymentBean;
import com.allegis.search.model.job.JobSkill;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.PersonShiftPreference;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.ReadContext;
import com.google.cloud.spanner.ResultSet;
import com.google.cloud.spanner.Struct;


public class OrderTransform implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static List<String> COLUMNS = GeneralUtils.toNameList(
        	Order.Column.EffectiveDate,
        	Order.Column.ATS_Job__c,
        	Order.Column.OpportunityId
    );

    
    private DeadLetterService deadLetterService;

    public OrderTransform(DeadLetterService deadLetterService) {
        super();
        this.deadLetterService = deadLetterService;
    }

    

	private Counter gotAssociatedPosition = Metrics.counter(OrderTransform.class.getSimpleName(), "Got Associated Position");    
	private Counter fixGeo = Metrics.counter(OrderTransform.class.getSimpleName(), "Fixed GEO");    
	private Counter opportunityNotFound = Metrics.counter(OrderTransform.class.getSimpleName(), "Order-opportunity Not Found");    
	private Counter applicantNotHas_Application = Metrics.counter(OrderTransform.class.getSimpleName(), "Applicant !Has_Application");    
	private Counter gotShiftCounter = Metrics.counter(TaskTransform.class, "Got Shift");
   


    /**
     * Use to Process Order Table ResultSet <br>
     * 
     * @param person
     * @param resultSet
     * @return application effectiveDates
     */
    public List<Date> transform(PersonDoc person, ResultSet resultSet, DatabaseClient spannerClient) {
        List<Date> effectiveDates = new ArrayList<Date>();
        DateTime currDateTime = new DateTime(person.submitTime);
        
        List<EmploymentBean> psudoEmpHist = new ArrayList<>();
        person.recent_applications = new ArrayList<String>();
        
        while (resultSet.next()) {
            Date effectiveDate = GeneralUtils.getNullSafeDate(resultSet,  Order.Column.EffectiveDate, person, deadLetterService);
            String atsJob = GeneralUtils.getNullSafeString(resultSet, Order.Column.ATS_Job__c);
            boolean hasApplication = GeneralUtils.getNullSafeBoolean(resultSet, Order.Column.Has_Application__c);
            String jobId = GeneralUtils.getNullSafeString(resultSet,  Order.Column.OpportunityId);
            String status = GeneralUtils.getNullSafeString(resultSet,  Order.Column.Status);
            String submittalNotProceedingReason = GeneralUtils.getNullSafeString(resultSet, Order.Column.Submittal_Not_Proceeding_Reason__c);            
            
            if (effectiveDate == null) { 
            	continue;  // Cannot do anything w/o a date
            }
            
            // ... FROM Order WHERE ATS_Job__c = NULL ...
            // orderWrap.order.Has_application__c == true
            if(hasApplication && atsJob == null) {
                effectiveDates.add(effectiveDate);
                if (null!=jobId) {  // if application within 90s, add the JobI	d to array
	                int calcDays = Days.daysBetween(new DateTime(effectiveDate), currDateTime).getDays();
	                if (calcDays <= 90)  {
	                	person.recent_applications.add(jobId);
	                }
                }
            }
            
            
            OrderStatusRelevance orderStatus = OrderStatusRelevance.find(status, submittalNotProceedingReason);
            if (jobId == null || orderStatus == null)  {
            	continue;  // Unusable associations
            }
                       
            
            // TODO Remove this DQ analysis after it is shown and discussed (shouldn't we include these contradictory applicants above where we look for recent_applications)
            if(atsJob == null) {
                int calcDays = Days.daysBetween(new DateTime(effectiveDate), currDateTime).getDays();
                if (calcDays <= 90)  {
                	if(orderStatus == OrderStatusRelevance.Applicant && ! hasApplication) {
                		applicantNotHas_Application.inc();
                	}
                }
            }

            /////////  From here on down we are processing opportunity associations.   ///////////
            double lookBackYears;
            switch (orderStatus) {
	            case Started: lookBackYears = 3.5; break;
	            case Applicant: lookBackYears = 1.3; break;
	            default: lookBackYears = 3;
            }
            
            Date lookBackThershold = new Date((long)(person.submitTime.getTime() - (lookBackYears * ONE_YEAR_MILS)));
            
            if (effectiveDate.after(lookBackThershold))  {
        		// Lets get create a profile from order data            	
            	OrderEmploymentBean psudoEmpBean =  constructPsudoEmploymentHistoryBlock(spannerClient, orderStatus, jobId, effectiveDate, person);
	            if (psudoEmpBean != null)  {
	            	psudoEmpBean.orderId = GeneralUtils.getNullSafeString(resultSet, Order.Column.Id);
		            psudoEmpHist.add(psudoEmpBean);   
	            }
            }
        }
        
        if ( ! psudoEmpHist.isEmpty()) {
        	gotAssociatedPosition.inc();
        	
        	person.associated_jobs = psudoEmpHist;
        	
            HeavyLifting.extractPositiveSentiment(person, psudoEmpHist, SourceType.AJ);
            boolean gotShift = PersonShiftPreference.getInstance().extract(person, psudoEmpHist,  SourceType.AJ);            
            if (gotShift)  {
            	gotShiftCounter.inc();
            }
            
        	double latitude = 0;
        	double longitude = 0;
        	int count = 0;
            if (person.geolocation == null)   {
            	
            	for (Iterator<EmploymentBean> iterator = psudoEmpHist.iterator(); iterator.hasNext();) {
            		OrderEmploymentBean psudoEmpBean = (OrderEmploymentBean) iterator.next();	
            		if (psudoEmpBean.geoLocation != null) {
            			count++;
            			latitude += psudoEmpBean.geoLocation.lat;
            			longitude += psudoEmpBean.geoLocation.lon;
            		}           		
				}
            	
            	if (count > 0)  {
            		// save average geolocation 
            		fixGeo.inc();	            	
            		person.geolocation = new GeoLocation(latitude/count, longitude/count);	  
            		person.associated_jobs_geolocation = true;
            	}
            }
        }
        
        return effectiveDates;
    }
    
    
    static List<String> opportunity_columns = GeneralUtils.toNameList(
    		Opportunity.Column.Name,
    		Opportunity.Column.Req_Job_Title__c,
    		Opportunity.Column.EnterpriseReqSkills__c,
    		Opportunity.Column.Req_Job_Description__c,
    		Opportunity.Column.Req_Qualification__c,
    		Opportunity.Column.Req_GeoLocation__Latitude__s,
    		Opportunity.Column.Req_GeoLocation__Longitude__s,
    		Opportunity.Column.Req_Work_Remote__c,
    		Opportunity.Column.Req_Worksite_City__c,
    		Opportunity.Column.Req_Worksite_State__c,
    		Opportunity.Column.Req_Worksite_Country__c,
    		Opportunity.Column.AccountId
    );
    

    private static final long ONE_YEAR_MILS = (24L * 60L * 60L * 1000L) * 365L;
    
    private OrderEmploymentBean constructPsudoEmploymentHistoryBlock(DatabaseClient spannerClient, OrderStatusRelevance status, String opportunityId, Date effectiveDate, PersonDoc person){       
    	
    	OrderEmploymentBean psudoEmpBean = null;
    	String accountId=null;
    	
    	try (ReadContext readContext = spannerClient.singleUse())  // force quick closure
        {
            SpannerKey primaryKey = SpannerKey.of( opportunityId );
            Struct oppRow = readContext.readRow(Table.Opportunity.TABLE , primaryKey.get(), opportunity_columns);

            if (oppRow == null) {
                opportunityNotFound.inc();
                return null;
            }
            
            psudoEmpBean = new OrderEmploymentBean(GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Name));
            psudoEmpBean.opportunityId = opportunityId;
            psudoEmpBean.status = status.name();
            psudoEmpBean.setPositionTitle(GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_Job_Title__c));   
            
            String description = GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_Job_Description__c);
            String qualifications = GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_Qualification__c);
            if (qualifications!= null && ! qualifications.trim().isEmpty()) {
            	description += " QualificationSummary: "+qualifications; // append this extra text for term extraction
            }            
            psudoEmpBean.setPositionDescription(GeneralUtils.scrubCRLFTab(description));
            
            String latitude = GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_GeoLocation__Latitude__s);
            String longitude = GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_GeoLocation__Longitude__s);
            
            String skillsJson = GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.EnterpriseReqSkills__c);
            populateSkills(skillsJson, person, psudoEmpBean);

       
			if (!GeneralUtils.getNullSafeBoolean(oppRow, Opportunity.Column.Req_Work_Remote__c.toString())) {
				psudoEmpBean.geoLocation = GeneralUtils.parseGeoLocation(person, latitude, longitude);
			}

            Date endDate;
            Date startDate;
            
            // experienceFactor is in months
            long experienceMils = (ONE_YEAR_MILS / 12) * status.getExperienceFactor();

            if (status == OrderStatusRelevance.Started)  {
            	// go forward by 1/2 the time because the person was performing these skills for some time after the effective date
            	startDate = new Date(effectiveDate.getTime() - experienceMils / 2);
            	endDate = new Date(effectiveDate.getTime() + experienceMils / 2);
            	
            	// but do not go past the current date
            	if (endDate.after(person.submitTime))  {
            		long shiftMils = endDate.getTime() - person.submitTime.getTime();
            		
            		startDate =  new Date(startDate.getTime() - shiftMils);  // shift back by difference  

            		endDate = person.submitTime;    // today
            	}
            }
            else {
            	// go back from effective date
				startDate = new Date(effectiveDate.getTime() - experienceMils);            	
            	endDate = effectiveDate;
            }
             
            psudoEmpBean.setCity(GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_Worksite_City__c));   
            psudoEmpBean.setState(GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_Worksite_State__c));   
            psudoEmpBean.setCountry(GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.Req_Worksite_Country__c));   
            accountId = GeneralUtils.getNullSafeString(oppRow,  Opportunity.Column.AccountId);   

            psudoEmpBean.setHistoryRange(new HistoryRange(startDate, endDate));            
        }      
    	
    	if (null!=accountId) {
    		// for Starts, lookup account name and further details: we want accurate work history data
	    	try (ReadContext readContext = spannerClient.singleUse())  // force quick closure
	        {
	            SpannerKey primaryKey = SpannerKey.of( accountId );
	            //name,Trade_Name_DBA__c,Nickname__c,jigsaw,jigsawcompanyid,dunsnumber,DandbCompanyId,DUNS__c,Domestic_Ultimate_DUNS__c,Global_Ultimate_DUNS__c,Parent_HQ_DUNS__c
	            Struct acctRow = readContext.readRow(Table.Account.TABLE , primaryKey.get(), Arrays.asList(
	            		"Name","Trade_Name_DBA__c","Nickname__c","JigsawCompanyId","DUNS__c","Domestic_Ultimate_DUNS__c","Global_Ultimate_DUNS__c","Parent_HQ_DUNS__c"));
	            if (acctRow != null) {
	            	psudoEmpBean.accountName = GeneralUtils.getNullSafeString(acctRow,  Account.Column.Name); 
	            	StringBuilder sb = new StringBuilder();
	            	appendJson(sb, acctRow, "Trade_Name_DBA__c");
	            	appendJson(sb, acctRow, "Nickname__c");
	            	appendJson(sb, acctRow, "JigsawCompanyId");
	            	appendJson(sb, acctRow, "DUNS__c");
	            	appendJson(sb, acctRow, "Domestic_Ultimate_DUNS__c");
	            	appendJson(sb, acctRow, "Global_Ultimate_DUNS__c");
	            	appendJson(sb, acctRow, "Parent_HQ_DUNS__c");
	            	if (sb.length()>0) {
	            		sb.append("}");
	            		psudoEmpBean.accountDetails = sb.toString();
	            	}
	            }
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
    	}
    	
        return psudoEmpBean;       
    }
   

    
    private void appendJson(StringBuilder sb, Struct acctRow, String col) {
    	String s = GeneralUtils.getNullSafeString(acctRow,  col);
    	if (StringUtils.isNotBlank(s)) {
    		if (sb.length()==0) {
    			sb.append("{");
    		} else {
    			sb.append(", ");
    		}
    		sb.append("\"" + col + "\": " + "\"" + s + "\"");
    	}
	}



	private void populateSkills(String skillsJson, PersonDoc person, OrderEmploymentBean psudoEmpBean) {
        if(skillsJson == null)
        	return;

        List<JobSkill> skills = JobSkill.extractJobSkills(skillsJson, person, Opportunity.Column.EnterpriseReqSkills__c.name(), deadLetterService);
		
		if (skills == null)
			return;
        
        
    	String desiredSkills = "";
    	String allSkills = "";
    	
        for(JobSkill jobSkill : skills) {
            // TODO toLowerCase() seems wrong to me. Because the are fed to the DVS 
            // and it causes acronyms to become skills.  But that is what happens in the Aspire version. 
        	
        	if (allSkills.isEmpty())
        		allSkills += " EnterpriseReqSkills: ";
        	else 
        		allSkills += AbstractProfileSource.SKILL_VALUE_DELIMITER;
        	
        	allSkills += jobSkill.skill_name;       	

            if(jobSkill.desired)  {
            	if ( ! desiredSkills.isEmpty()) 
            		desiredSkills += AbstractProfileSource.SKILL_VALUE_DELIMITER;
            		
                desiredSkills += jobSkill.skill_name;
            }
        }
        
        if ( ! allSkills.isEmpty())  // add to the description so that skills can be dated
        	psudoEmpBean.setPositionDescription(psudoEmpBean.getPositionDescription() + allSkills);
      
        // TODO hack: pluck this out later and append to un-dated (but structured) skills. 
        psudoEmpBean.skills = desiredSkills;  
    }

}
