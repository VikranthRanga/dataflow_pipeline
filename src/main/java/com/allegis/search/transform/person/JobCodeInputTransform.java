package com.allegis.search.transform.person;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Distribution;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.MetricsNamespace;
import com.allegis.search.http.RestClient;
import com.allegis.search.model.person.JobCodeOutput;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.CounterSet;
import com.allegis.search.utils.DateUtils;
import com.allegis.search.utils.HashingUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Transform job code classifier specific POJO into an json object.
 * 
 * @author jperecha
 *
 */
public class JobCodeInputTransform implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger( JobCodeInputTransform.class );
	private static final long serialVersionUID = -5675527455304846455L;
	
	private static String CONFIDENCE_PARAM = "confidence";
	private static String ONET_CODE_PARAM = "onet_code";
	private static String ONET_DESCR_PARAM = "onet_description";
	private static String UNSUCCESS_MSG = "unsuccessful";
	private static final String SOCKET_TIMEOUT_EXCEPTION_MSG = "Read timed out";
	private static final String PREDICTIONS_MODEL_FAIL_ERROR = "Model unable to be called";
	
	private static final Integer CONNECTION_TIMEOUT = 3 * 1000;//3 seconds
	private static final Integer SOCKET_TIMEOUT = 3 * 1000; //3 seconds
	private static final Integer CONNECTION_REQUEST_TIMEOUT = 3 * 1000; //3 seconds

	// create some metrics gathering objects
	private CounterSet badStatusCount = new CounterSet("JobCode Status");	
    private static Counter jobCode = Metrics.counter(JobCodeInputTransform.class, "Got Job Code Classifier");  
    
    private CounterSet jobRespCounters = new CounterSet("Job Code Response");	
    private Distribution jobRespTime = Metrics.distribution(MetricsNamespace.STAGE.name(), "Job Code Response");
	
	
	/**
	 * Default Constructor
	 */
	public JobCodeInputTransform() {
		super();	
	}
	
	/**
	 * Builds upon the job code classifier work history to create an input json object
	 * which will be sent to the job code rest api.
     * 
	 * @param person	the person object to build on
	 * @param hist		the job code classifier specific work history
	 * @param restClient	the rest client
	 * @param url		the request url
	 * @return the modified person object
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PersonDoc transform( PersonDoc person, JsonArray workHistArr, RestClient restClient, String url, DeadLetterService deadLetterService ) throws Exception {
		
		if ( workHistArr != null && workHistArr.size() > 0 ) {
			makeJobCodeRequest( person, workHistArr, restClient, url, deadLetterService);
		}
		
		return person;	
	}
	
	/**
	 * Create a request string for the job code classifier api.
	 * 
	 * @param person	the person object to build on
	 * @param workHistArr	the json array of job code classifier work history
	 * @param restClient the rest client
	 * @param url	the request url
	 * @throws URISyntaxException 
	 */
	private void makeJobCodeRequest( PersonDoc person, JsonArray workHistArr, RestClient restClient, String url, DeadLetterService deadLetterService ) throws Exception {
		
		JsonArray wholisticJobCodeArr = null;
		
		// Set timeouts for rest connection and response 
		RequestConfig Default = RequestConfig.DEFAULT;
	    RequestConfig requestConfig = RequestConfig.copy(Default)
	            .setConnectTimeout(CONNECTION_TIMEOUT)
	            .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT)
	            .setSocketTimeout(SOCKET_TIMEOUT)
	            .build();
	    
		HttpPost httpPost = new HttpPost();
		httpPost.setURI( new URI(url.toString()) );
		httpPost.setEntity(new StringEntity(workHistArr.toString(), ContentType.APPLICATION_JSON));		
		httpPost.setConfig(requestConfig);
		long startTime = System.currentTimeMillis();
		
		try ( CloseableHttpResponse response = restClient.makeRequest( httpPost ); ){            
			
			jobCode.inc();
            int statusCode = response.getStatusLine().getStatusCode();           
            // We got a response, check if it includes an content
            if( statusCode == HttpStatus.SC_OK ) {     
            	String content = IOUtils.toString( response.getEntity().getContent(), "UTF-8" );
            	if ( StringUtils.isNotBlank(content) ) {
            		// Convert response to json object
            		wholisticJobCodeArr = parseResponse(content, deadLetterService, person); 
            	}
            }           
            else {
            	logger.warn("Jobcode classifier api returned status code="+statusCode+" for Id="+person.id);
            	badStatusCount.inc(String.valueOf(statusCode));  
            }
            
            // ONLY update the talent if we received the wholistic section in the response
            if ( null != wholisticJobCodeArr && !wholisticJobCodeArr.isJsonNull() && wholisticJobCodeArr.size() > 0 ) {
            	person.job_functions = applyJobCodesBlock( wholisticJobCodeArr );
            	person.meta.jobcode_hash = HashingUtils.getHash( workHistArr.toString() );
                person.meta.jobcode_last_modified_date = DateUtils.reformatToDateTime( DateUtils.now() );
                person.meta.jobcode_needsUpdate = true;
            }
            
		} catch (Exception x) {
			
			jobRespCounters.inc(x.getClass().getSimpleName());
			logger.error("Error while calling job code classifier api: "+x.getMessage() + " for Id="+person.id);
			if ( x.getMessage().equalsIgnoreCase(SOCKET_TIMEOUT_EXCEPTION_MSG) ) {
				deadLetterService.writePredictionTimeoutFailure( x.getMessage(), person );
			} else {			
				throw x;
			}
        } finally  {        	
        	stratify(System.currentTimeMillis() - startTime, jobRespCounters, jobRespTime);
        	httpPost.reset();
        }
	}
	
	private void stratify(long duration, CounterSet counterSet, Distribution distribution) {

		distribution.update(duration);
		
		int max = 6000;
		if( duration >= max ) {
		    counterSet.inc( "> 6s");
		}else {
    		for( int i=1000; i < 6000; i+=1000 ) {
    		    if( duration < i ) {
    		        counterSet.inc( "< "+ i/1000 +"s");
    		        break;
    		    }
    		}
		}
	}
	
	/**
	 * Parse the output response of job code classifier api
	 * 
	 * @param content the string response
	 * @return the parsed response
	 */
	public static JsonArray parseResponse( String content, DeadLetterService deadLetterService, PersonDoc person ) {
		
		JsonArray outputArr = null;
		JsonParser parser = new JsonParser();
		JsonElement responseJson = parser.parse( content );
		String status = responseJson.getAsJsonObject().get("status").getAsString();
		if (status.equalsIgnoreCase(UNSUCCESS_MSG)) {
			String message = responseJson.getAsJsonObject().get("message").getAsString();
			if ( message.equalsIgnoreCase(PREDICTIONS_MODEL_FAIL_ERROR) ) {
				deadLetterService.writePredictionModelFailure(message, person);
			}
		}
		else {
			JsonElement outputElement =  responseJson.getAsJsonObject().get("wholistic_job_codes");		
			if ( !outputElement.isJsonNull() ) {
				outputArr =  responseJson.getAsJsonObject().getAsJsonArray("wholistic_job_codes");			
			}
		}
		return outputArr;
	}
	
	/**
	 * Builds and applies a set of holistic job code "blocks" from the given Job Code Array (i.e. a subset of the Job Code Classifier output) 
	 * and applies it onto the supplied person
	 * 
	 * @param wholisticJobCodeArr	the json array of job codes
	 */
	public List<JobCodeOutput> applyJobCodesBlock( JsonArray wholisticJobCodeArr ) {
		
		List<JobCodeOutput> jobCodeOutput = new ArrayList<JobCodeOutput>();
        if( null != wholisticJobCodeArr && ! wholisticJobCodeArr.isJsonNull() ) {
    		for ( int i = 0; i < wholisticJobCodeArr.size(); i++ ) {
        		JsonObject wholisticJobCodeObj = wholisticJobCodeArr.get(i).getAsJsonObject();
        		jobCodeOutput.add( new JobCodeOutput(wholisticJobCodeObj.get( CONFIDENCE_PARAM ).getAsDouble(),
        				wholisticJobCodeObj.get( ONET_CODE_PARAM ).getAsString(), wholisticJobCodeObj.get( ONET_DESCR_PARAM ).getAsString()) ); 		
        	}
        }
		return jobCodeOutput;
	}
}
