package com.allegis.search.transform.person;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.allegis.candidate.profile.ContactInfoBean;
import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.search.model.OrderEmploymentBean;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.IndustryCodeWorkHist;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.utils.WorkHistUtils;

/**
 * Extract data points for industry code classifier. Transform employment history and data points into 
 * an industry code classifier specific POJO.
 * 
 * @author jperecha
 *
 */
public class IndustryCodeWorkHistTransform implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String AG_WORK_HIST_SOURCE = "AG";
    private static final String RESUME_WORK_HIST_SOURCE = "Resume";
    private static final int WORK_HIST_BLOCKS_SIZE = 25;
    private static int count = 0;
        
    public IndustryCodeWorkHistTransform() {
    	super();
    }
    
    /**
     * Builds upon the supplied employment history to create classifier specific 
     * work history input.
     * 
     * @param person	the person object to build on
     * @param hist		the list of classifier specific work history
     * 
     * @return the modified list of classifier specific work history
     * @throws IOException
     */
    public static List<IndustryCodeWorkHist> transform( PersonDoc person, List<IndustryCodeWorkHist> hist ) throws IOException {
    	 
    	if ( null != person.employ_hist ) {
    		// Making a copy of employ_hist to avoid mutation issues during sorting
    		List<Employment> employHist = person.employ_hist;
	    	WorkHistUtils.sortWorkHist(employHist);
	    	
	    	int maxWorkHistBlocks = employHist.size();
	    	if ( maxWorkHistBlocks > WORK_HIST_BLOCKS_SIZE ) {
	    		maxWorkHistBlocks = Integer.min(maxWorkHistBlocks, WORK_HIST_BLOCKS_SIZE);
	    	}
	    	
	    	for ( int i=0; i<maxWorkHistBlocks; i++ )
	    	{   	
	    		IndustryCodeWorkHist industryCodeWorkHist = new IndustryCodeWorkHist();
	    		industryCodeWorkHist.talentId = person.id; 
	    		
	    		Employment histblock = employHist.get(i);	 
	    		industryCodeWorkHist.workHistId = histblock.Id;
	    		industryCodeWorkHist.accountId = histblock.ClientAccountId;	
	    		industryCodeWorkHist.division = histblock.DivisionName;  
	    		industryCodeWorkHist.companyName = histblock.OrganizationName;
	    		industryCodeWorkHist.startDate = histblock.StartDate;
	    		industryCodeWorkHist.endDate = histblock.EndDate;
	    		//add city, state and country 
//	    		industryCodeWorkHist.endDate = histblock.EndDate;
//	    		industryCodeWorkHist.endDate = histblock.EndDate;
//	    		industryCodeWorkHist.endDate = histblock.EndDate;
	    		OrderEmploymentBean oeb = findAssociatedJob(person.associated_jobs, histblock.OrderId);
				if (null!=oeb) {
					industryCodeWorkHist.city =  oeb.getCity();
					industryCodeWorkHist.state =  oeb.getState();
					if (null!=oeb.accountName)
						industryCodeWorkHist.companyName = oeb.accountName;										
				} 	
	    		if (null != histblock.RegionCode && !histblock.RegionCode.isEmpty()) {
    				industryCodeWorkHist.country = histblock.RegionCode;
    			}
    			// if the country from work history is null, get G2 country data.
    			else {
    				industryCodeWorkHist.country = person.country_code;
    			}
	    		industryCodeWorkHist.source = AG_WORK_HIST_SOURCE;
	    		hist.add( industryCodeWorkHist );
	    	}
    	}
    	
    	if ( null != person.meta.resumeProfile ) {
    		if ( person.meta.resumeProfile.getEmploymentHistory().size() > 0 ) {
    			// Making a copy of employmentHistory to avoid mutation issues during sorting
    			List<EmploymentBean> resumeEmployHist = person.meta.resumeProfile.getEmploymentHistory();
    			WorkHistUtils.sortResumeWorkHist(resumeEmployHist);
    			
    			int maxResumeWorkHistBlocks = resumeEmployHist.size();
    	    	if ( maxResumeWorkHistBlocks > WORK_HIST_BLOCKS_SIZE ) {
    	    		maxResumeWorkHistBlocks = Integer.min(maxResumeWorkHistBlocks,WORK_HIST_BLOCKS_SIZE);
    	    	}
				ContactInfoBean resumeContactInfo = person.meta.resumeProfile.getContactInfo();    	    	
	    		for ( int i=0; i<maxResumeWorkHistBlocks; i++ ) {
	    			IndustryCodeWorkHist industryCodeWorkHist = new IndustryCodeWorkHist();
	    			industryCodeWorkHist.talentId = person.id; 
	    			
	    			EmploymentBean histblock = resumeEmployHist.get(i);	    			
	    			industryCodeWorkHist.companyName = histblock.getEmploymentOrgName();
	    			if ( null != histblock.getHistoryRange()) {
	    				industryCodeWorkHist.startDate = histblock.getHistoryRange().getStartDate();
	    				industryCodeWorkHist.endDate = histblock.getHistoryRange().getEndDate();
	    			}
	    			
	    			
	    			// Location logic: look for data in this sequence
	    			// 1. Resume-work_history block
	    			// 2. Resume (top level)
	    			// 3. Person (contact record)
	    			if (! StringUtils.isBlank(histblock.getCountry())) {
	    				industryCodeWorkHist.country = histblock.getCountry();
	    			} else if (null!=resumeContactInfo && ! StringUtils.isBlank(resumeContactInfo.getCountry())) {
	    				industryCodeWorkHist.country = resumeContactInfo.getCountry();
	    			} else { 
	    				industryCodeWorkHist.country = person.country_code;
	    			}
	    			
	    			if (! StringUtils.isBlank(histblock.getState())) {
		        		industryCodeWorkHist.state = histblock.getState();
		        		industryCodeWorkHist.city = histblock.getCity();	        			    				
	    			} else if (null!=resumeContactInfo && ! StringUtils.isBlank(resumeContactInfo.getState())) {
		        		industryCodeWorkHist.state = resumeContactInfo.getState();
		        		industryCodeWorkHist.city = resumeContactInfo.getCity();
	    			} else {
		        		industryCodeWorkHist.state = person.country_sub_division_code;
		        		industryCodeWorkHist.city = person.city_name;	    				
	    			}
	        		industryCodeWorkHist.source = RESUME_WORK_HIST_SOURCE;
	        		industryCodeWorkHist.workHistId = String.valueOf(i); 
	        		hist.add( industryCodeWorkHist );
	    		} 
    		}
    	}
    	return hist;    	
    }
 
    private static OrderEmploymentBean findAssociatedJob(List<EmploymentBean> associated_jobs, String orderId) {
    	if (null==orderId || null==associated_jobs)
    		return null;
    	for (EmploymentBean emp : associated_jobs) {
    		OrderEmploymentBean oeb = (OrderEmploymentBean) emp;
    		if (null!= oeb.orderId && oeb.orderId.equals(orderId))
    			return oeb;
    	}    	
		return null;
	}
}
