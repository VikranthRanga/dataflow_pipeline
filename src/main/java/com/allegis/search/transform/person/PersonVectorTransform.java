package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.allegis.candidate.profile.ContactInfoBean;
import com.allegis.classification.EntityType;
import com.allegis.docvector.candidate.ATSCandidateService;
import com.allegis.docvector.candidate.dict.CandidateNameBlacklistTerms;
import com.allegis.docvector.model.VectorData;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.IndustryCodeOutput;
import com.allegis.search.model.person.JobCodeOutput;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.Resume_N;
import com.allegis.search.transform.CommonVectorTransform;
import com.searchtechnologies.aspire.math.SimpleVector;

/**
 * 
 * The PersonVectorTransform provides the formation / manipulation of vector based details for Persons
 *
 */
public class PersonVectorTransform implements Serializable {
    private static final long serialVersionUID = 1L;

    static final Set<String> redFlags = new HashSet<>(Arrays.asList(new String[]{
            "PLUS_ADDRESS", "BLACKLISTED_EMAIL_HOST", "SKILL_IN_NAME", "TEST_ACCOUNT"}));
    static final Set<String> yellowFlags =  new HashSet<>(Arrays.asList(new String[]{
            "OLD_RESUME", "MISMATCH", "1_LETTER_NAME", "INVALID_EMAIL"}));
    
    public static final String UNKNOWN = "unknown";  //default given when a job code or industry can't be classified

    /**
     * Adds all the vector data form VectorData
     * 
     * @param vectorData  a reference to the vector data
     * @param person    the person object to build on
     * @param isProdEnv		boolean whether prod or lower tier environments used for auditing
     * @return          the modified person object
     */
    public static PersonDoc transform( VectorData vectorData, PersonDoc person, boolean isProdEnv) {
    	PersonDoc personOut = CommonVectorTransform.transform(vectorData, person, isProdEnv);
    	return personOut;
    }

    /**
     * Adds skill family vector from the docvector and (employment_position_title or titlevector)
     * 
     * @param vectorData    a reference to the vector data to extract skill families from
     * @param person        a reference to the person to apply skills vectors too
     * @param isProdEnv		boolean whether prod or lower tier environments used for auditing
     * @return              an updated copy of the person
     */
    public static PersonDoc transformSkillFamily( VectorData vectorData, PersonDoc person, boolean isProdEnv ) {

        SimpleVector docvector = vectorData.getDocVector();               
        if (docvector == null || docvector.size == 0)  { 
            // quick exit if no docvector
            return person;  
        }

        // Find the best job title
        String title = null;
        if (person.title != null) {  // TODO populate this field in prior stage
            title = person.title;
        } else {
            SimpleVector titleVector = vectorData.getTitleVector();
            if (titleVector != null && titleVector.size > 0) { 
                // Get the highest weighted title; Should already be sorted 
                title = titleVector.getKeyString(0);
            }
        }
        return  CommonVectorTransform.transformSkillFamily(vectorData, person, title, EntityType.TALENT, isProdEnv);        
    }

    /**
     * Capture top skills, top titles and most relevant job title.
     * 
     * @param vectorData    a reference to the vector data to pull top terms from
     * @param person        a reference to a person to persist the top terms against
     * @return              an update reference of the person 
     */
    public static PersonDoc transformTopVectorTerms( VectorData vectorData, PersonDoc person ) {
        person.top_skills = CommonVectorTransform.getTopTerms(vectorData.getSkillsVector(), 3);
        person.top_titles = CommonVectorTransform.getTopTerms(vectorData.getTitleVector(), 3);        

        // get the top title
        if (person.top_titles != null && ! person.top_titles.isEmpty())  {
            person.most_relevant_job_title = person.top_titles.get(0);
        }
        return person;
    }


    /**
     * Uses CandidateService.analyzeCandidateName to set full_name and is_full_name_blacklisted.
     * 
     * @param candidateService a reference to the Ats Candidate service
     * @param blacklistTerms   a reference to the Candidate black list terms object
     * @param person           a reference to the person being transformed
     */
    public static void transformG2FullName(ATSCandidateService candidateService, CandidateNameBlacklistTerms blacklistTerms, PersonDoc person)
    {   
    	ContactInfoBean contactInfoBean = new ContactInfoBean();
    	
    	contactInfoBean.setFirstName((person.given_name != null) ? person.given_name : "");
    	contactInfoBean.setLastName((person.family_name != null) ? person.family_name : "");
    	contactInfoBean.setFullName((person.full_name != null) ? person.full_name : "");
	    
	    if (StringUtils.isNotBlank(person.given_name) && StringUtils.isNotBlank(person.family_name) ) {
	    	// this (initial) value might be replaced during the subsequent analyze call
	    	person.full_name = person.given_name + " " + person.family_name;
	    	contactInfoBean.setFullName(person.full_name); 
        }	    
	    candidateService.analyzeCandidateName(contactInfoBean, blacklistTerms);
	    person.full_name = contactInfoBean.getAnalyzedFullName();
	    person.is_full_name_modified = contactInfoBean.isCandidateFullNameModified();

        setDataQualityRollup(person);
    }

    /**
     * set Data Quality Flags and Rollup <br>
     * Fields: person.data_quality_flags, person.rollup
     * 
     * @param person a reference to the person being transformed
     * @throws NumberFormatException
     */
    public static void setDataQualityRollup(PersonDoc person) throws NumberFormatException {
        Set<String> candFlags = new LinkedHashSet<String>();

        if (person.meta.data_quality_record_flag != null) {
            String[] dqRawFlags = person.meta.data_quality_record_flag.split(";");

            for (String currentFlag : dqRawFlags) {
                currentFlag = currentFlag.trim();
                if (currentFlag.length() >= 0) {
                    candFlags.add(currentFlag);
                }
            }
        }

        // Handling of found flags in candidate
        String rollup = "High";

        if (candFlags.size() == 0) {
            candFlags.add("CLEAN");
        }

        Set<String> redFlagsintersection = new HashSet<String>(candFlags);
        redFlagsintersection.retainAll(redFlags);

        Set<String> yellowFlagsIntersection = new HashSet<String>(candFlags);
        yellowFlagsIntersection.retainAll(yellowFlags);

        // candFlags.grep(~/DUP_RESUME_[1-9][0-9]*/)
        List<String> regexList = new ArrayList<String>();
        for (String value : candFlags) {
            if (value.matches("DUP_RESUME_[1-9][0-9]*")) {
                regexList.add(value);
            }
        }

        if ((person.is_full_name_modified != null && person.is_full_name_modified) || redFlagsintersection.size() > 0) {
            rollup = "Low";
        } else if (regexList.size() > 0) {
            String numOfDupsStr = regexList.get(0).split("_")[2];
            if (numOfDupsStr != null) {
                Integer numOfDups = Integer.parseInt(numOfDupsStr);
                if (numOfDups >= 5) {
                    rollup = "Low";
                } else {
                    rollup = "Medium";
                }
            }

        } else if (yellowFlagsIntersection.size() > 0) {
            rollup = "Medium";
        }

        person.rollup = rollup;
        person.data_quality_flags = new ArrayList<String>(candFlags);
    }

    /**
     * Exclusively uses the /dict/CandidateNameBlacklist.txt file to set is_full_name_blacklisted.
     * 
     * FIXME this implementation is a cleaned up version of of the code taken from person-app.xml
     * It has several serious flaws:
     *   1. Partial matches of terms will be flagged as blacklisted 
     *   		Example: blacklisted term "bi" will match "robinson"
     *   2. Multi-word blacklisted terms will never be found if the terms are separated between first an last name.
     *   3. Skills and job titles will not be considered.
     * 
     * @param candidateService a reference to the Ats Candidate service
     * @param blacklistTerms   a reference to the Candidate black list terms object
     * @param person           a reference to the person being transformed
     */
    public static void transformParsedResumeFullName(ATSCandidateService candidateService, CandidateNameBlacklistTerms blacklistTerms, PersonDoc person) 
    {
        Resume_N resume = person.resume_1;
        if (resume == null) {
            return;
        }

        // This values were set in the resume parse stage. 
        String first_name = resume.first_name;
        String last_name = resume.last_name;

        if (first_name != null) {
            first_name = first_name.toLowerCase();
        }

        if (last_name != null) {
            last_name = last_name.toLowerCase();
        }

        Map<String, Map<String, Pattern>> blacklistTermsMap = blacklistTerms.getTermMap();

        Set<String> blacklistTermsSet = blacklistTermsMap.get("blacklist").keySet();
        for (String term: blacklistTermsSet) {
            if ((first_name != null && first_name.contains(term)) 
                    || (last_name != null && last_name.contains(term))) {
                resume.is_full_name_blacklisted = true;
                break;
            }
        }		 		
    }
    
    /**
     * Adds skillset vector from the person's vectors and (employment_position_title or titlevector)
     * 
     * @param person        a reference to the person to get skillsets for
     * @return              an updated copy of the job
     */
    public static PersonDoc transformSkillset( PersonDoc person ) {
    	// Create job history blocks
    	String[] rawText = null;
    	List<Employment> employmentHistory = person.employ_hist;
    	if (employmentHistory != null) {
    		rawText = new String[employmentHistory.size()];
    		int i = 0;
        	for (Employment employment : employmentHistory) {
        		if (employment != null) rawText[i] = employment.Description;
        		i++;
        	}
    	}
    	
    	// Call recommender with person and job history blocks
        return CommonVectorTransform.transformSkillset(person, rawText);        
    }
    
    /**
     * Converts the job "functions" for a talent into a vector
     * @param job_functions the talent's job function collection
     * @return      the vectorized form of the job functions, null otherwise
     */
    public static SimpleVector transformJobCodeVector( List<JobCodeOutput> job_functions ) {
        if( null == job_functions ) {
            return null;
        }
        SimpleVector vect = new SimpleVector();
        for(JobCodeOutput jobcode : job_functions ) {
            if( jobcode.confidence.floatValue() != 0 && ! jobcode.onet_description.equalsIgnoreCase(UNKNOWN) ) {
                vect.set(jobcode.onet_description, jobcode.confidence.floatValue() );
            }
        }
        vect.sort(2);
        if( vect.size == 0 ) {
            return null;
        }else {
            return vect;
        }
    }
    
    
    /**
     * Converts the industry codes for a talent into a vector
     * @param industry_code  the talent's industry code collection
     * @return      the vectorized form of the industry codes, null otherwise
     */
    public static SimpleVector transformIndustryVector( List<IndustryCodeOutput> industry_code ) {
        if( null == industry_code ) {
            return null;
        }
        SimpleVector vect = new SimpleVector();
        for(IndustryCodeOutput code : industry_code ) {
            if( code.confidence.floatValue() != 0 && ! code.sector_description.equalsIgnoreCase(UNKNOWN) ) {
                vect.set(code.sector_description, code.confidence.floatValue());
            }
        }
        vect.sort(2);
        
        if( vect.size == 0 ) {
            return null;
        }else {
            return vect;
        }
    }
}
