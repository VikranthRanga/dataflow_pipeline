package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.Table.TalentPredictions;
import com.allegis.search.http.RestClient;
import com.allegis.search.model.person.IndustryCodeWorkHist;
import com.allegis.search.model.person.JobCodeOutput;
import com.allegis.search.model.person.JobCodeWorkHist;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.ProvenTrackRecordData;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.DateUtils;
import com.allegis.search.utils.GeneralUtils;
import com.allegis.search.utils.HashingUtils;
import com.allegis.search.utils.ProvenTrackRecordUtils;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.Struct;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * The talent predictions transform is responsible for applying the transformations to
 * {@see PersonDoc} for purposes of the talent predictive service calls
 * @author jalpino
 *
 */
public class TalentPredictionsTransform implements Serializable {

	private static final long serialVersionUID = 6408506618559591875L;
	
	private RestClient restClient;
	private DatabaseClient ingest10xSpannerClient;
	private	DeadLetterService deadLetterService;
	
	private JobCodeInputTransform jobCodeInputTransform = new JobCodeInputTransform();
    private IndustryCodeInputTransform industryCodeInputTransform = new IndustryCodeInputTransform();
    private ProvenTrackRecordInputTransform provenTrackRecordInputTransform = new ProvenTrackRecordInputTransform();
	 
    private static final Logger logger = LoggerFactory.getLogger( TalentPredictionsTransform.class );
    
	/** 
	 * Constructor
	 * @param ingest10xSpannerClient   a reference to an existing spanner client for the 'ingest10x' db
	 * @param restClient               a reference to an existing Rest Client
	 */
	public TalentPredictionsTransform( DatabaseClient ingest10xSpannerClient, RestClient restClient, DeadLetterService deadLetterService) {
	    this.ingest10xSpannerClient = ingest10xSpannerClient;
	    this.restClient = restClient;
	    this.deadLetterService = deadLetterService;
	}
	
	 
	/**
	 * Perform the base transformation for predictive service outputs onto the talent
	 * @param person       the person to update
	 * @param resultsSet   a reference to the talent predictions from Spanner
	 * @return             the updated person
	 * @throws Exception
	 */
	public PersonDoc transform(PersonDoc person, Struct resultsSet) throws Exception {
	    
	    String jobCodeJSON = GeneralUtils.getNullSafeString( resultsSet, TalentPredictions.Column.JobCode );
        if( jobCodeJSON != null) {
            @SuppressWarnings("serial")
            List<JobCodeOutput> jobCodeOutput = new Gson().fromJson(jobCodeJSON, new TypeToken<List<JobCodeOutput>>() {}.getType());
            person.job_functions = jobCodeOutput;
        }
        person.meta.jobcode_hash = GeneralUtils.getNullSafeString( resultsSet, TalentPredictions.Column.JobCodeHash );
        person.meta.jobcode_last_modified_date = DateUtils.reformatToDateTime(GeneralUtils.getNullSafeTimeStamp(resultsSet, TalentPredictions.Column.JobCodeLastModifiedDate.name()));
        
        
        String industryCodeJSON = GeneralUtils.getNullSafeString( resultsSet, TalentPredictions.Column.IndustryCode );
        if( industryCodeJSON != null ) { 	
        	person.meta.industry_code_resp = industryCodeJSON;
        	//method parses response and sets person industry code and norm company names fields
            industryCodeInputTransform.parseResponse(person);
        }

        person.meta.industrycode_hash = GeneralUtils.getNullSafeString( resultsSet, TalentPredictions.Column.IndustryCodeHash );
        person.meta.industrycode_last_modified_date =  DateUtils.reformatToDateTime(GeneralUtils.getNullSafeTimeStamp(resultsSet, TalentPredictions.Column.IndustryCodeLastModifiedDate.name()) );

        String provenTrackRecord = GeneralUtils.getNullSafeString( resultsSet, TalentPredictions.Column.ProvenTrackRecord );
        if( provenTrackRecord != null ) {
        	JsonObject provenTrackRecordOutputToJson = new JsonParser().parse(provenTrackRecord).getAsJsonObject();                      	 
        	ProvenTrackRecordUtils.buildProvenTrackRecordFields(person, provenTrackRecordOutputToJson);            
        }

        person.meta.proventrackrecord_hash = GeneralUtils.getNullSafeString( resultsSet, TalentPredictions.Column.ProvenTrackRecordHash );
        person.meta.proventrackrecord_last_modified_date =  DateUtils.reformatToDateTime(GeneralUtils.getNullSafeTimeStamp(resultsSet, TalentPredictions.Column.ProvenTrackRecordLastModifiedDate.name()) );

        return person;
	}
	
	
	/**
	 * Attempts to update the JobCode information on the supplied person by issuing a call out to the given endpoint
	 * @param person           a reference to the Person to update
	 * @param jobCodeEndpoint  the job code API endpoint
	 * @param overrideHashCompare if true, override the hashcode comparison in determining if a call should be made
	 * @return                 the updated Person
	 */
	public PersonDoc updateJobCode( PersonDoc person, String jobCodeEndpoint, boolean overrideHashCompare ) {
	    
	    List<JobCodeWorkHist> jobCodeWorkHist = new ArrayList<>();
	    try {
	        jobCodeWorkHist = JobCodeWorkHistTransform.transform( person, jobCodeWorkHist, this.ingest10xSpannerClient ); 
	        
	        if( null != jobCodeWorkHist && ! jobCodeWorkHist.isEmpty() ){
	            JsonArray jobCodeWorkHistJsonArray =  JobCodeWorkHistTransform.transformJobCodeWorkHistToJsonArray(jobCodeWorkHist);
	            
	            // Compare the hash to determine if we need to make a call to the service
	            if( overrideHashCompare || ! HashingUtils.matchesHash(jobCodeWorkHistJsonArray.toString(),  person.meta.jobcode_hash)) {
	                
	                jobCodeInputTransform.transform( person, jobCodeWorkHistJsonArray, this.restClient, jobCodeEndpoint, deadLetterService );
	               
	            }
	        }
	    }  catch (Exception x) {
			logger.warn("Error while updating job code for Id="+person.id);			
        }   
	    return person;
	}
	
	
	/**
     * Attempts to update the Industry Code information on the supplied person by issuing a call out to the given endpoint
     * @param person            a reference to the Person to update
     * @param industryEndpoint  the job code API endpoint
     * @param overrideHashCompare if true, override the hashcode comparison in determining if a call should be made
     * @return                  the updated Person
     */
    public PersonDoc updateIndustryCode( PersonDoc person, String industryEndpoint, boolean overrideHashCompare ) {
        
        List<IndustryCodeWorkHist> industryCodeWorkHist = new ArrayList<>();
        try {
	        industryCodeWorkHist = IndustryCodeWorkHistTransform.transform( person, industryCodeWorkHist );   
	        if( null != industryCodeWorkHist && ! industryCodeWorkHist.isEmpty() ){
	            
	            JsonArray industryCodeWorkHistJsonArray = industryCodeInputTransform.transformIndustryCodeWorkHistToJsonArray(industryCodeWorkHist);
	            
	            // Compare the hash to determine if we need to make a call to the service
	            if( overrideHashCompare || ! HashingUtils.matchesHash(industryCodeWorkHistJsonArray.toString(),  person.meta.industrycode_hash)) {
	                
	                industryCodeInputTransform.transform( person, industryCodeWorkHistJsonArray, this.restClient, industryEndpoint, deadLetterService );
	                
	            }
	        }
        } catch (Exception x) {		
			logger.warn("Error while updating industry code for Id="+person.id); 			
        }
        
        return person;
    }
    
    /**
     * Attempts to update the Proven Track Record information on the supplied person by issuing a call out to the given endpoint
     * @param person              a reference to the Person to update
     * @param ptrEndpoint         the Proven Track Record API endpoint
     * @param overrideHashCompare if true, override the hashcode comparison in determining if a call should be made
     * @return                  the updated Person
     */
    public PersonDoc updateProvenTrackRecord( PersonDoc person, String ptrEndpoint, boolean overrideHashCompare ) throws Exception {
        boolean isInputValid = true;
    	ProvenTrackRecordData ptrData = ProvenTrackRecordDataTransform.transform( person );   
        if( null != ptrData ){
            
            JsonObject provenTrackRecordDataToJson = provenTrackRecordInputTransform.transformProvenTrackRecordDataToJsonObject(ptrData);
            
            // Do not call the proven track record API if all we have is an ID and a STATUS. It means that we don't have any ptr information
            // for the backing model to evaluate
            if ( provenTrackRecordDataToJson.size() == 2 && provenTrackRecordDataToJson.has("id") && provenTrackRecordDataToJson.has("status") ) {
            	isInputValid = false;
        	}
            if ( isInputValid ) {
	            // Compare the hash to determine if we need to make a call to the service
	            if( overrideHashCompare || !HashingUtils.matchesHash(provenTrackRecordDataToJson.toString(),  person.meta.proventrackrecord_hash) ) {               
	            	provenTrackRecordInputTransform.transform( person, provenTrackRecordDataToJson, this.restClient, ptrEndpoint );              
	            } 
            }
        }
        
        return person;
    }

}
