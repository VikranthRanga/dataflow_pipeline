package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.CandidateStatus;
import com.allegis.search.enums.RateFrequency;
import com.allegis.search.enums.Table.Account;
import com.allegis.search.model.HeavyLifting;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.PersonShiftPreference;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.AccountUtils;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.Struct;

/**
 * The AccountContactTransform provides the formation / manipulation of a the
 * Account and Contact details
 * for Persons
 * 
 * @author jalpino
 *
 */
public class AccountTransform implements Serializable {
    private static final long serialVersionUID = 1L;
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger( AccountTransform.class );
    
    private final DeadLetterService deadLetterService;
     
    public AccountTransform(DeadLetterService deadLetterService) {
        super();
        this.deadLetterService = deadLetterService;
    }

    
    // Database field names
    public static class ACCOUNT {
	    /**
	     * Enum used for mapping to Salesforce fields
	     */
	    public static enum Columns {
	        candidate_id(Account.Column.Id),
	        talent_id(Account.Column.Talent_Id__c),
	        peoplesoft_id(Account.Column.Peoplesoft_ID__c),
	        source_system_id(Account.Column.Source_System_Id__c),
	        group_filter(Account.Column.Talent_Ownership__c),
	
	        do_not_call(Account.Column.Do_Not_Contact__c),
	        do_not_recruit(Account.Column.Do_Not_Recruit_Talent__c),
	
	        skills_comments(Account.Column.Skill_Comments__c),
	        
	        shift(Account.Column.Shift__c),
	
	        person_availability(Account.Column.Date_Available__c),
	        qualifications_last_activity_date(Account.Column.LastActivityDate),
	        qualifications_last_contact_date(Account.Column.Talent_Profile_Last_Modified_Date__c),
	        talent_created_date(Account.Column.CreatedDate),
	
	        candidate_status(Account.Column.Candidate_Status__c),
	        current_employer_name(Account.Column.Talent_Current_Employer_Formula__c),
	        placement_type(Account.Column.Desired_Placement_type__c),
	        total_desired_compensation(Account.Column.Desired_Total_Compensation__c),
	
	        // used for generated fields
	        skills(Account.Column.Skills__c),
	        desired_rate_frequency(Account.Column.Desired_Rate_Frequency__c),
	        min_desired_rate(Account.Column.Desired_Rate__c),
	        max_desired_rate(Account.Column.Desired_Rate_Max__c),
	        talent_preference_internal(Account.Column.Talent_Preference_Internal__c),
	        
	        //JSON data which holds communities skills
	        talent_preference__c(Account.Column.Talent_Preference__c),
	        
	        //Communities profile last modified date
	        TC_Talent_Profile_Last_Modified_Date__c(Account.Column.TC_Talent_Profile_Last_Modified_Date__c),
	        
	        talent_end_date(Account.Column.Talent_End_Date__c),
	        RecordTypeId(Account.Column.RecordTypeId),  // fetch this to filter out non-talent types from realtime feed
	        Talent_Committed_Flag__c(Account.Column.Talent_Committed_Flag__c),  // fetch this to filter out uncommitted from realtime feed
	        
	        security_clearance_type(Account.Column.Talent_Security_Clearance_Type__c);
	
	        Columns(Account.Column column) {
	            this.salesforceName = column.name();
	        }
	
	        public String salesforceName;
	
	        public static List<String> columnValues() {
	            List<String> columns = new ArrayList<String>();
	            for (Columns columnsMapping : Columns.values()) {
	                columns.add(columnsMapping.salesforceName);
	            }
	            return columns;
	        }
	    }
	    public static final List<String> COLUMNS = ACCOUNT.Columns.columnValues();
    }

    private Counter gotShiftCounter = Metrics.counter(AccountTransform.class.getSimpleName(), "Got Shift");

    
    /**
     * Builds upon the supplied person with the provided data
     * 
     * @param data
     *            Talent Account and Contact details
     * @param person
     *            the person object to build on
     * @return the modified person object
     */
    public PersonDoc transform(Struct data, PersonDoc person) {
        
        person.group_filter = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.group_filter.salesforceName);
        person.visibleNumber = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.talent_id.salesforceName);
        person.peoplesoft_id = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.peoplesoft_id.salesforceName);
        person.source_system_id = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.source_system_id.salesforceName);

        person.do_not_call = GeneralUtils.getNullSafeBoolean(data, ACCOUNT.Columns.do_not_call.salesforceName);
        person.do_not_recruit = GeneralUtils.getNullSafeBoolean(data, ACCOUNT.Columns.do_not_recruit.salesforceName);
        
        String candidate_status_str = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.candidate_status.salesforceName);
        
        if (candidate_status_str == null) {
            person.status = CandidateStatus.DEFAULT.status();
        }else {
            person.status = candidate_status_str;
        }
        
        person.companyName = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.current_employer_name.salesforceName);
        
        String placement_type_str = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.placement_type.salesforceName);
        if(placement_type_str != null) {
            person.placement_type = Arrays.asList(placement_type_str);
        }
        
        String shiftPreferenceRaw = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.shift.salesforceName);
        boolean gotShift = PersonShiftPreference.getInstance().extractAccountPreference(person, shiftPreferenceRaw, deadLetterService); 
        if (gotShift)  {
        	gotShiftCounter.inc();
        }
        
        person.security_clearance = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.security_clearance_type.salesforceName );
        
        setDesiredPay(data, person);
        setDates(data, person);
        setSkills(data, person);
        setOwnership(data, person);
        setTalentPreferenceFields(data, person);        
		HeavyLifting.extract(person, person.skills_comments, "account", "skills_comments");
		
        setCommunitiesTalentPreferenceFields(data, person);

        person.generatedtimestamp = (person.qualifications_last_contact_date != null) ? person.qualifications_last_contact_date : person.createdDate;
        
        return person;
    }


    /**
     * Method used to set Desired Pay Fields
     * @param data Struct Spanner Object
     * @param person PersonDoc Object
     */
    private void setDesiredPay(Struct data, PersonDoc person) {
        String desiredRateFrequency =  GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.desired_rate_frequency.salesforceName);
        String minDesiredRate =  GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.min_desired_rate.salesforceName);
        String maxDesiredRate =  GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.max_desired_rate.salesforceName);
        
        if(desiredRateFrequency!=null && minDesiredRate!=null) {
            try {
                String hourlyRate = AccountUtils.convertDesiredRateToHourly( RateFrequency.getFrequency(desiredRateFrequency), Double.parseDouble(minDesiredRate));
                person.min_desired_payrate = Double.parseDouble(hourlyRate);
            }catch(NumberFormatException e) {
            	deadLetterService.writeDataQualityWarning(e, ACCOUNT.Columns.min_desired_rate.salesforceName+"; value="+minDesiredRate, person);
                person.min_desired_payrate = 0.0;
            }
        }
        
        if(desiredRateFrequency!=null && maxDesiredRate!=null) {
            try {
                String hourlyRate = AccountUtils.convertDesiredRateToHourly( RateFrequency.getFrequency(desiredRateFrequency), Double.parseDouble(maxDesiredRate));
                person.max_desired_payrate = Double.parseDouble(hourlyRate);
            }catch(NumberFormatException e) {
            	deadLetterService.writeDataQualityWarning(e, ACCOUNT.Columns.max_desired_rate.salesforceName+"; value="+maxDesiredRate, person);
                person.max_desired_payrate = 0.0;
            }
        }
        
        String totalDesiredCompensation =  GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.total_desired_compensation.salesforceName);
        if(totalDesiredCompensation != null) {
            try {
                person.total_desired_compensation = Double.parseDouble(totalDesiredCompensation);
            }catch(NumberFormatException e) {
            	deadLetterService.writeDataQualityWarning(e, ACCOUNT.Columns.total_desired_compensation.salesforceName+"; value="+totalDesiredCompensation, person);
                person.total_desired_compensation = 0.0;
            }
        }else {
            // looking at the person index, it seems like it defaults to 0
            person.total_desired_compensation = 0.0;
        }
    }
    
    /**
     * Method used to set Dates for PersonDoc
     * 
     * It sets:
     *  person_availability
     *  qualifications_last_activity_date
     *  qualifications_last_contact_date
     *  talent_created_date
     * 
     * @param data Struct Spanner Object
     * @param person PersonDoc Object
     */
    private void setDates(Struct data, PersonDoc person) {
        
        person.person_availability = GeneralUtils.getNullSafeDate(data, ACCOUNT.Columns.person_availability.salesforceName, person, deadLetterService);
        person.qualifications_last_activity_date = GeneralUtils.getNullSafeDate(data, ACCOUNT.Columns.qualifications_last_activity_date.salesforceName, person, deadLetterService);
        
        // TODO What timezone is saleforce in?
        // TODO: format to zulu time for Elasticsearch? 
        person.qualifications_last_contact_date = GeneralUtils.getNullSafeDate(data, ACCOUNT.Columns.qualifications_last_contact_date.salesforceName, person, deadLetterService);
        person.createdDate = GeneralUtils.getNullSafeDate(data, ACCOUNT.Columns.talent_created_date.salesforceName, person, deadLetterService);
        
        person.placement_end_date = GeneralUtils.getNullSafeDate(data, ACCOUNT.Columns.talent_end_date.salesforceName, person, deadLetterService);
        
        person.communities_profile_last_modified_date =  GeneralUtils.getNullSafeDate(data, ACCOUNT.Columns.TC_Talent_Profile_Last_Modified_Date__c.salesforceName, person, deadLetterService);
    }
    
    /**
     * Used to parse skill json
     * 
     * skills__c ->
     * JsonString({"skills":["Forklift","Production Support","Production
     * Supervision","Union Experience"]})
     * 
     * @param data Struct Spanner Object
     * @param person PersonDoc Object
     */
    private void setSkills(Struct data, PersonDoc person) {
        String skillsJson = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.skills.salesforceName);
        if(skillsJson != null) {
            JSONObject parseJsonObject = AccountUtils.parseJsonObject(skillsJson, person, "skills", this.deadLetterService);
            person.skills =  AccountUtils.parseSkillJson(parseJsonObject);
        }
        person.skills_comments = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.skills_comments.salesforceName);
    }

    /**
     * Set Ownership Field,  Currently team decided that this field is not needed
     * this method is here as a placeholder for future usage
     * 
     * @param data Struct Spanner Object
     * @param person PersonDoc Object
     */
    private void setOwnership(Struct resultSet, PersonDoc person) {
        // talent.Target_Accounts__r
        // TODO it seems we are missing field or table from spanner
        // Spanner has below fields
        // Target_Account_Stamp_0__c STRING(MAX) Yes
        // ...
        // Target_Account_Stamp_5__c STRING(MAX) Yes
    }
    
    /**
     * Set languages_spoken, security clearance and national_op fields
     * 
     * @param data Struct Spanner Object
     * @param person PersonDoc Object
     */
    private void setTalentPreferenceFields(Struct data, PersonDoc person) {
        // explicitly set the language of origin for this Talent.
        // Note, this previously used BasisTech libraries to determine
        // this under the "LanguageEncodingDetectorEn" stage
        person.lang = "English";

        String languageJson = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.talent_preference_internal.salesforceName);
        
        if(languageJson != null) {
            JSONObject parseJsonObject = AccountUtils.parseJsonObject(languageJson, person, ACCOUNT.Columns.talent_preference_internal.salesforceName, this.deadLetterService);
            person.languages_spoken = AccountUtils.parseLanguagePreferenceJson(parseJsonObject);
            person.national_op = AccountUtils.parseNationalOpportunitiesPreferenceJson(parseJsonObject);
        }
    }
    
    
    /**
     * Set Communities talent skills
     * 
     * @param data Struct Spanner Object
     * @param person PersonDoc Object
     */
    private void setCommunitiesTalentPreferenceFields(Struct data, PersonDoc person) {
        String talentPreferenceCJSON = GeneralUtils.getNullSafeString(data, ACCOUNT.Columns.talent_preference__c.salesforceName);
        
        if(talentPreferenceCJSON != null) {
            JSONObject parseJsonObject = AccountUtils.parseJsonObject(talentPreferenceCJSON, person, "talent_preference__c", this.deadLetterService);
            person.communities_skills = AccountUtils.parseCommunitiesSkillJSON(parseJsonObject);
        }
        /*else {
        	//Test Data
        	List<String> skills = new ArrayList<String>();
        	skills.add("java");
        	skills.add("C#");
        	skills.add("spark");
        	person.communities_skills = skills;
        }*/
    }
   

}
