package com.allegis.search.transform.person;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.Months;

import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.ProvenTrackRecordActivityInfo;
import com.allegis.search.model.person.ProvenTrackRecordCounters;
import com.allegis.search.model.person.ProvenTrackRecordData;
import com.allegis.search.model.person.ProvenTrackRecordWorkInfo;
import com.allegis.search.utils.WorkHistUtils;

/**
 * Transform Proven Track Record data points into an ProvenTrackRecordData POJO.
 * In this transform, the following datapoints are transformed - 
 * 		1. top activity date from recent first "sorted activity dates" is considered 
 * 		to calculate "recency_month" data point.
 * 		2. "pfDetail" datapoint is built by concatenating top 10 descriptions of activities 
 * 		with "Performance Feedback" or"Service Touchpoint.
 * 		3. "maCount" datapoint - count the number of companies where "multiple assignments" are held
 * 		4. "maDetail" datapoint - count the number of positions held at the companies where
 * 		multiple assignments were held 
 * 
 * 
 * @author jperecha
 */
public class ProvenTrackRecordDataTransform implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public ProvenTrackRecordDataTransform() {
		super();
	}
	
	/**
	 * Transform Proven Track Record data points into an ProvenTrackRecordData
	 * 
	 * @param person	the person object to build on
	 * @return
	 * @throws IOException
	 */
	public static ProvenTrackRecordData transform (PersonDoc person) throws IOException {
		ProvenTrackRecordData ptrData = new ProvenTrackRecordData();
		
		if ( null != person.meta.provenTrackRecordCounters ) {
			ProvenTrackRecordCounters ptrCounters = person.meta.provenTrackRecordCounters;
			ptrData.id = person.id;
			ptrData.status = person.status;
			
			ptrData.pf_count = (ptrCounters.pfCounter != 0) ? ptrCounters.pfCounter : null;
			ptrData.rc_count = (ptrCounters.rcCounter != 0) ? ptrCounters.rcCounter : null;
			
			int frDetail = ptrCounters.posFinishReasonCounter - ptrCounters.negFinishReasonCounter;
			int rrDetail = ptrCounters.rrPositiveCounter - ptrCounters.rrNegativeCounter;
			
			ptrData.fr_detail = (frDetail != 0) ? frDetail : null;
			ptrData.rr_detail = (rrDetail != 0) ? rrDetail : null;
			
			// Get recent activity date from the top of the sorted set
			if ( null != ptrCounters.lastActivityDate && ptrCounters.lastActivityDate.size() > 0 ) {
				Date activityDate = (Date) ptrCounters.lastActivityDate.first();
				int months = Months.monthsBetween(new DateTime(activityDate), new DateTime(System.currentTimeMillis()))
		                .getMonths();
				ptrData.recency_month = months;
			}
		}
		
		if ( null != person.meta.provenTrackRecordActivityInfo && !person.meta.provenTrackRecordActivityInfo.isEmpty() ) {
			List<ProvenTrackRecordActivityInfo> activityInfo = person.meta.provenTrackRecordActivityInfo;
			WorkHistUtils.sortActivityInfo(activityInfo);
			ptrData.pf_detail = concatDescription(activityInfo);
		}	
		
		if ( null != person.meta.provenTrackRecordWorkInfo && !person.meta.provenTrackRecordWorkInfo.isEmpty()) {
			List<ProvenTrackRecordWorkInfo> workInfo = person.meta.provenTrackRecordWorkInfo;
			getMultipleWorkAssignmentsInfo(workInfo, ptrData);
		}
		
		return ptrData;
		
	}

	/**
	 * Method to get maCount and maDetail data points
	 * 
	 * @param workInfo	work info related to proven track record 
	 * @param ptrData the proven track record data object to build upon
	 */
	private static void getMultipleWorkAssignmentsInfo(List<ProvenTrackRecordWorkInfo> workInfo, ProvenTrackRecordData ptrData) {
		
		if ( null != workInfo ) {
			int maCount = 0; 
			int maDetail = 0;
			Map<String,List<ProvenTrackRecordWorkInfo>> multipleAssignmentsByCompany = new HashMap<>();
			
			multipleAssignmentsByCompany = workInfo.stream().collect(Collectors.groupingBy(ProvenTrackRecordWorkInfo::getSourceCompanyId));
			
			for (Map.Entry<String,List<ProvenTrackRecordWorkInfo>> entry : multipleAssignmentsByCompany.entrySet())  {
				if (entry.getValue().size() > 1) {
					maCount++;
					maDetail += entry.getValue().size();
				}
			}
			ptrData.ma_count = (maCount != 0) ? maCount : null;
			ptrData.ma_detail = (maDetail != 0) ? maDetail : null;
		}
		
	}

	/**
	 * Method to remove duplicate descriptions and concat top 10 descriptions of activities 
	 * with "Performance Feedback" or"Service Touchpoint
	 * 
	 * @param activityInfo the activity info list of descriptions
	 * @return
	 */
	private static String concatDescription(List<ProvenTrackRecordActivityInfo> activityInfo) {
		
		HashSet<String> descrSet = new HashSet<>();
		String description = "";
		
		// Iterate over sorted descriptions to remove duplicates and prepare set of top 10 descriptions
		for ( ProvenTrackRecordActivityInfo info:activityInfo ) {
			descrSet.add(info.description);
			if ( descrSet.size() == 10 ) {
				break;
			}
		}
		
		// concatenate descriptions
		for ( String descr:descrSet ) {
			description = description.concat(descr).concat(";");
		}		
		if ( description.length() > 1 ) {
			description = description.substring( 0, description.length() - 1 );
		}
		return description;
	}
}
