package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.EventType;
import com.allegis.search.enums.NegativeRecrRelationType;
import com.allegis.search.enums.PositiveRecrRelationType;
import com.allegis.search.enums.Table.Event;
import com.allegis.search.model.person.AssociatedPositionOpening;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.ProvenTrackRecordCounters;
import com.allegis.search.model.person.SubmitHist;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;
import com.allegis.search.utils.UserUtils;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.ResultSet;

/**
 * Sourced from event table.
 * Updates PersonDoc fields:
 * 			submit_hist
 * 			interactions
 * 			recruiter_last_activity_date
 * 			submittal_status
 * 			recent_submittal_date
 * 			associated_position_openings
 */
public class EventTransform implements Serializable {
	private static final long serialVersionUID = 1L;

    public static List<String> COLUMNS = GeneralUtils.toNameList(
    		Event.Column.Type,
    		Event.Column.LastModifiedById,
    		Event.Column.WhatId,
    		Event.Column.LastModifiedDate,
    		Event.Column.Not_Proceeding_Reason__c,
    		Event.Column.Activity_Type__c,
    		Event.Column.ActivityDate,
    		Event.Column.Category__c,
    		Event.Column.ActivityDateTime
    );

	// create some metrics gathering objects
	private static Counter gotAssociatedPositionCounter = Metrics.counter(EventTransform.class, "Got Associated Position");
	private static Counter gotSubmitalCounter = Metrics.counter(EventTransform.class, "Got Submittal");
	private static Counter gotInteractionCounter = Metrics.counter(EventTransform.class, "Got Interaction");
	
	private static Set<String> deboost_submittal_type = new HashSet<>(Arrays.asList(			
			"Offer Accepted" ));



    // TODO This is a copy of a hardcoded Salesforce HRXML creation.  It may get lost here.
	// Valid types that are considered a interactions
    private static String eventInteractionDomainValues[] = {
		"Meal",
		"Linked",
		"Submitted",
		"Interviewing",
		"Offer Accepted",
		"Service Touchpoint",
    	"Meeting",
    	"Internal Interview",
    	"Performance Feedback",
    	"Not Proceeding",
    	"Not Proceeding - Applicant",
    	"Not Proceeding - Submitted",
    	"Not Proceeding - Offer Accepted",
    	"Not Proceeding - Linked",
    	"Not Proceeding - Interviewing",
    	"Not Proceeding - Started",
    	"Not Proceeding - Interview Requested"
    };
    private static Set<String> eventInteractionDomain = new HashSet<>(Arrays.asList(eventInteractionDomainValues));

    //List of values that store the opportunityId as whatId for events belonging to a talent.
	private static Set<String> crm_all_submittal_values = new HashSet<>(Arrays.asList(
			"Submitted",
			"Interviewing",
			"Offer Accepted",
			"Started",
			"Linked",
			"Not Proceeding",
			"Applicant",
			"Screening",
			"Offer Extended",
			"Interview Requested",
			"LPQ in Process",
			"Internal Interviewing",
			"On Hold",
			"Referencing",
			"Network Referral",
			"Anonymous Promo",
			"Pursuing",
			"Carve-Out",
			"Not Proceeding - Linked",
			"Not Proceeding - Applicant",
			"Not Proceeding - Screening",
			"Not Proceeding - Offer Extended",
			"Not Proceeding - Interview Requested",
			"Not Proceeding - LPQ in Process",
			"Not Proceeding - Internal Interviewing",
			"Not Proceeding - On Hold",
			"Not Proceeding - Referencing",
			"Not Proceeding - Network Referral",
			"Not Proceeding - Anonymous Promo",
			"Not Proceeding - Pursuing",
			"Not Proceeding - Carve-Out",
			"Not Proceeding - Submitted",
			"Not Proceeding - Interviewing",
			"Not Proceeding - Offer Accepted",
			"Not Proceeding - Started"
			));

    /**
     * 2 years of milliseconds
     */
    private static final long TWO_YEARS_MILS = (24L * 60L * 60L * 1000L) * 730L;


    private DeadLetterService deadLetterService;
    private AssociatedPositionTransform associatedPositionTransform;

    public EventTransform(DeadLetterService deadLetterService) {
		super();
		this.deadLetterService = deadLetterService;
		this.associatedPositionTransform = new AssociatedPositionTransform(deadLetterService);
	}


    /**
     * Builds upon the supplied person with the provided data
     * @param resultSet     Event details
     * @param person    the person object to build on
     * @return          the modified person object
     */
    public PersonDoc transform( ResultSet resultSet, PersonDoc person, DatabaseClient spannerClient) {

    	Date twoYearsAgo = new Date(System.currentTimeMillis() - TWO_YEARS_MILS);

    	Set<String> interactions = new HashSet<>();  // Set will make values distinct
		List<SubmitHist> submitHist = new ArrayList<SubmitHist>();
		Date mostRecentSubmitDate = new Date(0);
		
		ProvenTrackRecordCounters ptrCounters = person.meta.provenTrackRecordCounters;
		SortedSet<Date> activityDatesSet = new TreeSet<>();		
	
    	AssociatedPositionOpening apo = person.associated_position_openings;

	    while (resultSet.next()) {
	    	String whatId = GeneralUtils.getNullSafeString(resultSet, Event.Column.WhatId.name());
	    	String type = GeneralUtils.getNullSafeString(resultSet, Event.Column.Type.name());
	    	String activityType = GeneralUtils.getNullSafeString(resultSet, Event.Column.Activity_Type__c.name());
	    	String categoryType = GeneralUtils.getNullSafeString(resultSet, Event.Column.Category__c.name());
	    	
	    	Date date = GeneralUtils.getNullSafeDate(resultSet, Event.Column.LastModifiedDate.name(), person, this.deadLetterService);
	    	Date activityDate = GeneralUtils.getNullSafeDate(resultSet, Event.Column.ActivityDate.name(), person, this.deadLetterService);
	    	
	    	if (type == null || date == null)
	    		continue;  // safety test

			// Capture associatedPositionOpening for downstream
			if (crm_all_submittal_values.contains(type) && (apo == null || date.after(apo.SubmittedDate)))
			{
				if (apo == null) {
					apo = new AssociatedPositionOpening();
					person.associated_position_openings = apo;
					gotAssociatedPositionCounter.inc();
				}

				apo.whatId = whatId;
				apo.SubmittedDate = date;
				apo.ActivityDate = GeneralUtils.getNullSafeDate(resultSet, Event.Column.ActivityDateTime.name(), person, this.deadLetterService);
				apo.SubmittedById = GeneralUtils.getNullSafeString(resultSet, Event.Column.LastModifiedById.name());
				apo.SubmittedByName = UserUtils.getName(spannerClient, apo.SubmittedById);
				apo.NotProceedingReason = GeneralUtils.getNullSafeString(resultSet, Event.Column.Not_Proceeding_Reason__c.name());
			}
			
			 
			//Capture most recent submit date for deboosting talent based on submittal type
			if (date != null && date.after(mostRecentSubmitDate)) { 
				mostRecentSubmitDate = date;
				if(deboost_submittal_type.contains(type)) {					
				    person.deboost_recent_submital_days_since_epoch = GeneralUtils.daysSinceEpoch( mostRecentSubmitDate ); 
				}
				else {
					person.deboost_recent_submital_days_since_epoch = 0L;
				}
            }
			

			// Capture submittal history
	    	if ("Submitted".equals(type) && date.after(twoYearsAgo)) {
	    		// need the most recent submittal for for eagerness
	    		if (person.recent_submittal_date == null || (date != null && person.recent_submittal_date.before(date)))
	    		{
	    			person.recent_submittal_date = date;
	    			person.submittal_status = type;   // currently this will only be null or "Submitted"
	    		}

	    		// Add to submit_hist
	    		String userId = GeneralUtils.getNullSafeString(resultSet, Event.Column.LastModifiedById.name());
	    		String peopleSoftUserId = UserUtils.getPeopleSoftId(spannerClient, userId);

	        	submitHist.add(new SubmitHist(peopleSoftUserId, date, type, userId));
	    	}

	    	// Capture interactions
	    	if (eventInteractionDomain.contains(type)) {

	    		person.recruiter_activity = true;

	    		// need the most recent interaction date for eagerness
	    		if (person.recruiter_last_activity_date == null || person.recruiter_last_activity_date.before(date))  {
	    			person.recruiter_last_activity_date = date;
	    		}

	    		// add to interactions for only these specific events
	    		if (type.equals("Meeting")) {
	    			interactions.add("Meeting");
	    		}
	    		else if (type.equals("Internal Interview")) {
	    			interactions.add("IOI");
	    		}
	    	}
	    	
	    	// Capture PTR Counters - positive recruiter relationship
    		if (PositiveRecrRelationType.isPostiveType(activityType) || PositiveRecrRelationType.isPostiveType(categoryType)) {
    			ptrCounters.rrPositiveCounter++;
    		}
    		
    		// Capture PTR Counters - negative recruiter relationship
    		if (NegativeRecrRelationType.isNegativeType(activityType)) {
    			ptrCounters.rrNegativeCounter++;
    		}
    		
    		// Capture PTR Counters - event type is Service Touchpoint/Performance Feedback
    		if (EventType.SERV_TOUCHPOINT.equals(type) || EventType.PERF_FEEDBACK.equals(type)) {
    			ptrCounters.pfCounter++;
    		}
    		
	    	// Capture datapoints pf_detail and recency_month for PTR within last two years
	    	if (activityDate.after(twoYearsAgo)) {

	    		// recency
	    		if( EventType.OFFER_ACCEPTED.equals(categoryType) || EventType.MEAL.equals(categoryType) || EventType.MEETING.equals(categoryType) ||
	    				EventType.INTERVIEWING.equals(categoryType) || EventType.INTERNAL_INTERVIEW.equals(categoryType) ) {
	    			activityDatesSet.add(activityDate);
	    		}	    			    		
	    	}    	
	    }
	    
	    if ( !activityDatesSet.isEmpty() ) {
	    	// Check if another upstream transform such as TaskTransform has already added some activity dates	    	
	    	if ( ptrCounters.lastActivityDate == null ) {
	    		ptrCounters.lastActivityDate = activityDatesSet;
	    	} else {
	    		ptrCounters.lastActivityDate.addAll(activityDatesSet);
	    	}
    	}	    	    
	    person.meta.provenTrackRecordCounters = ptrCounters;
	    

	    if ( ! submitHist.isEmpty())  {
	    	// only accept the top 100 to match APEX logic
	    	person.submit_hist = trimToMostRecent(submitHist, 100);
	    	gotSubmitalCounter.inc();
	    }

	    if ( ! interactions.isEmpty())  {
	    	// Check if another upstream transform such as TaskTransform has already added some interactions
	    	if (person.interactions == null) {
	    		person.interactions = interactions;
	    	}
	    	else  {
	    		person.interactions.addAll(interactions);
	    	}
	    	gotInteractionCounter.inc();
	    }

	    if (apo != null)  {
	    	//  Got associated position opening
	    	if (associatedPositionTransform.transformOrder(apo, person, spannerClient))  {
		    	associatedPositionTransform.transformOpportunity(apo, person, spannerClient);
	    	}
	    	associatedPositionTransform.transformSubmittedBy(apo, person, spannerClient);
	    }

		return person;
    }

    

    /**
	 * Put most recent on top and trim to specified size 
     * @param submitHist 
	 */
	public List<SubmitHist> trimToMostRecent(List<SubmitHist> submitHist, int size)  {
		// descending (most recent first)
		submitHist.sort((lhs, rhs) -> {  
	    	if (rhs.SubmittedDate == lhs.SubmittedDate)
	    		return 0;
	    	if (rhs.SubmittedDate == null)
	    		return -1;
	    	if (lhs.SubmittedDate == null)
	    		return 1;
	    	return rhs.SubmittedDate.compareTo(lhs.SubmittedDate); 
	    });
		if (submitHist.size() > size)  {
			// ArrayList$SubList is not serializable, so repackage it.
			return new ArrayList<>(submitHist.subList(0, size));
		}
		else {
			return submitHist;
		}
	}


}
