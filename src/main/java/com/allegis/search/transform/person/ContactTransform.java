package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.Table.Contact;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.ContactUtils;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.ResultSet;

/**
 * The AccountContactTransform provides the formation / manipulation of a the Account and Contact details
 * for Persons
 *
 * @author jalpino
 *
 */
public class ContactTransform implements Serializable {
    private static final long serialVersionUID = 1L;
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger( ContactTransform.class );

    /**
     * CONTACT TABLE Metadata
     * 
     * <pre>
     * CREATE INDEX Contact_index_2_ES ON contact ( AccountId, IsDeleted ) 
     * STORING ( Talent_Id__c, FirstName, LastName, MailingStreet, MailingCity, MailingState, MailingPostalCode, 
     *   Talent_Country_Text__c, MailingLatitude, MailingLongitude, Preferred_Phone_Value__c, Preferred_Email_Value__c, 
     *   Work_Email__c, Other_Email__c, Email, MobilePhone, Phone, OtherPhone, HomePhone, 
     *   Data_Quality_Record_Flag__c, Data_Quality_Resume_Flag__c)
     * </pre>
     * 
     * @author mannrivera
     */
    public static class CONTACT{
        /**
         * Enum used for mapping to Salesforce fields
         */
        public static enum Columns {
            contact_id(Contact.Column.Id),
            given_name(Contact.Column.FirstName),
            family_name(Contact.Column.LastName),

            address_line(Contact.Column.MailingStreet),
            city_name(Contact.Column.MailingCity),
            country_sub_division_code(Contact.Column.MailingState),
            postal_code(Contact.Column.MailingPostalCode),
            country_code(Contact.Column.Talent_Country_Text__c),

            latitude(Contact.Column.MailingLatitude),
            longitude(Contact.Column.MailingLongitude),

            preferred_phone(Contact.Column.Preferred_Phone_Value__c),
            preferred_email(Contact.Column.Preferred_Email_Value__c),

            communication_work_email(Contact.Column.Work_Email__c),
            communication_other_email(Contact.Column.Other_Email__c),
            communication_email(Contact.Column.Email),

            communication_mobile_phone(Contact.Column.MobilePhone),
            communication_work_phone(Contact.Column.Phone),
            communication_other_phone(Contact.Column.OtherPhone),
            communication_home_phone(Contact.Column.HomePhone),
            
            employment_position_title(Contact.Column.Title),
            
            tc_title__c(Contact.Column.TC_Title__c),

            data_quality_record_flag(Contact.Column.Data_Quality_Record_Flag__c),
            data_quality_resume_flag(Contact.Column.Data_Quality_Resume_Flag__c);

            Columns(Contact.Column col) {
                this.saleforceFieldName = col.name();
            }

            public String saleforceFieldName;

            public static List<String> columnValues() {
                List<String> columns = new ArrayList<String>();
                for(Columns columnsMapping : Columns.values()) {
                    columns.add(columnsMapping.saleforceFieldName);
                }
                return columns;
            }
        }

        // Set of columns used in transformation
        public static final List<String> COLUMNS = Columns.columnValues();
    }

    private DeadLetterService deadLetterService;

    public ContactTransform(DeadLetterService deadLetterService) {
        super();
        this.deadLetterService = deadLetterService;
    }

  

    /**
     * Builds upon the supplied person with the provided data
     * @param data      Talent Account and Contact details
     * @param person    the person object to build on
     * @return          the modified person object
     */
    public PersonDoc transform( ResultSet resultSet, PersonDoc person ) {
        person.contact_id = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.contact_id.saleforceFieldName);
        person.title = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.employment_position_title.saleforceFieldName);
        setCandidateFullName(resultSet, person);
        setLocationData(resultSet, person);
        setPhoneEmailData(resultSet, person);
        setDataQualityFlags(resultSet, person);
        talentDataCleanUp(resultSet, person);
        
        setCommunitiesJobTitle(resultSet, person);
        
        return person;
    }

    /**
     * Set Candidate FullName
     * Aspire <component name="SetCandidateFullName" subType="default" factoryName="aspire-groovy">
     *
     * In the Vectorizor stage this willbe screened for blacklisted terms.
     *
     * @param resultSet
     * @param person
     */
    private void setCandidateFullName(ResultSet resultSet, PersonDoc person ) {
        person.family_name = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.family_name.saleforceFieldName);
        person.given_name = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.given_name.saleforceFieldName);
    }


    /**
     * Method used to set Phone and Email Fields
     * @param resultSet
     * @param person
     */
    private void setPhoneEmailData(ResultSet resultSet, PersonDoc person) {
        person.communication_work_email = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.communication_work_email.saleforceFieldName);
        person.communication_other_email = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.communication_other_email.saleforceFieldName);
        person.communication_email = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.communication_email.saleforceFieldName);

        person.communication_mobile_phone = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.communication_mobile_phone.saleforceFieldName);
        person.communication_work_phone = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.communication_work_phone.saleforceFieldName);
        person.communication_other_phone = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.communication_other_phone.saleforceFieldName);
        person.communication_home_phone = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.communication_home_phone.saleforceFieldName);

        person.communication_preferred_phone = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.preferred_phone.saleforceFieldName);
        person.communication_preferred_email = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.preferred_email.saleforceFieldName);

        ContactUtils.formatPhoneNumbers(person);
    }

    /**
     * Method used to set Location Fields
     * @param resultSet
     * @param person
     */
    private void setLocationData(ResultSet resultSet, PersonDoc person) {
        person.streetAddress = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.address_line.saleforceFieldName);
        person.city_name = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.city_name.saleforceFieldName);
        person.country_sub_division_code = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.country_sub_division_code.saleforceFieldName);
        person.postal_code = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.postal_code.saleforceFieldName);

        String countryCode = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.country_code.saleforceFieldName);
        // case 'country_code': candidateDocObj.add(field.name, fieldValue.toUpperCase());
        person.country_code = countryCode != null ? countryCode.toUpperCase() : countryCode;

        String latitude = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.latitude.saleforceFieldName);
        String longitude = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.longitude.saleforceFieldName);
        GeneralUtils.populateGeoLocation(person, latitude, longitude, deadLetterService);
    }

	/**
     * Set Data Quality Flags in metadata,  VectorCreationTransform
     * 
     * @param resultSet data from table
     * @param person the person object to build on
     */
    private void setDataQualityFlags(ResultSet resultSet, PersonDoc person ) {
        person.meta.data_quality_record_flag = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.data_quality_record_flag.saleforceFieldName);
        person.meta.data_quality_resume_flag = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.data_quality_resume_flag.saleforceFieldName);
    }

    /**
     * The purpose of this stage is to make additional adjustments
     * to the values extracted from the TransformFieldNames
     *
     * Look for the existence of an @ symbol from within the Full Name and/or phone number fields. A at symbol
     * is primarily found when an email address exists in one of the aforementioned fields.
     *
     * @param resultSet
     * @param person
     */
    private void talentDataCleanUp(ResultSet resultSet, PersonDoc person ) {
        // full_name
        String tempField = person.full_name;
        boolean has_illegal_char = tempField != null && tempField.contains("@");
        if(has_illegal_char)
            person.full_name = "";

        // communication_preferred_phone
        tempField = person.communication_preferred_phone;
        has_illegal_char = tempField != null && tempField.contains("@");
        if(has_illegal_char)
            person.communication_preferred_phone = "";

        // communication_home_phone
        tempField = person.communication_home_phone;
        has_illegal_char = tempField != null && tempField.contains("@");
        if(has_illegal_char)
            person.communication_home_phone = "";

        // communication_work_phone
        tempField = person.communication_work_phone;
        has_illegal_char = tempField != null && tempField.contains("@");
        if(has_illegal_char)
            person.communication_work_phone = "";

        // communication_other_phone
        tempField = person.communication_other_phone;
        has_illegal_char = tempField != null && tempField.contains("@");
        if(has_illegal_char)
            person.communication_other_phone = "";

        // communication_mobile_phone
        tempField = person.communication_mobile_phone;
        has_illegal_char = tempField != null && tempField.contains("@");
        if(has_illegal_char)
            person.communication_mobile_phone = "";
    }
    
    
    
	/**
     * Set Communities job title 
     * 
     * @param resultSet data from table
     * @param person the person object to build on
     */
    private void setCommunitiesJobTitle(ResultSet resultSet, PersonDoc person ) {
    	String communitiesJobTitle = GeneralUtils.getNullSafeString(resultSet, CONTACT.Columns.tc_title__c.saleforceFieldName);
    	
    	if(communitiesJobTitle != null) {
    		person.communities_job_title = Arrays.asList(communitiesJobTitle.split(",")); 
    	}
    	/*else {	  
        	//Test Data
        	List<String> title = new ArrayList<String>();
        	title.add("senior java developer");
        	title.add("java developer");
        	title.add("domain architect");
        	  
    		person.communities_job_title = title;
    	}*/
    }
    
  

}
