package com.allegis.search.transform.person;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.aspire.DictConfig;
import com.allegis.search.enums.CandidateStatus;
import com.allegis.search.enums.Table.WorkHistory;
import com.allegis.search.enums.Table.WorkHistory.Column;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.ProvenTrackRecordCounters;
import com.allegis.search.model.person.ProvenTrackRecordWorkInfo;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.ResultSet;

public class WorkHistoryTransform implements Serializable {
    private static final String UNITED_STATES = "united states";

    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(WorkHistoryTransform.class);

    public static List<String> COLUMNS = GeneralUtils.toNameList(   		
    		WorkHistory.Column.Id,
    		WorkHistory.Column.OrderID__c,
    		WorkHistory.Column.End_Date__c, 
    		WorkHistory.Column.Start_Date__c, 
    		WorkHistory.Column.SourceCompany__c,
    		WorkHistory.Column.Job_Title__c, 
    		WorkHistory.Column.Payment_Frequency_Type__c, 
    		WorkHistory.Column.Pay_Rate__c, 
    		WorkHistory.Column.Finish_Reason__c,
    		WorkHistory.Column.DivisionName__c,
    		WorkHistory.Column.Client_Account_ID__c,
    		WorkHistory.Column.RegionCode__c,
    		WorkHistory.Column.Finish_Code__c,
    		WorkHistory.Column.SourceCompanyId__c
    );

    private static Set<String> positiveFinishReason = new HashSet<>(Arrays.asList(
    		"End of Assignment/Position",
    	    "Contractor Went Permanent",
    	    "Went Direct with Client"
    ));
    
    private static Set<String> negativeFinishReason = new HashSet<>(Arrays.asList(
    		"Smoking in Prohibited Areas",
    		"Misstatement on Appl/Resume",
    		"Damage from Negligence / Conduct",
    		"Unlawful Materials Possession",
    		"Unauth Business Equip Use",
    		"Profane / Vulgar Language",
    		"Engage / Threaten Violence",
    		"Customer Fired Contractor",
    		"Illegal Substance Possessn / Use",
    		"Sleeping on the Job",
    		"Under Influence Alcohol / Drugs",
    		"Insubordination",
    		"Misconduct",
    		"Health / Safety Violence",
    		"Dishonesty",
    		"Rude / Unprofessional Behavior",
    		"Did Not Show",
    		"Failed Background Check",
    		"Unsatisfactory Performance",
    		"Resignation with no notice",
    		"Job Abandonment",
    		"Attendance",
    		"Not Qualified for Job"
    ));

    // create some metrics gathering objects
    private static Counter count = Metrics.counter(WorkHistoryTransform.class, "Got Work History");
    
    private static final Map<String, Float> countryInflationRatesMap = loadCountryInflationRatesMap();
    private static final String[] blacklistInflationRatesArray = new String[] { "argentina", "venezuela", "bolivia" };
    private static final Set<String> blacklistInflationRatesList = new HashSet<>(
            Arrays.asList(blacklistInflationRatesArray));
    
    private static Map<String, Float> loadCountryInflationRatesMap() { // Load inflation rates
        InputStream inputStream = WorkHistoryTransform.class.getResourceAsStream(DictConfig.COUNTRY_INFLATION);
        Map<String, Float> ratesMap = new HashMap<>();
        Scanner scanner = new Scanner(inputStream);
        while (scanner.hasNextLine()) {

            String line = scanner.nextLine();
            String[] countryInflationRates = line.split("\t");
            float inflationRate = Float.parseFloat(countryInflationRates[1].trim());
            ratesMap.put(countryInflationRates[0].trim(), inflationRate);
        }
        scanner.close();
        return ratesMap;
    }

    
    private DeadLetterService deadLetterService;
    
    public WorkHistoryTransform(DeadLetterService deadLetterService) {
		super();
		this.deadLetterService = deadLetterService;
	}

    
    /**
     * Builds upon the supplied person with the provided data
     * 
     * @param data
     *            Talent Account and Contact details
     * @param person
     *            the person object to build on
     * @return the modified person object
     */
    public PersonDoc transform(ResultSet resultSet, PersonDoc person) {
    	    	
    	ProvenTrackRecordCounters ptrCounters = person.meta.provenTrackRecordCounters;
    	List<ProvenTrackRecordWorkInfo> ptrWorkExpList = new ArrayList<ProvenTrackRecordWorkInfo>();
    	    	
        List<Employment> history = new ArrayList<>();
        while (resultSet.next()) {
            Employment employment = new Employment();
            history.add(employment);
            employment.OrganizationName = getNullSafeString(resultSet, WorkHistory.Column.SourceCompany__c);
            employment.PositionTitle = getNullSafeString(resultSet, WorkHistory.Column.Job_Title__c);
            employment.Description = null;
            employment.FinishReason = getNullSafeString(resultSet, WorkHistory.Column.Finish_Reason__c);
            employment.PaymentFrequency = getNullSafeString(resultSet, WorkHistory.Column.Payment_Frequency_Type__c);
            employment.PayRate = getNullSafeString(resultSet, WorkHistory.Column.Pay_Rate__c);
            employment.StartDate = getNullSafeDate(resultSet, WorkHistory.Column.Start_Date__c, person);
            employment.EndDate = getNullSafeDate(resultSet, WorkHistory.Column.End_Date__c, person);
            employment.Id = getNullSafeString(resultSet, WorkHistory.Column.Id); 
            employment.OrderId = getNullSafeString(resultSet, WorkHistory.Column.OrderID__c);
            employment.DivisionName = getNullSafeString(resultSet, WorkHistory.Column.DivisionName__c);
            employment.ClientAccountId = getNullSafeString(resultSet, WorkHistory.Column.Client_Account_ID__c);
            employment.RegionCode = getNullSafeString(resultSet, WorkHistory.Column.RegionCode__c);
            employment.FinishCode = getNullSafeString(resultSet, WorkHistory.Column.Finish_Code__c);
            employment.SourceCompanyId = getNullSafeString(resultSet, WorkHistory.Column.SourceCompanyId__c);
        }
        
        if (!history.isEmpty()) {       	
            // descending (most recent first)
            history.sort((lhs, rhs) -> {
                if (rhs.StartDate == lhs.StartDate)
                    return 0;
                if (rhs.StartDate == null)
                    return -1;
                if (lhs.StartDate == null)
                    return 1;
                return rhs.StartDate.compareTo(lhs.StartDate);
            });

            // Add to, or create the employment list
            if (person.employ_hist == null) {
                person.employ_hist = history;
            } else {
                person.employ_hist.addAll(history);
            }
            
            for (Employment employment : history) {
            	
            	String finishReason = employment.FinishReason;
                boolean isMostRecentFinishReasonUnknown = person.most_recent_finish_reason == null || person.most_recent_finish_reason.equals("UNKNOWN");
                if (isMostRecentFinishReasonUnknown &&  finishReason!= null) {
                    // Get the first w/ a finish reason
                    person.most_recent_finish_reason = finishReason;
                }

                // Get the first w/ a pay rate and a start date
                if (person.actual_payrate == null && employment.PayRate != null && employment.StartDate != null) {
                    person.actual_payrate = computePay(person, employment);
                    if (person.actual_payrate != null) {
                        person.payrateInterval = employment.PaymentFrequency;
                        person.payrate_title = employment.PositionTitle;
                    }
                }
                if (person.most_recent_finish_reason == null && person.actual_payrate == null) {
                    break; // nothing more to get
                }
                
                // Capture PTR Counters - positive/negative finish reasons
                if (finishReason != null) {
                	if (positiveFinishReason.contains(finishReason)) {
                		ptrCounters.posFinishReasonCounter++;
                	}
                	if (negativeFinishReason.contains(finishReason)) {
                		ptrCounters.negFinishReasonCounter++;
                	}
                }                
                // Capture PTR info for datapoints - ma_count and ma_detail (multiple assignments info)
                if ( employment.FinishCode != null && !employment.FinishCode.equals("ADJ") ) {
                	ptrWorkExpList.add(new ProvenTrackRecordWorkInfo(employment.Id, employment.SourceCompanyId));
                }               
            }                   
    	    person.meta.provenTrackRecordCounters = ptrCounters;
    	    
            if ( !ptrWorkExpList.isEmpty() ) {
            	// Check if another upstream transform has already added any work info for proven track record  	    	
            	if ( person.meta.provenTrackRecordWorkInfo == null ) {
    	    		person.meta.provenTrackRecordWorkInfo = ptrWorkExpList;
    	    	} else {
    	    		person.meta.provenTrackRecordWorkInfo.addAll(ptrWorkExpList);
    	    	}
            }

            count.inc();
        }

        return person;
    }


    private Date getNullSafeDate(ResultSet resultSet, Column col, PersonDoc person) {
		return GeneralUtils.getNullSafeDate(resultSet, col.name(), person, deadLetterService);
	}


	private String getNullSafeString(ResultSet resultSet, Column col) {
		return GeneralUtils.getNullSafeString(resultSet, col.name());
	}


	/**
     * Populates a few top level payrate fields from values found in the most recent
     * employment
     * 
     * @param person
     * @param employment
     * @return true if criteria met and calculated
     */
    public Float computePay(PersonDoc person, Employment employment) {
        float parsedValue = Float.parseFloat(employment.PayRate);

        // calculate number of months between current date and recent employment start
        // date
        int months = Months.monthsBetween(new DateTime(employment.StartDate), new DateTime(System.currentTimeMillis()))
                .getMonths();

        if ( CandidateStatus.CURRENT.equals(person.status)) {
            if (months < 12) {
                return parsedValue;
                
            } else {
                
                String country = person.country_code;

                // default the inflation rate to United States if the country is not found in
                // the inflation
                // rates map (or) if the country's inflation rate is greater than 50% and less
                // than -10%
                if (country == null || !countryInflationRatesMap.containsKey(country) || blacklistInflationRatesList.contains(country)) {
                    country = UNITED_STATES;
                }

                float inflationRate = countryInflationRatesMap.get(country);

                // Adjust rate of inflation for every 12 months
                float numOfAdjustmentPeriods = months / 12;

                // calculate pay rate based on adjusted rate of inflation formula
                float parsedPayRate = (float) (parsedValue * Math.pow((1 + (inflationRate / 100)), numOfAdjustmentPeriods));
                
                return parsedPayRate;
            
            }
        } else if ( CandidateStatus.FORMER.equals(person.status)) {
            if (months < 12) {
                return parsedValue;
            }
        }
        return 0.0F; //return 0.0 instead of null
    }

}
