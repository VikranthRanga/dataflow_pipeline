package com.allegis.search.transform.person;

import java.io.Serializable;
import java.net.URI;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Distribution;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.MetricsNamespace;
import com.allegis.search.http.RestClient;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.ProvenTrackRecordData;
import com.allegis.search.utils.CounterSet;
import com.allegis.search.utils.DateUtils;
import com.allegis.search.utils.HashingUtils;
import com.allegis.search.utils.ProvenTrackRecordUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Transform Proven Track Record json object by calling PTR endpoint for the PTR info for each talent.
 * 
 * @author jperecha
 *
 */
public class ProvenTrackRecordInputTransform implements Serializable {

	private static final long serialVersionUID = -1011585847870189344L;

	private static final Logger logger = LoggerFactory.getLogger( ProvenTrackRecordInputTransform.class );
		
	private static final Integer CONNECTION_TIMEOUT = 18 * 1000;//5 seconds
	private static final Integer SOCKET_TIMEOUT = 18 * 1000; //5 seconds
	private static final Integer CONNECTION_REQUEST_TIMEOUT = 18 * 1000; //5 seconds

	private CounterSet badStatusCount = new CounterSet("ProvenTrackRecord API Status");	
    private static Counter ptrCode = Metrics.counter(ProvenTrackRecordInputTransform.class, "Got Proven Track Record");    
	
    private CounterSet ptrRespCounters = new CounterSet("PTR Response");	
    private Distribution ptrRespTime = Metrics.distribution(MetricsNamespace.STAGE.name(), "PTR Response");
    
    /**
	 * Default Constructor
	 */
    public ProvenTrackRecordInputTransform() {
    	super();
    }
	
    /**
     * Transform proven track record specific POJO into an json object.
     * 
     * @param ptrData the proven track record POJO
     * @return the proven track record json object
     */
    public JsonObject transformProvenTrackRecordDataToJsonObject(ProvenTrackRecordData ptrData) {
    	JsonElement ptrDataObj = new Gson().toJsonTree(ptrData);
   
    	return ptrDataObj.getAsJsonObject();
    }
    
    /**
     * Builds upon the proven track record json object to create an REST request
	 * which will be sent to the proven track record api.
	 * 
     * @param person		the person object to build on
     * @param ptrDataObj	the proven track record json object
     * @param restClient	the rest client
     * @param url			the request url
     * @return the modified person object
     * @throws Exception
     */
    public PersonDoc transform( PersonDoc person, JsonObject ptrDataObj, RestClient restClient, String url ) throws Exception {
		
		if ( ptrDataObj != null && ptrDataObj.size() > 0 ) {
			makeProvenTrackRecordRequest( person, ptrDataObj, restClient, url);
		}		
		return person;	
	}
    
    /**
     * Create a request string for the proven track record api.
     * 
     * @param person		the person object to build on
     * @param ptrDataObj	the proven track record json object
     * @param restClient	the rest client
     * @param url			the request url
     * @throws Exception
     */
    private void makeProvenTrackRecordRequest( PersonDoc person, JsonObject ptrDataObj, RestClient restClient, String url ) throws Exception {
		
		JsonObject ptrOutput = null;
		
		// Set timeouts for rest connection and response 
		RequestConfig Default = RequestConfig.DEFAULT;
	    RequestConfig requestConfig = RequestConfig.copy(Default)
	            .setConnectTimeout(CONNECTION_TIMEOUT)
	            .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT)
	            .setSocketTimeout(SOCKET_TIMEOUT)
	            .build();
		
		HttpPost httpPost = new HttpPost();
		httpPost.setURI( new URI(url.toString()) );
		httpPost.setEntity(new StringEntity(ptrDataObj.toString(), ContentType.APPLICATION_JSON));
		httpPost.setConfig(requestConfig);
		long startTime = System.currentTimeMillis();
		
		try ( CloseableHttpResponse response = restClient.makeRequest( httpPost ); ){            
			
			ptrCode.inc();
            int statusCode = response.getStatusLine().getStatusCode();   
            
            // We got a response, check if it includes an content
            if( statusCode == HttpStatus.SC_OK ) {              	
            	logger.info("Proven track record api returned status code="+statusCode+" for Id="+person.id);
            	String content = IOUtils.toString( response.getEntity().getContent(), "UTF-8" );
            	if ( StringUtils.isNotBlank(content) ) {
            		// Convert response to json object
            		ptrOutput = parseResponse(content); 
            	}
            }           
            else {
            	logger.warn("Proven track record api returned status code="+statusCode+" for Id="+person.id);
            	badStatusCount.inc(String.valueOf(statusCode));  
            }
            
            // ONLY update the talent if we received the ouput
            if ( null != ptrOutput && !ptrOutput.isJsonNull() ) {
            	ProvenTrackRecordUtils.buildProvenTrackRecordFields(person, ptrOutput); 
            	person.meta.proventrackrecord_output = ptrOutput.toString();
        	    person.meta.proventrackrecord_hash = HashingUtils.getHash(ptrDataObj.toString());
                person.meta.proventrackrecord_last_modified_date = DateUtils.reformatToDateTime( DateUtils.now() );
                person.meta.proventrackrecord_needsUpdate = true;
            }
            
		} catch (Exception x) {
			ptrRespCounters.inc(x.getClass().getSimpleName());
			logger.error("Error while calling Proven track record api: "+x.getMessage() + " for Id="+person.id); 
			throw x;
        } finally  {        	
        	stratify(System.currentTimeMillis() - startTime, ptrRespCounters, ptrRespTime);
        	httpPost.reset();
        }
	}
    
	private void stratify(long duration, CounterSet counterSet, Distribution distribution) {

		distribution.update(duration);
		
		int max = 6000;
        if( duration >= max ) {
            counterSet.inc( "> 6s");
        }else {
            for( int i=1000; i < 6000; i+=1000 ) {
                if( duration < i ) {
                    counterSet.inc( "< "+ i/1000 +"s");
                    break;
                }
            }
        }
        
	}
    
    /**
	 * Parse the output response of proven track record api
	 * 
	 * @param content the string response
	 * @return the parsed response
	 */
	public static JsonObject parseResponse( String content ) {
		
		JsonObject outputObj = null;
		JsonParser parser = new JsonParser();
		JsonElement responseJson = parser.parse( content );
		
		if ( !responseJson.isJsonNull() ) {
			outputObj = responseJson.getAsJsonObject();			
		}
		return outputObj;
	}	
}
