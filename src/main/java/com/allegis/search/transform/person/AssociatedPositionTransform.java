package com.allegis.search.transform.person;

import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.Table;
import com.allegis.search.enums.Table.Opportunity;
import com.allegis.search.enums.Table.Order;
import com.allegis.search.enums.Table.User;
import com.allegis.search.model.CommonDoc;
import com.allegis.search.model.person.AssociatedPositionOpening;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;
import com.allegis.search.utils.SpannerKey;
import com.allegis.search.utils.UserUtils;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.ReadContext;
import com.google.cloud.spanner.Struct;

/**
 * AssociatedPositionTransform
 * <pre>
 * High Level Logic 
 * 
 * - For a given talent, pick up all the events from the events table.
 * - Of those events, pick the latest event and take that event's "whatid". This whatid is the orderId.
 * - Using that orderId read the orders table in the order stage.
 * - The order table gives us the opportunity id which we use in the opportunity stage.
 * - once the opportunity stage is done, we have the associated position opening which we can write to ES
 * </pre>
 * 
 * TODO: find any example where a candidate had more than one entry in associated_postion_openings, verify this.
 * 
 * @author cmahajan
 *
 */
public class AssociatedPositionTransform {
    

    static List<String> orderColumns = GeneralUtils.toNameList(
        	Order.Column.Status,
        	Order.Column.OrderNumber,
        	Order.Column.OpportunityId
    );

    
    private DeadLetterService deadLetterService;
     
    
    public AssociatedPositionTransform(DeadLetterService deadLetterService) {
		super();
		this.deadLetterService = deadLetterService;
	}


    static List<String> opportunityColumns = GeneralUtils.toNameList(
    		Opportunity.Column.Opportunity_Num__c,
    		Opportunity.Column.Req_Job_Title__c,
    		Opportunity.Column.Global_Account_Name__c,
    		Opportunity.Column.Req_Pay_Rate_Max__c
    );

    

    // create some metrics gathering objects
    private static Counter orderCounter = Metrics.counter(AssociatedPositionOpening.class, "Got orders");
    private static Counter orderMissingCounter = Metrics.counter(AssociatedPositionOpening.class, "Order Not Found");


    /**
     * Builds upon the supplied POJO with data found in the foreign key to the order table
     * 
     * @param apo - AssociatedPositionOpening POJO
     * @param accountId - person.id
     * @param spannerClient - the person object to build on
     * @return - true if has an order else false
     */
    public boolean transformOrder(AssociatedPositionOpening apo, CommonDoc doc, DatabaseClient spannerClient)  {

        String whatId = (apo != null) ? apo.whatId : null;

        if (whatId == null)
        	return false;

  		try (ReadContext readContext = spannerClient.singleUse())  // force quick closure
  		{
  			SpannerKey primaryKey = SpannerKey.of( whatId );
            Struct row = readContext.readRow(Table.Order.TABLE , primaryKey.get(), orderColumns);

        	if (row == null) {
        		orderMissingCounter.inc();
        		deadLetterService.writeDataQualityWarning("Missing "+Table.Order.TABLE+" table row", "id="+whatId, doc);
         		return false;
        	}
        	else  {
        	    
        		orderCounter.inc();            		
        		apo.PositionOpeningID = GeneralUtils.getNullSafeString(row, Order.Column.OrderNumber.name());
        		apo.Status = GeneralUtils.getNullSafeString(row, Order.Column.Status.name());
        		apo.opportunityId = GeneralUtils.getNullSafeString(row, Order.Column.OpportunityId.name());
         		return true;
        	}
        }
    }
    
     

	private static Counter opportunityCounter = Metrics.counter(AssociatedPositionOpening.class, "Got opportunity");
    private static Counter opportunityMissingCounter = Metrics.counter(AssociatedPositionOpening.class, "Opportunity missing");
   
    
    /**
     * Builds upon the supplied POJO with data found in the foreign key to the Opportunity table
     * 
     * @param apo - AssociatedPositionOpening POJO
     * @param doc - person
     * @param spannerClient - the person object to build on
     * @return - true if has opportunityId else false
     */
    public boolean transformOpportunity(AssociatedPositionOpening apo, CommonDoc doc, DatabaseClient spannerClient)  {

        String opportunityId = (apo != null) ? apo.opportunityId : null;

        if (opportunityId == null) 
        	return false;
        
  		try (ReadContext readContext = spannerClient.singleUse())  // force quick closure
  		{
  			SpannerKey primaryKey = SpannerKey.of( opportunityId );
            Struct row = readContext.readRow(Opportunity.TABLE , primaryKey.get(), opportunityColumns );

        	if (row == null) {
        		deadLetterService.writeDataQualityWarning("Missing "+Opportunity.TABLE+" table row", "id="+opportunityId, doc);
        		opportunityMissingCounter.inc();
          		return false;
        	}
        	else  {                	
                apo.MaxPayRate = GeneralUtils.getNullSafeDouble(row, Opportunity.Column.Req_Pay_Rate_Max__c.name(), doc, deadLetterService);
                apo.StaffingOrderID = GeneralUtils.getNullSafeString(row, Opportunity.Column.Opportunity_Num__c.name());
                apo.PositionTitle = GeneralUtils.getNullSafeString(row, Opportunity.Column.Req_Job_Title__c.name());
                apo.ClientName = GeneralUtils.getNullSafeString(row, Opportunity.Column.Global_Account_Name__c.name());        
                opportunityCounter.inc();                 
          		return true;
        	}
        }
    }
 
    /**
     * Builds upon the supplied POJO with data found in the foreign key to the user table
     * 
     * @param apo - AssociatedPositionOpening POJO
     * @param doc - person.id
     * @param spannerClient - the person object to build on
     * @return - true if has SubmittedById else false
     */
	public boolean transformSubmittedBy(AssociatedPositionOpening apo, CommonDoc doc, DatabaseClient spannerClient) {
        String submittedById = (apo != null) ? apo.SubmittedById : null;
        
        if (submittedById == null) 
        	return false;
		
		apo.SubmittedByName = UserUtils.getName(spannerClient, apo.SubmittedById);
    	if (apo.SubmittedByName == null) {
    		deadLetterService.writeDataQualityWarning("Missing "+User.TABLE+" table row", "id="+apo.SubmittedById, doc);
    		// there are no appropriate counters for this table
    	}
    	return true;
	}

}