package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.Table.TalentDocument;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.Resume_N;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.ResultSet;

/**
 * The AccountContactTransform provides the formation / manipulation of a the
 * Account and Contact details
 * for Persons
 * 
 * @author jalpino
 *
 */
public class ResumeInfoTransform implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger( ResumeInfoTransform.class );

    
    private static final String DOC_OTHER_TYPE = "Other";
    private static final String DOC_SOCIAL_PROFILE_TYPE = "Social Profile";
    
    public static List<String> COLUMNS = GeneralUtils.toNameList(
	        TalentDocument.Column.Id,
	        TalentDocument.Column.CreatedDate,	// Documents are never updated, only created. So we treat it as a "modified date"
	        TalentDocument.Column.Document_Type__c,
	        TalentDocument.Column.HTML_Version_Available__c
    );

    private static Counter count = Metrics.counter(WorkHistoryTransform.class, "Got Resume Info");
    private static Counter countSkippedSocialProfile = Metrics.counter(WorkHistoryTransform.class, "Skipped "+DOC_SOCIAL_PROFILE_TYPE);

    
    private DeadLetterService deadLetterService;
    
    public ResumeInfoTransform(DeadLetterService deadLetterService) {
		super();
		this.deadLetterService = deadLetterService;
	}


    /**
     * Builds upon the supplied person with the provided data
     * 
     * @param data
     *            Talent Account and Contact details
     * @param person
     *            the person object to build on
     * @return the modified person object
     */
    public PersonDoc transform(ResultSet resultSet, PersonDoc person) {
        List<Resume_N> resumes = new ArrayList<>();
        while (resultSet.next()) {
            
            String docType = GeneralUtils.getNullSafeString(resultSet, TalentDocument.Column.Document_Type__c.name());
            String resumeId = resultSet.getString(TalentDocument.Column.Id.name());

            if(DOC_SOCIAL_PROFILE_TYPE.equalsIgnoreCase( docType ) ) {
            	//  Temporally skip these until the full solution for Social Profile is implemented
            	logger.info("Skipped '"+DOC_SOCIAL_PROFILE_TYPE+"' type on resumeId="+resumeId+",  for talent id="+person.id);
            	countSkippedSocialProfile.inc();
            	continue;
            }

            if( ! DOC_OTHER_TYPE.equalsIgnoreCase( docType ) ) {
                // Take the freshest resume data and populate Resume_N
                Resume_N resume = new Resume_N(resumeId);
                resume.modified = GeneralUtils.getNullSafeDate(resultSet, TalentDocument.Column.CreatedDate.name(), person, deadLetterService);
                resume.type = docType;
                resume.hasHtmlVersion = GeneralUtils.getNullSafeBoolean(resultSet, TalentDocument.Column.HTML_Version_Available__c.name() );
                resumes.add(resume);
            }
        }

        if ( ! resumes.isEmpty()) {
            // get the most recent
            resumes.sort((lhs, rhs) -> {
                if (rhs.modified == lhs.modified)
                    return 0;
                if (rhs.modified == null)
                    return -1;
                if (lhs.modified == null)
                    return 1;
                return rhs.modified.compareTo(lhs.modified); // descending
            });

            person.resume_1 = resumes.get(0);            
            person.qualifications_last_resume_modified_date = person.resume_1.modified;
            person.resume_count = resumes.size();
            count.inc();
        }

        return person;
    }

}
