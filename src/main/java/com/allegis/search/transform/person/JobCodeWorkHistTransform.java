package com.allegis.search.transform.person;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.search.enums.Table.Opportunity;
import com.allegis.search.enums.Table.Order;
import com.allegis.search.enums.Table.WorkHistory;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.JobCodeWorkHist;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.utils.DateUtils;
import com.allegis.search.utils.GeneralUtils;
import com.allegis.search.utils.SpannerKey;
import com.allegis.search.utils.WorkHistUtils;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.ReadContext;
import com.google.cloud.spanner.Struct;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Extract data points for job code classifier. Transform employment history and data points into 
 * an job code classifier specific POJO.
 * 
 * @author jperecha
 *
 */
public class JobCodeWorkHistTransform implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String AG_WORK_HIST_SOURCE = "AG";
    private static final String RESUME_WORK_HIST_SOURCE = "Resume";
    private static final int WORK_HIST_BLOCKS_SIZE = 25;
    
    public static List<String> WORK_HIST_COLUMN = GeneralUtils.toNameList(
    		WorkHistory.Column.OrderID__c
    );   
    
    public static List<String> ORDER_COLUMN = GeneralUtils.toNameList(
    		Order.Column.OpportunityId
    );
    
    public static List<String> OPPORTUNITY_COLUMNS = GeneralUtils.toNameList(
    		Opportunity.Column.Legacy_Product__c,
    		Opportunity.Column.Req_RVT_Occupation_Code__c,
    		Opportunity.Column.Req_Job_Description__c
    );
    
    private static Counter orderToOpportunityNotFoundCounter = Metrics.counter(JobCodeWorkHistTransform.class.getSimpleName(), "Order to Opportunity Not Found");
    private static Counter workHistToOpportunityNotFoundCounter= Metrics.counter(JobCodeWorkHistTransform.class.getSimpleName(), "Work Hist to Opportunity Not Found");
    
    public JobCodeWorkHistTransform() {
    	super();
    }
    
    /**
     * Builds upon the supplied employment history to create classifier specific 
     * work history input.
     * 
     * @param person	the person object to build on
     * @param hist		the list of classifier specific work history
     * @param spannerClient	the spanner client to connect to spanner
     * 
     * @return the modified list of classifier specific work history
     * @throws IOException
     */
    public static List<JobCodeWorkHist> transform( PersonDoc person, List<JobCodeWorkHist> hist, DatabaseClient spannerClient ) throws IOException {
    	 
    	if ( null != person.employ_hist ) {
    		// Making a copy of employ_hist to avoid mutation issues during sorting
    		List<Employment> employHist = person.employ_hist;
	    	WorkHistUtils.sortWorkHist(employHist);
	    	
	    	int maxWorkHistBlocks = employHist.size();
	    	if ( maxWorkHistBlocks > WORK_HIST_BLOCKS_SIZE ) {
	    		maxWorkHistBlocks = Integer.min(maxWorkHistBlocks, WORK_HIST_BLOCKS_SIZE);
	    	}
	    	
	    	for ( int i=0; i<maxWorkHistBlocks; i++ )
	    	{
	    		JobCodeWorkHist jobCodeWorkHist = new JobCodeWorkHist();
	    		jobCodeWorkHist.talentId = person.id; 
	    		
	    		Employment histblock = employHist.get(i);
	    		jobCodeWorkHist.startDate = histblock.StartDate;
	    		jobCodeWorkHist.endDate = histblock.EndDate;
	    		jobCodeWorkHist.positionTitle = histblock.PositionTitle; 
	    		jobCodeWorkHist.workHistId = histblock.Id;
	    		jobCodeWorkHist.orderId = histblock.OrderId;
	    		jobCodeWorkHist.source = AG_WORK_HIST_SOURCE;
	    		getDataPoints( jobCodeWorkHist.orderId, spannerClient, jobCodeWorkHist );     		
	    		hist.add( jobCodeWorkHist );
	    	}
    	}
    	
    	if ( null != person.meta.resumeProfile ) {
    		if ( person.meta.resumeProfile.getEmploymentHistory().size() > 0 ) {
    			// Making a copy of employmentHistory to avoid mutation issues during sorting
    			List<EmploymentBean> resumeEmployHist = person.meta.resumeProfile.getEmploymentHistory();
    			WorkHistUtils.sortResumeWorkHist(resumeEmployHist);
    			
    			int maxResumeWorkHistBlocks = resumeEmployHist.size();
    	    	if ( maxResumeWorkHistBlocks > WORK_HIST_BLOCKS_SIZE ) {
    	    		maxResumeWorkHistBlocks = Integer.min(maxResumeWorkHistBlocks,WORK_HIST_BLOCKS_SIZE);
    	    	}
    	    	
	    		for ( int i=0; i<maxResumeWorkHistBlocks; i++ ) {
	    			JobCodeWorkHist jobCodeWorkHist = new JobCodeWorkHist();
	    			jobCodeWorkHist.talentId = person.id; 
	    			
	    			EmploymentBean histblock = resumeEmployHist.get(i);
	    			jobCodeWorkHist.positionTitle = histblock.getPositionTitle();
	    			jobCodeWorkHist.positionDescr = histblock.getPositionDescription();
	    			if ( null != histblock.getHistoryRange()) {
	    				jobCodeWorkHist.startDate = histblock.getHistoryRange().getStartDate();
	    				jobCodeWorkHist.endDate = histblock.getHistoryRange().getEndDate();
	    			}
	    			jobCodeWorkHist.source = RESUME_WORK_HIST_SOURCE;
	        		hist.add( jobCodeWorkHist );
	    		} 
    		}
    	}
    	return hist;    	
    }
    
    public static JsonArray transformJobCodeWorkHistToJsonArray(List<JobCodeWorkHist> JobCodeWorkHistList) {
    	
    	JsonArray workHistArr = new JsonArray();
    	
    	for ( JobCodeWorkHist workHist:JobCodeWorkHistList ) {
			
			JsonObject workHistObj = new JsonObject();
			workHistObj.addProperty( "talent_id", workHist.getNullSafeTalentId() );
			workHistObj.addProperty( "hcs_code", workHist.getNullSafeHcsCode() );
			workHistObj.addProperty( "work_history_id", workHist.getNullSafeWorkHistId() );
			workHistObj.addProperty( "ph_id", workHist.getNullSafePhId() );			
			workHistObj.addProperty( "job_title", workHist.getNullSafePositionTitle() );
			workHistObj.addProperty( "description", workHist.getNullSafePositionDescr() );
			workHistObj.addProperty( "source", workHist.source );
			if ( workHist.startDate != null ) {
				workHistObj.addProperty( "start_date", DateUtils.reformatToDate(workHist.startDate) );	
			} else {
				workHistObj.addProperty( "start_date", "" );					
			}
			if ( workHist.endDate != null ) {
				workHistObj.addProperty( "end_date", DateUtils.reformatToDate(workHist.endDate) );				
			} else {
				workHistObj.addProperty( "end_date", "" );					
			}
			
			workHistArr.add( workHistObj );
		}
    	
    	return workHistArr;
    	
    }
    
    /**
     * Method to get data points ph_id and hcs_code from Spanner
     * 
     * @param workHistId	the work history id
     * @param spannerClient	the spanner client to connect to spanner
     * @param jobCodeWorkHist	the job code classifier specific work history
     */
    private static void getDataPoints( String orderId, DatabaseClient spannerClient, JobCodeWorkHist jobCodeWorkHist ) {    	   	
            
        if ( null != orderId ) {
	    	try ( ReadContext orderContext = spannerClient.singleUse() )  // force quick closure
	        {
	            SpannerKey orderKey = SpannerKey.of( orderId );
	            Struct orderRow = orderContext.readRow( Order.TABLE, orderKey.get(), ORDER_COLUMN );
	
	            if ( orderRow == null  ) {               
	            	orderToOpportunityNotFoundCounter.inc();
	                return;
	            }
	            
	            String opportunityId = GeneralUtils.getNullSafeString( orderRow, Order.Column.OpportunityId.name() );
	            		            
	            try ( ReadContext opportunityContext = spannerClient.singleUse() )
	            {		
	            	SpannerKey opportunityKey = SpannerKey.of( opportunityId );
	            	Struct opportunityRow = opportunityContext.readRow(
	                            Opportunity.TABLE, opportunityKey.get(), OPPORTUNITY_COLUMNS );
	            
            		if ( opportunityRow == null ) {               
            			workHistToOpportunityNotFoundCounter.inc();
                        return;
                    }
            		String phId = GeneralUtils.getNullSafeString( opportunityRow, Opportunity.Column.Legacy_Product__c.name() );
        			String hcsCode = GeneralUtils.getNullSafeString( opportunityRow, Opportunity.Column.Req_RVT_Occupation_Code__c.name() );
        			String description = GeneralUtils.scrubCRLFTab(GeneralUtils.getNullSafeString( opportunityRow, Opportunity.Column.Req_Job_Description__c.name() ));
        			jobCodeWorkHist.phId = phId;
        			jobCodeWorkHist.hcsCode = hcsCode;
        			jobCodeWorkHist.positionDescr = description;
	            }
	        }
        }                 
    }
    
}
