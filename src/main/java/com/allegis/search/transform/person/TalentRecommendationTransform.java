package com.allegis.search.transform.person;

import java.io.Serializable;
import java.util.List;

import com.allegis.search.enums.Table.TalentRecommendation;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.ProvenTrackRecordCounters;
import com.allegis.search.utils.GeneralUtils;
import com.google.cloud.spanner.ResultSet;

/**
 * Sourced from talent recommendation table.
 * used to extract datapoint for Proven Track Record
 * 
 * @author jperecha
 */
public class TalentRecommendationTransform implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static List<String> COLUMNS = GeneralUtils.toNameList(
			TalentRecommendation.Column.Reference_Check_Completed__c
    );
	
	 /**
     * Builds upon the supplied person with the provided data
     * @param resultSet     TalentRecommendation details
     * @param personDoc    the person object to build on
     * @return          the modified person object
     */
    public PersonDoc transform(ResultSet resultSet, PersonDoc personDoc) {
    	
    	ProvenTrackRecordCounters ptrCounters = personDoc.meta.provenTrackRecordCounters;
    	
    	while (resultSet.next()) {    		
    		Boolean isRefCheckComplete = GeneralUtils.getNullSafeBoolean(resultSet,TalentRecommendation.Column.Reference_Check_Completed__c.name());
        	if (isRefCheckComplete) {
        		ptrCounters.rcCounter++;
        	}
    	}    	
    	personDoc.meta.provenTrackRecordCounters = ptrCounters;
	    
    	return personDoc;
    }

}
