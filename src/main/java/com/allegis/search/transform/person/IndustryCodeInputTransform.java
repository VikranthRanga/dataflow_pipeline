package com.allegis.search.transform.person;

import java.io.Serializable;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.beam.repackaged.core.org.apache.commons.lang3.ObjectUtils;
import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Distribution;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.MetricsNamespace;
import com.allegis.search.http.RestClient;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.IndustryCodeOutput;
import com.allegis.search.model.person.IndustryCodeWorkHist;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.CounterSet;
import com.allegis.search.utils.DateUtils;
import com.allegis.search.utils.HashingUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.Gson;
import com.google.common.reflect.TypeToken;

/**
 * Transform industry code classifier specific POJO into an json object.
 * 
 * @author jperecha
 *
 */
public class IndustryCodeInputTransform implements Serializable {

	private static final long serialVersionUID = -1440339609507910707L;

	private static final Logger logger = LoggerFactory.getLogger( IndustryCodeInputTransform.class );
	
	private static final String WORK_HISTORY_INDUSTRY_CODES = "work_history_industry_codes";
	private static final String WHOLISTIC_INDUSTRY_CODES = "wholistic_industry_codes";
	private static final String CONFIDENCE_PARAM = "confidence";
	private static final String SECTOR_DESCRIPTION_PARAM = "sector_description";
	private static final String COMPANY_NAME_PARAM = "company_name";
	private static final String NAICS_DESC_PARAM = "naics_description ";
	private static final String NAICS_CODE_PARAM = "naics_code";
	private static final String WORKHIST_ID_PARAM = "work_history_id";
	private static final String SOCKET_TIMEOUT_EXCEPTION_MSG = "Read timed out";

	private static final Integer CONNECTION_TIMEOUT = 5 * 1000;//5 seconds
	private static final Integer SOCKET_TIMEOUT = 5 * 1000; //5 seconds
	private static final Integer CONNECTION_REQUEST_TIMEOUT = 5 * 1000; //5 seconds

	private SimpleDateFormat SDFormat = new SimpleDateFormat("yyyy-MM-dd");
	// create some metrics gathering objects
	private CounterSet badStatusCount = new CounterSet("Industry API Status");	
    private static Counter industryCode = Metrics.counter(IndustryCodeInputTransform.class, "Got Industry Code Classifier");    
	
    private CounterSet indRespCounters = new CounterSet("Industry Response");	
    private Distribution indRespTime = Metrics.distribution(MetricsNamespace.STAGE.name(), "Industry Response");
	    
	/**
	 * Default Constructor
	 */
	public IndustryCodeInputTransform() {
		super();	
	}
	
	/**
	 * Builds upon the industry code classifier work history to create an input json object
	 * which will be sent to the industry code classifier api.
     * 
	 * @param person	the person object to build on
	 * @param hist		the industry code classifier specific work history
	 * @param restClient	the rest client
	 * @param url		the request url
	 * @return the modified person object
	 * @throws Exception 
	 */
	public PersonDoc transform( PersonDoc person, JsonArray workHistArr, RestClient restClient, String url, DeadLetterService deadLetterService ) throws Exception {
		
		if ( workHistArr != null && workHistArr.size() > 0 ) {
			makeIndustryCodeRequest( person, workHistArr, restClient, url, deadLetterService);
		}
		
		return person;	
	}
	
	 public JsonArray transformIndustryCodeWorkHistToJsonArray(List<IndustryCodeWorkHist> industryCodeWorkHist) {
	    	
		 	JsonArray workHistArr = new JsonArray();	
			
			for ( IndustryCodeWorkHist workHist:industryCodeWorkHist ) {
									
				JsonObject workHistObj = new JsonObject();
				workHistObj.addProperty( "talent_id", workHist.getNullSafeTalentId() );
				workHistObj.addProperty( "work_history_id", workHist.getNullSafeWorkHistId() );
				workHistObj.addProperty( "accountid", workHist.getNullSafeAccountId() );
				workHistObj.addProperty( "division", workHist.getNullSafeDivision() );	
				workHistObj.addProperty( "company_name", workHist.getNullSafeCompanyName() );							
				workHistObj.addProperty( "country", workHist.getNullSafeCountry() );	
				workHistObj.addProperty( "state", workHist.getNullSafeState() );	
				workHistObj.addProperty( "city", workHist.getNullSafeCity() );				
				workHistObj.addProperty( "source", workHist.source );
				
				
				if ( workHist.startDate != null ) {
					workHistObj.addProperty( "start_date", SDFormat.format(workHist.startDate) );	
				} else {
					workHistObj.addProperty( "start_date", "" );					
				}
				if ( workHist.endDate != null ) {
					workHistObj.addProperty( "end_date", SDFormat.format(workHist.endDate) );				
				} else {
					workHistObj.addProperty( "end_date", "" );					
				}
				
				workHistArr.add( workHistObj );
			}
	    	
	    	return workHistArr;
	    	
	    } 
	
	/**
	 * Create a request string for the industry code classifier api.
	 * 
	 * @param person	the person object to build on
	 * @param workHistArr	the json array of industry code classifier work history
	 * @param restClient the rest client
	 * @param url	the request url
	 * @throws Exception 
	 */
	private void makeIndustryCodeRequest( PersonDoc person, JsonArray workHistArr, RestClient restClient, String url, DeadLetterService deadLetterService ) throws Exception {
		// Set timeouts for rest connection and response 
		RequestConfig Default = RequestConfig.DEFAULT;
	    RequestConfig requestConfig = RequestConfig.copy(Default)
	            .setConnectTimeout(CONNECTION_TIMEOUT)
	            .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT)
	            .setSocketTimeout(SOCKET_TIMEOUT)
	            .build();
		
		HttpPost httpPost = new HttpPost();
		httpPost.setURI( new URI(url.toString()) );
		httpPost.setEntity(new StringEntity(workHistArr.toString(), ContentType.APPLICATION_JSON));
		httpPost.setConfig(requestConfig);
		long startTime = System.currentTimeMillis();
		
		try ( CloseableHttpResponse response = restClient.makeRequest( httpPost ); ){            
			
			industryCode.inc();
            int statusCode = response.getStatusLine().getStatusCode();   
            
            // We got a response, check if it includes an content
            if( statusCode == HttpStatus.SC_OK ) {              	
            	logger.info("Industry code classifier api returned status code="+statusCode+" for Id="+person.id);
            	String content = IOUtils.toString( response.getEntity().getContent(), "UTF-8" );
            	if ( StringUtils.isNotBlank(content) ) {
            		//save entire response
            		person.meta.industry_code_resp = content;
            	}
            }          
            else {
            	
            	logger.warn("Industry code classifier api returned status code="+statusCode+" for Id="+person.id);
            	badStatusCount.inc(String.valueOf(statusCode));  
            }
            
           	parseResponse( person );
           	// ONLY update the talent if industry code (parsed wholistic array) is not null
           	if (null != person.industry_code && !person.industry_code.isEmpty()) {
           		person.meta.industrycode_hash = HashingUtils.getHash(workHistArr.toString());
           		person.meta.industrycode_last_modified_date = DateUtils.reformatToDateTime( DateUtils.now() );
           		person.meta.industrycode_needsUpdate = true;
           	} 
            
		} catch (Exception x) {
			
			indRespCounters.inc(x.getClass().getSimpleName());
			logger.error("Error while calling industry code classifier api: "+x.getMessage() + " for Id="+person.id); 
			if ( x.getMessage().equalsIgnoreCase(SOCKET_TIMEOUT_EXCEPTION_MSG) ) {
				deadLetterService.writePredictionTimeoutFailure( x.getMessage(), person );
			} else {
				throw x;
			}
        } finally  {        	
        	stratify(System.currentTimeMillis() - startTime, indRespCounters, indRespTime);
        	httpPost.reset();
        }
	}
	
	private void stratify(long duration, CounterSet counterSet, Distribution distribution) {

		distribution.update(duration);
		
		int max = 6000;
        if( duration >= max ) {
            counterSet.inc( "> 6s");
        }else {
            for( int i=1000; i < 6000; i+=1000 ) {
                if( duration < i ) {
                    counterSet.inc( "< "+ i/1000 +"s");
                    break;
                }
            }
        }
        
	}

	/**
	 * Parses the response into the wholistic industry code array and the work history codes array then
	 * sets the index fields industry_code and norm_company_names
	 * @param PersonDoc
	 */
	public void parseResponse(PersonDoc person) {
		JsonArray indCodesArr = null;
		JsonArray workHistCodesArr = null;
		
		if (null != person.meta.industry_code_resp && !person.meta.industry_code_resp.isEmpty()) {
			if (person.meta.industry_code_resp.contains(WHOLISTIC_INDUSTRY_CODES)
					|| person.meta.industry_code_resp.contains(WORK_HISTORY_INDUSTRY_CODES)) {
				JsonParser parser = new JsonParser();
				JsonElement responseJson = parser.parse(person.meta.industry_code_resp);
				JsonElement indCodesElement = responseJson.getAsJsonObject().get(WHOLISTIC_INDUSTRY_CODES);
				JsonElement workHistElement = responseJson.getAsJsonObject().get(WORK_HISTORY_INDUSTRY_CODES);

				if (!indCodesElement.isJsonNull()) {
					if (indCodesElement.isJsonArray()) {
						indCodesArr = (JsonArray) indCodesElement;
					}
				}

				if (!workHistElement.isJsonNull()) {
					if (workHistElement.isJsonArray()) {
						workHistCodesArr = (JsonArray) workHistElement;
					}
				}

				if (null != indCodesArr && !indCodesArr.isJsonNull() && indCodesArr.size() > 0) {
					person.industry_code = applyIndustryCodesBlock(indCodesArr);
				}

				if (null != workHistCodesArr && !workHistCodesArr.isJsonNull() && workHistCodesArr.size() > 0) {
					
					List<Employment> employ = new ArrayList<>();
					Set<String> names = new HashSet<>();
					Map<String, String> compMap = new HashMap<>();
					employ = applyWorkHistoryCodesBlock(workHistCodesArr);
					for (Employment e : employ) {
						if (StringUtils.isNotEmpty(e.CompanyName)) {
							names.add(e.CompanyName);
						}
						if ((StringUtils.isNotEmpty(e.IndustryWorkHistoryId))
								&& (StringUtils.isNotEmpty(e.CompanyName))) {
							compMap.put(e.IndustryWorkHistoryId, e.CompanyName);
						}
					}
					person.meta.normCompanyMap = compMap;
					person.norm_company_names = names;

				}
			} else {
				@SuppressWarnings("serial")
				List<IndustryCodeOutput> industryCodeOutput = new Gson().fromJson(person.meta.industry_code_resp,
						new TypeToken<List<IndustryCodeOutput>>() {
						}.getType());
				person.industry_code = industryCodeOutput;
				
			}
		}
	}
	
	/**
	 * Builds and applies a set of wholistic industry code "blocks" from the industry code array
     * and returns that list
	 * @param indCodesArr
	 * @return List<IndustryCodeOutput>
	 */
	private List<IndustryCodeOutput> applyIndustryCodesBlock(JsonArray indCodesArr ) {
		List<IndustryCodeOutput> industryCodeOutput = new ArrayList<IndustryCodeOutput>();
		for (int i = 0; i < indCodesArr.size(); i++) {
			JsonObject wholisticIndustryCodeObj = indCodesArr.get(i).getAsJsonObject();
			industryCodeOutput.add(new IndustryCodeOutput(wholisticIndustryCodeObj.get(CONFIDENCE_PARAM).getAsDouble(),
							wholisticIndustryCodeObj.get(SECTOR_DESCRIPTION_PARAM).getAsString()));
		}
		
		return industryCodeOutput;
	}

	/**
	 * Builds and applies a list of work history from the given Work History Industry Code Array
	 * (i.e. a subset of the Industry Code Classifier output) 
     * and returns that list. 
     * List of work history output is used for the Snowflake Tables.
	 * @param workHistCodesArray 
	 * @return List of Employment items from work history 
	 */
	private List<Employment> applyWorkHistoryCodesBlock(JsonArray workHistCodesArr) {
		List<Employment> workHistoryOutput = new ArrayList<Employment>();
		if (workHistCodesArr != null && workHistCodesArr.size() > 0) {
			for (int i = 0; i < workHistCodesArr.size(); i++) {
				if (ObjectUtils.isNotEmpty(workHistCodesArr.get(i).getAsJsonObject())) {
					JsonObject workHistoryCodeObj = workHistCodesArr.get(i).getAsJsonObject();
					Double confidence = workHistoryCodeObj.get(CONFIDENCE_PARAM).isJsonNull() ? 0.0 : workHistoryCodeObj.get(CONFIDENCE_PARAM).getAsDouble();
					String companyName = workHistoryCodeObj.get(COMPANY_NAME_PARAM).isJsonNull() ? "" : workHistoryCodeObj.get(COMPANY_NAME_PARAM).getAsString();
					String naicsDescParam = workHistoryCodeObj.get(NAICS_DESC_PARAM).isJsonNull() ? "" : workHistoryCodeObj.get(NAICS_DESC_PARAM).getAsString();
					String naicsCodeParam = workHistoryCodeObj.get(NAICS_CODE_PARAM).isJsonNull() ? "" : workHistoryCodeObj.get(NAICS_CODE_PARAM).getAsString();
					String sectorDescParam = workHistoryCodeObj.get(SECTOR_DESCRIPTION_PARAM).isJsonNull() ? "" : workHistoryCodeObj.get(SECTOR_DESCRIPTION_PARAM).getAsString();
					String workhistIdParam = workHistoryCodeObj.get(WORKHIST_ID_PARAM).isJsonNull() ? "" : workHistoryCodeObj.get(WORKHIST_ID_PARAM).getAsString();
					workHistoryOutput.add(new Employment(confidence, companyName, naicsDescParam, naicsCodeParam,
							sectorDescParam, workhistIdParam));
				}
			}
		}	
		return workHistoryOutput;		
	}
}
