package com.allegis.search.transform.person;

import java.io.IOException;
import java.io.Serializable;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.allegis.candidate.profile.ContactInfoBean;
import com.allegis.candidate.profile.ResumeSource;
import com.allegis.search.model.HeavyLifting;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.PersonShiftPreference;
import com.allegis.search.model.person.ResumeProfile;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.utils.GeneralUtils;


/**
 * Parses HRXML and captures the resuume body/teaser (text) names and profile.
 * 
 */
public class ResumeParseTransform implements Serializable {
    private static final long serialVersionUID = 1L;

    static final Logger logger = LoggerFactory.getLogger(ResumeParseTransform.class);

    private static Counter count = Metrics.counter(ResumeParseTransform.class, "Got Resume");
    private static Counter wrongVersionCount = Metrics.counter(ResumeParseTransform.class, "Wrong SOVREN");
	private Counter gotShiftCounter = Metrics.counter(TaskTransform.class, "Got Shift");
 
    
    private DeadLetterService deadLetterService;
    
    public ResumeParseTransform(DeadLetterService deadLetterService) {
		super();
		this.deadLetterService = deadLetterService;
	}


    
    /**
     * Builds upon the supplied person with the provided data
     * 
     * @param xml
     *            Sovren resume XML as string
     * @param person
     *            the person object to build on
     * @return the modified person object
     * @throws IOException 
     * @throws ParserConfigurationException 
     * @throws JAXBException 
     * @throws SAXException 
     */
    public PersonDoc transform(String xml, PersonDoc person) throws ParserConfigurationException, IOException, SAXException, JAXBException {

        ResumeSource resumeSource = ResumeSource.parse(xml);
        
        if (resumeSource != null) {
            // Since we will be adding data to this PersonDoc, we have to clone it.
            // Otherwise we will get an Immutablity exception
            person = SerializationUtils.clone(person);

            person.resume_1.body = GeneralUtils.scrubCRLFTab(resumeSource.getText());
            if (person.resume_1.body != null) {
            	person.teaser = StringUtils.substring(person.resume_1.body, 0, 300);
            	
            	// Using the resume text length (words only), instead of the HTML length,
            	// It is a better measure of quality.
            	person.resume_1.size = person.resume_1.body.length();
            }
            else {            	
            	// Having an empty string here makes it easier to query (and differentiate) 
            	// missing parsed resumes which will have a null value.
            	person.resume_1.body = "";  
            }
            
            ContactInfoBean contactInfo = resumeSource.getContactInfo();
            if (contactInfo != null) {
                person.resume_1.first_name = resumeSource.getContactInfo().getFirstName();
                person.resume_1.last_name = resumeSource.getContactInfo().getLastName();
                // Check for blacklisted names downstream where the tokenizer lives.
            }

            person.sov_months_experience = resumeSource.getProfile().getMonthsOfWorkExperience();
            
            // Forward to other stages like docvector service and blockhash
            person.meta.resumeProfile = new ResumeProfile(resumeSource);
            
            count.inc();
            
            String parserVersion = resumeSource.getParserVersion();
            if (parserVersion == null || ! parserVersion.startsWith("9.")) {
                wrongVersionCount.inc();
            	deadLetterService.writeDataQualityWarning("Wrong Sovren", "version:"+parserVersion+"; fileId:"+person.resume_1.id, person);            	
            }
            
            HeavyLifting.extractPositiveSentiment(person, resumeSource.getEmploymentHistory(), resumeSource.getSourceType());           
            HeavyLifting.extractPositiveSentiment(person, resumeSource.getSkills(), resumeSource.getSourceType().name(), "skills");           
            HeavyLifting.extractPositiveSentiment(person, resumeSource.getProfile().getQualificationSummary(), resumeSource.getSourceType().name(), "qualification");           
            HeavyLifting.extractPositiveSentiment(person, resumeSource.getProfile().getExecutiveSummary(), resumeSource.getSourceType().name(), "executive"); 
            
            boolean gotShift = PersonShiftPreference.getInstance().extract(person, resumeSource.getEmploymentHistory(), resumeSource.getSourceType());           
            if (gotShift)  {
            	gotShiftCounter.inc();
            }
        }
             
        return person;
    }
  
}
