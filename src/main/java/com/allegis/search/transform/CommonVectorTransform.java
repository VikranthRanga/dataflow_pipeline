package com.allegis.search.transform;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.allegis.classification.EntityType;
import com.allegis.classification.skillfamily.DomainClassification;
import com.allegis.classification.skillfamily.SkillFamilyClassificationManager.DomainClassWrapper;
import com.allegis.classification.skillfamily.SkillFamilyClassificationService;
import com.allegis.docvector.DocVectorService;
import com.allegis.docvector.NonExtractedData;
import com.allegis.docvector.common.VectorMatchType;
import com.allegis.docvector.model.MappedDocument;
import com.allegis.docvector.model.MappedField;
import com.allegis.docvector.model.VectorData;
import com.allegis.recommendation.SkillsetRecommender;
import com.allegis.search.enums.DomainAuditType;
import com.allegis.search.model.MatchDoc;
import com.allegis.search.utils.VectorPrecision;
import com.searchtechnologies.aspire.math.SimpleVector;
import com.searchtechnologies.aspire.tokenization.ExtractedData;

/**
 * A Utility class with a common set of vector transformations
 */
public class CommonVectorTransform {
    
	private static final String TERM_DELIM = "|";
	
	private static final SkillsetRecommender skillsetRecommender = new SkillsetRecommender();

    /**
     * Adds all the vector data form VectorData
     * 
     * @param vectorData  a reference to the vector data
     * @param entity    the person (or job) object to build on
     * @return          the modified person (or job) object
     */
    public static <T extends MatchDoc> T transform( VectorData vectorData, T entity, boolean isProdEnv) {
    	
        entity.docvector = vectorData.getDocVectorStr();
        entity.docvector_magnitude = vectorData.getDocVectorMagnitude();

        entity.skillsvector = vectorData.getSkillsVectorStr();
        entity.skillsvector_magnitude = vectorData.getSkillsVectorMagnitude();

        entity.keywordvector = vectorData.getKeywordVectorStr();
        entity.keywordvector_magnitude = vectorData.getKeywordVectorMagnitude();

        entity.acronymvector = vectorData.getAcronymVectorStr();
        entity.acronymvector_magnitude = vectorData.getAcronymVectorMagnitude();

        entity.titlevector = vectorData.getTitleVectorStr();
        entity.titlevector_magnitude = vectorData.getTitleVectorMagnitude();
        
        if (vectorData.getNonExtractedData() != null && vectorData.getNonExtractedData().size() > 0) {
        	entity.nonextracted_data = vectorData.getNonExtractedData();
        } else {
        	entity.nonextracted_data = null;
        }
        
        // Index audit data only for the lower tier (non-PROD) environments
        if ( ! isProdEnv) {
            entity.skillsvector_audit = vectorData.getSkillsVectorAuditList();
            entity.keywordvector_audit = vectorData.getKeywordVectorAuditList();
            entity.acronymvector_audit = vectorData.getAcronymVectorAuditList();
            entity.titlevector_audit = vectorData.getTitleVectorAuditList();
        }

        return entity;
    }
    
    /**
     * Given a list of terms, this method will return a list of them that are considered to be non-extracted Job Titles
     * @param terms a set of terms to evaluate
     * @param id    the id of the document from which the supplied terms came from
     * @param field the source field from which the supplied terms came from
     * @param type  the term entity type to check for being a non-extracted term
     * @param svcs  a reference to a doc vector service
     * @return      a list of non extracted term info
     */
    public static List<NonExtractedData> getNonExtractedTerms( List<String> terms, String id, String field, VectorMatchType type, DocVectorService svcs ){
        
        List<NonExtractedData> nxterms = new ArrayList<>();
        
        // If no terms were supplied, there is nothing to return
        if( null == terms || terms.isEmpty() ){
            return nxterms;
        }
        
        // Build the structure to look for extracted terms, we will remove them from the supplied
        // list of 'terms' to determine which ones are non-extracted
        
        MappedField mappedField = new MappedField(field, String.join(TERM_DELIM, terms));
        MappedDocument mapdoc = new MappedDocument( id );
        mapdoc.addField(mappedField);
        Map<String,ExtractedData> results = svcs.getExtractedTerms(mapdoc, id);
        
        List<String> nonExtractedTerms = new ArrayList<>();
        nonExtractedTerms.addAll( terms );
        
        // Remove the extracted terms from the list of terms supplied to this method
        for(Map.Entry<String,ExtractedData> kvp : results.entrySet() ) {
            if( kvp.getValue().hasType( type.getValue() ) ) {
                nonExtractedTerms.remove( kvp.getKey() );
            }
        }
        
        // Create the response structure
        for(String t : nonExtractedTerms) {
            nxterms.add( new NonExtractedData("", type.getValue(), field, t) ); 
        }
        
        return nxterms;
        
    }
    

    /**
     * Adds skill family vector from the docvector and (employment_position_title or titlevector)
     * 
     * @param vectorData    a reference to the vector data to extract skill families from
     * @param entity        a reference to the person to apply skills vectors too
     * @return              an updated copy of the person
     */
    public static <T extends MatchDoc> T transformSkillFamily( VectorData vectorData, T entity, String title, EntityType type,
    		boolean isProdEnv) {
    	//AuditList contains the domain used for related title expansion,
    	//domain before confidence check and final domain
    	List<String> auditList = new ArrayList<String>();
    	
        SimpleVector docvector = vectorData.getDocVector();               
        if (docvector == null || docvector.size == 0)  { 
            // quick exit if no docvector
            return entity;  
        }

        // NOTE: It is possible that title is null; not sure how the classifier handles it 
        // but it has always been that way  (DCH)
        auditList.add(vectorData.getSfDomAudit() + ": "+DomainAuditType.RT);
        String opCo = entity.group_filter;
        if (null != opCo && opCo.equalsIgnoreCase("MLA")) {
            // Handle MLA contingency 
        	entity.sf_domain = "LAW";
        	entity.sf_domain_confidence = 1.0;
        	entity.sf_vector = "Law:1.0";
            if ( ! isProdEnv) {
            	auditList.add("LAW: " + DomainAuditType.BCC);
            	auditList.add("LAW: " + DomainAuditType.FIN);
            	entity.sf_domain_audit = auditList;
            }

        } else {
        	//Create a skill family vector by getting Domain Class Wrapper
        	//Wrapper includes final domain classification plus sf domain before
        	//skill family confidence checks
            DomainClassWrapper domWrapper = SkillFamilyClassificationService.getInstance().getClassification(docvector, type, title);
            DomainClassification dom = domWrapper.getDomainClassification();
            //Get skill family set Before the Confidence Check(CC) - for auditing purposes only
        	String domBeforeCC = domWrapper.getDomainBCC();
            if (dom != null) {
                String sfVectorStr = dom.getFamiliesAsVectorString();
                if(sfVectorStr != null && ! sfVectorStr.isEmpty()) {
                	entity.sf_vector = sfVectorStr;
                    SimpleVector sfvec = new SimpleVector(sfVectorStr);
                    entity.sf_vector_magnitude = VectorPrecision.magnitudeFormat(sfvec.magnitude());
          
                    List<String> topTerm = getTopTerms(sfvec, 1);
                    if (null!=topTerm) {
                    	entity.most_relevant_skill_family = topTerm.get(0);
                    }
                }
                entity.sf_domain = dom.getDomain();
                entity.sf_domain_confidence = dom.getDomainConfidence();
            }
            
            // Index audit data only for the lower tier (non-PROD) environments
            if ( ! isProdEnv) {
            	//add domain used before confidence checks and after (final) to domain audit list
            	auditList.add(domBeforeCC + ": " + DomainAuditType.BCC);
            	auditList.add(entity.sf_domain + ": " + DomainAuditType.FIN);
            	entity.sf_domain_audit = auditList;
            }
        }	
        return entity;
    }

    /**
     * Helper used to safely grab the top terms.
     * 
     * @param vector    a reference to the vector to get terms from
     * @param desired   number of top terms to get
     * @return          the top number of terms from the given vector
     */
    public static List<String> getTopTerms(SimpleVector vector, int desired) 
    {
        if (vector == null || vector.size == 0)
            return null;
        desired = (vector.size < desired) ? vector.size : desired; 
        List<String> list = new ArrayList<>(desired);
        for (int i = 0; i < desired; i++)  {
            list.add(vector.getKeyString(i));
        }
        return list;
    }
    
    /**
     * Adds skillset vector from the entity's vectors and (job history or position description)
     * 
     * @param entity        a reference to the person/job to get skillsets for
     * @param rawText		an array of raw text to get skillsets for (job history or position description/top skills)
     * @return              an updated copy of the person/job
     */
    public static <T extends MatchDoc> T transformSkillset(T entity, String[] rawText) {
    	// Get skillsets
    	String skillsets = skillsetRecommender.vecLimitRecommend(entity.skillsvector, entity.titlevector, entity.acronymvector, entity.keywordvector, rawText);
    	// If skillsets is not null or empty
		if (skillsets != null && skillsets.length() != 0) {

			entity.most_relevant_skillset = skillsets.split(":")[0]; // first skillset is the most relevant one
			entity.skillsetvector = skillsets;

			if (!StringUtils.isEmpty(entity.skillsetvector.trim())) {
				SimpleVector vect = new SimpleVector(entity.skillsetvector);
				entity.skillsetvector_magnitude = VectorPrecision.magnitudeFormat(vect.magnitude());
			}
		}
    	return entity;
    }
}
