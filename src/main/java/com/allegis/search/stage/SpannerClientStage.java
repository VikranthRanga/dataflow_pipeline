package com.allegis.search.stage;

import com.google.cloud.spanner.DatabaseClient;

/**
 * Defines stages that make use of Spanner
 */
public interface SpannerClientStage {

    /**
     * @return an instance of the Spanner client
     */
	DatabaseClient getSpannerClient();

}
