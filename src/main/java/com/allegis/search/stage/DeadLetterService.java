package com.allegis.search.stage;

import java.io.Serializable;

import org.checkerframework.checker.initialization.qual.Initialized;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.UnknownKeyFor;

import com.allegis.search.io.ElasticsearchIO.FailedDocument;
import com.allegis.search.model.CommonDoc;
import com.allegis.search.options.PredictionsServiceOptions;


/**
 *  A Dead Letter Service is intended to be used for the persistence of warning and exceptions 
 *  related to failures and un-desired situations during the processing of documents through
 *  our ingestion pipelines
 */
public class DeadLetterService implements Serializable {

	private static final long serialVersionUID = 1381449718291810756L;

	/**
	 * Capture exception details and persist to Spanner
	 * 
	 * @param docId    the id of the document being processed
	 * @param e        a reference to the exception
	 */
	public void writeException(CommonDoc doc, Exception e) {}

	/**
	 * Capture general data quality issues and persist to Spanner.
	 * 
	 * @param generalMessage   a simple, "groupable" message to associate with the warning (i.e. "file not found")
	 * @param docId            the id of the document being processed
	 */
	public void writeDataQualityWarning(String generalMessage, CommonDoc doc) {}

	/**
	 * Capture general data quality issues, originating from an exception, and provide 
	 * other relevant details and persist to Spanner.
	 * 
	 * @param e            a reference to the exception
	 * @param extraParams  string representation of additional parameters relevant to the warning
	 * @param docId        the id of the document being processed
	 */
	public void writeDataQualityWarning(Exception e, String extraParams, CommonDoc doc) {}

	/**
	 * Capture general data quality issues, and provide other relevant details and persist to Spanner.
	 * 
	 * @param generalMessage   a simple, "groupable" message to associate with the warning (i.e. "file not found")
	 * @param extraParams      extra parameters. sample: "path=gs:/root/dir/file; docId=123456788"
	 * @param docId            the id of the document being processed
	 */
	public void writeDataQualityWarning(String generalMessage, String extraParams, CommonDoc doc) {}
	
	/**
	 * Capture general data quality issues that indicate the record was skipped, and provide other relevant details and persist to Spanner.
	 * This record will be skipped.
	 * 
	 * @param generalMessage   a simple, "groupable" message to associate with the warning (i.e. "file not found")
	 * @param docId            the id of the document being processed
	 */
	public void writeDataQualitySkip(String generalMessage, CommonDoc doc) {}

	/**
	 * Capture details of a document that failed to get predictions due to model failure
	 * 
	 * @param generalMessage a simple message captured from dsia unsuccessful response
	 * @param doc	a reference to the failed document
	 */
	public void writePredictionModelFailure( String generalMessage, CommonDoc doc ) {}
	
	/**
	 * Capture details of a document that failed to get predictions due to read timeout
	 * 
	 * @param exceptionMessage  a timeout exception message
	 * @param doc	a reference to the failed document
	 */
	public void writePredictionTimeoutFailure( String exceptionMessage, CommonDoc doc ) {}

	public void captureContext(PredictionsServiceOptions options, @UnknownKeyFor @NonNull @Initialized String jobName) {
	}
}