package com.allegis.search.stage;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;

import com.allegis.search.enums.MetricsNamespace;
import com.allegis.search.exception.FatalDataQualityException;
import com.allegis.search.exception.FatalFrameworkException;
import com.allegis.search.model.CommonDoc;
import com.allegis.search.model.MatchDoc;
import com.allegis.search.options.CommonOptions;

/**
 * 
 * The AbstractStage is an extension of the {@link SuperAbstractStage} intended for Stages that
 * take in {@link MatchDoc}s and output {@link MatchDoc}s
 *
 *  @param <I>   a MatchDoc
 *  @param <O>   a MatchDoc
 */
public abstract class AbstractStage<I extends CommonDoc, O extends CommonDoc> extends SuperAbstractStage<I,O> {
    
    private static final long serialVersionUID = -5125832319642963294L;

    private static Counter failure = Metrics.counter( MetricsNamespace.STAGE.name(), "Failures");

    /**
     * We need to know up front if deadLetterService will be Spanner or Mock
     * @param isDebug   a flag to indicate if the pipeline is running in debug mode
     * @param env       the target environment context for this execution
     */
    public AbstractStage(CommonOptions options) {
    	super(options);
	}


	/**
     * Provides a safe wrapper around {@link processStage} to prevent exceptions from escaping the 
     * @see #processStage(I,OutputReceiver,ProcessContext) method implemented by this Stage. Exceptions
     * will be trapped and placed into the {@see GlocalCollections}
     * 
     * NOTE: Moving this common method to the {@link SuperAbstractStage} results in a "Too many methods with @ProcessElement annotation" exception
     * 
     * @param doc       the {@link Element} being processed
     * @param context   the {@link ProcessContext} of this stage
     */
    @SuppressWarnings( "unchecked" )
    @ProcessElement
    public void processElement( @Element I doc, ProcessContext context ) throws FatalFrameworkException {

        try{

            // Invoke the stage
            processStage(doc, context);
            
        }catch( Exception e ){
            // update metrics
            failure.inc();
            
            deadLetterService.writeException(doc, e);
            
            // If the exception should be reported to the framework, rethrow
            if( e instanceof FatalFrameworkException ) {
                throw (FatalFrameworkException) e;
                
            // If the exception is not fatal to the ingestion of this document, throw the original
            // back onto the pipeline
            }else if( !( e instanceof FatalDataQualityException ) ) {
                context.output( (O) doc );
            }            
        }
    }

   
}
