package com.allegis.search.stage.person;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;

import com.allegis.search.enums.FeederType;
import com.allegis.search.model.person.PersonDoc;
import com.google.api.services.bigquery.model.TableRow;

/**
 * A simple Big Query table row to Person doc wrapper class that instantiate person doc and stores the talent doc id.
 * 
 */
public class BigQueryRowToPersonDoc extends DoFn<TableRow, PersonDoc> {

    private static final long serialVersionUID = -7099287395429344746L;
    
    final private String jobRunningDateTimeStamp;
    final private String optionalPipelineMessage;

    private Counter nulls = Metrics.counter( "BQIO Feeder", "null ids");
    private Counter good = Metrics.counter( "BQIO Feeder", "process ids");
    
    public BigQueryRowToPersonDoc(String jobRunningDateTimeStamp, String optionalPipelineMessage) {
    	super();
    	
    	this.jobRunningDateTimeStamp = jobRunningDateTimeStamp;
    	this.optionalPipelineMessage = optionalPipelineMessage;
    }
    
    @ProcessElement
    public void processElement(@Element TableRow row, ProcessContext context) {
        
        String id = (String) row.get("Id");
        
        if (id == null)  {
            nulls.inc();
        }
        else  {
            PersonDoc doc = new PersonDoc();
            doc.id = id;
            doc.isRealTime = false;
            doc.isDelete = false;
            doc.isQaData = false;
            doc.feederId = FeederType.Bulk.name()+ " " +jobRunningDateTimeStamp+ " "+optionalPipelineMessage;
           
            good.inc();
            context.output(doc);
        }
    }
}
