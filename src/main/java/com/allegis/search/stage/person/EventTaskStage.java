package com.allegis.search.stage.person;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.commons.lang3.SerializationUtils;

import com.allegis.search.enums.Table.Event;
import com.allegis.search.enums.Table.Task;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.EventTransform;
import com.allegis.search.transform.person.TaskTransform;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.KeySet;
import com.google.cloud.spanner.ResultSet;

/**
 * Sourced from event and task tables.
 * Populates PersonDoc fields:
 * 			submit_hist
 * 			interactions
 * 			recruiter_last_activity_date
 * 			submittal_status
 * 			recent_submittal_date
 */
public class EventTaskStage extends AbstractStage<PersonDoc,PersonDoc>{
    private static final long serialVersionUID = 1L;

    /**
     * Default Constructor. This exist simply so that we can mock out resources in this stage
     */
    public EventTaskStage(CommonOptions options) {
    	super(options);
    }

    EventTransform eventTransform;
    TaskTransform taskTransform;

    @Override
    public void setUp() {
        eventTransform = new EventTransform(deadLetterService);
        taskTransform = new TaskTransform(deadLetterService);
    }


    @Override
    public void processStage( PersonDoc person, DoFn<PersonDoc, PersonDoc>.ProcessContext context ) throws Exception {
        //  Key is (AccountId, IsDeleted, IsArchived)	
        SpannerKey key = SpannerKey.of(  person.id, "false", "false"  );
        
        boolean isCloned = false;
        // We use a try-with-resource block to automatically release resources held by ResultSet.
        try (ResultSet resultSet = spannerClient
                .singleUse()
                .readUsingIndex( Event.TABLE, Event.INDEX, KeySet.singleKey( key.get() ), EventTransform.COLUMNS)) 
        {
            person = SerializationUtils.clone( person );
            isCloned = true;
      		eventTransform.transform(resultSet, person, spannerClient);
        }

        try (ResultSet resultSet = spannerClient
                .singleUse()
                .readUsingIndex( Task.TABLE, Task.INDEX, KeySet.singleKey( key.get() ), TaskTransform.COLUMNS)) 
        {
        	if (!isCloned) {
                person = SerializationUtils.clone(person);
            }
            taskTransform.transform(resultSet, person);
        }

        context.output(person);
    }       

}
