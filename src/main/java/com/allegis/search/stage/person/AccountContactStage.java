package com.allegis.search.stage.person;


import java.util.Arrays;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;

import com.allegis.search.enums.Table.Account;
import com.allegis.search.enums.Table.Contact;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.stage.DeadLetterService;
import com.allegis.search.transform.person.AccountTransform;
import com.allegis.search.transform.person.AccountTransform.ACCOUNT;
import com.allegis.search.transform.person.ContactTransform;
import com.allegis.search.transform.person.ContactTransform.CONTACT;
import com.allegis.search.utils.GeneralUtils;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.KeySet;
import com.google.cloud.spanner.ReadContext;
import com.google.cloud.spanner.ResultSet;
import com.google.cloud.spanner.Struct;

/**
 * Gets the Account and Contact data rows and populates PersonDoc
 * Ths should be exactly one account for each contact.
 */
public class AccountContactStage extends AbstractStage<PersonDoc, PersonDoc>{

        private static final long serialVersionUID = 1L;
        private static final List<String> EXCLUDED_GROUP_FILTERS = Arrays.asList("MLA","AP");

        /**
         * Default Constructor. This exist simply so that we can mock out resources in this stage
         */
        public AccountContactStage(CommonOptions options) {
        	super(options);
        }

        private Counter contactNotFoundCounter = Metrics.counter(AccountContactStage.class.getSimpleName(), "Contact Not Found");
        private Counter contactDupFoundCounter = Metrics.counter(AccountContactStage.class.getSimpleName(), "Contact Duplicate Found");
        private Counter accountNotFoundCounter = Metrics.counter(AccountContactStage.class.getSimpleName(), "Account Not Found");
        private Counter notTalentAccountCounter= Metrics.counter(AccountContactStage.class.getSimpleName(), "Account Not Talent");
        private Counter notCommittedAccountCounter= Metrics.counter(AccountContactStage.class.getSimpleName(), "Account Not Committed");

        ContactTransform contactTransform;
        AccountTransform accountTransform;
        
        private DeadLetterService deadLetterService;

        @Override
        public void setUp() {
            contactTransform = new ContactTransform(deadLetterService);
            accountTransform = new AccountTransform(deadLetterService);
        }

        @Override
        public void processStage( PersonDoc doc, DoFn<PersonDoc, PersonDoc>.ProcessContext context ) throws Exception {
            
            
            // Since we will be adding data to this PersonDoc, we have to clone it.
            // Otherwise we will get an Immutablity exception
            PersonDoc candidate = SerializationUtils.clone( doc );

            // Build the key to and get Account info for this talent
            try (ReadContext readContext = spannerClient.singleUse())  // force quick closure
            {
                SpannerKey primaryKey = SpannerKey.of( candidate.id );
                Struct accountRow = readContext.readRow( Account.TABLE, primaryKey.get(), ACCOUNT.COLUMNS );

                if (accountRow == null) {
                    deadLetterService.writeDataQualitySkip("Missing account table row", doc);
                    accountNotFoundCounter.inc();
                    return;
                }
                
                String recordTypeId = GeneralUtils.getNullSafeString(accountRow, Account.Column.RecordTypeId.name());
                if ( ! Account.RECORD_TYPE_ID_FOR_TALENT.equals(recordTypeId))  {
                    notTalentAccountCounter.inc();   // can only come from real-time
                    return;                	
                }

                boolean committed = GeneralUtils.getNullSafeBoolean(accountRow, Account.Column.Talent_Committed_Flag__c.name());
                if ( ! committed)  {
                    notCommittedAccountCounter.inc();   // can only come from real-time
                    return;                	
                }
                
                String groupFilter = GeneralUtils.getNullSafeString(accountRow, Account.Column.Talent_Ownership__c.name());
            	// Block if group_filter  MLA,AP is ingested directly through topic
                if (StringUtils.isNotEmpty(groupFilter) && EXCLUDED_GROUP_FILTERS.contains(groupFilter) ) {
                    return;
                }

                accountTransform.transform(accountRow, candidate);

                // Build the foreign key to and get Contact info for this talent
                SpannerKey key = SpannerKey.of( candidate.id, "false" );
                
                try (ReadContext contactContext = spannerClient.singleUse();
                        ResultSet contactResultSet = contactContext.readUsingIndex(
                                Contact.TABLE, Contact.INDEX, KeySet.singleKey( key.get() ), CONTACT.COLUMNS))
                {
                    if (contactResultSet.next())  {
                        contactTransform.transform(contactResultSet, candidate);

                        // Add the updated Candidate back onto the pipeline
                        context.output( candidate );

                        if (contactResultSet.next())  {
                            deadLetterService.writeDataQualityWarning("Duplicate contact table rows",  doc);
                            contactDupFoundCounter.inc();
                        }
                    }
                    else {
                        deadLetterService.writeDataQualitySkip("Missing contact table row",  doc);
                        contactNotFoundCounter.inc();
                        return;
                    }
                }
            }
        }

}
