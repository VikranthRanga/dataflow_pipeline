package com.allegis.search.stage.person;


import java.util.Date;
import java.util.List;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.commons.lang3.SerializationUtils;

import com.allegis.search.enums.Table.Order;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.EagernessTransform;
import com.allegis.search.transform.person.OrderTransform;
import com.allegis.search.utils.GeneralUtils;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.KeySet;
import com.google.cloud.spanner.ResultSet;
/**
 * This stage sets eagerness fields <br>
 * 
 * Most of the fields used to generate eagerness values come from previous stages.  <br>
 * It is important for this stage to be late in the pipeline. <br>
 * <br>
 * This stage requires an index on <b>order</b> table. index template
 *
 * HINT: source of truth for index name lives in EagernessTransform.ORDER.INDEX
 *  
 */
public class OrderStage extends AbstractStage<PersonDoc,PersonDoc>{

    private static final long serialVersionUID = 1L;

    /**
     * Default Constructor. This exist simply so that we can mock out resources in this stage
     */
    public OrderStage(CommonOptions options) {
    	super(options);
    }
    
    public static List<String> COLUMNS = GeneralUtils.toNameList(
        	Order.Column.Id,
        	Order.Column.Status,
        	Order.Column.Has_Application__c,
        	Order.Column.EffectiveDate,
        	Order.Column.ATS_Job__c,
        	Order.Column.OpportunityId,
        	Order.Column.Submittal_Not_Proceeding_Reason__c
    );
    
    
    private EagernessTransform eagernessTransform;
    private OrderTransform orderTransform;

    @Override
    public void setUp() {
        eagernessTransform = new EagernessTransform();
        orderTransform = new OrderTransform(deadLetterService);
    }


    @Override
    public void processStage( PersonDoc doc, DoFn<PersonDoc, PersonDoc>.ProcessContext context ) throws Exception {
        // Since we will be adding data to this PersonDoc, we have to clone it. 
        // Otherwise we will get an Immutablity exception
        PersonDoc person = SerializationUtils.clone( doc );

        //  Key is (countactId, isDeleted) 
        SpannerKey key = SpannerKey.of(  person.contact_id, "false" );
        
        // We use a try-with-resource block to automatically release resources held by ResultSet.
        try (ResultSet resultSet = spannerClient.singleUse()
                    .readUsingIndex(Order.TABLE,  Order.INDEX, KeySet.singleKey( key.get() ), COLUMNS))
        {
            List<Date> applicationEffectiveDates = orderTransform.transform(person, resultSet, spannerClient);
            eagernessTransform.transform(person, applicationEffectiveDates);
        }

        context.output(person);
    }

}
