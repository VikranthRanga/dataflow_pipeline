package com.allegis.search.stage.person;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.commons.lang3.SerializationUtils;

import com.allegis.search.enums.Table.Experience;
import com.allegis.search.enums.Table.WorkHistory;
import com.allegis.search.model.HeavyLifting;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.model.person.PersonShiftPreference;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.ExperienceTransform;
import com.allegis.search.transform.person.WorkHistoryTransform;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.KeySet;
import com.google.cloud.spanner.ResultSet;

/**
 * Handle employment, certification, and education histories.
 */
public class ExperienceStage extends AbstractStage<PersonDoc, PersonDoc> {
    private static final long serialVersionUID = 1L;

    /**
     * Default Constructor. This exist simply so that we can mock out resources in
     * this stage
     */
    public ExperienceStage(CommonOptions options) {
    	super(options);
    }

    ExperienceTransform experienceTransform;
    WorkHistoryTransform workHistoryTransform;

    @Override
    public void setUp() {
        experienceTransform = new ExperienceTransform(deadLetterService);
        workHistoryTransform = new WorkHistoryTransform(deadLetterService);
    }

    private Counter gotShiftCounter = Metrics.counter(ExperienceStage.class.getSimpleName(), "Got Shift");

    @Override
    public void processStage(PersonDoc person, ProcessContext context) throws Exception {
        // Key is (account, isDeleted)
        SpannerKey key = SpannerKey.of(  person.id, "false" );
        KeySet keyset = KeySet.singleKey( key.get() );
        
        // We use a try-with-resource block to automatically release resources held by
        // ResultSet.
        boolean isCloned = false;
        try (ResultSet resultSet = spannerClient.singleUse().readUsingIndex(Experience.TABLE,
                Experience.INDEX, keyset, ExperienceTransform.COLUMNS)) {
            person = SerializationUtils.clone(person);
            isCloned = true;
            experienceTransform.transform(resultSet, person);
        }

        // We use a try-with-resource block to automatically release resources held by
        // ResultSet.
        try (ResultSet resultSet = spannerClient.singleUse().readUsingIndex(WorkHistory.TABLE,
                WorkHistory.INDEX, keyset, WorkHistoryTransform.COLUMNS)) {
            if (!isCloned) {
                person = SerializationUtils.clone(person);
            }
            workHistoryTransform.transform(resultSet, person);
        }

        HeavyLifting.extractPositiveSentiment(person, person.getEmploymentHistory(), person.getSourceType());           
        boolean gotShift = PersonShiftPreference.getInstance().extract(person, person.getEmploymentHistory(), person.getSourceType());           
        if (gotShift)  {
        	gotShiftCounter.inc();
        }
        
        context.output(person);
    }


}
