package com.allegis.search.stage.person;

import org.apache.commons.lang3.SerializationUtils;

import com.allegis.search.enums.Table.TalentRecommendation;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.TalentRecommendationTransform;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.KeySet;
import com.google.cloud.spanner.ResultSet;

/**
 * Sourced from talent recommendation table.
 * used to extract datapoint for Proven Track Record
 * 
 * @author jperecha
 */
public class TalentRecommendationStage extends AbstractStage<PersonDoc, PersonDoc> {

	private static final long serialVersionUID = 1L;
	
	public TalentRecommendationStage(CommonOptions options) {
		super(options);
	}
	
	TalentRecommendationTransform talentRecommendationTransform;
		
	@Override
    public void setUp() {
		talentRecommendationTransform = new TalentRecommendationTransform();
	}
	
	@Override
    public void processStage(PersonDoc person, ProcessContext context) throws Exception {
		String contactId = person.contact_id;
        if(contactId != null) {
            
            SpannerKey key = SpannerKey.of( contactId, "false" );

        	try (ResultSet resultSet = spannerClient
                    .singleUse()
                    .readUsingIndex( TalentRecommendation.TABLE, TalentRecommendation.INDEX, KeySet.singleKey( key.get() ), TalentRecommendationTransform.COLUMNS)) 
            {                                      
        		if (null == resultSet) {
        			deadLetterService.writeDataQualityWarning("Missing "+ TalentRecommendation.TABLE +" table row", "contact id="+contactId, person);
        		}
        		else {
        			person = SerializationUtils.clone( person );
        			talentRecommendationTransform.transform(resultSet, person);
        		}
            }	        	
        }
        context.output(person);
	}
}
