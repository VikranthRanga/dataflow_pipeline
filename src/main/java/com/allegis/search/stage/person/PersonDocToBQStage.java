package com.allegis.search.stage.person;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.candidate.profile.ContactInfoBean;
import com.allegis.candidate.profile.EmploymentBean;
import com.allegis.search.model.OrderEmploymentBean;
import com.allegis.search.model.person.Employment;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.pipeline.TalentPredictionsAppPipeline;
import com.google.api.services.bigquery.model.TableRow;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class PersonDocToBQStage extends DoFn<PersonDoc, TableRow> {

	private static final long serialVersionUID = 5765359627971014290L;

	private static final Logger logger = LoggerFactory.getLogger( PersonDocToBQStage.class );

    private Counter resume = Metrics.counter(TalentPredictionsAppPipeline.class.getSimpleName(), "resume");
    private Counter experience = Metrics.counter(TalentPredictionsAppPipeline.class.getSimpleName(), "experience");
    private Counter allegis = Metrics.counter(TalentPredictionsAppPipeline.class.getSimpleName(), "allegis");
    private Counter exception = Metrics.counter(TalentPredictionsAppPipeline.class.getSimpleName(), "exception");
    private Counter no_industry = Metrics.counter(TalentPredictionsAppPipeline.class.getSimpleName(), "no_industry");
    private Counter no_success = Metrics.counter(TalentPredictionsAppPipeline.class.getSimpleName(), "no_success");
    private Counter no_id = Metrics.counter(TalentPredictionsAppPipeline.class.getSimpleName(), "no_id");

    private enum Source { Resume, Allegis, Recruiter };
    
	@ProcessElement
    public void processElement(@Element PersonDoc person, ProcessContext context) throws Exception {
		// 3 different work history lists are possible:
		//   1) resume
		//   2) allegis: talent_work_history__c 
		//   3) recruiter entered: talent_experience__c type='Work'
									
		try {
			if (null == person.meta.industry_code_resp) {
				logger.info("no industry " + person.id + " " + person.employ_hist);
				no_industry.inc();
			} else if (person.meta.industry_code_resp.contains("\"message\":\"success\"")) {		
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				JsonElement responseJson = JsonParser.parseString(person.meta.industry_code_resp);
				JsonArray workHistElement = (JsonArray) responseJson.getAsJsonObject().get("work_history_industry_codes");
				for (int i=0; i<workHistElement.size(); i++) {
					TableRow tr = new TableRow();
					tr.set("accountid", person.id);
					//getting the city, state and country from Person object
					tr.set("city_person", person.city_name);
					tr.set("state_person", person.country_sub_division_code);
					tr.set("country_person", person.country_code);
					tr.set("ordinal", i);
					JsonObject el = (JsonObject) workHistElement.get(i);
					if (null!=el) {
						tr.set("normalized_companyname", el.getAsJsonPrimitive("company_name").getAsString());
						tr.set("duns_number", el.getAsJsonPrimitive("duns_number").getAsString());																						
					}
					String id = el.get("work_history_id").getAsString();
					// some id's will be actual salesforce id's: ignore those,
					// we are focused only on the "fake" ids which come from resumes
					int index = -1;
					try {											
						index = Integer.parseInt(id);
						EmploymentBean resumeWh = person.meta.resumeProfile.getEmploymentHistory().get(index);
						resume.inc();
						tr.set("source", Source.Resume.toString());
						tr.set("documentId", person.resume_1.id);
						tr.set("companyname", resumeWh.getEmploymentOrgName());
						tr.set("city", resumeWh.getCity());
						tr.set("state", resumeWh.getState());
						tr.set("country", resumeWh.getCountry());
						ContactInfoBean resumeContactInfo = person.meta.resumeProfile.getContactInfo();
						if (resumeContactInfo != null) {
							tr.set("city_resumetop", resumeContactInfo.getCity());
							tr.set("state_resumetop", resumeContactInfo.getState());
							tr.set("country_resumetop", resumeContactInfo.getCountry());
						}
						if (null!=resumeWh.getHistoryRange()) {
							if (null!=resumeWh.getHistoryRange().getStartDate())
								tr.set("startdate", sdf.format(resumeWh.getHistoryRange().getStartDate()));
							if (null!=resumeWh.getHistoryRange().getEndDate())
								tr.set("enddate", sdf.format(resumeWh.getHistoryRange().getEndDate()));
						}
						tr.set("jobtitle", resumeWh.getPositionTitle());
						tr.set("jobdescription", resumeWh.getPositionDescription());
					} catch (NumberFormatException e) {
						Employment emp = findEmploymentHistory(person.employ_hist, id);
						if (null==emp) {
							no_id.inc();
							logger.info("no id " + person.id + " id=" + id + " " + person.employ_hist);
							continue;
						} else {
							tr.set("companyname", emp.OrganizationName);
							if (null!=emp.StartDate)
								tr.set("startdate", sdf.format(emp.StartDate));
							if (null!=emp.EndDate)
								tr.set("enddate", sdf.format(emp.EndDate));
							tr.set("jobtitle", emp.PositionTitle);													
							// orderID is present only for Allegis work history
							if (null==emp.OrderId) {
								experience.inc();
								tr.set("source", Source.Recruiter.toString());
								tr.set("teid", id);
								tr.set("jobdescription", emp.Description);																									
								// location can NEVER be present on Experience Work entries
							} else {
								allegis.inc();
								tr.set("source", Source.Allegis.toString());
								tr.set("twhid", id);	
								OrderEmploymentBean oeb = findAssociatedJob(person.associated_jobs, emp.OrderId);
								if (null!=oeb) {
									tr.set("city", oeb.getCity());
									tr.set("state", oeb.getState());
									tr.set("country", oeb.getCountry());
									tr.set("jobdescription", oeb.getPositionDescription());
									if (null!=oeb.accountName)
										tr.set("accountName", oeb.accountName);										
									if (null!=oeb.accountDetails)
										tr.set("accountDetails", oeb.accountDetails);
								}
							}
						}
					}
					context.output(tr);
				}
			} else {
				logger.info("no success " + person.id + " " + person.employ_hist);
				no_success.inc();
				context.output(TalentPredictionsAppPipeline.replayIds, makeTableRow(person.id));
			}
		} catch (Exception e) {
			logger.info("exception " + person.id + " " + e.getMessage());
			e.printStackTrace();
			exception.inc();
			context.output(TalentPredictionsAppPipeline.replayIds, makeTableRow(person.id));
			throw e;
		}
    }

    private TableRow makeTableRow(String id) {
    	TableRow tr = new TableRow();
    	tr.set("accountid", id);
    	return tr;
	}

	private OrderEmploymentBean findAssociatedJob(List<EmploymentBean> associated_jobs, String orderId) {
    	if (null==orderId || null==associated_jobs)
    		return null;
    	for (EmploymentBean emp : associated_jobs) {
    		OrderEmploymentBean oeb = (OrderEmploymentBean) emp;
    		if (null!= oeb.orderId && oeb.orderId.equals(orderId))
    			return oeb;
    	}    	
		return null;
	}

	// iterate through the list and find matching id
    private Employment findEmploymentHistory(List<Employment> employ_hist, String id) {
    	if (null==id || null==employ_hist)
    		return null;
    	for (Employment emp : employ_hist) {
    		if (emp.Id.equals(id))
    			return emp;
    	}
    	return null;
	}

/*
CREATE TABLE `connected-ingest-load.temp.talent_all_work_history` (
accountid STRING(18) NOT NULL,
source STRING(10) NOT NULL,
documentid STRING(18),
teid STRING(18),
twhid STRING(18),
ordinal INT64 NOT NULL,
companyname STRING,
city STRING,
state STRING,
country STRING,
city_person STRING,
state_person STRING,
country_person STRING,
city_resumetop STRING,
state_resumetop STRING,
country_resumetop STRING,
startdate DATE,
enddate DATE,
jobtitle STRING,
jobdescription STRING,
normalized_companyname STRING,
duns_number STRING(9),
accountName STRING,
accountDetails STRING
)	    



CREATE TABLE `connected-ingest-load.temp.replay_ids` (
accountid STRING(18) NOT NULL
)
*/

}
