package com.allegis.search.stage.person;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.candidate.profile.GenericSource;
import com.allegis.candidate.profile.ProfileSource;
import com.allegis.candidate.profile.ProfileSource.SourceType;
import com.allegis.docvector.candidate.ATSCandidateService;
import com.allegis.docvector.candidate.ATSCandidateServiceImpl;
import com.allegis.docvector.candidate.dict.CandidateNameBlacklistTerms;
import com.allegis.docvector.common.GenericTitleTerms;
import com.allegis.docvector.common.VectorMatchType;
import com.allegis.docvector.model.VectorData;
import com.allegis.docvector.termstats.GenericTermStats;
import com.allegis.search.model.person.AltVectorState;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.PredictionsServiceOptions;
import com.allegis.search.service.impl.RelatedTermsServiceImpl;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.OrderTransform;
import com.allegis.search.transform.person.PersonVectorTransform;
import com.allegis.search.utils.GCPConfigUtils;
import com.allegis.search.utils.IngestionConfig;
import com.allegis.search.utils.VectorPrecision;
import com.searchtechnologies.aspire.math.SimpleVector;

/**
 * Fetches the Sovren parsed resume from Google Storage and parses the XML into ResumeSource.
 * Several simple field are populated in the PersonDoc.
 * Then the CandidateProfile data is copied to a light a weight CandidateProfile instance
 * in order to forward as meta date to down stream stages.
 */
public class VectorizeStage extends AbstractStage<PersonDoc, PersonDoc> {

    //private static final int DOCVECTOR_RECOMPUTE_THRESHOLD = 12;
	private static final Logger logger = LoggerFactory.getLogger(VectorizeStage.class);
    private static final long serialVersionUID = 1L;
    private boolean isProd;

    public VectorizeStage(PredictionsServiceOptions options) {
    	super(options);
    	
    	// Is the target in PROD?
        isProd = "prod".equalsIgnoreCase(GCPConfigUtils.getAllegisEnv(options));
    }

    private static CandidateNameBlacklistTerms blacklistTerms;
    
    // Default scope so other stages can reference this obj
    static ATSCandidateService candidateService;

    static {
        
        // START Candidate Service initialization
        // NOTE: DO NOT USE AN environment specific property in this static block that is different across environments.
        // {@link IngestionConfig} and {@link AllegisConfig} will not retain their internal ENV value when serialized
        
        GenericTermStats termStats = new GenericTermStats();
        Map<String, Integer> termStatsMap = termStats.loadPersonTermStats();
        logger.info("Term stats count: " + termStatsMap.size());

        GenericTitleTerms titleTerms = new GenericTitleTerms();
        Map<String, Double> genericTitlesMap = titleTerms.loadGenericTitles();
        logger.info("Generic titles count: " + genericTitlesMap.size());

        candidateService = ATSCandidateServiceImpl.getInstance(termStats, genericTitlesMap, IngestionConfig.totalTalentDocs);
        candidateService.setNormalizeTFIDFWeight(true);
        candidateService.setUseFreshnessCap(true);

        // Set relative weighting for skills not on explicit fields.
        candidateService.addNegativeRelativeWeightingFields(VectorMatchType.TITLE_MATCH_TYPE, "G2PositionTitle");
        candidateService.addNegativeRelativeWeightingFields(VectorMatchType.TITLE_MATCH_TYPE, "G2EmploymentPositionTitle");
        candidateService.addNegativeRelativeWeightingFields(VectorMatchType.TITLE_MATCH_TYPE, "sovrenPositionTitle");
        candidateService.addNegativeRelativeWeightingFields(VectorMatchType.TITLE_MATCH_TYPE, "sovrenEmploymentPositionTitle");
        candidateService.setNegativeRelativeWeightMultiplier(0.2);

        // Blend Intersect coefficient to blend across the sources.
        candidateService.setBlendIntersectCoefficient(0.2);

        // environment does not matter here, since we are using "local" RelatedJobTitles only
        RelatedTermsServiceImpl rtService = RelatedTermsServiceImpl.getInstance("PROD");

        candidateService.setRelatedTermsService(rtService);
        ///////////////////////////////////////////////
        // FINISHED Candidate Service initialization
        ///////////////////////////////////////////////

        /*
         * Initialize blacklisted terms for detecting unwanted term in candidate names
         */
        blacklistTerms = new CandidateNameBlacklistTerms();
        
        blacklistTerms.loadBlacklistDictionary();
    }

    
    
	private Counter useAssociatedJobs = Metrics.counter(OrderTransform.class.getSimpleName(), "Blend Associated Jobs");    

    @Override
    public void processStage(PersonDoc person, DoFn<PersonDoc, PersonDoc>.ProcessContext context) throws Exception {
 
        // person is mutated during createVectors
        person = SerializationUtils.clone(person);

        List<ProfileSource> profileSources = new ArrayList<>();
        profileSources.add(person);  // Always add G2
        
        if (person.meta.resumeProfile != null) {
            // Add Sovren
        	profileSources.add(person.meta.resumeProfile);
        } 

        VectorData vectorData = candidateService.createVectors(profileSources.toArray(new ProfileSource[profileSources.size()]), person.id);

        if (vectorData != null) {
            PersonVectorTransform.transform(vectorData, person, isProd);
            PersonVectorTransform.transformSkillFamily(vectorData, person, isProd);
            PersonVectorTransform.transformTopVectorTerms(vectorData, person);
            PersonVectorTransform.transformSkillset(person);
            
        	if (vectorData.getDocVector() != null) {
        		person.docvector_length = vectorData.getDocVector().size;
        	}
       	
        	// If got associated jobs, recompute vectors after saving the old state.
        	if (person.associated_jobs != null)  {
        		// TODO once adding associated jobs to vectors becomes the accepted norm, compute only one time using AJ
        		
            	person.pre_associated_jobs_docvector_length = person.docvector_length;        		
        		person.pre_associated_jobs_vector_state = new AltVectorState(person);  // save previous state for analytics.
        		
        		GenericSource ajSource = new GenericSource(SourceType.AJ);
        		ajSource.employmentHistory = person.associated_jobs;
            	profileSources.add(ajSource);  // add to the the G2 and Resume sources 
            	
            	vectorData = candidateService.createVectors(profileSources.toArray(new ProfileSource[profileSources.size()]), person.id);
            	useAssociatedJobs.inc();
            	
            	person.docvector_length = (vectorData.getDocVector() != null) ? vectorData.getDocVector().size : 0;
            	
            	PersonVectorTransform.transform(vectorData, person, isProd);
            	PersonVectorTransform.transformSkillFamily(vectorData, person, isProd);
            	PersonVectorTransform.transformTopVectorTerms(vectorData, person);
                PersonVectorTransform.transformSkillset(person);
        	}
        	
        }

        // This lives in this stage because heavy candidateService is here.
        PersonVectorTransform.transformG2FullName(candidateService, blacklistTerms, person);
        PersonVectorTransform.transformParsedResumeFullName(candidateService, blacklistTerms, person);
        
        SimpleVector jobcodeVect = PersonVectorTransform.transformJobCodeVector( person.job_functions );
        if( null != jobcodeVect ) {
            person.jobcodevector = VectorPrecision.vectorFormat(jobcodeVect.toString(true));
            person.jobcodevector_magnitude = VectorPrecision.magnitudeFormat(jobcodeVect.magnitude());
        }
        
        SimpleVector industryVect = PersonVectorTransform.transformIndustryVector( person.industry_code );
        if( null != industryVect ) {
            person.industryvector = VectorPrecision.vectorFormat(industryVect.toString(true));
            person.industryvector_magnitude = VectorPrecision.magnitudeFormat(industryVect.magnitude());
        }
        
        context.output(person);
    }
    
}

