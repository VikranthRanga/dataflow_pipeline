package com.allegis.search.stage.person;

import org.apache.beam.sdk.transforms.DoFn;

import com.allegis.search.enums.Table.TalentPredictions;
import com.allegis.search.model.person.PersonDoc;
import com.google.cloud.spanner.Mutation;
import com.google.cloud.spanner.Mutation.WriteBuilder;
import com.google.cloud.spanner.Value;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Talent Predictions DB Update to Spanner. 
 * 
 * @author psivarad
 *
 */
public class TalentPredictionsDBUpdateStage extends DoFn<PersonDoc, Mutation> {
	
  	private static final long serialVersionUID = -5543325697254915708L;
   
    @ProcessElement
    public final void processElement( @Element PersonDoc doc, final OutputReceiver<Mutation> out ) {
    	
        // Only update if service outputs have changed
        // NOTE: The following condition is checked prior to this stage in the TalentPredictionsCompositeStage
    	if(doc.meta.jobcode_needsUpdate || doc.meta.industrycode_needsUpdate || doc.meta.proventrackrecord_needsUpdate){    
    	    
	    	WriteBuilder talentPredictionsWriteBuilder = Mutation.newInsertOrUpdateBuilder( TalentPredictions.TABLE )
			        .set( TalentPredictions.Column.AccountId.name()).to( doc.getCandidate_id())
			        .set( TalentPredictions.Column.LastModifiedDate.name()).to(Value.COMMIT_TIMESTAMP);
	    	
	    	
	    	Gson gson = new GsonBuilder().serializeNulls().serializeSpecialFloatingPointValues().create();
	    	
	    	if( doc.meta.jobcode_needsUpdate ) {
	    	    talentPredictionsWriteBuilder
			        .set( TalentPredictions.Column.JobCodeHash.name()).to( doc.meta.jobcode_hash )
			        .set( TalentPredictions.Column.JobCodeLastModifiedDate.name()).to( doc.meta.jobcode_last_modified_date);
	    	    
	    	    String jobcodes = doc.job_functions == null ? null : gson.toJson(doc.job_functions).toString();
	    	    talentPredictionsWriteBuilder = talentPredictionsWriteBuilder.set( TalentPredictions.Column.JobCode.name()).to( jobcodes );
	    	}
	    	
	    	if( doc.meta.industrycode_needsUpdate ) {
	    	    talentPredictionsWriteBuilder
			        .set( TalentPredictions.Column.IndustryCodeHash.name()).to(doc.meta.industrycode_hash)
			        .set( TalentPredictions.Column.IndustryCodeLastModifiedDate.name()).to( doc.meta.industrycode_last_modified_date);
	    	    //Add entire response: wholistic industry and work history industry codes to DB 
	    	    String industrycodes = doc.meta.industry_code_resp;
	    	    talentPredictionsWriteBuilder = talentPredictionsWriteBuilder.set( TalentPredictions.Column.IndustryCode.name()).to( industrycodes );
	    	}
	    	
	    	if( doc.meta.proventrackrecord_needsUpdate ) {
	    	    talentPredictionsWriteBuilder
			        .set( TalentPredictions.Column.ProvenTrackRecordHash.name()).to(doc.meta.proventrackrecord_hash)
			        .set( TalentPredictions.Column.ProvenTrackRecordLastModifiedDate.name()).to( doc.meta.proventrackrecord_last_modified_date);
	    	    
	    	    String proventrackrecord = doc.meta.proventrackrecord_output == null ? null : doc.meta.proventrackrecord_output;
	    	    talentPredictionsWriteBuilder = talentPredictionsWriteBuilder.set( TalentPredictions.Column.ProvenTrackRecord.name()).to( proventrackrecord );
	    	}
	    	
	    	out.output(talentPredictionsWriteBuilder.build());     
    	}
    }
}

