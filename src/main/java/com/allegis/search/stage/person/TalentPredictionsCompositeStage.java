package com.allegis.search.stage.person;

import org.apache.beam.sdk.io.gcp.spanner.SpannerIO;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PDone;

import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.utils.GCPConfigUtils;

/**
 * The Talent Predictions Composite Stage is responsible for composing the tasks required to persist
 * updated predictions into the talent predictions "staging" repo.
 * 
 * @author psivarad
 *
 */
public class TalentPredictionsCompositeStage extends PTransform<PCollection<PersonDoc>, PDone> {

	private static final long serialVersionUID = -1200603047973848043L;
	
	CommonOptions options;
    
    public TalentPredictionsCompositeStage( CommonOptions options ) {
    	this.options = options;
    }
    
    @Override
    public PDone expand( PCollection<PersonDoc> input ) {
                  			
    	input
    	
    	    /*.apply("Filter non-updates", Filter.by(new SerializableFunction<PersonDoc, Boolean>() {
                        private static final long serialVersionUID = 4806895801708042396L;

                        @Override
            	        public Boolean apply(PersonDoc person) {
                            return person.meta.industrycode_needsUpdate || person.meta.jobcode_needsUpdate;
            	        }
    	          }))
    	    */
	        .apply("Create Mutations", ParDo.of(new TalentPredictionsDBUpdateStage()))
			
			.apply("Spanner Write", SpannerIO.write()
			                .withSpannerConfig(GCPConfigUtils.getPredictionsSpannerConfig(options))
			                .withFailureMode(SpannerIO.FailureMode.FAIL_FAST) );   //This is the default mode.  I just want it to be shown explicitly 
    
        return PDone.in(input.getPipeline());
        
    }

}
