package com.allegis.search.stage.person;


import org.apache.beam.sdk.transforms.DoFn;
import org.apache.commons.lang3.SerializationUtils;

import com.allegis.search.enums.Table.TalentDocument;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.ResumeInfoTransform;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.KeySet;
import com.google.cloud.spanner.ResultSet;

/**
 * The gather resume info for the most recent resume. 
 * The documents themselves are handled in other downstream stages.
 */
public class ResumeInfoStage extends AbstractStage<PersonDoc,PersonDoc>{

    private static final long serialVersionUID = 1L;

    /**
     * Default Constructor. This exist simply so that we can mock out resources in this stage
     */
    public ResumeInfoStage(CommonOptions options) {
    	super(options);
    }

    ResumeInfoTransform resumeInfoTransform;

    @Override
    public void setUp() {
        resumeInfoTransform = new ResumeInfoTransform(deadLetterService);
    }

    @Override
    public void processStage( PersonDoc doc, DoFn<PersonDoc, PersonDoc>.ProcessContext context ) throws Exception {
        // Since we will be adding data to this PersonDoc, we have to clone it. 
        // Otherwise we will get an Immutablity exception
        PersonDoc person = SerializationUtils.clone( doc );

        // TODO: Once the data sync issue is resolved, revery back to using this key
        //  Key is (account, isDeleted, HTML_Version_Available__c)	
        // Key key = Key.newBuilder().append(person.id).append("false").append("true").build();
        
        // Key is (account, isDeleted)  
        SpannerKey key = SpannerKey.of( person.id, "false" );
        
        // We use a try-with-resource block to automatically release resources held by ResultSet.
        try (ResultSet resultSet = spannerClient
                .singleUse()
                .readUsingIndex(TalentDocument.TABLE, TalentDocument.INDEX, KeySet.singleKey( key.get() ), ResumeInfoTransform.COLUMNS)) 
        {
            resumeInfoTransform.transform(resultSet, person);
        }
        context.output(person);
    }


}
