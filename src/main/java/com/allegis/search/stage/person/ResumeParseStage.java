package com.allegis.search.stage.person;


import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;
import org.xml.sax.SAXException;

import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.ResumeParseTransform;
import com.allegis.search.utils.CloudStorageUtils;
import com.allegis.search.utils.GCPConfigUtils;

/**
 * Fetches the Sovren parsed resume from Google Storage and parses the XML into ResumeSource.
 * Several simple field are populated in the PersonDoc.
 * Then the CandidateProfile data is copied to a light a weight CandidateProfile instance
 * in order to forward as meta date to down stream stages.
 */
public class ResumeParseStage extends AbstractStage<PersonDoc,PersonDoc>{
    private static final long serialVersionUID = -5583815764212203279L;
    private String resumePath;

    private Counter notFoundCounter = Metrics.counter(ResumeParseStage.class.getSimpleName(), "File Not Found");
    private Counter parseErrorCounter = Metrics.counter(ResumeParseStage.class.getSimpleName(), "Parse Errors");

    public ResumeParseStage(CommonOptions options) {
    	super(options);
        this.resumePath = GCPConfigUtils.getXmlResumePath(options);
    }
    

    ResumeParseTransform resumeParseTransform;

    @Override
    public void setUp() {
    	resumeParseTransform = new ResumeParseTransform(deadLetterService);
    }



    @Override
    public void processStage(PersonDoc person, DoFn<PersonDoc, PersonDoc>.ProcessContext context) 
            throws IOException, ParserConfigurationException 
    {
        if (person.resume_1 != null) {
            String fullPath = resumePath + person.resume_1.id + ".xml";
            try {

                String xml = CloudStorageUtils.readGoogleFile(fullPath);

                if (xml == null || xml.length() < 200) {
                    deadLetterService.writeDataQualityWarning("Skipping XML less than 200 length.", "xml="+xml, person);
                }
                else {
                    // we get a new reference to person because it is cloned in the transform only if we have something to update.
                    person = resumeParseTransform.transform(xml, person);
                }
            } catch (FileNotFoundException fnfe) {
                deadLetterService.writeDataQualityWarning("XML file not found", "path="+ fullPath, person);
                notFoundCounter.inc();
            } catch (SAXException | JAXBException e) {
                deadLetterService.writeDataQualityWarning(e, "path="+fullPath, person);
                parseErrorCounter.inc();
            }
        }
        context.output(person);

    }
}
