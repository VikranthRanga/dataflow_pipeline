package com.allegis.search.stage.person;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.FeatureFlag;
import com.allegis.search.enums.Table.TalentPredictions;
import com.allegis.search.http.RestClient;
import com.allegis.search.http.impl.GcpOAuthAwareRestClientImpl;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.CommonOptions;
import com.allegis.search.options.PredictionsServiceOptions;
import com.allegis.search.stage.AbstractStage;
import com.allegis.search.transform.person.RecruiterRelationshipTransform;
import com.allegis.search.transform.person.TalentPredictionsTransform;
import com.allegis.search.util.AllegisConfig;
import com.allegis.search.util.AllegisSafeConfig;
import com.allegis.search.util.GCPConfig;
import com.allegis.search.utils.FeatureFlagUtil;
import com.allegis.search.utils.GCPConfigUtils;
import com.allegis.search.utils.SpannerKey;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.Struct;

/**
 * The Talent Predictions Stage is responsible for populating Talent with the outputs from our Predictive Services. 
 * Additionally, it makes the determinations of whether or not those predictions need to be updated/refreshed.
 * 
 */
public class TalentPredictionsStage extends AbstractStage<PersonDoc, PersonDoc> {

	
	private static final long serialVersionUID = 90073516628799623L;
	private static final Logger logger = LoggerFactory.getLogger(TalentPredictionsStage.class);
 
	final private String jobCodeURL;
	final private String industryCodeURL;
	final private String provenTrackRecordURL;
    
    private RestClient client;
    private AllegisSafeConfig allegisSafeConfig;
    private FeatureFlagUtil featureFlagUtil = null;
    
    protected boolean enableJobCodePredictiveService = false;
    protected boolean enableIndustryCodePredictiveService = false;
    protected boolean enableProvenTrackRecordPredictiveService = false;
    protected boolean isBulkIngestionMode = false;
    protected boolean isTalentPredictionsAppPipeline = false;
    
    // Hash Comparison Overrides
    private boolean overrideJobCodeHash = false;
    private boolean overrideIndustryCodeHash = false;
    private boolean overridePTRHash = false;
    
    public static final List<String> TALENT_PREDICTION_COLUMNS = Arrays.asList(
    		TalentPredictions.Column.JobCode.name(),
    		TalentPredictions.Column.JobCodeHash.name(),
    		TalentPredictions.Column.JobCodeLastModifiedDate.name(),
    		TalentPredictions.Column.IndustryCode.name(),
    		TalentPredictions.Column.IndustryCodeHash.name(),
    		TalentPredictions.Column.IndustryCodeLastModifiedDate.name(),
    		TalentPredictions.Column.ProvenTrackRecord.name(),
    		TalentPredictions.Column.ProvenTrackRecordHash.name(),
    		TalentPredictions.Column.ProvenTrackRecordLastModifiedDate.name(),
    		TalentPredictions.Column.recruiterRelationships.name());
    
    private DatabaseClient predictionsClient;
    private TalentPredictionsTransform predictionsTransform;
    private RecruiterRelationshipTransform recruiterRelationshipTransform;
    private GCPConfig gcpConfig;
    
	public TalentPredictionsStage(CommonOptions options, GCPConfig gcpConfig, boolean isTalentPredictionsAppPipeline) {
		
		super(options);
		
		this.gcpConfig = gcpConfig;
		
		this.jobCodeURL = gcpConfig.getJobCodeClassifierEndpoint();
		this.industryCodeURL = gcpConfig.getIndustryCodeClassifierEndpoint();
		this.provenTrackRecordURL = gcpConfig.getTalentPTREndpoint();
		
	    this.allegisSafeConfig = GCPConfigUtils.getAllegisConfig(options);
	    this.isTalentPredictionsAppPipeline = isTalentPredictionsAppPipeline;
	    
	    logger.info("Using Job Code Classifier endpoint: "+ this.jobCodeURL +"from ENV="+ this.allegisSafeConfig.getEnv() );
	    logger.info("Using Industry Code Classifier endpoint: "+ this.industryCodeURL +"from ENV="+ this.allegisSafeConfig.getEnv() );
	    logger.info("Using PTR endpoint: "+ this.provenTrackRecordURL +"from ENV="+ this.allegisSafeConfig.getEnv() );
	}
	
	@Override
    public void setUp() { 
	    
        if( this.predictionsClient == null ){
            this.predictionsClient = GCPConfigUtils.getPredictionsSpannerClient(this.spanner, this.gcpEnv);
        }
        
    }
	
	@Override
    public void startBundle( StartBundleContext context ) {
		PredictionsServiceOptions predictionsServiceOptions = context.getPipelineOptions().as(PredictionsServiceOptions.class);
        
        if( null == this.client) {
            AllegisConfig.forceReload( this.allegisEnv );   // to set the system property
            logger.info("Initializing gcp oauth client using env vars: "+ this.allegisSafeConfig.getEnv() );
            this.client = new GcpOAuthAwareRestClientImpl( true, true, this.allegisSafeConfig, this.gcpConfig );
    	}
        
        if( null == this.predictionsTransform ) {
            this.predictionsTransform = new TalentPredictionsTransform(this.spannerClient, this.client, deadLetterService);
        }
        
        if (null == this.recruiterRelationshipTransform) {
        	this.recruiterRelationshipTransform = new RecruiterRelationshipTransform();
        }
        
        // Initialize the feature flag helper if it hasn't already been done so
        if( null == featureFlagUtil ) {
            this.featureFlagUtil = new FeatureFlagUtil( predictionsServiceOptions.getEnv().toUpperCase(), this.spannerClient );
        }
        
        // Get flags from options if the pipeline is TalentPredictionsAppPipeline
        if(this.isTalentPredictionsAppPipeline) {

            // Which service calls should be enabled
        	this.enableJobCodePredictiveService = predictionsServiceOptions.getJobCode();
            this.enableIndustryCodePredictiveService = predictionsServiceOptions.getIndustryCode();
            this.enableProvenTrackRecordPredictiveService = predictionsServiceOptions.getProvenTrackRecord();
            
            // Should we override the hash comparison in determining if a call should be made
            this.overrideIndustryCodeHash = predictionsServiceOptions.getOverrideIndustryCodeHash();
            this.overrideJobCodeHash = predictionsServiceOptions.getOverrideJobCodeHash();
            this.overridePTRHash = predictionsServiceOptions.getOverrideProvenTrackRecordHash();
            
        }
        else
        {
        	// Before we begin the next bundle, check again if this pipeline is processing the job_functions and industry code functions. 
            String jobFunctionFeatureFlag = this.featureFlagUtil.getValue(FeatureFlag.JOB_FUNCTIONS);
            String industryCodeFeatureFlag = this.featureFlagUtil.getValue(FeatureFlag.INDUSTRY_CODE);
            String provenTrackRecordFeatureFlag = this.featureFlagUtil.getValue(FeatureFlag.PROVEN_TRACK_RECORD);
            
            if(jobFunctionFeatureFlag != null && !jobFunctionFeatureFlag.trim().isEmpty()) {
            	this.enableJobCodePredictiveService =Boolean.parseBoolean(jobFunctionFeatureFlag);
            }
              
            if(industryCodeFeatureFlag != null && !industryCodeFeatureFlag.trim().isEmpty()) {
            	this.enableIndustryCodePredictiveService =Boolean.parseBoolean(industryCodeFeatureFlag);
            }
            
            if(provenTrackRecordFeatureFlag != null && !provenTrackRecordFeatureFlag.trim().isEmpty()) {
            	this.enableProvenTrackRecordPredictiveService = Boolean.parseBoolean(provenTrackRecordFeatureFlag);
            }
        } 	
        
//        if(predictionsServiceOptions.getBulkIngestionMode() != null) {
//        	this.isBulkIngestionMode = predictionsServiceOptions.getBulkIngestionMode() || predictionsServiceOptions.getBulkLimit() != null;
//        }
    }
	
	@Override
	public void processStage( PersonDoc person, ProcessContext context ) throws Exception {
	    		
		    
	    boolean hasSerializedPerson = false;
	    
	    // Update the talent with the pre-computed predictive service outputs
	    SpannerKey personIdKey = SpannerKey.of( person.id );
	    Struct talentPredictionsResultSet = this.predictionsClient.singleUse().readRow(TalentPredictions.TABLE, personIdKey.get(), TALENT_PREDICTION_COLUMNS );
	    if( talentPredictionsResultSet != null ) {
	        // SpannerKey key = SpannerKey.of( person.id )
	        // key.get()
	        person = SerializationUtils.clone(person);
	        hasSerializedPerson = true;
	        
	        person = this.predictionsTransform.transform(person, talentPredictionsResultSet);
	        person = this.recruiterRelationshipTransform.transform(talentPredictionsResultSet, person);
	    }
	    
	    
	    // Only consider updating the predictive service outputs if we are NOT is Bulk Talent Ingestion mode
	    // AND, the feature flags OR pipeline options for the Predictive Service pipeline tell us to do so 
	    // Additionally, the calls to the predictive services will only occur if they meet the criteria of the service
	    // AND we detected that the output could change by comparing a hash of the inputs that we would supply
	    if(!isBulkIngestionMode){
	    	
	       // Job code
		   if( enableJobCodePredictiveService ){
                try {
                    
                    if( ! hasSerializedPerson ) {
                        person = SerializationUtils.clone(person);
                        hasSerializedPerson = true;
                    }
                    
                    person = this.predictionsTransform.updateJobCode(person, this.jobCodeURL, this.overrideJobCodeHash );
		        	
                }catch( Exception e ) {
                     // timeouts will throw SocketTimeoutException || ConnectTimeoutException
                     this.deadLetterService.writeException(person, e);
                 }
                
		   }
 
			// Industry Code
			if(enableIndustryCodePredictiveService) {
			    try {
			        
			        if( ! hasSerializedPerson ) {
                        person = SerializationUtils.clone(person);
                        hasSerializedPerson = true;
                    }
			        
					person = this.predictionsTransform.updateIndustryCode(person, industryCodeURL, this.overrideIndustryCodeHash );
					
			    }catch( Exception e ) {
                     this.deadLetterService.writeException(person, e);
                 }
			}
			
			// Proven Track Record
			if(enableProvenTrackRecordPredictiveService) {
			    try {
			        
			        if( ! hasSerializedPerson ) {
                        person = SerializationUtils.clone(person);
                        hasSerializedPerson = true;
                    }
			        
					person = this.predictionsTransform.updateProvenTrackRecord(person, provenTrackRecordURL, this.overridePTRHash);
					
			    }catch( Exception e ) {
                     this.deadLetterService.writeException(person, e);
                 }
			}
	    	
	    }
	
	    context.output(person);
	}
	 
	
	@Override
    public void teardown( ) {
        // close request client
        if( null != this.client ) {
            try {
                this.client.close();
            }catch( Exception e) {
                logger.info("Unable to close the Rest client used to get talent prediction service");
            }
        }
    }
}
