package com.allegis.search.stage;

import org.apache.beam.sdk.transforms.DoFn;

import com.allegis.search.options.CommonOptions;
import com.allegis.search.options.PredictionsServiceOptions;
import com.allegis.search.utils.GCPConfigUtils;
import com.google.cloud.spanner.DatabaseClient;
import com.google.cloud.spanner.Spanner;

/**
 * 
 * The SuperAbstractStage provides a base "template" for Stages to extend from so that they can benefit from generic 
 * behaviors that are common to most Stages. For example, error trapping during the {@see ProcessElement} to 
 * ensure that a bundle (i.e. group of PCollection elements being processed together) is not discarded on failure. 
 * Note, thrown exceptions from other lifecycle methods ({@see StartBundle} and {@see FinishBundle}) are <b>NOT</b>
 * trapped and  will cause a bundle to be discarded (@see ParDo}. Its up to the implementation to determine the 
 * appropriate action when failure occurs in those methods.
 *
 *  Setup
 *    Repeatedly process bundles:
 *     StartBundle
 *        Repeated ProcessElement
 *     FinishBundle
 *  Teardown
 */
public abstract class SuperAbstractStage<I,O> extends DoFn<I,O> implements SpannerClientStage {
    
    private static final long serialVersionUID = 7458172021397866984L;
    
    protected Spanner spanner;
    protected DatabaseClient spannerClient;
    protected DeadLetterService deadLetterService;
    
    protected final String allegisEnv;
    protected final String gcpEnv;
    protected final boolean isDebug;

    /**
     * We need to know up front if deadLetterService will be Spanner or Mock
     * @param isDebug   a flag to indicate if the pipeline is running in debug mode
     * @param env       the target environment context for this execution
     */
    public SuperAbstractStage(CommonOptions options) {
    	this.allegisEnv =  GCPConfigUtils.getAllegisEnv(options);
    	this.gcpEnv = GCPConfigUtils.getGcpEnv(options);
    	this.isDebug = options.getDebugMode();
	}


	/**
     * The concrete implementation for this stage's transformations. This method is called with each element
     * in the PCollection
     * 
     * @param in        the {@link Element} being processed
     * @param context   the {@link ProcessContext} of this stage
     */
    public abstract void processStage( I in, ProcessContext context ) throws Exception;
    
    /**
     * The concrete implementation of any {@see DoFn.Setup} functionality needed for this stage. This is a good
     * place to establish resources that do not need to be closed. When a fresh instance of this stage
     * is created on a Worker, this method will be called.
     */
    public void setUp() {}
    
    /**
     * The concrete implementation of any {@see DoFn.Teardown} functionality needed for this stage. This method is 
     * <b>NOT</b> garunteed to be run when the instance of the stage is deconstructed. Consider using
     * {@see AbstractStage#finishBundle() } instead.   
     */
    public void teardown() {}
    
    /**
     * The concrete implementation of any  {@see DoFn.StartBundle} functionality needed for this stage. This method
     * is called by the Worker when it begins processing a new bundle. This is a good place to create resources that
     * need to be close, such as database and http clients.
     * @param startBundleContext   a reference to the bundle context
     */
    public void startBundle(StartBundleContext startBundleContext) {}
    
    /**
     * The concrete implementation of any  {@see DoFn.FinishBundle} functionality needed for this stage. This method
     * will be called by the Worker when it has completed iterating through each element of a given bundle. This is a 
     * good place to clean up resources (i.e. calling close())
     * @param finishBundleContext   a reference to the bundle context
     */
    public void finishBundle(FinishBundleContext finishBundleContext) {}
    
    
    /**
     * Called when a fresh instance of the implementing stage is created on a worker, this method
     * will load the {@see IngestionConfig} properties specified by the supplied {@see AllegisIngestionOptions#getEnv()}
     * option. 
     */
    @Setup
    public void baseClassSetUp() {
        
        if( this.spanner == null ){
            this.spanner = GCPConfigUtils.buildSpannerInstance(gcpEnv);
            this.spannerClient = GCPConfigUtils.getSpannerClient(spanner, gcpEnv);
        }
       
        if( this.deadLetterService == null ) {
                deadLetterService = new DeadLetterService();
        }
        setUp();
    }
    
    
    @Override
    public DatabaseClient getSpannerClient()  {
        return spannerClient;
    }

    
    @Teardown
    public void baseClassTeardown() {
        // sanity check
        if (spanner == null) { return; }

        // close the resource
        spanner.close();
        spanner = null;
        deadLetterService = null;

        teardown();
    }

    @StartBundle
    public void baseClassStartBundle(StartBundleContext startBundleContext) {
    	PredictionsServiceOptions options =  startBundleContext.getPipelineOptions().as(PredictionsServiceOptions.class);
        deadLetterService.captureContext( options, options.getJobName() );
        startBundle(startBundleContext);
    }
    
    @FinishBundle
    public void baseClassFinishBundle(FinishBundleContext finishBundleContext) {
        finishBundle(finishBundleContext);
    }
    
   
}
