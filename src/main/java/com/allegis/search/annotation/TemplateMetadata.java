package com.allegis.search.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.ValueProvider;

/**
 * TemplateMetadata is used to provide additional information to the build plugin (TODO:Add link to real class here) that generates 
 * the Google Cloud Dataflow template metadata. Annotations from this class are intended to be used in 
 * classes that extend {@link PipelineOptions} and that are defined with {@link ValueProvider} option types. 
 * 
 * In short, these annotations are used in the {@link AllegisIngestionOptionsData} class so that we fill in 
 * all of the available fields when we generate the metadata from that class.  
 * 
 * For more information on metadata for Dataflow Templates,
 * go here: https://cloud.google.com/dataflow/docs/guides/templates/creating-templates#metadata
 *  
 * @author jalpino
 *
 */
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TemplateMetadata {

    /*
     * A flag to instruct the template builder to ignore this field when generating
     * metadata
     */
    boolean ignore() default false;
    
    /*
     * The value for the "label" metadata field. This will be used as a field
     * heading in the Google Dataflow Template form
     */
    String label() default "";
    
    /*
     * The value for the "regexes" metadata field. The value must conform to 
     * POSIX-egrep regular expressions in string form. 
     */
    String[] regexes() default "";
    
    /*
     * Defines the order in which this meta information will be placed in the 
     * template, which determines the order in which they are displayed on screen 
     */
    int order() default 0;
    
}
