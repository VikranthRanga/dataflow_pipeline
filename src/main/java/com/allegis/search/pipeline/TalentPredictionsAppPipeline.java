package com.allegis.search.pipeline;

import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowWorkerHarnessOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionTuple;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.beam.sdk.values.TupleTagList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.allegis.search.enums.DocType;
import com.allegis.search.enums.Table.Account;
import com.allegis.search.model.person.PersonDoc;
import com.allegis.search.options.PredictionsServiceOptions;
import com.allegis.search.stage.person.AccountContactStage;
import com.allegis.search.stage.person.BigQueryRowToPersonDoc;
import com.allegis.search.stage.person.EventTaskStage;
import com.allegis.search.stage.person.ExperienceStage;
import com.allegis.search.stage.person.OrderStage;
import com.allegis.search.stage.person.PersonDocToBQStage;
import com.allegis.search.stage.person.ResumeInfoStage;
import com.allegis.search.stage.person.ResumeParseStage;
import com.allegis.search.stage.person.TalentPredictionsStage;
import com.allegis.search.stage.person.TalentRecommendationStage;
import com.allegis.search.util.GCPConfig;
import com.allegis.search.utils.GCPConfigUtils;
import com.allegis.search.utils.GeneralUtils;
import com.google.api.services.bigquery.model.TableRow;

/*
--runner=DirectRunner --stagingLocation=gs://person-app/staging/mm/ 
--tempLocation=gs://person-app/temp/mm/ --jobName=dsia-predictions-pipeline-mm --env=LOAD  
--project=connected-ingest-load --maxNumWorkers=20 --bulkLimit=50000 --industryCode=true 
--bulkIngestionMode=false --overrideIndustryCodeHash=true --region=us-central1

 * 
 */

public class TalentPredictionsAppPipeline implements Serializable {

		private static final long serialVersionUID = 8716149116971757369L;

		private static final Logger logger = LoggerFactory.getLogger( TalentPredictionsAppPipeline.class );
	    private static final String PROD = "PROD";
	    
	    private static String startTimeStamp;
	    
	    private String jobId;
	 
	    
	 	final private static TupleTag<PersonDoc> initPersonsCollection = new TupleTag<PersonDoc>(){ private static final long serialVersionUID = 3614412864993761249L; };
	    final private static TupleTag<PersonDoc> personsDeletionCollection = new TupleTag<PersonDoc>(){ private static final long serialVersionUID = 3614412864993761249L; };

	 	final private static TupleTag<TableRow> rowsForBQ = new TupleTag<TableRow>(){ private static final long serialVersionUID = -2348742003870383187L; };

	    final public static TupleTag<TableRow> replayIds = new TupleTag<TableRow>() { private static final long serialVersionUID = -2348742003870383187L; };

	    /**
	     * Entry point for this pipeline.
	     */
	    public static void main( String[] args ) {
	    	new TalentPredictionsAppPipeline().runMain(args, DocType.PERSON.name().toLowerCase());
	    }

	    
	    /**
	     * Entry point for this pipeline.
	     * @param docType  this is pipeline specific: disallow this to be set from command-line
	     */
	    private void runMain( String[] args, String docType ) {
	    	
	    	 DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yy-MM-dd HH:mm");  
	         startTimeStamp = dateFormat.format(new Date().toInstant().atZone(ZoneId.systemDefault()));

	        // Register the custom options that can be presented and used in this pipeline
	        PipelineOptionsFactory.register( PredictionsServiceOptions.class );
	        PredictionsServiceOptions predictionsServiceOptions = PipelineOptionsFactory.fromArgs(args).withValidation().as( PredictionsServiceOptions.class );
	        
	        DataflowWorkerHarnessOptions dataflowWorkerharnessOptions = PipelineOptionsFactory.fromArgs(args).withValidation().as( DataflowWorkerHarnessOptions.class ); 
	        
	        jobId = dataflowWorkerharnessOptions.getJobId(); 
	        
	        if (jobId == null)  {
	            // not dataflow; must be a direct runner.  
	            // So lets use the constructor's timestamp 
	        	jobId = startTimeStamp;
	        }
	        else {
	        	// This prevents the start timstamp from moving
	            // 2020-03-23_11_07_28-13812170545373906600
	            jobId = jobId.substring(0, 16);
	        }
	        
	        // Create and initialize the pipeline
	        Pipeline pipeline = Pipeline.create( predictionsServiceOptions );
	        
	        boolean isDataflowRunner = DataflowRunner.class.isAssignableFrom( predictionsServiceOptions.getRunner() );

	    	// Protect against accidental running of DirectRunner against PROD.
	        // !! Don't check in this code if it has been commented !!! 
	    	if( ! isDataflowRunner && predictionsServiceOptions.getEnv().equalsIgnoreCase( PROD ) && ! predictionsServiceOptions.getOverrideDirectRunnerProd() ) {
	    	    String ruleBreaker = "\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"+
	    	                         "You are attempting run the DirectRunner against the PROD environment.\n"+
	    	                         "If you are absolutely sure that you want to do this, then you must supply \n"+
	    	                         "the '--overrideDirectRunnerProd=true' options flag when invoking this pipeline \n"+
	    	                         "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
	    	    logger.error( ruleBreaker );
	    	    System.exit(1);
	    	}
	    	

	    	RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
	        logger.info("------------------------------------------------------------------");
	        logger.info("  JVM ARGUMENTS ");
	        logger.info("------------------------------------------------------------------");
	    	List<String> jvmArgs = runtimeMxBean.getInputArguments();
	    	for (String arg : jvmArgs) {
	    		logger.info(arg);
	 		}

			 logger.info("javax.net.ssl.trustStore "+  System.getenv().get("javax.net.ssl.trustStore") );

	        // Dump options
	        logger.info("------------------------------------------------------------------");
	        logger.info("  OPTIONS ");
	        logger.info("------------------------------------------------------------------");
	        logger.info( predictionsServiceOptions.toString() );

	        logger.info("GOOGLE_APPLICATION_CREDENTIALS: "+  System.getenv().get("GOOGLE_APPLICATION_CREDENTIALS") );
	        

			GCPConfig gcpConfig = GCPConfigUtils.getGcpConfig(predictionsServiceOptions);
			
			PCollectionTuple personsTuple;
			
			String talentIdQuery = getBulkTalentQuery(gcpConfig.getBigQueryDataset(), predictionsServiceOptions.getBulkLimit(), null, null);
			
			boolean isTalentPredictionsAppPipeline = true;
			
			personsTuple  = pipeline
					.apply("Query Ids", BigQueryIO.readTableRows().fromQuery( talentIdQuery ).usingStandardSql().withoutValidation())
			        .apply("To PersonDoc", ParDo.of(new BigQueryRowToPersonDoc(jobId, predictionsServiceOptions.getOptionalPipelineMessage())))
			        .apply("Account-Contact", ParDo.of(new AccountContactStage(predictionsServiceOptions))
	                .withOutputTags(initPersonsCollection, TupleTagList.of(personsDeletionCollection)));
			

	        //Generate person doc to call the talent predictions service
			PCollection<PersonDoc> allPersons = personsTuple.get(initPersonsCollection)
					.apply("Experience", ParDo.of(new ExperienceStage(predictionsServiceOptions)))
					.apply("Event-Task", ParDo.of(new EventTaskStage(predictionsServiceOptions)))
					.apply("Talent Recommendation", ParDo.of(new TalentRecommendationStage(predictionsServiceOptions) ))
	                .apply("Resume Info", ParDo.of(new ResumeInfoStage(predictionsServiceOptions)))
	                .apply("Resume Parse", ParDo.of(new ResumeParseStage(predictionsServiceOptions)))   // Dependent on ResumeInfoStage
					.apply("Orders", ParDo.of(new OrderStage(predictionsServiceOptions)))
	                .apply("Talent Predictions Service", ParDo.of(new TalentPredictionsStage(predictionsServiceOptions, gcpConfig, isTalentPredictionsAppPipeline)));

			PCollectionTuple bqTuple = allPersons.apply("PersonDoc to TableRow", ParDo.of(new PersonDocToBQStage())
					.withOutputTags(rowsForBQ, TupleTagList.of(replayIds)));			
			
			
			bqTuple.get(rowsForBQ)
	    	        .apply("WriteToBQ", BigQueryIO.writeTableRows()
	    	        		.withCreateDisposition(CreateDisposition.CREATE_NEVER)
	    	        		.to("temp.talent_all_work_history"));        		
			
			bqTuple.get(replayIds)
	        	.apply("WriteReplyIds", BigQueryIO.writeTableRows()
	        		.withCreateDisposition(CreateDisposition.CREATE_NEVER)
	        		.to("temp.replay_ids"));        		
	        
	        // Run the pipeline
	        PipelineResult results = pipeline.run();

	        // log some metrics from the pipeline when running in debug mode with the 
	        // Direct Runner
	        if( ! isDataflowRunner && predictionsServiceOptions.getDebugMode() ) {
	            results.waitUntilFinish();
	            GeneralUtils.dumpMetrics( results );
	        }
			logger.info("completed");
	    }
	    
	    private static final List<String> EXCLUDED_GROUP_FILTERS = Arrays.asList("MLA","AP");
	    
	    private static final String ID="Id"; // column name for primary key
	    private static final String EXTRA_BOOL_CLAUSE_FORMAT = " AND (%s)";
	    private static final String LIMIT_CLAUSE_FORMAT = " LIMIT %d";


		private static final String GENERATEDTIMESTAMP_VALUE = "IFNULL("
			    + Account.Column.Talent_Profile_Last_Modified_Date__c+", "
			    + Account.Column.CreatedDate+")";

	    private static final String SELECT_CLAUSE_FORMAT =  
			"SELECT "+ID+" FROM `%s.v_account_current`"
				+ " WHERE "+Account.Column.RecordTypeId+" = '"+Account.RECORD_TYPE_ID_FOR_TALENT+"'" // means type is 'Talent'
				+ " AND "+Account.Column.Talent_Committed_Flag__c+" = 'true'"            // only committed
				+ " AND isDeleted = 'false'"                         // flagged for deletion
	            + " AND " + Account.Column.Talent_Ownership__c+ " NOT IN ('"+ String.join("','", EXCLUDED_GROUP_FILTERS) +"')";
//+ " and id='0011E00001ioruOQAQ'";


	    private static final String EXCLUDE_INACTIVE_CLAUSE = " AND Talent_Ownership__c <> 'INACTIVE'";
	    private static final String DELTA_BOOL_CLAUSE_FORMAT = " AND "+GENERATEDTIMESTAMP_VALUE+" >= '%s'";

	    /**
	     * Returns a query that can be used to retrieve ALL talent ids
	     * @param bqDataSet     the bq dataset aligned for the env in which Ids will be queried for
	     * @param limit         if supplied, this will limit the number of records to be returned by the query
	     * @param deltaStart    an optional string representation of a Timestamp from which to limit talent returned in the query.
	     *                      The supplied timestamp will be used to identify talent with a "createdDate" or "profile last modified date" AFTER the supplied value.
	     *                      The timestamp format should match the following: 2020-12-31T00:00:00.000Z
	     * @param extraBool     if supplied, this addition 'clause' will be added to the query to further limit the records returned (i.e. "firstname like 'joe%'")
	     * @return              a formatted query to extract Talent Accounts Ids
	     */
	    private String getBulkTalentQuery( String bqDataSet, Integer limit, String deltaStart, String extraBool ) {
	        String query = String.format(SELECT_CLAUSE_FORMAT, bqDataSet);
	        
	        if(deltaStart != null && ! deltaStart.isEmpty()) {
	            // DELTA ingestion
	            query += String.format(DELTA_BOOL_CLAUSE_FORMAT, deltaStart);
	        } 
	        else  {
	            // FULL BULK ingestion
	            
	            // We will only allow recently inactivated talent because this will effectively hide this 
	            // talent because of the group_filter value it becomes.
	            
	            query += EXCLUDE_INACTIVE_CLAUSE;       // This will exclude nulls too
	        }
	                
	        if(extraBool != null && ! extraBool.isEmpty()) {
	            query += String.format(EXTRA_BOOL_CLAUSE_FORMAT, extraBool);
	        }

	        // Note: on large record sets, the order by causes an OOM, therefore we have left it out. 
	                
	        if(limit != null && limit > 0) {
	            query += String.format(LIMIT_CLAUSE_FORMAT, limit);
	        }
	        
	        logger.info("Query: "+query);
	        return query;
	    }
}
