package com.allegis.search.pipeline.feeder;

import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.options.ValueProvider.NestedValueProvider;
import org.apache.beam.sdk.transforms.SerializableFunction;

public class FeederUtils {

    public static final String ATTR_FEEDER_ID = "feederId";
    public static final String ATTR_IS_REALTIME = "isRealtime";
    public static final String ATTR_IS_DELETED = "isDeleted";
	public static final String ATTR_IS_QA = "isQA";
	public static final String ATTR_RELEASE = "release";
	public static final String DEFAULT_FEEDER_ID = "UNKNOWN";


	/**
	 * Used to construct the topic path using the ENV settings and the provided name
	 * @param projectId    the id of the project
	 * @param name         the name of the topic
	 * @return             a fully constructed path to the supplied topic
	 */
	public static NestedValueProvider<String, String> buildTopicPath(String projectId, ValueProvider<String> name  ) {
	    final String projId = projectId;
		return NestedValueProvider.of(
				name,
				new SerializableFunction<String, String>() {
					private static final long serialVersionUID = 1L;

					@Override
					public String apply(String name) {
					    return buildTopicPath( projId, name);
					}
				});
	}
	

	/**
	 * Used to construct the topic path using the ENV settings and the provided name
	 * @param projectId    the id of the project
	 * @param name         the name of the topic
	 * @return             a fully constructed path to the supplied topic
	 */
	public static String buildTopicPath(String projectId, String name) {
		if (name.startsWith("projects/"))
			return name;  // must be a full path
		
	    return "projects/"+ projectId +"/topics/"+name;
	}

}
