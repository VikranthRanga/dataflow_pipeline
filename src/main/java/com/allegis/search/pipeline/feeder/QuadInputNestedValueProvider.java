package com.allegis.search.pipeline.feeder;

import java.io.Serializable;

import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.options.ValueProvider.NestedValueProvider;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.vendor.guava.v26_0_jre.com.google.common.base.MoreObjects;

public class QuadInputNestedValueProvider<T, FirstT, SecondT, ThirdT, FourthT> implements ValueProvider<T>, Serializable {
    private static final long serialVersionUID = 8564386535338249471L;

    /** Pair like struct holding 4 values. */
    public static class TranslatorInput<FirstT, SecondT, ThirdT, FourthT> {
        private final FirstT x;
        private final SecondT y;
        private final ThirdT z;
        private final FourthT zx;
        
        public TranslatorInput(FirstT x, SecondT y, ThirdT z, FourthT zx) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.zx = zx;
        }

        public FirstT getX() {
            return x;
        }

        public SecondT getY() {
            return y;
        }

        public ThirdT getZ() {
            return z;
        }

        public FourthT getZx() {
            return zx;
        }
        
    }

    private final ValueProvider<FirstT> valueX;
    private final ValueProvider<SecondT> valueY;
    private final ValueProvider<ThirdT> valueZ;
    private final ValueProvider<FourthT> valueZx;
    
    private final SerializableFunction<QuadInputNestedValueProvider.TranslatorInput<FirstT, SecondT, ThirdT, FourthT>, T> translator;
    private transient volatile T cachedValue;

    public QuadInputNestedValueProvider(
            ValueProvider<FirstT> valueX,
            ValueProvider<SecondT> valueY,
            ValueProvider<ThirdT> valueZ,
            ValueProvider<FourthT> valueZx,
            SerializableFunction<QuadInputNestedValueProvider.TranslatorInput<FirstT, SecondT, ThirdT, FourthT>, T> translator) {
        
        this.valueX = valueX;
        this.valueY = valueY;
        this.valueZ = valueZ;
        this.valueZx = valueZx;
        
        this.translator = translator;
    }

    /** Creates a {@link NestedValueProvider} that wraps two provided values. */
    public static <T, FirstT, SecondT, ThirdT, FourthT> QuadInputNestedValueProvider<T, FirstT, SecondT, ThirdT, FourthT> of(
            ValueProvider<FirstT> valueX,
            ValueProvider<SecondT> valueY,
            ValueProvider<ThirdT> valueZ,
            ValueProvider<FourthT> valueZx,
            SerializableFunction<QuadInputNestedValueProvider.TranslatorInput<FirstT, SecondT, ThirdT, FourthT>, T> translator) {
        QuadInputNestedValueProvider<T, FirstT, SecondT, ThirdT, FourthT> factory =
                new QuadInputNestedValueProvider<>(valueX, valueY, valueZ, valueZx, translator);
        return factory;
    }

    @Override
    public T get() {
        if (cachedValue == null) {
            cachedValue = translator.apply(new QuadInputNestedValueProvider.TranslatorInput<>(valueX.get(), valueY.get(), valueZ.get(), valueZx.get()));
        }
        return cachedValue;
    }

    @Override
    public boolean isAccessible() {
        return valueX.isAccessible() && valueY.isAccessible() && valueZ.isAccessible() && valueZx.isAccessible();
    }

    @Override
    public String toString() {
        if (isAccessible()) {
            return String.valueOf(get());
        }
        return MoreObjects.toStringHelper(this)
                .add("valueX", valueX)
                .add("valueY", valueY)
                .add("valueZ", valueZ)
                .add("valueZx", valueZx)
                .add("translator", translator.getClass().getSimpleName())
                .toString();
    }
}