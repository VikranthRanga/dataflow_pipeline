package com.allegis.search;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * The ThreadIdConverter is used by logback to provide additional distinction to the 
 * thread id logged in our statements. *
 */
public class ThreadIdConverter extends ClassicConverter {

	private static volatile int nextId = 0;
	private static final ThreadLocal<String> threadId = new ThreadLocal<String>() {
		@Override
		protected String initialValue() {
			return Integer.toString(nextId());
		}
	};

	private static synchronized int nextId() {
		return ++nextId;
	}

	@Override
	public String convert(ILoggingEvent event) {
		return threadId.get();
	}
}