package com.allegis.search.options;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.StreamingOptions;

public interface PredictionsServiceOptions extends CommonOptions, StreamingOptions  {
	
    @Description("If true, the job code classification service will be invoked to fetch job code based on the data input.")
    @Default.Boolean(false)
    Boolean getJobCode();
    void setJobCode( Boolean enableJobCode);
    
    @Description("If true, a call to the job code API will override the input hash comparison.")
    @Default.Boolean(false)
    Boolean getOverrideJobCodeHash();
    void setOverrideJobCodeHash( Boolean override);
    
    // -----------------------------------------
        
    @Description("If true, the industry code classification service will be invoked to fetch industry code based on the data input.")
    @Default.Boolean(false)
    Boolean getIndustryCode();
    void setIndustryCode( Boolean enableIndustryCode);
    
    @Description("If true, a call to the industry code classification service will override the input hash comparison.")
    @Default.Boolean(false)
    Boolean getOverrideIndustryCodeHash();
    void setOverrideIndustryCodeHash( Boolean override);

    // -----------------------------------------
        
    @Description("If true, the proven track record service will be invoked to fetch proven track record score based on the data input.")
    @Default.Boolean(false)
    Boolean getProvenTrackRecord();
    void setProvenTrackRecord( Boolean enableProvenTrackRecord);
    
    @Description("If true, a call to the Provent Track Record service will override the input hash comparison.")
    @Default.Boolean(false)
    Boolean getOverrideProvenTrackRecordHash();
    void setOverrideProvenTrackRecordHash( Boolean override);

    // -----------------------------------------
    
    @Description("The release number for this execution. Generally, this will be set by Bamboo and is used for feature flag detection")
    @Default.String("release-num")
    String getReleaseNum();
    void setReleaseNum(String value);
    
    @Description("If true, this flag will allow for DirectRunner invocation using 'PROD' resources. Tread carefully in overriding this option.")
    @Default.Boolean( false )
    Boolean getOverrideDirectRunnerProd();
    void setOverrideDirectRunnerProd(Boolean value);
    
    @Description("This option parameter defines whether to run the ingestion in bulk or realtime mode. Default is realtime")
    @Default.Boolean( false )
    Boolean getBulkIngestionMode();
    void setBulkIngestionMode(Boolean bulkIngestionMode);
    
    @Description("Optionally sets the max number of entities to bulk ingest.  This will also set the mode to bulk (batch) mode.")
    Integer getBulkLimit();
    void setBulkLimit(Integer limit);
    
    @Description("This optional parameter defines the optional pipeline message that can be used for instrumentation/logging purposes")
    String getOptionalPipelineMessage();
    void setOptionalPipelineMessage(String optionalPipelineMessage);

}
