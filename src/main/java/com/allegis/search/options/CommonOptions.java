package com.allegis.search.options;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.Validation.Required;

/**
 * A set of common Pipeline Options shared by more than one 
 * set of option classes.
 * 
 */
public interface CommonOptions extends PipelineOptions {
    
    @Required
    @Description("The GCP & Allegis environment for which this service should operate (DEV | TEST | LOAD | PROD)")
    String getEnv();
    void setEnv( String env);
    
    @Description("The Allegis environment override of the --env setting (LOCAL | DEV | TEST | LOAD | PROD | UAT)")
    String getAllegisEnv();
    void setAllegisEnv( String env);

    @Description("If true, additional output may be emitted")
    @Default.Boolean( false )
    Boolean getDebugMode();
    void setDebugMode(Boolean debugMode);
  
}
